// @ts-ignore
import TDClient from "tdweb/dist/tdweb";
import {getBrowser, getOSName} from "../Util/Common";
import packageJson from "../../package.json";
import {EventEmitter} from "events";
import SyncPromise from "../Util/SyncPromise/SyncPromise";
import AppStore from "../Store/AppStore";
import SyncStore from "../Util/SyncPromise/SyncStore";
import LangStore from "../Store/LangStore";
import ErrorStore from "../Store/ErrorStore";
import UsersStore from "../Store/UsersStore";
import NotificationsStore from "../Store/NotificationsStore";
import ChatsStore from "../Store/ChatsStore";
import SuperGroupStore from "../Store/SuperGroupStore";
import BasicGroupStore from "../Store/BasicGroupStore";
import MessageStore from "../Store/MessageStore";
import FilesStore from "../Store/FilesStore";

const WASM_FILE_NAME = 'b4b0d61282108a31908dd6b2dbd7067b';

export interface ITDClient {
    send: any
    SEND: any
    onUpdate: any

    [key: string]: any
}

interface IAction {
    type: string
    payload: any

    [key: string]: any
}

class Api extends EventEmitter {
    private client: ITDClient;

    constructor() {
        super();
        const options = {
            logVerbosityLevel: 0,
            jsLogVerbosityLevel: 0,
            mode: "wasm", // 'wasm-streaming'/'wasm'/'asmjs'
            prefix: false ? "tdlib_test" : "tdlib",
            readOnly: false,
            isBackground: false,
            useDatabase: false,
            wasmUrl: `${WASM_FILE_NAME}.wasm?_sw-precache=${WASM_FILE_NAME}`
        };
        // this.client = new TDClient(options);     //создали клиента
        this.client = {SEND:()=>{},send:()=>{},onUpdate:()=>{}};     //создали клиента
        this.client.onUpdate = this.onUpdate;          //привязали обработчик
    }
    private onUpdate = (update: any) => {
        this.UPDATE(update);
    };
    send = (request: any) => {
        if(!this.client.send) return ;
        this.SEND(request);
        this.client.send(request)
            .then((result: any) => {
                this.UPDATE(result);
                return result;
            })
            .catch((error: any) => {
                this.ERROR(error);
            });
        return request
    };

    //ГЛАВНЫЕ ОБРАБОТЧИКИ
    private UPDATE = (upd:any) =>{
        if (process.env.REACT_APP_DEV_MODE){
            if(upd['@extra']){
                console.log('OTVET >>>',upd)
            }else{
                if(upd['@type']!=='updateConnectionState' && upd['@type']!=='updateOption' && upd['@type']!=='updateSelectedBackground' && upd['@type']!=='ok'){
                    console.log('____________ UPDATE >>>',upd)
                }
            }
        };
        FilesStore.switching(upd);
        AppStore.switching(upd);
        LangStore.switching(upd);
        UsersStore.switching(upd);
        SuperGroupStore.switching(upd);
        BasicGroupStore.switching(upd);
        NotificationsStore.switching(upd);
        MessageStore.switching(upd);
        ChatsStore.switching(upd);

        //синхронизатор
        SyncStore.add(upd);
        if(upd._show === undefined){
            console.log('############### НЕ ОБРАБОТАЛИ СОБЫТИЕ - UPDATE',upd);
        }

    };
    private SEND = (data:any) =>{
        if (process.env.REACT_APP_DEV_MODE) console.log('SEND >>>',data);
    };
    private ERROR = (err:any) =>{
        if (process.env.REACT_APP_DEV_MODE) console.error('ERROR >>>',err,err.message);
        SyncStore.add(err);
        ErrorStore.switching(err);
        if(err._show === undefined){
            console.log('############### НЕ ОБРАБОТАЛИ СОБЫТИЕ - UPDATE',err);
        }
    };

}

export default new Api;
