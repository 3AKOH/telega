import React, {useContext, useEffect, useState} from 'react';
import PortalContainer from "./Containers/PortalContainer/PortalContainer";
import LoginContainer from "./Containers/LoginContainer/LoginContainer";
import MainContainer from "./Containers/MainContainer/MainContainer";
import ModalContainer from "./Containers/ModalContainer/ModalContainer";
import AppState, {AppContext} from "./Context/App/AppState";
import AppStore, {IAppStoreStatus} from "./Store/AppStore";
import Backdrop from "./Components/Backdrop/Backdrop";
import {modalClose} from "./Context/App/state/modals";
import NoneSignal from "./Components/NoneSignal/NoneSignal";
import ChatsStore from "./Store/ChatsStore";
import SyncStore from "./Util/SyncPromise/SyncStore";
import SyncPromise from "./Util/SyncPromise/SyncPromise";
import MediaContainer from "./Containers/MediaContainer/MediaContainer";
import {Route, Switch} from "react-router";
import PoligonUI from "./Poligon/PoligonUI/PoligonUI";

const App: React.FC = () => {
    const {appState, dispatch} = useContext(AppContext);
    const [load, setLoad] = useState({
        init: false,                            //инициализировали?

        portal: false,
        noneSignal: false,
        login: false,
        main: true,
    });
    const UPDATE_AUTH = () =>{
        setLoad(prev=>{
            if(!prev.init) return prev;
            if(AppStore.getAuth() == IAppStoreStatus.ready){
                //список запросов
                SyncPromise([
                    ChatsStore._getChats()
                ],(d:any)=>{
                    console.log('ГОТОВО!',d);
                    setLoad(prev=>{
                        return {
                            ...prev,
                            portal: false,
                            main: true,
                        }
                    })
                });
                return prev
            }else{
                return {
                    ...prev,
                    init: true,
                    portal: false,
                    login: true,
                }
            }
        })
    }
    const INIT_PARAMETRS = (status:boolean)=>{
        setLoad(prev=>{
            if(!status){
                return {
                    ...prev,
                    init: true,
                    portal: false,
                    noneSignal: true
                }
            }else {
                return {
                    ...prev,
                    init: true,
                }
            }
        });
        UPDATE_AUTH();
    };
    useEffect(()=>{
        AppStore.on("INIT_PARAMETERS", INIT_PARAMETRS);
        AppStore.on("UPDATE_AUTH", UPDATE_AUTH);
        return ()=>{
            AppStore.off("INIT_PARAMETERS", INIT_PARAMETRS);
            AppStore.off("UPDATE_AUTH", UPDATE_AUTH);
        }
    },[]);
    return (
        <div className='App'>
            <Backdrop onClick={()=>dispatch(modalClose())} isOpen={appState.modals.isExist} timeout={300} classNames='Backdrop-node'/>
            <PortalContainer isOpen={load.portal}/>
            <Switch>
                <Route exact path="/" component={LoginContainer}/>
                <Route component={MainContainer}/>
            </Switch>
            {/*{load.noneSignal && <NoneSignal/>}*/}
            {/*{load.login && <LoginContainer/>}*/}
            {/*{load.main && <MainContainer/>}*/}
            <ModalContainer/>
            {/*<MediaContainer/>*/}
        </div>
    );
};

export default App;
