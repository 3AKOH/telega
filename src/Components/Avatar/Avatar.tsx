import React from 'react';

interface IProps {
    url: string | null
    txt?: string
    size?: 'big' | 'small' | 'medium'
    bg?: string
    component?: any

    [inx: string]: any
}

const Avatar: React.FC<IProps> = (props) => {
    const {url, txt, size, bg = '#fff', component} = props;
    let cls = ['Avatar'];
    if (size) cls.push(size);
    return (
        <>
            {
                React.createElement(
                    component || "div",
                    {...props, style:{background:bg}, className: cls.join(' ')},
                    url ? <img src={url}/> : <b>{txt}</b>
                )
            }
        </>
    );
};

export default Avatar;