import React from 'react';
import ClassName from 'classnames';
import AvatarX from "../AvatarX";
import RippleX from "../../UI/RippleX/RippleX";
import IconX from "../../UI/Icon/IconX";

interface IProps {
    url: string|null
}

const AvatarChange:React.FC<IProps> = (props) => {
    const {url} = props;
    return (
        <div className={ClassName('AvatarChange')}>
            <RippleX className='AvatarChange__img'>
                <AvatarX url={url} txt={''} type={120} noneTxtIfExitImg={true} className='AvatarChange__avatar'/>
                <div className='AvatarChange__icon'><IconX name='icon_cameraadd'/></div>
            </RippleX>
        </div>
    );
};

export default AvatarChange;