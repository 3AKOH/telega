import React from 'react';
import ClassName from 'classnames';
import AvatarX from "../AvatarX";

interface IProps {
    url: string|null
    title: string
    online?: boolean
    lastTime: string
    desc: string
    type: 'group'|'user'
}

const AvatarInfo:React.FC<IProps> = (props) => {
    const {url,title,online=false,lastTime,type,desc} = props;
    const renderDesc = () => {
        switch (type) {
            case "group":
                return <div className={ClassName('-desc')}>{desc}</div>
            default: return <div className={ClassName('-status',{'-online':online})}>{online ? 'online' : 'none'}</div>
        }
    }
    return (
        <div className={ClassName('AvatarInfo','-'+type)}>
            <div className='AvatarInfo__img'>
                <AvatarX url={url} txt={title} type={120} noneTxtIfExitImg={true}/>
            </div>
            <div className='AvatarInfo__desc'>
                <div className='-title'>{title}</div>
                <div className='-wrap'>{renderDesc()}</div>
            </div>
        </div>
    );
};

export default AvatarInfo;