import React from 'react';
import ClassName from 'classnames';
import './AvatarX.scss'
import {genColor} from "../../Util/Common";

interface IProps {
    url: string | null
    noneTxtIfExitImg?: boolean
    txt: string
    component?: any
    type?: 120|44|54|33
    className?:string

    [inx: string]: any
}

const AvatarX: React.FC<IProps> = (props) => {
    const {url, txt, component,type,noneTxtIfExitImg=false, className} = props;
    const typeCls = type ? '-s'+type : '';
    const firstChars = txt.split(' ').map((ele,idx)=>idx>1?'':ele[0]);
    const style = txt!=='' ? {background:genColor(txt)}:{};
    return (
        <>
            {
                React.createElement(
                    component || "div",
                    {...props, style, className: ClassName('AvatarX',className,typeCls,{'-none':noneTxtIfExitImg && url})},
                    url ? <img src={url} className='AvatarX__item'/> : <b className='AvatarX__item'>{firstChars}</b>
                )
            }
        </>
    );
};

export default AvatarX;