import React from 'react';
import CSSTransition from "react-transition-group/CSSTransition";
import ClassName from 'classnames';


interface IProps {
    onClick?: any
    isOpen:any
    timeout:any
    style?:any
    classNames:any
    bgColor?:boolean
}

const Backdrop:React.FC<IProps> = ({onClick,isOpen,timeout,style,classNames,bgColor=true}) => {
    return (
        <>
            <CSSTransition in={isOpen} timeout={timeout} classNames={classNames && classNames} style={style && style} unmountOnExit mountOnEnter>
                <div className={ClassName('Backdrop',{'-none':!bgColor})} onClick={onClick ? onClick : null}/>
            </CSSTransition>
        </>
    );
};

export default Backdrop;