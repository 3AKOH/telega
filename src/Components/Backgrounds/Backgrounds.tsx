import React from 'react';
import RippleX from "../UI/RippleX/RippleX";
import TESTS from "../../assets/img/test.jpg";
import './Backgrounds.scss';
import EMPTY from "../../assets/img/empty.png";
import ClassName from 'classnames';

interface IProps {
    dataColors: string[]
    curColor: string
    newColor: any
}

const Backgrounds:React.FC<IProps> = ({children,curColor,dataColors,newColor}) => {
    return (
        <div className='Backgrounds'>
            {dataColors.map(color=>{
                return <div key={color} onClick={()=>newColor(color)}><RippleX className={ClassName('Backgrounds__item',{'-active':color==curColor})} style={{background: color}}><div className='-wrap'><img src={EMPTY}/></div></RippleX></div>
            })}
        </div>
    );
};

export default Backgrounds;