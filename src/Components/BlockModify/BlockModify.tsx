import React, {useEffect, useState} from 'react';
import TransitionGroup from "react-transition-group/TransitionGroup";
import CSSTransition from "react-transition-group/CSSTransition";

interface IProps {
    cssTransform: string
}
interface IElem {
    id:number
    txt:any
    status:boolean
}
const data:IElem[] = [
    {
        id: 1,
        txt: null,
        status: false
    },
    {
        id: 2,
        txt: null,
        status: false
    }
]

const BlockModify: React.FC<IProps> = ({children, cssTransform}) => {
    const [text, setText] = useState([...data]);
    useEffect(() => {
        // setText([...text])
        let x = [...text]
        x[0] = {...x[0],txt:children};
        setText(x);
        // setText([...text,text[0]:{...text.text[0]}])
    }, [children]);
    return (
        <div className='BlockModify'>
            <TransitionGroup className='BlockModify__wrap'>
                {
                    text.map((txt) => (
                        <CSSTransition in={txt.status} timeout={500} key={txt.id} classNames={cssTransform}
                            unmountOnExit
                        >
                            <div className='BlockModify__item'>{txt.txt}</div>
                        </CSSTransition>
                    ))
                }
            </TransitionGroup>
        </div>
    );
};

export default BlockModify;