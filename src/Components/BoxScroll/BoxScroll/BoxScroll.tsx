import React, {createRef, useEffect, useState} from 'react';
import ClassName from 'classnames';
import '../BoxScroll.scss';
import {FixedSizeList} from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";
import DeviceEmitter from "../../../Emitter/DeviceEmitter";

interface IProps {
    getBoxSize?: any
    className?:string
    resize?:boolean
    isActive?:boolean
    style?: any
    topLineOff?: boolean
    botLineOff?: boolean
    children:any
    itemSize?:any
    itemCount?:any
}
interface IState {
    ref: any
    timer: any,
    linesOff:{
        top: boolean
        bot: boolean
    }
    initScroll: number,
    style:{
        marginRight:number
    }
    lines:{
        top: boolean
        bot: boolean
    }
    Y:{
        show:boolean
        top:number
        height:number
    }
    X:{
        show:boolean
        top:number
        height:number
    }
}
const State:IState = {
    ref: createRef(),
    timer: null,
    linesOff:{
        top: false,
        bot: false,
    },
    initScroll: 0,
    style:{
        marginRight:0,
    },
    lines:{
        top: false,
        bot: false,
    },
    Y:{
        show:false,       //отображение
        top:0,            //отступ сверху
        height:20         //высота бара
    },
    X:{
        show:false,
        top:0,
        height:20
    }
}
const BoxScroll:React.FC<IProps> = ({children,getBoxSize,className,resize,isActive,style={},itemSize,itemCount,topLineOff=false,botLineOff=false}) => {
    useEffect(() => {
        update()
        DeviceEmitter.on('RESIZE', update);
        return ()=>{
            DeviceEmitter.off('RESIZE', update);
        }
    }, []);
    const [scrollState, setScrollState] = useState<IState>({
        ...State,
        ref: createRef(),
        linesOff:{...State,top:topLineOff,bot:botLineOff},
        style:{...State.style, marginRight: -DeviceEmitter.getScrollWidth()},
        initScroll: DeviceEmitter.getScrollWidth()
    });
    const update = () => {
        let box = scrollState.ref.current;
        if(!box) return false;
        // box = box._outerRef

        const next = box.nextSibling;
        next.classList.add('-scroll');

        const status = {
            scroll: DeviceEmitter.getScrollWidth(),
            y: false,
            x: false,
        };
        //высчитываем скроллы
        const scrollSize = {
            top: box.scrollTop,
            bot: status.x ? box.scrollHeight-box.clientHeight-box.scrollTop : box.scrollHeight-box.clientHeight-box.scrollTop,
            left: box.scrollLeft,
            right: status.y ? status.scroll-box.clientWidth-box.scrollLeft : status.scroll-box.clientWidth-box.scrollLeft,
        };
        setScrollState((preState)=>{
            if(preState.timer) clearTimeout(preState.timer);
            const timerID = setTimeout(()=>{
                next.classList.remove('-scroll');
            },1000);
            return {
                ...scrollState,
                timer: timerID,
                Y: {
                    ...scrollState.Y,
                    show: box.clientHeight !==box.children[0].clientHeight,
                    top:(box.scrollTop/box.children[0].clientHeight)*100,
                    height:(box.clientHeight/box.children[0].clientHeight)*100,
                },
                lines: {
                    top: scrollSize.top>0,
                    bot: scrollSize.bot>0,
                },
                style: {
                    ...scrollState.style,
                    marginRight: status.scroll ? -status.scroll : -scrollState.initScroll
                }
            }
        })
        if(getBoxSize!==undefined) getBoxSize(scrollSize);
    };

    useEffect(()=>{
        update();  //при изменение содержимого
    },[children,resize]);

    return (
        <div className={ClassName('BoxScroll',className)} style={style}>
            <div className='BoxScroll__content' style={{...scrollState.style}} ref={scrollState.ref} onScroll={update}>
                <div className='BoxScroll__in'>
                    {children}
                </div>
            </div>
            <div className='BoxScroll__bars '>
                {scrollState.X.show && <div className='BoxScroll__bar -x'><div><i></i><span><i></i></span></div></div>}
                {scrollState.Y.show && <div className='BoxScroll__bar -y'><div><i></i><span style={{
                    top: scrollState.Y.top+"%",
                    height: scrollState.Y.height+"%",
                }}><i></i></span></div></div>}
            </div>
            <div className='BoxScroll__lines'>
                <div className={ClassName('-top',{'-show':!scrollState.linesOff.top && scrollState.lines.top})}/>
                <div className={ClassName('-bot',{'-show':!scrollState.linesOff.bot && scrollState.lines.bot})}/>
            </div>
        </div>
    );
};

export default BoxScroll;