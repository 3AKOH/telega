import React from 'react';
import './CircleFlay.scss';
import RippleX from "../../UI/RippleX/RippleX";

interface IProps {
    [idx:string]:any
}

const CircleFlay:React.FC<IProps> = (props) => {
    const {children} = props;
    return (
        <>
            {
                React.createElement('div',
                    {...props, className:'CircleFlay'},
                    children
                )
            }
        </>
    );
};

export default CircleFlay;