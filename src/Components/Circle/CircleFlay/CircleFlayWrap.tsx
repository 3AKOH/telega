import React, {useEffect, useState} from 'react';
import './CircleFlay.scss';
import RippleX from "../../UI/RippleX/RippleX";
import CSSTransition from "react-transition-group/CSSTransition";
import CircleFlay from "./CircleFlay";

export interface ICircleFlayWrap {
    isOpen: boolean
    onToggle: any
}
interface IProps {
    menu?: any
    children?: any
    [idx:string]:any
}
const InitialState = {
    showCircle: false,
    miniMenu: false,
};

const CircleFlayWrap:React.FC<IProps> = (props) => {
    const {children,menu} = props;
    const [state, setState] = useState(InitialState);
    useEffect(() => {
        setTimeout(() => {
            setState({...state, showCircle: true});
        }, 500)
    }, []);
    const handleToggleMinimenu = () => {
        setState({...state, miniMenu: !state.miniMenu});
    }
    const data = {
        isOpen:state.miniMenu,
        onToggle:handleToggleMinimenu
    }
    return (
        <CircleFlay>
            <CSSTransition in={state.showCircle} timeout={200} classNames='scale' mountOnEnter unmountOnExit>{children(data)}</CSSTransition>
            { menu && menu(data)}
        </CircleFlay>
    );
};

export default CircleFlayWrap;