import React from 'react';
import CSSTransition from "react-transition-group/CSSTransition";
import BoxScroll from "../BoxScroll/BoxScroll/BoxScroll";

interface IProps {
    isOpen: any
    closeFunc: any
}

const Drawer: React.FC<IProps> = ({children, closeFunc, isOpen}) => {
    let cls = ['Drawer'];
    if (!isOpen) cls.push('-close');
    return (
        <>
            <CSSTransition in={isOpen} timeout={300} classNames='Drawer-node' unmountOnExit>
                <div className={cls.join(' ')}>
                    <div className='Drawer__body'>
                        <div className='Drawer__dopil' onClick={closeFunc}/>
                        <BoxScroll>
                            {children}
                        </BoxScroll>
                    </div>
                </div>
            </CSSTransition>
        </>
    );
};

export default Drawer;