import React, {useContext} from 'react';
import {NavLink} from "react-router-dom";
import DrawerHeader from "./DrawerHeader/DrawerHeader";
import DrawerList from "../DrawerList/DrawerList";
import DrawerItem from "../DrawerList/DrawerItem/DrawerItem";
import LangStore from "../../../Store/LangStore";
import CheckboxX from "../../UI/Checkbox/CheckboxX/CheckboxX";
import {AppContext} from "../../../Context/App/AppState";
import {setTheme} from "../../../Context/App/AppAC";
import CodeCountry_Modal from "../../../Modals/CodeCountry_Modal/CodeCountry_Modal";
import ModalLink from "../../../Modals/ModalLink/ModalLink";
import LoginSettings_Modal from "../../../Modals/LoginSettings_Modal/LoginSettings_Modal";
import {VAR_CREATE_CHANNEL, VAR_CREATE_GROUP, VAR_DARK_THEME, VAR_SETTINGS} from "../../../vars";

interface IProps {

}

const DrawerContent: React.FC<IProps> = ({children}) => {
    const {appState,dispatch} = useContext(AppContext);
    const handleTheme = () =>{
        dispatch(setTheme(appState.themes.curTheme==='dark' ? 'default' : 'dark'))
    };
    return (
        <div className='DrawerContent'>
            <DrawerHeader className='DrawerContent__head'/>
            <div className='DrawerContent__body'>
                <DrawerList>
                    <DrawerItem icon={'icon_group'}>{LangStore.getString('NewGroup',null,VAR_CREATE_GROUP)}</DrawerItem>
                    <DrawerItem icon={'icon_channel'}>{LangStore.getString('NewChannel',null,VAR_CREATE_CHANNEL)}</DrawerItem>
                    <ModalLink link={(d:any)=><LoginSettings_Modal {...d}/>}>
                        <DrawerItem icon={'icon_settings'}>{LangStore.getString('SETTINGS',null,VAR_SETTINGS)}</DrawerItem>
                    </ModalLink>
                    <DrawerItem icon={'icon_moon'} component='label' checkbox={()=><CheckboxX onChange={handleTheme} checked={appState.themes.curTheme==='dark'}/>}>
                        {LangStore.getString('SETTINGS',null,VAR_DARK_THEME)}
                    </DrawerItem>
                </DrawerList>
            </div>
            <div className='DrawerContent__footer'>
                <div className='-name'>
                    <NavLink to='#'>Telegram Web</NavLink>
                </div>
                <div className='-version'>
                    <NavLink to='#'>Version 1.8.15</NavLink> — <NavLink to='#'>About the program</NavLink>
                </div>
            </div>
        </div>
    );
};

export default DrawerContent;