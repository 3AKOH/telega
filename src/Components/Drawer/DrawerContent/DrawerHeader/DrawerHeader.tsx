import React, {useEffect, useState} from 'react';
import Avatar from "../../../Avatar/Avatar";
import IconX from "../../../UI/Icon/IconX";
import ButtonX from "../../../UI/Button/ButtonX/ButtonX";
import RippleX from "../../../UI/RippleX/RippleX";
import ChatsStore, {IChatsStoreItem} from "../../../../Store/ChatsStore";
import FilesStore from "../../../../Store/FilesStore";
import AppStore from "../../../../Store/AppStore";
import UsersStore, {IUserStoreItem} from "../../../../Store/UsersStore";
import {genColor} from "../../../../Util/Common";

const bgIMG = '1.jpg';

interface IProps {
    className?:string
}
export interface IState {
    id: number
    user: IUserStoreItem
    img: any
}

const DrawerHeader:React.FC<IProps> = ({children,className}) => {
    const [state, setState] = useState<IState>(()=>{
        const id = AppStore.getOption("my_id");
        const user = UsersStore.get(id);
        return {
            id: id,
            user,
            img: {
                id: user.photo ? user.photo.small.id : 0,
                url: user.photo ? FilesStore.getUrl(user.photo.small.id) : null
            }
        }
    });
    const LOADED_FILE = (id:number) =>{
        if(!state.img.id || id !== state.img.id) return;
        setState({
            ...state,
            img:{
                id,
                url: FilesStore.getUrl(id)
            }
        })
    };
    useEffect(() => {
        FilesStore.on('LOADED_FILE',LOADED_FILE);
        return ()=>{
            FilesStore.off('LOADED_FILE',LOADED_FILE)
        }
    }, []);
    const handlOpenMy = () => {};
    return (
        <div className={`DrawerHeader ${className}`} style={{backgroundImage: `url(/img/bg/${bgIMG})`}}>
            <div className='DrawerHeader__avatar'>
                <Avatar url={state.img.url} txt={state.user.first_name[0]} bg={genColor(state.user.first_name[0])} size='big' onClick={handlOpenMy} component={RippleX}/>
            </div>
            <div className='DrawerHeader__info'>
                <div className='-contacts'>
                    <div className='-name'>{state.user.first_name}</div>
                    <div className='-phone'>{state.user.phone_number}</div>
                </div>
                <div className='-my'>
                    <RippleX onClick={handlOpenMy}><IconX name='icon_my'/></RippleX>
                </div>
            </div>
        </div>
    );
};

export default DrawerHeader;