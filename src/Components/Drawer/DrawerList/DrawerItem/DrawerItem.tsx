import React from 'react';
import IconX from "../../../UI/Icon/IconX";
import RippleX from "../../../UI/RippleX/RippleX";

interface IProps {
    icon?:string
    checkbox?:any
    [idx:string]:any
}

const DrawerItem: React.FC<IProps> = (props) => {
    const {children,icon,checkbox} = props;
    return (
        <RippleX className='DrawerList__item' {...props}>
            <div className='-iconBox'>
                {
                    icon && <IconX name={icon}/>
                }
            </div>
            <div className='-txt'>{children}</div>
            {checkbox && <div className='-check'>{checkbox()}</div>}
        </RippleX>
    );
};

export default DrawerItem;