import React from 'react';
import ClassName from 'classnames';

interface IProps {
    dopLineTop?: boolean
    dopLineBot?: boolean
}

const DrawerList: React.FC<IProps> = ({children, dopLineTop, dopLineBot}) => {
    return (
        <nav className={ClassName('DrawerList',{'-dopLineTop':dopLineTop},{'-dopLineBot':dopLineBot})}>
            {children}
        </nav>
    );
};

export default DrawerList;