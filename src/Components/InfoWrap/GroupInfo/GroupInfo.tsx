import React from 'react';
import InfoWrap from "../InfoWrap";
import InfoLinkList, {IInfoLinkList} from "../InfoLinkList/InfoLinkList";
import InfoAvatar from "../InfoAvatar/InfoAvatar";
import Avatar from "../../Avatar/Avatar";
import InfoLink from "../InfoLink/InfoLink";
import RippleX from "../../UI/RippleX/RippleX";
import CheckboxX from "../../UI/Checkbox/CheckboxX/CheckboxX";
import ButtonX from "../../UI/Button/ButtonX/ButtonX";
import BoxScroll from "../../BoxScroll/BoxScroll/BoxScroll";

interface IProps {

}

const dataInfo:IInfoLinkList[] = [
    {
        id: "1",
        icon:"icon_info",
        label: <div>ПРИВЕТ <br/> <p>РУсскоязычный портал разработчиков на TON.</p><p>Важно: это не официальный чат TON. позиция администраторов чата не является позицией TON/Telegram. <strong>Информация</strong></p></div>,
        to: null,
    },
    {
        id: "2",
        icon:"icon_kolokol",
        label: "Уведомления",
        to: null,
        checkbox: true,
    },
];
const dataList:IInfoLinkList[] = [
    {
        id: "1",
        icon:"icon_img",
        label: "106 фотографий",
        to: null
    },
    {
        id: "2",
        icon:"icon_video",
        label: "4 видео",
        to: null
    },
    {
        id: "3",
        icon:"icon_file",
        label: "18 файлов",
        to: null
    },
    {
        id: "4",
        icon:"icon_link",
        label: "294 ссылки",
        to: null
    }
];

const GroupInfo:React.FC<IProps> = ({children}) => {
    return (
        <BoxScroll className='Modal__content'>
            <InfoAvatar
                avatar={() => <Avatar url={null} size='big' txt='T' bg='T'/>}
                title='TON Developers [ru]'
                desc='509 участников'
            />
            <InfoWrap top={true}>
                <InfoLink icon_name='icon_info' hover={false} flexTop={true}>
                    <div className='InfoUserText'>
                        <div className='InfoUserText__title'>
                            <a href='#ds'>t.me/TONgramDev</a>
                            <div>Ссылка</div>
                        </div>
                        <div className='InfoUserText__body'>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci animi deleniti eaque et illum incidunt natus nostrum provident quod!
                            <br/>
                            <br/>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci animi deleniti eaque et illum incidunt natus nostrum provident quod!
                            <br/>
                            <br/>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci animi deleniti eaque et illum incidunt natus nostrum provident quod!
                        </div>
                        <div className='InfoUserText__end'>Информация</div>
                    </div>
                </InfoLink>
                <InfoLink component={RippleX} icon_name='icon_kolokol' end={() => <CheckboxX onChange={() => {
                }} checked={true}/>}>Уведомления</InfoLink>
                <div className='InfoButton'>
                    <ButtonX type='nav' block={true}>
                        Открыть канал
                    </ButtonX>
                </div>
            </InfoWrap>
            <InfoWrap top={true}>
                <InfoLink component={RippleX} icon_name='icon_img' >106 фотографий</InfoLink>
                <InfoLink component={RippleX} icon_name='icon_video'>4 видео</InfoLink>
                <InfoLink component={RippleX} icon_name='icon_file'>18 файлов</InfoLink>
                <InfoLink component={RippleX} icon_name='icon_link'>294 ссылки</InfoLink>
            </InfoWrap>
            <InfoWrap top={true}>
                <InfoLink component={RippleX} icon_name='icon_menu2'>Поделиться контактом</InfoLink>
                <InfoLink component={RippleX} icon_name='icon_empty'>Изменить контакт</InfoLink>
                <InfoLink component={RippleX} icon_name='icon_empty'>Удалить контакт</InfoLink>
                <InfoLink component={RippleX} icon_name='icon_empty'>Очистить историю</InfoLink>
                <InfoLink component={RippleX} icon_name='icon_empty'>Удалить сообщение</InfoLink>
                <InfoLink component={RippleX} icon_name='icon_empty'>
                    <div className='color_red'>Заблокировать</div>
                </InfoLink>
            </InfoWrap>
        </BoxScroll>
    );
};

export default GroupInfo;