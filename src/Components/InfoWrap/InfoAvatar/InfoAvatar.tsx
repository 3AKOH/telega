import Avatar from "../../Avatar/Avatar";
import React from "react";

interface IProps {
    title:string
    desc?:string
    avatar:any
}

const InfoAvatar:React.FC<IProps> = ({avatar,title,desc}) => {
    return (
        <div className='InfoAvatar'>
            <div className='InfoAvatar__ava'>
                {avatar()}
            </div>
            <div className='InfoAvatar__info'>
                <div className='-name'>{title}</div>
                <div className='-desc'>{desc}</div>
            </div>
        </div>
    );
};

export default InfoAvatar;