import React from 'react';

interface IProps {}

const InfoInputXWrap:React.FC<IProps> = ({children}) => {
    return (
        <div className='InfoInputXWrap'>
            {children}
        </div>
    );
};

export default InfoInputXWrap;