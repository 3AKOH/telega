import React from 'react';
import ClassName from 'classnames';
import IconX from "../../UI/Icon/IconX";

interface IProps {
    icon_name?: string
    component?: any
    hover?: boolean
    flexTop?: boolean
    className?: string
    end?: ()=>any
    [idx:string]:any
}

const InfoLink: React.FC<IProps> = (props) => {
    const {children,component,className,icon_name,end,hover=true,flexTop=false} = props;
    return (
        <>
            {React.createElement(
                component || 'div',
                {...props,className:ClassName('InfoLink',className,{'InfoLink__hover':hover},{'InfoLink__top':flexTop})},
                (
                    <>
                        {icon_name && <div className='InfoLink__icon'><IconX name={icon_name}/></div>}
                        <div className='InfoLink__text'>{children}</div>
                        {end && <div className='InfoLink__end'>{end()}</div>}
                    </>
                )
            )}
        </>
    );
};

export default InfoLink;