import React from 'react';
import IconX from "../../../UI/Icon/IconX";
import RippleX from "../../../UI/RippleX/RippleX";

interface IProps {
    checkbox?: boolean
    key?: any
    icon?: string
    to?: any
}

const InfoLinkItem:React.FC<IProps> = ({children,icon,to,checkbox}) => {
    let cls = ['InfoLinkList__item'];
    if(to) cls.push('-link');
    return (
        <RippleX className={cls.join(' ')}>
            <div className='-img'>
                {
                    icon && <IconX name={icon}/>
                }
            </div>
            <div className='-label'>
                {children}
            </div>
            {
                checkbox && <div className='-checkbox'><input type='checkbox'/></div>
            }
        </RippleX>
    );
};

export default InfoLinkItem;