import React from 'react';
import InfoLinkItem from "./InfoLinkItem/InfoLinkItem";

interface IProps {
    data: IInfoLinkList[]
}
export interface IInfoLinkList {
    id: string
    icon: string
    label?: any
    to: any
    component?:any
    checkbox?: boolean
}

const InfoLinkList:React.FC<IProps> = ({children,data}) => {
    return (
        <div className='InfoLinkList'>
            {
                data && data.map(item=>{
                    return <InfoLinkItem key={item.id} icon={item.icon} to={item.to}>{item.label}</InfoLinkItem>
                })
            }
        </div>
    );
};

export default InfoLinkList;