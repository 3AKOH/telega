import React from 'react';
import ClassName from 'classnames';
import IconX from "../../UI/Icon/IconX";

interface IProps {
    icon_name?: string
    component?: any
    hover?: boolean
    flexTop?: boolean
    title?: string
    icon_active?: boolean
    className?: string
    end?: ()=>any
    [idx:string]:any
}

const InfoLinkX: React.FC<IProps> = (props) => {
    const {children,component,className,title,icon_active=false,icon_name,end,hover=true,flexTop=false} = props;
    return (
        <>
            {React.createElement(
                component || 'div',
                {...props,className:ClassName('InfoLinkX',className,{'InfoLinkX__hover':hover},{'InfoLinkX__top':flexTop})},
                (
                    <>
                        {icon_name && <div className={ClassName('InfoLinkX__icon',{'-active':icon_active})}><IconX name={icon_name}/></div>}
                        <div className='InfoLinkX__text'>
                            <div className='-body'>{children}</div>
                            {title && <div className='-title'>{title}</div>}
                        </div>
                        {end && <div className='InfoLinkX__end'>{end()}</div>}
                    </>
                )
            )}
        </>
    );
};

export default InfoLinkX;