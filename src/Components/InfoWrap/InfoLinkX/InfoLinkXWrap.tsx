import React from 'react';

interface IProps {}

const InfoLinkXWrap:React.FC<IProps> = ({children}) => {
    return (
        <div className='InfoLinkXWrap'>
            {children}
        </div>
    );
};

export default InfoLinkXWrap;