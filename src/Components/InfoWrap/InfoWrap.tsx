import React from 'react';
import './InfoWrap.scss';

interface IProps {
    top?: any
    bot?: any
    topPadding?: boolean
    botPadding?: boolean
}

const InfoWrap:React.FC<IProps> = ({children,top,bot,topPadding=false,botPadding=false}) => {
    return (
        <div className='InfoWrap'>
            {
                top != undefined && <div className='InfoWrap__item'>{topPadding ? (<div className='InfoWrap__padding'>{top}</div>) : top}</div>
            }
            <div className='InfoWrap__body'>
                {children}
            </div>
            {
                bot != undefined && <div className='InfoWrap__item'>{botPadding ? (<div className='InfoWrap__padding'>{bot}</div>) : bot}</div>
            }
        </div>
    );
};

export default InfoWrap;