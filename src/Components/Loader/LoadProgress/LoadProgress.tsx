import React from 'react';
import ClassName from 'classnames';
import './LoadProgress.scss';

interface IProps {
    proc: number
    size?: 20 | 30 | 40| 46
}

const LoadProgress: React.FC<IProps> = (props) => {
    const {proc = 100,size=40} = props;
    const p = proc > 100 ? 101 : proc;
    const limit = {
        min: 790,
        max: 200
    };
    const deg = Math.round(limit.min - (limit.min-limit.max)*p/100);
    const lines = {
        "strokeDashoffset": deg+"px"
    };
    const s = size ? '-s'+size : '';
    return (
        <>
            <div className={ClassName('LoadProgress',s)}>
                <svg className="LoadProgress__svg" viewBox="0 0 200 200" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <circle className='LoadProgress__wrap' r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="800" stroke-dashoffset="0"/>
                    <circle className="LoadProgress__wrap LoadProgress__bar" r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="800" stroke-dashoffset="0" style={lines}/>
                </svg>
            </div>
        </>

    );
};

export default LoadProgress;