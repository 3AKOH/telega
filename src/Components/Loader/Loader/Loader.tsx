import React from 'react';
import './Loader.scss'

interface IProps {}

const Loader:React.FC<IProps> = ({children}) => {
    return (
        <svg className="Loader" viewBox="0 0 50 50">
            <circle className="path" cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle>
        </svg>
    );
};

export default Loader;