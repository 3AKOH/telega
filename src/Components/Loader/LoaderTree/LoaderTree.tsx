import React from 'react';
import './LoaderTree.scss'

interface IProps {}

const LoaderTree:React.FC<IProps> = ({children}) => {
    return (
        <div className="LoaderTree">
            <div className="LoaderTree__inner one"></div>
            <div className="LoaderTree__inner two"></div>
            <div className="LoaderTree__inner three"></div>
        </div>
    );
};

export default LoaderTree;