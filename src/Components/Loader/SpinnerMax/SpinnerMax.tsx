import React from 'react';
import './SpinnerMax.scss'
import IconX from "../../UI/Icon/IconX";

interface IProps {
    className?:string
}

const SpinnerMax:React.FC<IProps> = ({children,className}) => {
    return (
        <div className={`Spinner ${className}`}>
            <IconX name='icon_lock' className='InitializeApp__lock'/>
            <div className="Spinner-circle Spinner-circle-outer"></div>
            <div className="Spinner-circle-off Spinner-circle-inner"></div>
            <div className="Spinner-circle Spinner-circle-single-1"></div>
            <div className="Spinner-circle Spinner-circle-single-2"></div>
        </div>
    );
};

export default SpinnerMax;