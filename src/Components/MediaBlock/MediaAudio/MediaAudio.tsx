import React from 'react';
import TESTS from '../../../assets/img/test.jpg';
import MediaAudioItem from "./MediaAudioItem";

interface IProps {}

const MediaAudio:React.FC<IProps> = ({children}) => {
    return (
        <div className='MediaAudio'>
            <MediaAudioItem title={'Million Telegrams'}/>
            <MediaAudioItem title={'200,000 members'}/>
            <MediaAudioItem title={'Million Telegrams'}/>
            <MediaAudioItem title={'200,000 members'}/>
            <MediaAudioItem title={'Million Telegrams'}/>
            <MediaAudioItem title={'200,000 members'}/>
            <MediaAudioItem title={'Million Telegrams'}/>
            <MediaAudioItem title={'200,000 members'}/>
        </div>
    );
};

export default MediaAudio;