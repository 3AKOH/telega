import React, {useState} from 'react';
import RippleX from "../../UI/RippleX/RippleX";
import ClassName from 'classnames';
import AvatarX from "../../Avatar/AvatarX";
import IconX from "../../UI/Icon/IconX";
import TESTS from '../../../assets/img/test.jpg';
import LoadProgress from "../../Loader/LoadProgress/LoadProgress";
import {Simulate} from "react-dom/test-utils";
import InputRange from "../../UI/InputRange/InputRange";
import CSSTransition from "react-transition-group/CSSTransition";

interface IProps {
    load?: boolean
    title: string
}

const InitialState = {
    play: false,
    load: false,
    loaded: false,
    proc: 5,
    playRange: 0
};

const MediaAudioItem: React.FC<IProps> = (props) => {
    const [state, setState] = useState(InitialState);
    const {title, load = false} = props;
    const toggleLoading = () => {
        if(!state.loaded){
            if(!state.load){
                let x  = setInterval(()=>{
                    setState(prev => {
                        if(prev.loaded){
                            clearInterval(x);
                            return prev
                        }
                        if(prev.proc>100){
                            return {...prev, loaded: true,load: false}
                        }
                        return {...prev, proc: prev.proc+10}
                    })
                },100)
            }
            setState({...state, load: !state.load})
        }else{
            setState({...state, play: !state.play})
        }
    };
    const renderDoc = () => {
        return (
            <div className={ClassName('-img')} onClick={toggleLoading}>
                {
                    state.load ? (
                        <>
                            <div className='-flay -load -loading'><LoadProgress proc={state.proc} size={46}/>
                                <div className='-close'><IconX className='-icon' name='icon_close'/></div>
                            </div>
                        </>
                    ) : state.loaded ? (
                        <>
                            <CSSTransition timeout={100} in={state.play} classNames='scale100' unmountOnExit mountOnEnter>
                                <div className='-flay'><IconX name='icon_pause'/></div>
                            </CSSTransition>
                            <CSSTransition timeout={100} in={!state.play} classNames='scale100' unmountOnExit mountOnEnter>
                                <div className='-flay -play'><IconX name='icon_largeplay'/></div>
                            </CSSTransition>
                        </>
                    ) : (
                        <>
                            <div className={ClassName('-flay -load -play')}><IconX className='-icon' name='icon_largeplay'/></div>
                        </>
                    )
                }
                <div className='MediaAudio__circle'/>
            </div>
        );
    };
    const onChange = (e:any) =>{
        console.log(e.target.value);
        setState({...state,playRange: e.target.value})
    };
    return (
        <>
            {
                React.createElement(
                    'div',
                    {className: ClassName('MediaAudio__item')},
                    <>
                        <div className='MediaAudio__img'>
                            {renderDoc()}
                        </div>
                        <div className='MediaAudio__info'>
                            <div className='-title'>{title}</div>
                            <div className='-track'>
                                {state.loaded ? (
                                    <InputRange onChange={onChange} value={state.playRange}/>
                                ) : (
                                    <div className='-artist'>Best Artist</div>
                                )}
                            </div>
                            <div className='-loading'>3.1 MB — <u>Oct 24, 2019 at 19:20</u></div>
                        </div>
                    </>
                )
            }
        </>
    );
};

export default MediaAudioItem;