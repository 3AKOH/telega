import React, {useEffect, useState} from 'react';
import ButtonX from "../UI/Button/ButtonX/ButtonX";
import RippleX from "../UI/RippleX/RippleX";
import MediaBlockBar from "./MediaBlockBar/MediaBlockBar";
import './MediaBlock.scss'
import MediaMembers from "./MediaMembers/MediaMembers";
import MediaMedia from "./MediaMedia/MediaMedia";
import MediaLinks from "./MediaLinks/MediaLinks";
import MediaDocs from "./MediaDocs/MediaDocs";
import LoadingStep from "../Loader/LoadProgress/LoadProgress";
import MediaAudio from "./MediaAudio/MediaAudio";
import {Route, Switch} from "react-router";
import BoxScroll from "../BoxScroll/BoxScroll/BoxScroll";

interface IProps {}

const MediaBlock:React.FC<IProps> = ({children}) => {
    const [state, setState] = useState(0);
    useEffect(()=>{
        // setInterval(()=>{
        //     setState(prev=>prev+10)
        // },1000)
    },[])
    return (
        <div className='MediaBlock'>
            <div className='MediaBlock__top'>
                <MediaBlockBar active={true} component={RippleX}>Members</MediaBlockBar>
                <MediaBlockBar component={RippleX}>Media</MediaBlockBar>
                <MediaBlockBar component={RippleX}>Docs</MediaBlockBar>
                <MediaBlockBar component={RippleX}>Links</MediaBlockBar>
                <div className='-line'></div>
            </div>
            <div className='MediaBlock__body'>
                <Switch>
                    <Route exact path="/p2" component={()=><MediaMembers/>}/>
                    <Route exact path="/p3" component={()=><MediaMedia/>}/>
                    <Route exact path="/p4" component={()=><MediaLinks/>}/>
                    <Route exact path="/p5" component={()=><MediaDocs/>}/>
                    <Route exact path="/p6" component={()=><MediaAudio/>}/>
                    <Route exact component={()=><div className='MediaEmpty'>Список пуст</div>}/>
                </Switch>
                {/*<MediaMembers/>*/}
                {/*<MediaMedia/>*/}
                {/*<MediaLinks/>*/}
                {/*<MediaDocs/>*/}
                {/*<MediaAudio/>*/}
            </div>
        </div>
    );
};

export default MediaBlock;