import React from 'react';
import ClassName from "classnames";
import '../MediaBlock.scss'

interface IProps {
    active?: boolean
    component?: any
}

const MediaBlockBar:React.FC<IProps> = (props) => {
    const {active=false,component,children} = props;
    return (
        <>
            {React.createElement(
                component || 'div',
                {...props,className:ClassName('MediaBlock__nav')},
                (
                    <div className={ClassName('-wrap',{'-active':active})}>
                        {children}
                        {active && <i/>}
                    </div>
                )
            )}
        </>
    );
};

export default MediaBlockBar;