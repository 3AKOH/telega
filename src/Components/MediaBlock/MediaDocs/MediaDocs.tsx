import React from 'react';
import TESTS from '../../../assets/img/test.jpg';
import MediaDocsItem from "./MediaDocsItem";

interface IProps {}

const MediaDocs:React.FC<IProps> = ({children}) => {
    return (
        <div className='MediaDocs'>
            <MediaDocsItem title={'Super_Coller_Presentation_dsdsd_ds.pdf'}/>
            <MediaDocsItem title={'Super_Coller_Presentation.apk'}/>
            <MediaDocsItem title={'Super_Coller_Presentation.osm'}/>
            <MediaDocsItem title={'Super_Coller_Presentation.doc'}/>
            <MediaDocsItem title={'Super_Coller_Presentation.zip'}/>
            <MediaDocsItem title={'Super_Coller_Presentation.png'}/>
        </div>
    );
};

export default MediaDocs;