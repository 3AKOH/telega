import React, {useState} from 'react';
import RippleX from "../../UI/RippleX/RippleX";
import ClassName from 'classnames';
import AvatarX from "../../Avatar/AvatarX";
import IconX from "../../UI/Icon/IconX";
import TESTS from '../../../assets/img/test.jpg';
import LoadProgress from "../../Loader/LoadProgress/LoadProgress";
import {Simulate} from "react-dom/test-utils";

interface IProps {
    load?: boolean
    title: string
}

const InitialState = {
    load: false,
    loaded: false,
    proc: 5
};

const MediaDocsItem: React.FC<IProps> = (props) => {
    const [state, setState] = useState(InitialState);
    const {title, load = false} = props;
    const toggleLoading = () => {
        if(!state.loaded){
            let x  = setInterval(()=>{
                setState(prev => {
                    if(prev.loaded){
                        clearInterval(x);
                        return prev
                    }
                    if(prev.proc>100){
                        return {...prev, loaded: true,load: false}
                    }
                    return {...prev, proc: prev.proc+10}
                })
            },100)
        }
        if(!state.loaded) setState({...state, load: !state.load})
    };
    const renderDoc = () => {
        const typePoint = title.split('.');
        const t = typePoint.pop();
        const imgType = t === 'png' || t === 'jpg';
        const img = (t === 'png' || t === 'jpg') ? {backgroundImage: `url(${TESTS})`} : {};
        return (
            <div className={ClassName('-img', '-' + t)} style={img} onClick={toggleLoading}>
                {
                    state.load ? (
                        <>
                            <div className='-flay -load -loading'><LoadProgress proc={state.proc} size={40}/>
                                <div className='-close'><IconX className='-icon' name='icon_close'/></div>
                            </div>
                            {!imgType && <IconX className='-wrap' name='icon_doc_load'/>}
                        </>
                    ) : state.loaded ? (
                        <>
                            {!imgType && (
                                <>
                                    <div className='-flay'><span>{t}</span></div>
                                    <IconX className='-wrap' name='icon_docs'/>
                                </>
                            )}
                        </>
                    ) : (
                        <>
                            <div className={ClassName('-flay -load',{'-loading':imgType})}><IconX className='-icon' name='icon_download'/></div>
                            {!imgType && <IconX className='-wrap' name='icon_doc_load'/>}
                        </>
                    )
                }
            </div>
        );
    };
    return (
        <>
            {
                React.createElement(
                    'div',
                    {className: ClassName('MediaDocs__item')},
                    <>
                        <div className='MediaDocs__img'>
                            {renderDoc()}
                        </div>
                        <div className='MediaDocs__info'>
                            <div className='-title'>{title}</div>
                            <div className='-desc'>3.1 MB — <u>Oct 24, 2019 at 19:20</u></div>
                        </div>
                    </>
                )
            }
        </>
    );
};

export default MediaDocsItem;