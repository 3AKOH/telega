import React from 'react';
import AvatarInfo from "../../Avatar/AvatarInfo/AvatarInfo";
import MediaLinksItem from "./MediaLinksItem";
import TESTS from '../../../assets/img/test.jpg';

interface IProps {}

const MediaLinks:React.FC<IProps> = ({children}) => {
    return (
        <div className='MediaLinks'>
            <MediaLinksItem
                img={TESTS}
                title={'Karen Stanford Karen Stanford '}
                name={'Karen Stanford Karen Stanford который который Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford'}
                linkLabel={'http://slob.box/russeler'}
                link={'http://'}/>
            <MediaLinksItem
                img={null}
                title={'Karen Stanford Karen Stanford Karen'}
                name={'Karen Stanford Karen Stanford Karen'}
                linkLabel={'http://slob.box/russeler'}
                link={'http://'}/>
            <MediaLinksItem
                img={null}
                title={'Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford'}
                name={'sddf fdsf d fd'}
                linkLabel={'http://slob.box/russeler'}
                link={'http://'}/>
            <MediaLinksItem
                img={TESTS}
                title={'Karen Stanford Karen Stanford '}
                name={'Karen Stanford Karen Stanford который который Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford'}
                linkLabel={'http://slob.box/russeler'}
                link={'http://'}/>
            <MediaLinksItem
                img={null}
                title={'Karen Stanford Karen Stanford Karen'}
                name={'Karen Stanford Karen Stanford Karen'}
                linkLabel={'http://slob.box/russeler'}
                link={'http://'}/>
            <MediaLinksItem
                img={null}
                title={'Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford'}
                name={'sddf fdsf d fd'}
                linkLabel={'http://slob.box/russeler'}
                link={'http://'}/>
            <MediaLinksItem
                img={TESTS}
                title={'Karen Stanford Karen Stanford '}
                name={'Karen Stanford Karen Stanford который который Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford'}
                linkLabel={'http://slob.box/russeler'}
                link={'http://'}/>
            <MediaLinksItem
                img={null}
                title={'Karen Stanford Karen Stanford Karen'}
                name={'Karen Stanford Karen Stanford Karen'}
                linkLabel={'http://slob.box/russeler'}
                link={'http://'}/>
            <MediaLinksItem
                img={null}
                title={'Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford Karen Stanford'}
                name={'sddf fdsf d fd'}
                linkLabel={'http://slob.box/russeler'}
                link={'http://'}/>
        </div>
    );
};

export default MediaLinks;