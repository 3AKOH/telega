import React from 'react';
import RippleX from "../../UI/RippleX/RippleX";
import ClassName from 'classnames';
import AvatarX from "../../Avatar/AvatarX";

interface IProps {
    img: string | null
    name: string
    title: string
    link: string
    linkLabel: string
}

const MediaLinksItem: React.FC<IProps> = (props) => {
    const {img, name, title,link,linkLabel} = props;
    return (
        <div>
            {
                React.createElement(
                    RippleX,
                    {className: ClassName('MediaLinks__item')},
                    <>
                        <div className='MediaLinks__img'>
                            { img
                            ? <div className='-img' style={{backgroundImage:`url(${img})`}}/>
                            : <div className='-img'/>}
                        </div>
                        <div className='MediaLinks__info'>
                            <div className='-title'>{title}</div>
                            <div className='-name'>{name}</div>
                            <div className='-link'>{linkLabel}</div>
                        </div>
                    </>
                )
            }
        </div>
    );
};

export default MediaLinksItem;