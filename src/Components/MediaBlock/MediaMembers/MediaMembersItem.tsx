import React from 'react';
import RippleX from "../../UI/RippleX/RippleX";
import ClassName from 'classnames';
import AvatarX from "../../Avatar/AvatarX";

interface IProps {
    url: string | null
    name: string
    online?: boolean
    lastTime: string
}

const MediaMembersItem: React.FC<IProps> = (props) => {
    const {url, name, online = false, lastTime} = props;
    return (
        <>
            {
                React.createElement(
                    RippleX,
                    {className: ClassName('MediaMembers__item')},
                    <>
                        <div className='MediaMembers__img'>
                            <AvatarX url={url} txt={name} type={44}/>
                        </div>
                        <div className='MediaMembers__info'>
                            <div className='-name'>{name}</div>
                            <div className={ClassName('-status',{'-online':online})}>{online ? 'online': 'last seen just now'}</div>
                        </div>
                    </>
                )
            }
        </>
    );
};

export default MediaMembersItem;