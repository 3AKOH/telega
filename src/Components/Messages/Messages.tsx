import React, {createRef, useEffect, useState} from 'react';
import './Messages.scss';
import ClassName from 'classnames';
import MessagesItem from "./MessagesItem";
import STIKER from '../../assets/img/stiker.png';
import IMG from '../../assets/img/test.jpg';
import IMG2 from '../../assets/img/big.jpg';
import IMG3 from '../../assets/img/test_mini.jpg';
import DeviceEmitter from "../../Emitter/DeviceEmitter";
import MainContainerEmitter from "../../Emitter/MainContainerEmitter";

interface IProps {

}
interface IState {
    ref: any
    alightRight: boolean
    classic: boolean
    wrapCenter: boolean
}
const initalState = {
    ref: createRef(),
    alightRight: false,
    classic: false,
    wrapCenter: true
};

const Messages: React.FC<IProps> = (props) => {
    const {children} = props;
    const [state, setState] = useState<IState>(initalState);
    useEffect(() => {
        update(MainContainerEmitter.width);
        MainContainerEmitter.on('UPDATE', update);
        return ()=>{
            MainContainerEmitter.off('UPDATE', update);
        }
    }, []);
    const update = (width:number) => {
        setState(prev=>{
            if(!width){
                let box = state.ref.current;
                if (!box) return prev;
                width = box.clientWidth;
            }
            if(width > 900){
                return {...prev,alightRight:false}
            }else{
                return {...prev,alightRight:true}
            }
        })
    };
    return (
        <div className='Messages' ref={state.ref}>
            <div className={ClassName('Messages__wrap',{'-center':state.wrapCenter})}>
                <div className='Messages__content'>
                    {data.map((ele,idx)=>{
                        return (
                            <MessagesItem
                                resize={state.alightRight}
                                active={idx==1}
                                ava={true}
                                urlAva={null}
                                file={ele.file}
                                key={idx}
                                username={false? '' : ele.username}
                                full={false}
                                msg={ele.msg}
                                music={ele.music}
                                noneUgol={ele.noneUgol}
                                me={ele.me}
                                right={ele.me && !state.classic || ele.me && state.alightRight}
                                sticker={ele.sticker}
                                collection={ele.img}
                            />
                        )
                    })}
                </div>
            </div>
        </div>
    );
};

export default Messages;

const data = [
    {
        me:true,
        img:IMG,
        username:'zakon dads1',
        msg: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab blanditiis consequatur expedita fugiat nemo optio perferendis quas sapiente, sequi tempora. Harum, iste praesentium. Ad asperiores cum deleniti dolor dolore et inventore magnam magni quis. A adipisci animi, consequatur consequuntur deleniti dolor doloremque doloribus ea eaque explicabo incidunt inventore nulla quasi qui quibusdam saepe sed, similique?'
    },
    {
        music:'BaniBanassi',
        username:'zakon dads1',
        msg: 'Привет!'
    },
    {
        music:'BaniBanassi',
        username:'zakon dads1',
        msg: 'Привет!'
    },
    {
        img:IMG,
        noneUgol:true,
        username:'ruslan dads',
        msg: 'привет!'
    },
    {
        img:IMG2,
        username:'vlad dads',
        msg: 'lorem lorem mackds sdwegfd kfd'
    },
    {
        me: true,
        file:'Lorem ipsum dolor sit amet, consectetur adipisicing elit2.pdf',
        username:'vlad dads',
        msg: 'lorem lorem mackds sdwegfd kfd'
    },
    {
        file:'Settings.pdf',
        username:'vlad dads',
        msg: 'lorem lorem mackds sdwegfd kfd'
    },
    {
        me: true,
        file:'Lorem ipsum dolor sit amet, consectetur adipisicing elit.pdf',
        username:'vlad dads',
        msg: 'lorem lorem mackds sdwegfd kfd'
    },
    {
        file:'Lorem ipsum dolor sit amet, consectetur adipisicing elit.pdf',
        username:'vlad dads',
        msg: 'lorem lorem mackds sdwegfd kfd'
    },
    {
        sticker:STIKER,
        username:'sanek dads',
        msg: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab blanditiis consequatur expedita fugiat nemo optio perferendis quas sapiente, sequi tempora. Harum, iste praesentium. Ad asperiores cum deleniti dolor dolore et inventore magnam magni quis. A adipisci animi, consequatur consequuntur deleniti dolor doloremque doloribus ea eaque explicabo incidunt inventore nulla quasi qui quibusdam saepe sed, similique?'
    },
    {
        me:true,
        sticker:STIKER,
        username:'zakon dads1',
        msg: 'тоже можно что-то делать'
    },
    {
        username:'zakon dads1',
        msg: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab blanditiis consequatur expedita fugiat nemo optio perferendis quas sapiente, sequi tempora. Harum, iste praesentium. Ad asperiores cum deleniti dolor dolore et inventore magnam magni '
    },
    {
        music:'Lorem ipsum dolor sit amet, consectetur adipisicing elit.pdf',
        username:'zakon dads1',
        msg: 'Привет!'
    },
    {
        me:true,
        music:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. consectetur adipisicing elit.',
        username:'zakon dads1',
        msg: 'Привет!'
    },
    {
        me:true,
        music:'Lorem ipsum ',
        username:'zakon dads1',
        msg: 'Привет!'
    },
    {
        music:'BaniBanassi',
        username:'zakon dads1',
        msg: 'Привет!'
    },
    {
        username:'zakon dads1',
        msg: 'lorem lorem mackds sdwegfd kfd'
    },
]