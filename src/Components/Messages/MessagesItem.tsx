import React, {createRef, useEffect, useState} from 'react';
import './Messages.scss';
import ClassName from 'classnames';
import AvatarX from "../Avatar/AvatarX";
import IconX from "../UI/Icon/IconX";
import MediaAudioItem from "../MediaBlock/MediaAudio/MediaAudioItem";
import MediaDocsItem from "../MediaBlock/MediaDocs/MediaDocsItem";

interface IProps {
    noneUgol?: boolean
    ava?: boolean
    urlAva: string | null
    me?: boolean
    username: string
    full?: boolean
    right?: boolean
    msg: string
    file?: string
    music?: string
    sticker?: string
    collection?: string
    active?: boolean

    [inx: string]: any
}
interface IState {
    sdvigDate: boolean
    pixel: any
}

const MessagesItem: React.FC<IProps> = (props) => {
    const [state, setState] = useState<IState>({
        sdvigDate: false,
        pixel: createRef(),
    });
    const {active=false,music,file,resize,full = false,noneUgol = false,ava = true, right = false, sticker, collection, me = false, msg, urlAva, username} = props;
    useEffect(()=>{
        update();
    },[resize])
    const update = () =>{
        setState(prev=>{
            const p = state.pixel.current;
            if(!p) return prev;
            if(p.parentElement.clientWidth - p.offsetLeft > p.offsetWidth){
                return {...prev, sdvigDate: true}
            }else{
                return {...prev, sdvigDate: false}
            }
        })
    }
    const renderMsg = () =>{
        if(music){
            return (
                <div className='-wrapMsg '>
                    <div className='-ugol'>
                        <IconX name='icon_ugol'/>
                        <div className='-ten'/>
                    </div>
                    <div className='-playWraps'>
                        <MediaAudioItem title={music}/>
                    </div>
                    <div className='-date'>20:06</div>
                </div>
            )
        }else if(file){
            return (
                <div className='-wrapMsg '>
                    <div className='-ugol'>
                        <IconX name='icon_ugol'/>
                        <div className='-ten'/>
                    </div>
                    <div className='-playWraps'>
                        <MediaDocsItem title={file}/>
                    </div>
                    <div className='-date'>20:06</div>
                </div>
            )
        }else if(sticker){
            return (
                <div className='-sticker'>
                    <div className='-time'>20:06</div>
                    <img src={sticker} alt=''/>
                </div>
            )
        }else {
            return (
                <>
                    {collection && (
                        <div className='-collection'>
                            <div className='-www'>
                                <img src={collection}/>
                            </div>
                        </div>
                    )}
                    <div className='-wrapMsg'>
                        <div className='-ugol'>
                            <IconX name='icon_ugol'/>
                            <div className='-ten'/>
                        </div>
                        <div className='-txt'>
                            {username!='' && !me && !collection && (
                                <div className='-nick'>
                                    <div>{username}</div>
                                </div>
                            )}
                            <div className='-t'>
                                <span>{msg}</span>
                                <span className='-date -pixel' ref={state.pixel}>20:06</span>
                            </div>
                        </div>
                        <div className='-date'>20:06</div>
                    </div>
                </>
            )
        }
    }
    console.log(music);
    return (
        <div className={ClassName('Messages__item', {'-me': me}, {'-right': right}, {'-active': active}, {'-noneAvatar': !ava}, {'-music': !!music}, {'-file': !!file}, {'-noneUgol': noneUgol}, {'-sdvigDate': state.sdvigDate}, {'-full': full})}>
            {ava && (
                <div className='-ava'>
                    <AvatarX url={urlAva} txt={username} type={33}/>
                </div>
            )}
            <div className='-msg'>
                <div className='-wrapContent'>
                    {renderMsg()}
                </div>
            </div>
        </div>
    );
};

export default MessagesItem;