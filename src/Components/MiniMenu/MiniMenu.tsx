import React from 'react';
import CSSTransition from "react-transition-group/CSSTransition";
import './MiniMenu.scss';
import ClassName from 'classnames';
import Backdrop from "../Backdrop/Backdrop";

interface IProps {
    isOpen: boolean
    type?: 'topRight'|'topLeft'|'botRight'|'botLeft'
    onClose: any
}

const MiniMenu: React.FC<IProps> = ({children, isOpen, onClose,type='botRight'}) => {
    const cls = '-'+type;
    return (
        <>
            <CSSTransition timeout={200} in={isOpen} unmountOnExit mountOnEnter classNames='MiniMenu'>
                <>
                    <div className={ClassName('MiniMenu',cls)}>
                        {children}
                    </div>
                    <Backdrop isOpen={isOpen} classNames='show' onClick={onClose} timeout={200} bgColor={false}/>
                    {/*<div className='MiniMenu__wrap' onClick={onClose}/>*/}
                </>
            </CSSTransition>
        </>
    );
};

export default MiniMenu;