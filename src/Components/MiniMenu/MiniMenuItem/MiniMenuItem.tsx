import React from 'react';
import RippleX from "../../UI/RippleX/RippleX";
import '../MiniMenu.scss';

interface IProps {
    label: string
    [idx:string]:any
}

const MiniMenuItem:React.FC<IProps> = (props) => {
    const {label} = props;
    return (
        <RippleX {...props} className='MiniMenu__item'>{label}</RippleX>
    );
};

export default MiniMenuItem;