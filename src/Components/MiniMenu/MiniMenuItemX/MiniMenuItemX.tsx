import React from 'react';
import RippleX from "../../UI/RippleX/RippleX";
import ClassName from 'classnames';
import '../MiniMenu.scss';

interface IProps {
    status?: ''|'red'|'good'
    [idx:string]:any
}

const MiniMenuItemX:React.FC<IProps> = (props) => {
    const {children,status} = props;
    const cls = status ? '-'+status:'';
    return (
        <RippleX {...props} className={ClassName('MiniMenu__itemX',cls)}>{children}</RippleX>
    );
};

export default MiniMenuItemX;