import React from 'react';
import './NavBar.scss'
import ClassName from 'classnames';

interface IProps {
    dopLineBot?:boolean
    dopLineTop?:boolean
}

const NavBar:React.FC<IProps> = (props) => {
    const {dopLineBot,dopLineTop,children} = props;
    return (
        React.createElement('div',{
            className:ClassName('NavBar',{'-dopLineTop':dopLineTop},{'-dopLineBot':dopLineBot}),
            ...props
        },children)
    );
};

export default NavBar;