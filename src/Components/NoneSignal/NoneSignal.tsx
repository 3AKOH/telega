import React from 'react';
import './NoneSignal.scss';
import {VAR_NONE_SIGNAL} from "../../vars";

interface IProps {}

const NoneSignal:React.FC<IProps> = () => {
    return (
        <div className='NoneSignal'>
            <div className='NoneSignal__item'>
                {VAR_NONE_SIGNAL}
            </div>
        </div>
    );
};

export default NoneSignal;