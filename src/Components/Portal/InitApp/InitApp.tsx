import React, {useEffect, useState} from 'react';
import CSSTransition from "react-transition-group/CSSTransition";
import BlockModify from "../../BlockModify/BlockModify";
import SpinnerMax from "../../Loader/SpinnerMax/SpinnerMax";

interface IProps {
    text: string
    isOpen: boolean
    show:boolean
    showSpeed: number
}

const InitApp: React.FC<IProps> = ({text, isOpen,show,showSpeed}) => {
    return (
        <div className='InitializeApp'>
            <CSSTransition in={isOpen} timeout={showSpeed} classNames='scale'>
                <div className='InitializeApp__box'>
                    <CSSTransition timeout={showSpeed} in={show} classNames='scale'>
                        <SpinnerMax/>
                    </CSSTransition>
                    <div className='InitializeApp__label'>
                        {text}
                        <BlockModify cssTransform='modify'>{text}</BlockModify>
                    </div>
                </div>
            </CSSTransition>
        </div>
    );
};

export default InitApp;