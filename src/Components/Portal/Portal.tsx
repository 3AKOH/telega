import React, {useContext, useEffect, useState} from 'react';
import Backdrop from "../Backdrop/Backdrop";
import CSSTransition from "react-transition-group/CSSTransition";
import {AppContext} from "../../Context/App/AppState";
import {appInitAC} from "../../Context/App/AppAC";

interface IProps {
    isOpen: boolean
    initStatus: boolean      //изначальное состояние
    speedRamki: number
    speedInner: number
    children: any
}

interface IState {
    initBlock: boolean      //показан ли этот Root Блок
    initInner: boolean      //инициализировать скрытие?
    speedBlock: number      //скорость скрывания блока (дживжение "рамок")
    speedInner: number      //скорость исчезновение блока перед закрытием Root
}

let State: IState = {
    initBlock: false,
    initInner: false,
    speedBlock: 1000,
    speedInner: 300,
};
const Portal: React.FC<IProps> = ({children, isOpen, speedRamki, speedInner, initStatus}) => {
    const [state, setState] = useState<IState>({...State,speedBlock:speedRamki,speedInner:speedInner,initBlock: initStatus, initInner: initStatus});
    const speedTransition = (speedRamki / 1000) + 's';
    const speedTransitionInner = (speedInner / 1000) + 's';
    useEffect(() => {
        if (isOpen) {
            onOpen()
        } else {
            onClose()
        }
    }, [isOpen]);
    const onOpen = () => {
        setTimeout(() => {
            setState((state: IState) => ({...state, initInner: true}))
        }, state.speedBlock - state.speedBlock * .5);
        setState((state: IState) => ({...state, initBlock: true}))
    };
    const onClose = () => {
        setTimeout(() => {
            setState((preState: IState) => ({...preState, initBlock: false}))
        }, state.speedInner - state.speedInner * .5);
        setState((state: IState) => ({...state, initInner: false}))
    };
    return (
        <CSSTransition timeout={state.speedBlock} in={state.initBlock} classNames='Portal' unmountOnExit mountOnEnter>
            <div className='Portal'>
                <CSSTransition style={{transition: speedTransitionInner}} timeout={state.speedInner} in={state.initInner} classNames='scale' unmountOnExit mountOnEnter>
                    {children(state.initBlock)}
                </CSSTransition>
                <div className='Portal__item -left' style={{transition: speedTransition}}/>
                <div className='Portal__item -right' style={{transition: speedTransition}}/>
                <div className='Portal__item -left -two' style={{transition: speedTransition}}/>
                <div className='Portal__item -right -two' style={{transition: speedTransition}}/>
                <Backdrop isOpen={state.initBlock} timeout={state.speedInner} classNames='show' style={{transition: speedTransitionInner}}/>
            </div>
        </CSSTransition>
    );
};

export default Portal;