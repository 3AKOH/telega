import React from 'react';
import './TopBar.scss';

interface IProps {
    className?:string
    style?: any
    dopLineBot?:boolean
    dopLineTop?:boolean
}

const TopBar:React.FC<IProps> = ({children,style,className,dopLineBot,dopLineTop}) => {
    let cls = ['TopBar'];
    if(className) cls.push(className);
    if(dopLineTop) cls.push('-dopLineTop');
    if(dopLineBot) cls.push('-dopLineBot');
    return (
        React.createElement('div',{
            style,className:cls.join(' ')
        },children)
    );
};

export default TopBar;