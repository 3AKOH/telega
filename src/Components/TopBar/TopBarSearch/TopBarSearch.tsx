import React, {createRef, SyntheticEvent, useEffect, useState} from 'react';
import TopBar from "../TopBar";
import ButtonX from "../../UI/Button/ButtonX/ButtonX";
import IconX from "../../UI/Icon/IconX";
import CSSTransition from "react-transition-group/CSSTransition";
import ClassName from 'classnames';

interface IProps {
    toogleDrawer: any
    searchlabel: string
    searchActive: boolean
    changeLeftType: any
    isMini: boolean
}

const TopBarSearch:React.FC<IProps> = ({changeLeftType,toogleDrawer,searchActive,searchlabel='',isMini}) => {
    const [ref, setRef] = useState<any>(createRef());
    const onChange = (e:any) =>{
        if(e.target.value !== ''){
            changeLeftType('search',e.target.value)
        }else{
            changeLeftType('')
        }
    };
    useEffect(()=>{
        if(searchActive) ref.current.focus();
    },[searchActive]);
    return (
        <TopBar className='TopBarSearch'>
            <div className={ClassName('TopBarSearch__left',{'-mini':isMini})}>
                <ButtonX onClick={toogleDrawer} type='icon'><IconX name='icon_menu'/></ButtonX>
            </div>
            {
                !isMini && (
                    <div className='TopBarSearch__search'>
                        <div>
                            <input type='text' placeholder='Найти...' onChange={onChange} value={searchlabel} ref={ref}/>
                            <CSSTransition timeout={200} in={searchActive && searchlabel!==''} classNames='rotate' mountOnEnter unmountOnExit>
                                <div className='-close' onClick={()=>changeLeftType('')}><IconX name='icon_close' size='small'/></div>
                            </CSSTransition>
                        </div>
                    </div>
                )
            }
        </TopBar>
    );
};

export default TopBarSearch;