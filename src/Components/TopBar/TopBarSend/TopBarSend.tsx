import React, {useState} from 'react';
import TopBar from "../TopBar";
import ButtonX from "../../UI/Button/ButtonX/ButtonX";
import IconX from "../../UI/Icon/IconX";
import ClassName from 'classnames';
import TextareaAutosize from "react-textarea-autosize";
import BoxScroll from "../../BoxScroll/BoxScroll/BoxScroll";

interface IProps {
}

const State = {
    activeBtn: false,
    textHeightInit: 46,
    textHeight: 46
};

const TopBarSend: React.FC<IProps> = () => {
    const [state, setState] = useState(State);
    const handlechange = (e: any) => {
        if (e.target.value) {
            setState({...state, activeBtn: true})
        } else {
            setState({...state, activeBtn: false})
        }
    };
    const changeTextarea = (height: any) => {
        setState({...state, textHeight: height})
    };
    return (
        <TopBar className='TopBarSend' dopLineTop={true} style={{height: state.textHeight}}>
            <div className='TopBarSend__file' style={{height: state.textHeightInit}}>
                <ButtonX type='icon'><IconX name='icon_clip'/></ButtonX>
            </div>
            <div className='TopBarSend__input'>
                <BoxScroll>
                    <TextareaAutosize
                        minRows={1}
                        maxRows={6}
                        placeholder='Написать сообщение...'
                        onHeightChange={changeTextarea}
                        onChange={handlechange}
                    />
                </BoxScroll>
            </div>
            <div className='TopBarSend__button' style={{height: state.textHeightInit}}>
                {/*<ButtonX type='icon'><IconX name='icon_options'/></ButtonX>*/}
                <ButtonX type='icon'><IconX name='icon_smile'/></ButtonX>
                <ButtonX type='icon' className={ClassName({'-blue': state.activeBtn})}><IconX name='icon_send'/></ButtonX>
            </div>
        </TopBar>
    );
};

export default TopBarSend;