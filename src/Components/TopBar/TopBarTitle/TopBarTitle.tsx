import React, {useState} from 'react';
import TopBar from "../TopBar";
import ButtonX from "../../UI/Button/ButtonX/ButtonX";
import IconX from "../../UI/Icon/IconX";
import MiniMenuItem from "../../MiniMenu/MiniMenuItem/MiniMenuItem";
import MiniMenu from "../../MiniMenu/MiniMenu";
import ClassName from 'classnames';
import ModalLink from "../../../Modals/ModalLink/ModalLink";
import UserInfo_Modal from "../../../Modals/UserInfo_Modal/UserInfo_Modal";
import ButtonMenuWrap, {IButtonMenuWrap} from "../../UI/Button/ButtonMenu/ButtonMenu";
import MiniMenuItemX from "../../MiniMenu/MiniMenuItemX/MiniMenuItemX";

interface IProps {
    rightToggle: any
    rightBlock: boolean
    changeLefttype: any
}
const TopBarTitle:React.FC<IProps> = ({rightToggle,rightBlock,changeLefttype}) => {
    const hadleOpenChats = () => {}
    return (
        <TopBar className='TopBarTitle' dopLineBot={true}>
            <div className='TopBarTitle__back'>
                <ButtonX type='icon' onClick={hadleOpenChats}><IconX name='icon_back'/></ButtonX>
            </div>
            <ModalLink link={(d:any)=><UserInfo_Modal {...d}/>} className='TopBarTitle__name'>
                <div className='TopBarTitle__name-wrap'>
                    <div className='-name'>Руслан Рожков</div>
                    <div className='-date'>был(а) 3 часа назад</div>
                </div>
            </ModalLink>
            <div className='TopBarTitle__icons'>
                <div className='-item'>
                    <ButtonX type='icon' onClick={()=>changeLefttype('searchMessage')}><IconX name='icon_search'/></ButtonX>
                </div>
                <div className='-item -bar'>
                    <ButtonX className={ClassName({'-blue':rightBlock})} type='icon' onClick={rightToggle}><IconX name='icon_ramka'/></ButtonX>
                </div>
                <div className='-item'>

                </div>
            </div>
        </TopBar>
    );
};

export default TopBarTitle;