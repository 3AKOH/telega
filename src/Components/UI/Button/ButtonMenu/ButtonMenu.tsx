import React, {useState} from 'react';
import '../Button.scss'

export interface IButtonMenuWrap {
    isOpen: boolean
    onToggle: any
}
interface IProps {
    menu?: any
    children?: any
    [idx:string]:any
}
const InitialState = {
    miniMenu: false,
};
const ButtonMenuWrap: React.FC<IProps> = (props) => {
    const {children,menu} = props;
    const [state, setState] = useState(InitialState);
    const handleToggleMinimenu = () => {
        setState({...state, miniMenu: !state.miniMenu});
    }
    const data = {
        isOpen:state.miniMenu,
        onToggle:handleToggleMinimenu
    }
    return (
        <div>
            {children(data)}
            { menu && menu(data)}
        </div>
    )
};

export default ButtonMenuWrap;