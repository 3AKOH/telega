import React from 'react';
import '../Button.scss'
import Radium from 'radium';
import ClassNames from "classnames";
import {IThemes, ThemesDefault} from "../../../../Context/App/state/themes";
import {createTheme} from "../../../Util";
import {THEME_RIPPLES} from "../../../../Constants";
import RippleX from "../../RippleX/RippleX";

interface IProps {
    type?: "main" | "nav" | "link" | "icon"| "icon-hover"| "icon-dark"
    theme?: IThemes
    block?: boolean
    onClick?: any
    className?: string
    component?: "button" | "label"
    style?: any
}
const ButtonX: React.FC<IProps> = ({type="main",children, onClick, theme=ThemesDefault, className,block,component='button',style}) => {
    const Theme = createTheme(theme);
    let styles = {
        ripple: {
            borderRadius: 'radius',
        },
        button: {
            background: 'bg',
            color: 'color',
            ':hover': {
                background: 'bgHover',
                color: 'colorHover',
            }
        },
        icon:{
            fill:'fill',
            ':hover': {
                fill:'fillHover',
            }
        },
        className: 'className'
    };
    let newStyle = Theme.add(styles,['button',type]);
    // if(newStyle) Object.assign(newStyle.ripple,style);
    const dopClass = newStyle && newStyle.className;
    const st = newStyle && newStyle.button || {};
    return (
        <RippleX color={Theme.get(['button',type,'rapper']) || THEME_RIPPLES} className={ClassNames(
            'ButtonX',
            'ButtonX-'+type,
            {['ButtonX-block']:block},
            {['-'+dopClass]:dopClass},
            {[`ButtonX-${type}-ripple`]:!newStyle},
            {[`ButtonX-${type}-style`]:!newStyle},
            className
        )} style={Object.assign({},newStyle && newStyle.ripple,style)} >
            {
                React.createElement(
                    component,
                    {
                        onClick,
                        className:'-button',
                        style:{st}
                        },
                    <span>{children}</span>
                )
            }
        </RippleX>
    )
};

export default ButtonX;