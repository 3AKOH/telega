import React from 'react';
import '../CheckboxX.scss';
import ClassName from 'classnames';

interface IProps {
    error?: boolean
    className?: string
    type?: "checkbox" | "radio"
    // type?: "input" | "select" | "textarea"
    checked?: boolean
    onChange?:any
    [idx:string]:any
}

const CheckboxXX:React.FC<IProps> = ({error=false,className,type='checkbox',checked=false,onChange}) => {
    return (
        <label className={ClassName('CheckboxX',className,'-'+type,{"-checked":checked})} >
            <input
                type={type}
                onChange={(e:any)=>onChange(e.target.checked)}
                checked={checked}
            />
        </label>
    );
};

// const CheckboxX:React.FC<IProps> = (props) => {
//     const {className,input:{type='',checked,onChange}} = props;
//     return (
//         <label className={ClassName('CheckboxX',className,'-'+'checkbox',{"-checked":checked})} >
//             <input
//                 type={type}
//                 onChange={(e:any)=>onChange(e.target.checked)}
//                 checked={checked}
//             />
//         </label>
//     );
// };

export default CheckboxXX;