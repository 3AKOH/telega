import React from 'react';
import SVG from "../../../assets/svg-sprite/sprite.svg";

interface IProps {
    name: string
    className?: string
    size?: "large" | "small"
    classOn?: boolean
    style?: {[index:string]:any}
}
const IconX:React.FC<IProps> = ({name, className,style, classOn,size}) => {
    let cls = ['Icon'];
    if(size) cls.push('-'+size);
    if(classOn) cls.push(`svg-${name}-dims`);
    if(className) cls.push(className);
    // shape-rendering="crispEdges"
    return (
        <svg className={cls.join(' ')} {...style} viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false" xmlns={`${SVG}#${name}`} xmlnsXlink={`${SVG}#${name}`}>
            <use xlinkHref={`${SVG}#${name}`}/>
        </svg>
    );
};

export default IconX;