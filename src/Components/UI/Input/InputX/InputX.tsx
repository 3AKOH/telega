import React from 'react';
import ClassName from 'classnames';
import InputXX from "../InputXX/InputXX";

interface IProps {
    component?: 'input'|'textarea'
    name?: string
    placeholder?: string
    defaultValue?: string
    value?: string
    error?: boolean
    center?: boolean
    label?: string
    className?: string
    autocomplete?: 'off' | 'on'
    type?: '' | 'circle'
    [idx: string]: any
}
 
const InputX:React.FC<IProps> = (props) => {
    const {component='text',type='',className,name,placeholder,defaultValue,value,error,onChange,autocomplete='off'} = props;
    const cls = type ? '-'+type : '';
    return (
        <>
            {
                React.createElement(
                    InputXX,
                    {
                        input:{name,placeholder,defaultValue,value,onChange,autocomplete,component},
                        meta:{error},...props,
                        className:ClassName(cls,className)
                    }
                )
            }
        </>
    );
};

export default InputX;