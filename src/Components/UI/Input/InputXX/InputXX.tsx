import React, {createRef, useEffect, useState} from 'react';
import ClassName from 'classnames';
import InputX from "../InputX/InputX";
import '../InputX.scss';
import TextareaAutosize from "react-textarea-autosize";

interface IProps {
    [idx: string]: any
}

const InputXX: React.FC<IProps> = (props) => {
    const [ref, setRef] = useState<any>(createRef());
    const {label,className,center,meta:{error},onChange,focus=false,disabled=false} = props;
    useEffect(()=>{
        if(focus) ref.current.focus();
    },[]);
    const element = () =>{
        return props.input.component=='textarea' ? ()=><TextareaAutosize
            minRows={1}
            maxRows={6}
            {...props.input}
        /> : 'input';
    }
    return (
        <>
            <div className={ClassName('InputX', className, {'InputX__center': center},{'-error': error})}>
                {
                    React.createElement(
                        element(),
                        {onChange,...props.input,ref,disabled}
                    )
                }
                <div className={ClassName('InputX__line', {'InputX__error': error})}><span/></div>
                <div className='InputX__default'><span/></div>
                {label && <div className='InputX__label'><span>{label}</span></div>}
            </div>
        </>
    );
};

export default InputXX;