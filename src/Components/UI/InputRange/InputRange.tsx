import React from 'react';
import './InputRange.scss';

interface IProps {
    min?: number
    max?: number
    value?: number
    onChange: any
}

const InputRange:React.FC<IProps> = (props) => {
    const {min=0,max=100,value=0,onChange} = props;
    return (
        <div className='InputRange'>
            <div className='InputRange__bg'/>
            <div className='InputRange__play' style={{width:(value*100)/max+'%'}}/>
            <input type='range' min={min} max={max} value={value} onChange={onChange}/>
        </div>
    );
};

export default InputRange;