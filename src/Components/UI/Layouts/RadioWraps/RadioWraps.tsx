import React from 'react';
import './RadioWraps.scss'


interface IProps {}

const RadioWraps:React.FC<IProps> = ({children}) => {
    return (
        <div className='RadioWraps'>
            {children}
        </div>
    );
};

export default RadioWraps;