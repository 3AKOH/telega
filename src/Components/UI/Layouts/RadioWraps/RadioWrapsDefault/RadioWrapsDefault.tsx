import React from 'react';
import RadioWraps from "../RadioWraps";
import './RadioWrapsDefault.scss';
import CheckboxX from "../../../Checkbox/CheckboxX/CheckboxX";

interface IProps {
    checked?: boolean
    label?: string
}

const RadioWrapsDefault:React.FC<IProps> = ({label='',checked=false}) => {
    const handleChange = () => {}
    return (
        <RadioWraps>
            <label className='RadioWrapsDefault'>
                <div className='RadioWrapsDefault__l'>
                    <CheckboxX type='checkbox' defaultChecked={checked} onChange={handleChange} checked={true}/>
                    {/*<Field type='checkbox' defaultValue={checked}  name={}/>*/}
                    {/*<input type='checkbox' defaultChecked={checked}/>*/}
                </div>
                <div className='RadioWrapsDefault__r'>
                    {label}
                </div>
            </label>
        </RadioWraps>
    );
};

export default RadioWrapsDefault;