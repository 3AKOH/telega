import React, {createElement} from 'react';
import Radium from 'radium';
import ClassName from 'classnames';
// @ts-ignore
import Ripples from 'react-touch-ripple';
import {THEME_RIPPLES} from "../../../Constants";
import './RippleX.scss';

interface IProps {
    [inx: string]: any
}

const RippleX: React.FC<IProps> = (props) => {
    const {children,className,color} = props;
    return (
        <Ripples {...props} color={color || THEME_RIPPLES} className={ClassName('RippleX',className)}>
            {children}
        </Ripples>
    )
};


export default Radium(RippleX);