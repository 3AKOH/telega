import {ITheme, IThemes} from "../Context/App/state/themes";

export const createTheme = (theme:IThemes) => {
    return {
        get:function(path:string[]){
            return getThemeValue(theme, path);
        },
        add:function(style: { [index: string]: any },path:string[]){
            const palitra = getThemeValue(theme, path);
            if (!palitra) return null;
            return addTheme(style,palitra)
        },
    }
};
//применяем палитру к STYLE
const addTheme = (style: { [index: string]: any }, palitra: any) => {
    for (let elem in style) {
        if(typeof style[elem] == 'object'){
            style[elem] = addTheme(style[elem],palitra)
        }else {
            if (palitra[style[elem]]) {
                style[elem] = palitra[style[elem]]
            }
        }
    }
    return style
};
//поиск поля по path
const getThemeValue = (theme:IThemes, path:string[]) :ITheme|null => {
    if (!theme.list[theme.curTheme]) return null;
    return getFieldFromTheme(theme.list[theme.curTheme],path.reverse());
};
//рекурсивный поиск поля
const getFieldFromTheme = (obj:ITheme, path:string[]) :any =>{
    const name = path.splice(path.length-1,1);
    if(!obj[name[0]]) return null;
    if(path.length) return getFieldFromTheme(obj[name[0]],path);
    return obj[name[0]];
};