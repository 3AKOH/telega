

export const APP_FIRST_RUN_TIME = 20000;        //врямя чтобы понять что соединение по websocket не возможно
export const CHATS_LIMIT = 25;        //время чтобы понять что соединение изменилось
export const CHATS_MESSAGE_LIMIT = 30;        //время чтобы понять что соединение изменилось


export const MESSAGE_STICKER_WIDTH = 190;        //время чтобы понять что соединение изменилось

export const CONNECTION_CHANGE_TIME = 2000;        //время чтобы понять что соединение изменилось

export const THEME_RIPPLES = 'rgba(112,117,121,.24)';        //цвет вол по умолчанию
// export const THEME_RIPPLES = '#168acd';        //цвет вол по умолчанию

//SYNC_PROMISE - возможность дожидаться нескольких ответов
export const SYNC_PROMISE_DELAY = 10000;                 //задержка перед отменой запроса
export const SYNC_PROMISE_STORE_ELEM_MAX = 300;          //сколько максимально храним ответов в истории
export const SYNC_PROMISE_STORE_ELEM_SAVE = 150;         //сколько ответов оставить после удаления