import React, {useContext} from 'react';
import TopBarSearch from "../../Components/TopBar/TopBarSearch/TopBarSearch";
import {AppContext} from "../../Context/App/AppState";
import {changeLeftType, toggleDrawer} from "../../Context/App/AppAC";
import TopBarTitle from "../../Components/TopBar/TopBarTitle/TopBarTitle";
import Test6 from "../../pages/Test6/Test6";
import TopBarSend from "../../Components/TopBar/TopBarSend/TopBarSend";
import BoxScroll from "../../Components/BoxScroll/BoxScroll/BoxScroll";
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import IconX from "../../Components/UI/Icon/IconX";
import NavBar from "../../Components/NavBar/NavBar";
import ButtonMenuWrap, {IButtonMenuWrap} from "../../Components/UI/Button/ButtonMenu/ButtonMenu";
import MiniMenu from "../../Components/MiniMenu/MiniMenu";
import MiniMenuItemX from "../../Components/MiniMenu/MiniMenuItemX/MiniMenuItemX";
import ClassName from "classnames";
import ModalLink from "../../Modals/ModalLink/ModalLink";
import UserInfo_Modal from "../../Modals/UserInfo_Modal/UserInfo_Modal";
import TopBar from "../../Components/TopBar/TopBar";
import AvatarX from "../../Components/Avatar/AvatarX";
import Messages from "../../Components/Messages/Messages";

const bgIMG = '1.jpg';

interface IProps {
    changeRight:any
    isRight:boolean
}

const CenterContainer: React.FC<IProps> = ({changeRight,isRight}) => {
    const {appState, dispatch} = useContext(AppContext);
    const handleDrawer = () => {
        dispatch(toggleDrawer())
    };
    const handleChangeLeftType = (type:string, s:string) => {
        dispatch(changeLeftType(type,s))
    };
    return (
        <>
            <div className='ContentBody'>
                <div className='ContentBody__top'>
                    <NavBar dopLineBot={true}>
                        <div className='NavBar__left -overflow'>
                            <div className='NavBar__back'>
                                <ButtonX type='icon'><IconX name='icon_back'/></ButtonX>
                            </div>
                            <ModalLink link={(d:any)=><UserInfo_Modal {...d}/>} className='NavBar__profile'>
                                <div className='-ava'>
                                    <AvatarX url={null} txt={'Руслан Рожков'} type={44}/>
                                </div>
                                <div className='-info'>
                                    <div className='-name'>Руслан Рожков</div>
                                    <div className='-status'>был(а) 3 часа назад</div>
                                </div>
                            </ModalLink>
                        </div>
                        <div className='NavBar__right'>
                            <ButtonX type='icon-hover'><IconX name='icon_search'/></ButtonX>
                            <ButtonX className={ClassName({'-blue':isRight})} type='icon' onClick={changeRight}><IconX name='icon_ramka'/></ButtonX>
                            <div className='NavBar__iconWrap'>
                                <ButtonMenuWrap menu={({isOpen,onToggle}:IButtonMenuWrap)=>(
                                    <MiniMenu isOpen={isOpen} onClose={onToggle} type='botRight'>
                                        <MiniMenuItemX>
                                            <div className='MiniMenu__icon'>
                                                <IconX name='icon_unmute'/>
                                            </div>
                                            <div className='MiniMenu__label'>Unmute</div>
                                        </MiniMenuItemX>
                                        <MiniMenuItemX status='red'>
                                            <div className='MiniMenu__icon'>
                                                <IconX name='icon_delete'/>
                                            </div>
                                            <div className='MiniMenu__label'>Delete and Leave</div>
                                        </MiniMenuItemX>
                                    </MiniMenu>
                                )}>{
                                    ({isOpen,onToggle}:IButtonMenuWrap)=> <ButtonX type='icon-hover' className={ClassName({'-active':isOpen})} onClick={onToggle}><IconX name='icon_more'/></ButtonX>
                                }</ButtonMenuWrap>
                            </div>
                        </div>
                    </NavBar>
                </div>
                {/*<div className='ContentBody__body' style={{backgroundImage: `url(/img/bg/${bgIMG})`}}>*/}
                <div className='ContentBody__body' style={{background: appState.app.bgColor}}>
                    <Messages>
                        <Test6/>
                    </Messages>
                </div>
                <div className='ContentBody__bot'>
                    <TopBarSend/>
                </div>
            </div>
        </>
    );
};

export default CenterContainer;