import React, {useEffect, useState} from 'react';
import NavBar from "../../../Components/NavBar/NavBar";
import ButtonX from "../../../Components/UI/Button/ButtonX/ButtonX";
import IconX from "../../../Components/UI/Icon/IconX";
import BoxScroll from "../../../Components/BoxScroll/BoxScroll/BoxScroll";
import AvatarInfo from "../../../Components/Avatar/AvatarInfo/AvatarInfo";
import InfoLinkX from "../../../Components/InfoWrap/InfoLinkX/InfoLinkX";
import RippleX from "../../../Components/UI/RippleX/RippleX";
import MediaBlock from "../../../Components/MediaBlock/MediaBlock";
import AvatarChange from "../../../Components/Avatar/AvatarChange/AvatarChange";
import InputX from "../../../Components/UI/Input/InputX/InputX";
import MediaMembersItem from "../../../Components/MediaBlock/MediaMembers/MediaMembersItem";
import InfoInputXWrap from "../../../Components/InfoWrap/InfoInputX/InfoInputXWrap";
import CircleFlay from "../../../Components/Circle/CircleFlay/CircleFlay";
import ClassName from "classnames";
import MiniMenu from "../../../Components/MiniMenu/MiniMenu";
import MiniMenuItem from "../../../Components/MiniMenu/MiniMenuItem/MiniMenuItem";
import BlockModify from "../../../Components/BlockModify/BlockModify";
import CSSTransition from "react-transition-group/CSSTransition";
import MiniMenuItemX from "../../../Components/MiniMenu/MiniMenuItemX/MiniMenuItemX";
import TESTS from "../../../assets/img/test.jpg";
import EMPTY from "../../../assets/img/empty.png";
import Backgrounds from "../../../Components/Backgrounds/Backgrounds";

interface IProps {
}

const InitialState = {
    miniMenu: false,
    showCircle: false
};
const dataColors = ['red','#dd223d','#e23','#7a9','#999','#1f8','#f5f','#d2d','#e23','#729','#999','#1f8','blue','green','yellow','#199','#5f3','#af4','#d23','#a23','#734','#977','#197'];

const Background_LeftContainer: React.FC<IProps> = (props) => {
    const [state, setState] = useState(InitialState);
    const handleToggleMinimenu = () => {
        setState({...state, miniMenu: !state.miniMenu});
    }
    useEffect(() => {
        setTimeout(() => {
            setState({...state, showCircle: true});
        }, 2000)
    }, [])
    const newColor = (color:string) => {
        console.log(color);
    }
    return (
        <>
            <NavBar dopLineBot={true}>
                <div className='NavBar__left'>
                    <ButtonX type='icon-hover'><IconX name='icon_back'/></ButtonX>
                    <div className='NavBar__title'>Chat Background</div>
                </div>
            </NavBar>
            <BoxScroll className='Modal__content' topLineOff={true} botLineOff={true}>
                <Backgrounds curColor='#ddd' dataColors={dataColors} newColor={newColor}/>
            </BoxScroll>
        </>
    );
};

export default Background_LeftContainer;