import React, {useEffect, useState} from 'react';
import RippleX from "../../../Components/UI/RippleX/RippleX";
import Avatar from "../../../Components/Avatar/Avatar";
import {genColor} from "../../../Util/Common";
import ClassName from 'classnames';
import ChatsStore, {IChatsStoreItem} from "../../../Store/ChatsStore";
import UsersStore from "../../../Store/UsersStore";
import FilesStore from "../../../Store/FilesStore";
import SyncStore from "../../../Util/SyncPromise/SyncStore";
import SyncPromise from "../../../Util/SyncPromise/SyncPromise";

interface IProps {
    chatId: number

    [idx: string]: any
}

export interface IState {
    id: number
    chat: IChatsStoreItem
    img: any
}

const ChatsItem: React.FC<IProps> = (props) => {
    const {chatId} = props;
    const [state, setState] = useState<IState>(()=>{
        const chat = ChatsStore.get(chatId);
        return {
            id: chatId,
            chat,
            img: {
                id: chat.photo ? chat.photo.small.id : 0,
                url: chat.photo ? FilesStore.getUrl(chat.photo.small.id) : null
            }
        }
    });
    const LOADED_FILE = (id:number) =>{
        if(!state.img.id || id !== state.img.id) return;
        setState({
            ...state,
            img:{
                id,
                url: FilesStore.getUrl(id)
            }
        })
    };
    useEffect(() => {
        FilesStore.on('LOADED_FILE',LOADED_FILE);
        return ()=>{
            FilesStore.off('LOADED_FILE',LOADED_FILE)
        }
    }, []);
    const renderChat = () => {
        // let data;
        // switch (state.chat.type['@type']) {
        //     case 'chatTypePrivate':
        //         data = UsersStore.get(state.id);
        //         break;
        // }
        // console.log(state.id, state.chat,data);
        return (
            <>
                <RippleX className={ClassName('ChatsList__item', {'-active': false})} {...props}>
                    <div className='ChatsList__img'>
                        {/*<Avatar url={data.ava} txt={data.name[0]} bg={genColor(data.name[0])}/>*/}
                        <Avatar url={state.img.url} txt={state.chat.title[0]} bg={genColor(state.chat.title[0])}/>
                    </div>
                    <div className='ChatsList__info'>
                        <div className='ChatsList__name'>
                            <div className='-name'>{state.chat.title}</div>
                            <div className='-date'>{"xz"}</div>
                        </div>
                        <div className='ChatsList__msg'>
                            <div className='-text'><span>{state.chat.id}</span></div>
                            <div className='-count'>
                                {/*{*/}
                                {/*    data.countMessage !== 0 && <span>{data.countMessage}</span>*/}
                                {/*}*/}
                            </div>
                        </div>
                    </div>
                </RippleX>
            </>
        )
    }
    return renderChat();
};

export default ChatsItem;