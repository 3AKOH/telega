import React, {useEffect, useState} from 'react';
import ChatsItem from "../ChatsItem/ChatsItem";
import ChatsStore from "../../../Store/ChatsStore";

interface IProps {
}

interface IState {
    loading: boolean
    // chats: { [idx: string]: any }
    chats: [number?]
}

const initState: IState = {
    loading: true,
    chats: []
}

const ChatsList: React.FC<IProps> = () => {
    const [state, setState] = useState(initState);
    useEffect(() => {
        // ChatsStore.on("chats",CHATS)
        setState(prev => {
            return {
                ...state,
                loading: false,
                chats: ChatsStore.getChats()
            }
        })
    }, []);
    return (
        <div className='ChatsList'>
            {state.loading
                ? 'Loading...'
                : (
                    state.chats.map(chatId => chatId && <ChatsItem chatId={chatId} key={chatId}/>)
                )
            }
        </div>
    );
};

export default ChatsList;