import React from 'react';
import ChatsItem from "../ChatsItem/ChatsItem";
import ButtonX from "../../../Components/UI/Button/ButtonX/ButtonX";
import InputX from "../../../Components/UI/Input/InputX/InputX";
import IconX from "../../../Components/UI/Icon/IconX";
import Avatar from "../../../Components/Avatar/Avatar";

interface IProps {
    changeLeftType: any
}

const ChatsSearchMessageList: React.FC<IProps> = ({changeLeftType}) => {
    return (
        <>
            <div className='ChatsList__pre'>Поиск в чате:</div>
            <div className='ChatsList__InfoItem'>
                <div className='-l'>
                    {/*<Avatar url={contacts[1].ava} size='medium'/>*/}
                </div>
                <div className='-c'>
                    Stas Fantic Stas Fantic Stas Fantic
                </div>
                <div className='-r'>
                    <ButtonX type='icon' onClick={()=>changeLeftType('')}><IconX name='icon_close' size='small'/></ButtonX>
                </div>
            </div>

            <div className='ChatsList__pre'>Поиск по сообщениям:</div>
            <div className='ChatsList'>
                {/*{contacts.map((item, index) => {*/}
                {/*    let active = false;*/}
                {/*    if (index === 1) active = true;*/}
                {/*    return <ChatsItem chatId={index} key={item.id}/>*/}
                {/*})}*/}
            </div>
        </>
    );
};

export default ChatsSearchMessageList;