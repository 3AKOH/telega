import React, {useEffect, useState} from 'react';
import NavBar from "../../../Components/NavBar/NavBar";
import ButtonX from "../../../Components/UI/Button/ButtonX/ButtonX";
import IconX from "../../../Components/UI/Icon/IconX";
import BoxScroll from "../../../Components/BoxScroll/BoxScroll/BoxScroll";
import AvatarInfo from "../../../Components/Avatar/AvatarInfo/AvatarInfo";
import InfoLinkX from "../../../Components/InfoWrap/InfoLinkX/InfoLinkX";
import RippleX from "../../../Components/UI/RippleX/RippleX";
import MediaBlock from "../../../Components/MediaBlock/MediaBlock";
import AvatarChange from "../../../Components/Avatar/AvatarChange/AvatarChange";
import InputX from "../../../Components/UI/Input/InputX/InputX";
import MediaMembersItem from "../../../Components/MediaBlock/MediaMembers/MediaMembersItem";
import InfoInputXWrap from "../../../Components/InfoWrap/InfoInputX/InfoInputXWrap";
import CircleFlay from "../../../Components/Circle/CircleFlay/CircleFlay";
import ClassName from "classnames";
import MiniMenu from "../../../Components/MiniMenu/MiniMenu";
import MiniMenuItem from "../../../Components/MiniMenu/MiniMenuItem/MiniMenuItem";
import BlockModify from "../../../Components/BlockModify/BlockModify";
import CSSTransition from "react-transition-group/CSSTransition";
import MiniMenuItemX from "../../../Components/MiniMenu/MiniMenuItemX/MiniMenuItemX";

interface IProps {
}

const InitialState = {
    searchActive: false,
    searchText: '',
    miniMenu: false,
    showCircle: false
};

const Contacts_LeftContainer: React.FC<IProps> = (props) => {
    const [state, setState] = useState(InitialState);
    const handleToggleMinimenu = () => {
        setState({...state, miniMenu: !state.miniMenu});
    }
    useEffect(() => {
        setTimeout(() => {
            setState({...state, showCircle: true});
        }, 2000)
    }, [])
    const changeSearchText = (str:string) =>{
        setState({...state, searchText: str});
    };
    return (
        <>
            <NavBar>
                <div className='NavBar__left'>
                    <ButtonX type='icon-hover'><IconX name='icon_back'/></ButtonX>
                    <div className='NavBar__search'>
                        <label className='-wrap'>
                            <div className='-icon'><IconX name='icon_search'/></div>
                            <input type='text' placeholder='Search...' onChange={(e)=>changeSearchText(e.target.value)} value={state.searchText}/>
                            <CSSTransition timeout={200} in={state.searchText!==''} classNames='rotate' mountOnEnter unmountOnExit>
                                <div className='-close' onClick={()=>changeSearchText('')}><IconX name='icon_close' size='small'/></div>
                            </CSSTransition>
                        </label>
                    </div>
                </div>
            </NavBar>
            <BoxScroll className='Modal__content'  botLineOff={true}>
                <MediaMembersItem url={null} name={'Karen Stanford Karen Stanford Karen Stanford Karen Stanford'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Artur Pirozhkov'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Artur Pirozhkov'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
            </BoxScroll>
        </>
    );
};

export default Contacts_LeftContainer;