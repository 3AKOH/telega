import React, {useContext, useState} from 'react';
import TopBarSearch from "../../Components/TopBar/TopBarSearch/TopBarSearch";
import {AppContext} from "../../Context/App/AppState";
import {changeLeftType, toggleDrawer} from "../../Context/App/AppAC";
import ChatsList from "./ChatsList/ChatsList";
import ChatsSearchList from "./ChatsSearchList/ChatsSearchList";
import ChatsSearchMessageList from "./ChatsSearchMessageList/ChatsSearchList";
import AutoSizer from "react-virtualized-auto-sizer";
import {FixedSizeList, VariableSizeList} from "react-window";
import ChatsItem from "./ChatsItem/ChatsItem";
import BoxScroll from "../../Components/BoxScroll/BoxScroll/BoxScroll";
import NewGroup_LeftContainer from "./NewGroup_LeftContainer/NewGroup_LeftContainer";
import IconX from "../../Components/UI/Icon/IconX";
import CircleFlay from "../../Components/Circle/CircleFlay/CircleFlay";
import Main_LeftContainer from "./Main_LeftContainer/Main_LeftContainer";
import NewChannel_LeftContainer from "./NewChannel_LeftContainer/NewChannel_LeftContainer";
import Contacts_LeftContainer from "./Contacts_LeftContainer/Contacts_LeftContainer";
import Backdrop from "../../Components/Backdrop/Backdrop";
import Background_LeftContainer from "./Background_LeftContainer/Background_LeftContainer";
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import ClassName from "classnames";
import MiniMenu from "../../Components/MiniMenu/MiniMenu";
import MiniMenuItemX from "../../Components/MiniMenu/MiniMenuItemX/MiniMenuItemX";
import CSSTransition from "react-transition-group/CSSTransition";
import NavBar from "../../Components/NavBar/NavBar";
import {Route, Switch} from "react-router";
import PoligonUI from "../../Poligon/PoligonUI/PoligonUI";

interface IProps {
    isMini: boolean
}

const LeftContainer: React.FC<IProps> = ({isMini}) => {
    const [da, setDa] = useState(false);
    const {appState, dispatch} = useContext(AppContext);
    const handleDrawer = () => {
        dispatch(toggleDrawer())
    };
    const handleChangeLeftType = (type: string, s: string) => {
        dispatch(changeLeftType(type, s))
    };
    const blockRender = () => {
        switch (appState.left.type) {
            case "search":
                return <ChatsSearchList/>;
            case "searchMessage":
                return <ChatsSearchMessageList changeLeftType={handleChangeLeftType}/>;
            default:
                return <ChatsList/>
        }
    };
    return (
        <>
            <Switch>
                <Route exact path="/p2" component={()=><Background_LeftContainer/>}/>
                <Route exact path="/p3" component={()=><Contacts_LeftContainer/>}/>
                <Route exact path="/p4" component={()=><NewChannel_LeftContainer/>}/>
                <Route exact path="/p5" component={()=><NewGroup_LeftContainer/>}/>
                <Route exact component={()=><Main_LeftContainer/>}/>
            </Switch>
            {/*<Main_LeftContainer/>*/}
            {/*<Background_LeftContainer/>*/}
            {/*<Contacts_LeftContainer/>*/}
            {/*<NewChannel_LeftContainer/>*/}
            {/*<NewGroup_LeftContainer/>*/}
        </>
    )
    return (
        <>
            {/*<NavBar>*/}
            {/*    <div className='NavBar__left'>*/}
            {/*        <ButtonX type='icon-hover' className={ClassName({'-active':state.miniMenu2})} onClick={handleToggleMinimenu2}><IconX name='icon_menu'/></ButtonX>*/}
            {/*        <MiniMenu isOpen={state.miniMenu2} onClose={handleToggleMinimenu2} type='botLeft'>*/}
            {/*            <MiniMenuItemX>*/}
            {/*                <div className='MiniMenu__icon'>*/}
            {/*                    <IconX name='icon_group'/>*/}
            {/*                </div>*/}
            {/*                <div className='MiniMenu__label'>New Group</div>*/}
            {/*            </MiniMenuItemX>*/}
            {/*            <MiniMenuItemX>*/}
            {/*                <div className='MiniMenu__icon'>*/}
            {/*                    <IconX name='icon_user'/>*/}
            {/*                </div>*/}
            {/*                <div className='MiniMenu__label'>Contacts</div>*/}
            {/*            </MiniMenuItemX>*/}
            {/*            <MiniMenuItemX>*/}
            {/*                <div className='MiniMenu__icon'>*/}
            {/*                    <IconX name='icon_avatar_archivedchats'/>*/}
            {/*                </div>*/}
            {/*                <div className='MiniMenu__label'>Archived</div>*/}
            {/*                <div className='MiniMenu__count'><span>2</span></div>*/}
            {/*            </MiniMenuItemX>*/}
            {/*            <MiniMenuItemX>*/}
            {/*                <div className='MiniMenu__icon'>*/}
            {/*                    <IconX name='icon_savedmessages'/>*/}
            {/*                </div>*/}
            {/*                <div className='MiniMenu__label'>Saved</div>*/}
            {/*            </MiniMenuItemX>*/}
            {/*            <MiniMenuItemX>*/}
            {/*                <div className='MiniMenu__icon'>*/}
            {/*                    <IconX name='icon_settings'/>*/}
            {/*                </div>*/}
            {/*                <div className='MiniMenu__label'>Settings</div>*/}
            {/*            </MiniMenuItemX>*/}
            {/*            <MiniMenuItemX>*/}
            {/*                <div className='MiniMenu__icon'>*/}
            {/*                    <IconX name='icon_help'/>*/}
            {/*                </div>*/}
            {/*                <div className='MiniMenu__label'>Help</div>*/}
            {/*            </MiniMenuItemX>*/}
            {/*        </MiniMenu>*/}
            {/*        <div className='NavBar__search'>*/}
            {/*            <label className='-wrap'>*/}
            {/*                <div className='-icon'><IconX name='icon_search'/></div>*/}
            {/*                <input type='text' placeholder='Search...' onChange={(e)=>changeSearchText(e.target.value)} value={state.searchText}/>*/}
            {/*                <CSSTransition timeout={200} in={state.searchText!==''} classNames='rotate' mountOnEnter unmountOnExit>*/}
            {/*                    <div className='-close' onClick={()=>changeSearchText('')}><IconX name='icon_close' size='small'/></div>*/}
            {/*                </CSSTransition>*/}
            {/*            </label>*/}
            {/*        </div>*/}
            {/*    </div>*/}
            {/*</NavBar>*/}
            <TopBarSearch isMini={isMini} searchlabel={appState.left.search} toogleDrawer={handleDrawer} searchActive={appState.left.type !== '' ? true : false} changeLeftType={handleChangeLeftType}/>
            <BoxScroll className='Layout__list' topLineOff={true}>
                {/*<BoxScroll>*/}
                {blockRender()}
                {/*</BoxScroll>*/}
                {/*<button onClick={()=>setDa(true)}>da</button>*/}
                {/*<div className='HideMe'>*/}
                {/*{*/}
                {/*    da && rowHeights.map((el,inx) => (*/}
                {/*        <div style={{height:el}}>Row {el}={inx}</div>*/}
                {/*    ))*/}
                {/*}*/}
                {/*</div>*/}
                {/*<AutoSizer>*/}
                {/*    {({width, height}) => (*/}
                {/*        <VariableSizeList width={width} height={height} itemCount={rowHeights.length} itemSize={getItemSize}>*/}
                {/*            {Row}*/}
                {/*        </VariableSizeList>*/}
                {/*    )}*/}
                {/*</AutoSizer>*/}
            </BoxScroll>
        </>
    );
};
// const getChatsListSize = (index: number) => 62;
// const getChatsListRow = ({index, style}: any) => {
//     let active = false;
//     if (index === 1) active = true;
//     return (
//         <ChatsItem style={style} data={contacts[index]} key={contacts[index].id} active={active}/>
//     )
// };

const rowHeights = new Array(10000)
    .fill(true)
    .map(() => 25 + Math.round(Math.random() * 50));
const getItemSize = (index: any) => rowHeights[index];

const Row = ({index, style}: any) => (
    <div style={style}>Row {index}</div>
);

export default LeftContainer;