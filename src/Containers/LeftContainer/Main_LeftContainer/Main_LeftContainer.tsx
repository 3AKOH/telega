import React, {useEffect, useState} from 'react';
import NavBar from "../../../Components/NavBar/NavBar";
import ButtonX from "../../../Components/UI/Button/ButtonX/ButtonX";
import IconX from "../../../Components/UI/Icon/IconX";
import BoxScroll from "../../../Components/BoxScroll/BoxScroll/BoxScroll";
import AvatarInfo from "../../../Components/Avatar/AvatarInfo/AvatarInfo";
import InfoLinkX from "../../../Components/InfoWrap/InfoLinkX/InfoLinkX";
import RippleX from "../../../Components/UI/RippleX/RippleX";
import MediaBlock from "../../../Components/MediaBlock/MediaBlock";
import AvatarChange from "../../../Components/Avatar/AvatarChange/AvatarChange";
import InputX from "../../../Components/UI/Input/InputX/InputX";
import MediaMembersItem from "../../../Components/MediaBlock/MediaMembers/MediaMembersItem";
import InfoInputXWrap from "../../../Components/InfoWrap/InfoInputX/InfoInputXWrap";
import CircleFlay from "../../../Components/Circle/CircleFlay/CircleFlay";
import ClassName from "classnames";
import MiniMenu from "../../../Components/MiniMenu/MiniMenu";
import MiniMenuItem from "../../../Components/MiniMenu/MiniMenuItem/MiniMenuItem";
import BlockModify from "../../../Components/BlockModify/BlockModify";
import CSSTransition from "react-transition-group/CSSTransition";
import MiniMenuItemX from "../../../Components/MiniMenu/MiniMenuItemX/MiniMenuItemX";
import CircleFlayWrap, {ICircleFlayWrap} from "../../../Components/Circle/CircleFlay/CircleFlayWrap";
import ButtonMenuWrap, {IButtonMenuWrap} from "../../../Components/UI/Button/ButtonMenu/ButtonMenu";

interface IProps {
}

const InitialState = {
    searchActive: false,
    searchText: '',
};

const Main_LeftContainer: React.FC<IProps> = (props) => {
    const [state, setState] = useState(InitialState);
    const changeSearchText = (str:string) =>{
        setState({...state, searchText: str});
    };
    return (
        <>
            <CircleFlayWrap menu={({isOpen,onToggle}:ICircleFlayWrap)=>(
                <MiniMenu isOpen={isOpen} onClose={onToggle} type='topRight'>
                    <MiniMenuItemX>
                        <div className='MiniMenu__icon'>
                            <IconX name='icon_channel'/>
                        </div>
                        <div className='MiniMenu__label'>New Channel</div>
                    </MiniMenuItemX>
                    <MiniMenuItemX>
                        <div className='MiniMenu__icon'>
                            <IconX name='icon_group'/>
                        </div>
                        <div className='MiniMenu__label'>New Group</div>
                    </MiniMenuItemX>
                    <MiniMenuItemX>
                        <div className='MiniMenu__icon'>
                            <IconX name='icon_user'/>
                        </div>
                        <div className='MiniMenu__label'>New Private Chat</div>
                    </MiniMenuItemX>
                </MiniMenu>
            )}>
                {({isOpen,onToggle}:ICircleFlayWrap)=>(
                    <RippleX className='CircleFlay__button' onClick={onToggle}>
                        <CSSTransition timeout={100} in={isOpen} classNames='rotate100' unmountOnExit mountOnEnter>
                            <IconX name='icon_close'/>
                        </CSSTransition>
                        <CSSTransition timeout={100} in={!isOpen} classNames='rotate100' unmountOnExit mountOnEnter>
                            <IconX name='icon_newchat_filled'/>
                        </CSSTransition>
                    </RippleX>
                )}
            </CircleFlayWrap>

            <NavBar>
                <div className='NavBar__left'>
                    <ButtonMenuWrap menu={({isOpen,onToggle}:IButtonMenuWrap)=>(
                        <MiniMenu isOpen={isOpen} onClose={onToggle} type='botLeft'>
                            <MiniMenuItemX>
                                <div className='MiniMenu__icon'>
                                    <IconX name='icon_group'/>
                                </div>
                                <div className='MiniMenu__label'>New Group</div>
                            </MiniMenuItemX>
                            <MiniMenuItemX>
                                <div className='MiniMenu__icon'>
                                    <IconX name='icon_user'/>
                                </div>
                                <div className='MiniMenu__label'>Contacts</div>
                            </MiniMenuItemX>
                            <MiniMenuItemX>
                                <div className='MiniMenu__icon'>
                                    <IconX name='icon_avatar_archivedchats'/>
                                </div>
                                <div className='MiniMenu__label'>Archived</div>
                                <div className='MiniMenu__count'><span>2</span></div>
                            </MiniMenuItemX>
                            <MiniMenuItemX>
                                <div className='MiniMenu__icon'>
                                    <IconX name='icon_savedmessages'/>
                                </div>
                                <div className='MiniMenu__label'>Saved</div>
                            </MiniMenuItemX>
                            <MiniMenuItemX>
                                <div className='MiniMenu__icon'>
                                    <IconX name='icon_settings'/>
                                </div>
                                <div className='MiniMenu__label'>Settings</div>
                            </MiniMenuItemX>
                            <MiniMenuItemX>
                                <div className='MiniMenu__icon'>
                                    <IconX name='icon_help'/>
                                </div>
                                <div className='MiniMenu__label'>Help</div>
                            </MiniMenuItemX>
                        </MiniMenu>
                    )}>{
                        ({isOpen,onToggle}:IButtonMenuWrap)=> <ButtonX type='icon-hover' className={ClassName({'-active':isOpen})} onClick={onToggle}><IconX name='icon_menu'/></ButtonX>
                    }</ButtonMenuWrap>
                </div>
                <div className='NavBar__search'>
                    <label className='-wrap'>
                        <div className='-icon'><IconX name='icon_search'/></div>
                        <input type='text' placeholder='Search...' onChange={(e)=>changeSearchText(e.target.value)} value={state.searchText}/>
                        <CSSTransition timeout={200} in={state.searchText!==''} classNames='rotate' mountOnEnter unmountOnExit>
                            <div className='-close' onClick={()=>changeSearchText('')}><IconX name='icon_close' size='small'/></div>
                        </CSSTransition>
                    </label>
                </div>
            </NavBar>
            <BoxScroll className='Modal__content' topLineOff={true} botLineOff={true}>
                <MediaMembersItem url={null} name={'Karen Stanford Karen Stanford Karen Stanford Karen Stanford'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Artur Pirozhkov'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Karen Stanford Karen Stanford Karen Stanford Karen Stanford'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Artur Pirozhkov'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Karen Stanford Karen Stanford Karen Stanford Karen Stanford'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Artur Pirozhkov'} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
            </BoxScroll>
        </>
    );
};

export default Main_LeftContainer;