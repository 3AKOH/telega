import React, {useEffect, useState} from 'react';
import NavBar from "../../../Components/NavBar/NavBar";
import ButtonX from "../../../Components/UI/Button/ButtonX/ButtonX";
import IconX from "../../../Components/UI/Icon/IconX";
import BoxScroll from "../../../Components/BoxScroll/BoxScroll/BoxScroll";
import AvatarInfo from "../../../Components/Avatar/AvatarInfo/AvatarInfo";
import InfoLinkX from "../../../Components/InfoWrap/InfoLinkX/InfoLinkX";
import RippleX from "../../../Components/UI/RippleX/RippleX";
import MediaBlock from "../../../Components/MediaBlock/MediaBlock";
import AvatarChange from "../../../Components/Avatar/AvatarChange/AvatarChange";
import InputX from "../../../Components/UI/Input/InputX/InputX";
import MediaMembersItem from "../../../Components/MediaBlock/MediaMembers/MediaMembersItem";
import InfoInputXWrap from "../../../Components/InfoWrap/InfoInputX/InfoInputXWrap";
import CircleFlay from "../../../Components/Circle/CircleFlay/CircleFlay";
import ClassName from "classnames";
import MiniMenu from "../../../Components/MiniMenu/MiniMenu";
import MiniMenuItem from "../../../Components/MiniMenu/MiniMenuItem/MiniMenuItem";
import BlockModify from "../../../Components/BlockModify/BlockModify";
import CSSTransition from "react-transition-group/CSSTransition";
import MiniMenuItemX from "../../../Components/MiniMenu/MiniMenuItemX/MiniMenuItemX";
import CircleFlayWrap, {ICircleFlayWrap} from "../../../Components/Circle/CircleFlay/CircleFlayWrap";

interface IProps {
}

const InitialState = {
};

const NewGroup_LeftContainer: React.FC<IProps> = (props) => {
    const [state, setState] = useState(InitialState);
    return (
        <>
            <CircleFlayWrap>
                {({isOpen,onToggle}:ICircleFlayWrap)=>(
                    <RippleX className='CircleFlay__button' onClick={onToggle}>
                        <IconX name='icon_next'/>
                    </RippleX>
                )}
            </CircleFlayWrap>
            <NavBar>
                <div className='NavBar__left'>
                    <ButtonX type='icon-hover'><IconX name='icon_back'/></ButtonX>
                    <div className='NavBar__title'>New Group</div>
                </div>
            </NavBar>
            <BoxScroll className='Modal__content' topLineOff={true} botLineOff={true}>
                <AvatarChange url={null}/>
                <InfoInputXWrap>
                    <InputX defaultValue='Party Group' type='circle' placeholder='Название группы' label='Group Name'/>
                </InfoInputXWrap>
                <div className='MediaMembers'>
                    <div className='MediaMembers__title'>6 Members</div>
                    <MediaMembersItem url={null} name={'Karen Stanford Karen Stanford Karen Stanford Karen Stanford'} online={true} lastTime={'xz kogda'}/>
                    <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                    <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
                    <MediaMembersItem url={null} name={'Artur Pirozhkov'} lastTime={'xz kogda'}/>
                    <MediaMembersItem url={null} name={'Phet Names'} online={true} lastTime={'xz kogda'}/>
                    <MediaMembersItem url={null} name={'Zak names'} lastTime={'xz kogda'}/>
                </div>
            </BoxScroll>
        </>
    );
};

export default NewGroup_LeftContainer;