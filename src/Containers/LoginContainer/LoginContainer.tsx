import React, {useContext, useEffect, useState} from 'react';
import TELEGRAM from '../../assets/svg-line/telegram_login.svg';
import BGLEFT from '../../assets/svg-line/bg-left.svg';
import BGRIGHT from '../../assets/svg-line/bg-right.svg';
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import './LoginContainer.scss'
import CSSTransition from "react-transition-group/CSSTransition";
import {AppContext} from "../../Context/App/AppState";
import Themes from "../../Context/App/state/themes";
import KeysEmitter from "../../Emitter/KeysEmitter";
import IconX from "../../Components/UI/Icon/IconX";
import ModalLink from "../../Modals/ModalLink/ModalLink";
import LoginSettings_Modal from "../../Modals/LoginSettings_Modal/LoginSettings_Modal";
import CodeCountry_Modal from "../../Modals/CodeCountry_Modal/CodeCountry_Modal";
import {Field, Form} from "react-final-form";
import ClassName from 'classnames';
import RippleX from "../../Components/UI/RippleX/RippleX";
import {codeContry, ICodeContry} from "../../Modals/CodeCountry_Modal/Codes";
import InputXX from "../../Components/UI/Input/InputXX/InputXX";
import AppStore, {IAppStoreStatus} from "../../Store/AppStore";
import ErrorStore from "../../Store/ErrorStore";
import SyncPromise from "../../Util/SyncPromise/SyncPromise";
import LangStore from "../../Store/LangStore";
import {VAR_SETTINGS} from "../../vars";
import {AutoSave} from "../../Modals/AutoSave";

interface IProps {
}

interface ISteepData {
    button: string
    component: any
}

interface IState {
    start: boolean
    steep: number
    steepData: ISteepData[]
    toggle: boolean      //показан ли этот Root Блок
    error: string
    speed: number      //скорость скрывания блока (дживжение "рамок")
    code: ICodeContry
}

let State: IState = {
    start: false,
    steep: 0,
    steepData: [],
    toggle: true,
    error: '',
    speed: 300,
    code: codeContry[0]
};
const LoginContainer: React.FC<IProps> = () => {
    const changeSMS = () => {
        setState(state => {
            let newState = {
                ...state,
                steepData: [
                    ...state.steepData,
                ]
            };
            let block = newState.steepData[state.steep].component;
            block = [block[1], block[0]];
            newState.steepData[state.steep].component = [
                ...block
            ];
            return newState
        });
    };
    const steep0 = (state: IState) => (
        <div className='LoginIn__steep0'>
            <div className='-title'>Telegram Web</div>
            <div className='-desc'>Добро пожаловать в Telegram для Web.<br/>Быстрый и безопасный клиент.</div>
        </div>
    );
    const handleChangeCode = (d: any) => {
        console.log(d);
        setState(prev => {
            return {...prev, code: {...d}}
        });
    };
    const steep1 = (state: IState) => (
        <div className='LoginIn__steep1'>
            <div className='-wrap'>
                <div className='-title'>Ваш номер телефона</div>
                <div className='-desc'>Проверьте код страны и введите<br/>свой номер телефона.</div>
                <ModalLink className='-contry CodeCountry_Modal' link={(dops: any) =>
                    <CodeCountry_Modal onChange={handleChangeCode} data={codeContry} {...dops}/>} done={handleChangeCode} component={RippleX} nameLevel='level1'>
                    <div className='-line'/>
                    <div className='-l'>{state.code.s}</div>
                    <div className='-r'><IconX name='icon_ugol'/></div>
                </ModalLink>
                <div className='-phone'>
                    <div className='-l'>
                        <Field name='phone_code' type='text' component={InputXX} defaultValue={'+' + state.code.i} disabled={true}/>
                    </div>
                    <div className='-c'/>
                    <div className='-r'>
                        <Field name='phone' type='text' component={InputXX} defaultValue='' focus={true}/>
                    </div>
                </div>
            </div>
            <div className={ClassName('-error', {'-active': state.error})}>
                {state.error ? LangStore.getString(state.error, null, 'Некорректный номер телефона.') : ''}
            </div>
        </div>
    );
    const steep2 = (state: IState) => (
        <div className='LoginIn__steep2'>
            <div className='-wrap'>
                <div className='-title'>+7 921 949 27 21</div>
                <div className='-desc'>Введите код, который вы только что получили на другом устройстве</div>
                <div className='-code'>
                    <Field name='code' type='text' component={InputXX} defaultValue='5448'/>
                </div>
                <div className='-sms'>
                    <button className='btn-link' onClick={() => changeSMS()}>Отправить код по SMS</button>
                </div>
                <div className={ClassName('-error', {'-active': state.error})}>
                    {state.error ? LangStore.getString(state.error, null, 'Неверный код') : ''}
                </div>
            </div>
        </div>
    );
    const steep3 = (state: IState) => (
        <div className='LoginIn__steep2'>
            <div className='-wrap'>
                <div className='-title'>+7 921 949 27 21</div>
                <div className='-desc'>Код активации отправлен на Ваш телефон по SMS.<br/>Пожалуйста, введите его ниже.
                </div>
                <div className='-code'>
                    <Field name='codeSms' type='text' component={InputXX} defaultValue='5448'/>
                </div>
                <div className='-sms'>
                    <button className='btn-link' onClick={() => changeSMS()}>Отправить код в Telegram</button>
                </div>
                <div className={ClassName('-error', {'-active': state.error})}>
                    {state.error ? LangStore.getString(state.error, null, 'Неверный код') : ''}
                </div>
            </div>
        </div>
    );
    const getSteep = () => {
        let steep = 0;
        switch (AppStore.getAuth()) {
            case IAppStoreStatus["waitCode"]: steep = 2;
                break
        }
        return steep;
    }
    const {appState, dispatch} = useContext(AppContext);
    const [state, setState] = useState<IState>(()=>{
        return {
            ...State,
            steep: getSteep(),
            steepData: [
                {
                    button: "Начать общение",
                    component: [steep0],
                },
                {
                    button: "Продолжить",
                    component: [steep1],
                },
                {
                    button: "Отправить",
                    component: [steep2, steep3],
                }
            ]
        }
    });
    const addError = (err: string) => {
        if (err == 'PHONE_NUMBER_INVALID') setState(prev => ({...prev, error: ErrorStore._getNameErr(err)}))
    };
    const UPDATE_AUTH = (status:string) =>{
        console.log(status);
        setState({
            ...state,
            steep: getSteep()
        })
    }
    useEffect(() => {
        if (state.toggle) setState({...state, start: true})
    }, [appState.app.login])
    useEffect(() => {
        AppStore.on('UPDATE_AUTH', UPDATE_AUTH)
        KeysEmitter.on('ESC', handleBack.bind(null, appState.modals.isExist))
        return () => {
            console.log('ESSSSSSSSSSSSS');
            KeysEmitter.off('ESC', handleBack.bind(null, appState.modals.isExist))
        }
    }, []);
    const handleBack = () => {
        setState((prev: IState) => {
            if (prev.steep !== 0) return {...prev, steep: prev.steep - 1, error: ''};
            return prev;
        })
    };
    const handleSubmit = async (d: any) => {
        let next = false;
        switch (state.steep) {
            case 0:
                next = true
                break;
            case 1:
                if (d.phone) {
                    const num = d.phone_code + d.phone;
                    await SyncPromise([AppStore._sendPhoneNumber(num)], (d: any) => {
                        console.log(777777777, d);
                        if (d[0]['@type'] === 'error') {
                            addError(d[0].message);
                            return
                        }else{
                            next = true
                        }
                    });
                    console.log('SYNC?');
                }
                break;
            case 2:
                if (d.code && d.code.length > 4) {
                    SyncPromise([AppStore._sendTelegramCode(d.code)], (d: any) => {
                        console.log(d);
                    });
                }
                break;
        }
        if (next) if (state.steepData[state.steep + 1]) setState(prev => ({...prev, steep: prev.steep + 1}))
    };
    const handleValidate = (d: any) => {
        let error: any = [];
        let next = false;
        switch (state.steep) {
            case 0:
                next = true;
                break;
            case 1:
                console.log(111111111, d);
                if (!d.phone_code) error.phone_code = true;
                if (!d.phone) error.phone = true;
                break;
            case 2:
                if (!d.code) error.code = true;
                break;
        }
        return error;
    }
    return (
        <>
            <div className='LoginIn'>
                {/*шапка*/}
                <CSSTransition timeout={600} in={state.start && state.steep === 0} classNames='LoginInBarStart'>
                    <CSSTransition timeout={state.speed} in={state.steep === 0} classNames='LoginInBarToggle'>
                        <div className='LoginIn__head'>
                            <img src={BGLEFT} alt='Telegram Web' className='-left'/>
                            <img src={BGRIGHT} alt='Telegram Web' className='-right'/>
                            <img src={TELEGRAM} alt='Telegram Web' className='-logo'/>
                        </div>
                    </CSSTransition>
                </CSSTransition>
                <div className='LoginIn__wrap'>
                    {/*дополнительный кнопки*/}
                    <CSSTransition timeout={200} in={state.steep !== 0} classNames='show' unmountOnExit mountOnEnter>
                        <div className='LoginIn__buttons'>
                            <div className='-left'>
                                <ButtonX type={"icon"} onClick={handleBack}><IconX name='icon_back'/></ButtonX>
                            </div>
                            <div className='-right'>
                                {/*<ModalLink component={LoginSettings_Modal}>*/}
                                <ModalLink link={(d: any) => <LoginSettings_Modal {...d}/>}>
                                    <ButtonX type={"nav"}>{LangStore.getString('SETTINGS', null, VAR_SETTINGS)}</ButtonX>
                                </ModalLink>
                            </div>
                        </div>
                    </CSSTransition>
                    {/*основная часть*/}
                    <div className='LoginIn__body'>
                        <Form onSubmit={handleSubmit} validate={handleValidate}>
                            {({handleSubmit}) => (
                                <form onSubmit={handleSubmit}>
                                    {state.steepData[state.steep].component[0](state)}
                                    <ButtonX className='-button' block={true} theme={appState.themes}>{state.steepData[state.steep].button}</ButtonX>
                                </form>
                            )}
                        </Form>
                        <div className='-changeLang'>
                            <button className='btn-link'>Continue in English</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};


export default LoginContainer;

