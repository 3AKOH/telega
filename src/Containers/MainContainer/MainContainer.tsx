import React, {createRef, useContext, useEffect, useState} from 'react';
import Drawer from "../../Components/Drawer/Drawer";
import {NavLink, Route, Switch} from "react-router-dom";
import ClassName from 'classnames';
import IconX from "../../Components/UI/Icon/IconX";
import TopBar from "../../Components/TopBar/TopBar";
import TopBarSearch from "../../Components/TopBar/TopBarSearch/TopBarSearch";
import TopBarTitle from "../../Components/TopBar/TopBarTitle/TopBarTitle";
import TopBarSend from "../../Components/TopBar/TopBarSend/TopBarSend";
import DrawerContent from "../../Components/Drawer/DrawerContent/DrawerContent";
import GroupInfo from "../../Components/InfoWrap/GroupInfo/GroupInfo";
import Backdrop from "../../Components/Backdrop/Backdrop";
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import DeviceEmitter, {IDeviceEmitter, IDeviceEmitterData} from "../../Emitter/DeviceEmitter";
import PointerEmitter, {IPointerEmitter} from "../../Emitter/PointerEmitter";
import Test6 from "../../pages/Test6/Test6";
import LeftContainer from "../LeftContainer/LeftContainer";
import {AppContext} from "../../Context/App/AppState";
import {toggleDrawer} from "../../Context/App/AppAC";
import CenterContainer from "../CenterContainer/CenterContainer";
import {modalClose} from "../../Context/App/state/modals";
import ChatsStore from "../../Store/ChatsStore";
import RightContainer from "../RightContainer/RightContainer";
import MainContainerEmitter, {IMainContainerEmitter} from "../../Emitter/MainContainerEmitter";



interface ILayout {
    device: number
    mobile: {
        on: boolean
        menu: boolean
    }
    resize: {
        outside: boolean
        inner: boolean
    }
    status: {
        active: boolean
        startPoint: number
        prevPoint: number
        diff: number    //расстояние-разница до внутренего дива
        on: boolean
        place: number
        pos: number
        baseX: number
        link: { [key: string]: any }
    }
    body: {
        ref: any
        minWidth: number
    }
    custom: {
        outside: number
        miniOpen: number
        miniClose: number
    }
    wrap: {
        sleep: boolean
        ref: any
        width: number
    }
    left: {
        ref: any
        open: boolean
        mini: boolean
        width: number
        maxWidth: number
        miniOpen: number
        miniClose: number
    }
    right: {
        ref: any
        open: boolean
        width: number
        maxWidth: number
    }
}

const Layout: ILayout = {
    device: 0,
    mobile: {
        on: false,
        menu: true
    },
    resize: {
        outside: true,
        inner: true
    },
    status: {
        active: false,
        startPoint: 0,
        prevPoint: 0,
        diff: 0,
        on: false,           //был ли выбран эелмент для сдвига?
        place: 0,          //место left[0]/right[1]
        pos: 0,             //позиция outside[0]/inner[1]
        baseX: 0,
        link: {}
    },
    body: {
        ref: createRef(),
        minWidth: 0
    },
    custom: {
        outside: 30,
        miniOpen: 50,
        miniClose: 50,
    },
    wrap: {
        sleep: false,
        ref: createRef(),
        width: 1500,
    },
    left: {
        ref: createRef(),
        open: true,
        mini: false,
        miniOpen: 120,
        miniClose: 50,
        width: 300,
        maxWidth: 700,
    },
    right: {
        ref: createRef(),
        open: false,
        width: 350,
        maxWidth: 450,
    }
};
interface ILocalStore {
    wrap:{
        sleep: boolean
        width: number
    }
    left:{
        open: boolean
        mini: boolean
        width: number
    }
    right:{
        open: boolean
        width: number
    }
}

//pos[0-1]:0 .... content ..... pos[1-0]:1
const MainContainer = () => {
    const {appState,dispatch} = useContext(AppContext);
    // const [Dr, setDr] = useState(false);
    useEffect(()=>{
        DeviceEmitter.on('RESIZE',ramkiDevice);
        return ()=>{
            DeviceEmitter.off('RESIZE',ramkiDevice);
        }
    },[]);
    const [layout, setLayout] = useState<ILayout>(()=>{
        let loc = localStorage.getItem('mainState');
        if(loc){
            const loc2:ILocalStore = JSON.parse(loc);
            return {
                ...Layout,
                device: DeviceEmitter.getData().device.type,
                mobile: {
                    on: DeviceEmitter.getData().device.type <= 3,
                    menu: true,
                },
                wrap:{
                    ...Layout.wrap,
                    ...loc2.wrap
                },
                left: {
                    ...Layout.left,
                    ...loc2.left
                },
                right: {
                    ...Layout.right,
                    ...loc2.right
                }
            }
        }
        return Layout
    });
    useEffect(()=>{
        updateMainEmitter(layout);
    },[layout.right.open]);
    const saveProperty = (data:ILayout) => {
        const loc:ILocalStore = {
            wrap: {
                sleep: data.wrap.sleep,
                width: data.wrap.width,
            },
            left: {
                mini: data.left.mini,
                open: data.left.open,
                width: data.left.width,
            },
            right: {
                open: data.right.open,
                width: data.right.width,
            }
        };
        localStorage.setItem('mainState', JSON.stringify(loc));
    }   //сохранить нстроки в localStore

    const updateMainEmitter = (layout:ILayout) => {
        MainContainerEmitter.setWidth(layout.body.ref.current.clientWidth)
    };
    //изменяем размер устройства
    const ramkiDevice = (d:IDeviceEmitterData) => {
        setLayout(layout => {
            updateMainEmitter(layout);
            if(d.device.type===layout.device) return layout;
            let newData:ILayout = {
                ...layout,
                mobile: {
                    ...layout.mobile,
                    on: DeviceEmitter.getData().device.type <= 3
                },
                resize: {...layout.resize}
            };
            if (d.device.type >= 0) {
                newData.resize.inner = false;
                newData.resize.outside = false
            }
            if (d.device.type >= 3) {
                newData.resize.inner = true
            }
            if (d.device.type >= 5) {
                newData.resize.outside = true
            }
            return newData
        });
    };
    const ramkiMove = (d:IPointerEmitter) => {
        let render = false;
        setLayout((layout)=>{
            updateMainEmitter(layout);
            let newData = {
                ...layout
            };
            if (layout.status.on && layout.status.link) {
                if (d.on && d.move && d.wind) {
                    let newWidth = 0;
                    const style = getComputedStyle(layout.status.link.ref.current);
                    const minWidth = parseInt(style.minWidth || '', 10);

                    if (layout.status.link.open !== undefined) {  //inner
                        if (layout.status.place === 0) {        //left
                            // render = true;
                            newWidth = layout.status.baseX + d.move.x;
                            if ((newWidth >= minWidth && newWidth <= layout.left.maxWidth)) {
                                // console.log('redd');
                                newData.left = {
                                    ...layout.left,
                                    width: newWidth
                                };
                                if (!layout.status.active) newData.status.startPoint = 0;
                            }

                            //обработка MINI
                            newData.status.startPoint = d.move.x - newData.status.prevPoint + newData.status.startPoint;

                            // console.log(newData.status.startPoint,newData.status.prevPoint);
                            if (!layout.left.mini) {  //full
                                if (newData.status.active) {
                                    if (d.move.x < layout.left.miniClose) {
                                        // newData.left.mini = true;
                                        newData.status.active = false;
                                    }
                                } else {
                                    if (newData.status.startPoint <= -layout.left.miniOpen) {
                                        // newData.left.mini = true;
                                        newData.status.active = true
                                    }
                                }
                            } else {
                                if (newData.status.active) {
                                    if (newData.status.startPoint > -layout.left.miniOpen) {
                                        newData.left.mini = false;
                                        layout.status.active = false
                                    }
                                } else {
                                    if (d.move.x >= layout.left.miniClose) {
                                        newData.left.mini = false;
                                        newData.status.active = true;
                                        newData.left.width = layout.left.width - d.move.x;
                                        newData.status.baseX = minWidth
                                    }
                                }
                            }
                        } else {
                            newWidth = layout.status.baseX - d.move.x;
                            if ((newWidth >= minWidth && newWidth <= layout.right.maxWidth)) {
                                render = true;
                                newData.right = {
                                    ...layout.right,
                                    width: newWidth
                                };
                            }
                        }
                        newData.status.prevPoint = d.move.x;  //предыдущий поинтер
                    } else {  //outside
                        newData.wrap = {
                            ...layout.wrap,
                        };
                        if (layout.status.place === 0) {        //left
                            newData.wrap.width = layout.status.baseX - d.move.x * 2;
                            if (layout.wrap.sleep) {  //full
                                if (d.wind.x0 >= layout.custom.outside) {
                                    newData.wrap.sleep = false;
                                }
                            } else {
                                if (d.wind.x0 < layout.custom.outside) {
                                    newData.wrap.sleep = true;
                                }
                            }
                        } else {
                            newData.wrap.width = layout.status.baseX + d.move.x * 2;
                            if (layout.wrap.sleep) {  //full
                                if (d.wind.x1 >= layout.custom.outside) {
                                    newData.wrap.sleep = false;
                                }
                            } else {
                                if (d.wind.x1 < layout.custom.outside) {
                                    newData.wrap.sleep = true;
                                }
                            }
                        }
                        render = true;
                    }
                } else {
                    newData = {
                        ...layout,
                        status: Layout.status,
                        body: Layout.body,
                    };
                    if(layout.left.ref.current){
                        const styleLeft = getComputedStyle(layout.left.ref.current);
                        newData.left.width = parseInt(styleLeft.width || '', 10);
                    }
                    if(layout.right.ref.current){
                        const styleRight = getComputedStyle(layout.right.ref.current);
                        newData.right.width = parseInt(styleRight.width || '', 10);
                    }
                    if(layout.wrap.ref.current){
                        const styleWrap = getComputedStyle(layout.wrap.ref.current);
                        newData.wrap.width = parseInt(styleWrap.width || '', 10);
                    }
                    PointerEmitter.off('DOWN',ramkiMove);
                    PointerEmitter.off('MOVE',ramkiMove);
                    PointerEmitter.off('UP',ramkiMove);
                    saveProperty(newData);
                }
                return newData;
            }
            return layout
        });
    };

    const onClickDown = (e: any) => {
        if (!e.target || !e.target.dataset.pos || !e.target.dataset.place) return false;
        const place = parseInt(e.target.dataset.place, 10);
        const pos = parseInt(e.target.dataset.pos, 10);

        //определеням минималку BODY
        const styleBody = getComputedStyle(layout.body.ref.current);
        const minWidth = parseInt(styleBody.minWidth || '', 10);     //размер BODY

        let newO: ILayout = {
            ...layout,
            status: {
                ...layout.status,
                diff: e.target.children[0].offsetLeft - (e.pageX - Math.floor(e.target.getBoundingClientRect().left)),
                on: true,
                place: place,
                pos: pos
            },
            body: {
                ...layout.body,
                minWidth: minWidth
            }
        };
        if ((place === 0 && pos === 0) || (place === 1 && pos === 0)) {
            if(!layout.resize.outside) return false;
            newO.status.link = layout.wrap;
            newO.status.baseX = layout.wrap.width
        } else if (place === 0 && pos === 1) {
            if(!layout.resize.inner) return false;
            newO.status.link = layout.left;
            newO.status.baseX = layout.left.width
        } else {
            if(!layout.resize.inner) return false;
            newO.status.link = layout.right;
            newO.status.baseX = layout.right.width
        }
        PointerEmitter.on('DOWN',ramkiMove);
        PointerEmitter.on('MOVE',ramkiMove);
        PointerEmitter.on('UP',ramkiMove);
        setLayout(newO);
        e.preventDefault()
    };
    const handlRightToggle = () => {
        const d = {
            ...layout,
            right: {
                ...layout.right,
                open: !layout.right.open,
            }
        }
        saveProperty(d);
        setLayout(d);
    }

    const renderLeft = () => {
        const mini = ['Layout__left'];
        if (layout.left.mini) {
            mini.push('-mini');
        }
        return (
            <div className={mini.join(' ')} style={{maxWidth: layout.left.width}} ref={layout.left.ref}>
                <div className='Layout__in'>
                    <LeftContainer isMini={layout.left.mini}/>
                </div>
                <div className={ClassName('Layout__line -outside',{'-on':layout.resize.outside})} onMouseDown={onClickDown} data-pos={0} data-place={0}>
                    <div/>
                </div>
                <div className={ClassName('Layout__line -inner',{'-on':layout.resize.inner})} onMouseDown={onClickDown} data-pos={1} data-place={0}>
                    <div/>
                </div>
            </div>
        )
    };
    const renderRight = () => {
        if(!layout.right.open) return (
            <div className='Layout__right' style={{maxWidth: 0,minWidth: 0}} >
                <div className={ClassName('Layout__line -outside -right',{'-on':layout.resize.outside})} onMouseDown={onClickDown} data-pos={0} data-place={1}>
                    <div/>
                </div>
            </div>
        );
        return (
            <div className='Layout__right' style={{maxWidth: layout.right.width}} ref={layout.right.ref}>
                <div className='Layout__in'>
                    <RightContainer />
                </div>
                <div className={ClassName('Layout__line -outside -right',{'-on':layout.resize.outside})} onMouseDown={onClickDown} data-pos={0} data-place={1}>
                    <div/>
                </div>
                <div className={ClassName('Layout__line -inner -right',{'-on':layout.resize.inner})} onMouseDown={onClickDown} data-pos={1} data-place={1}>
                    <div/>
                </div>
            </div>
        )
    };

    let maxWidth = '';
    if(layout.mobile.on){
        maxWidth = ""
    }else{
        if (layout.wrap.sleep) {
            maxWidth = "100%"
        } else {
            maxWidth = layout.wrap.width + 'px'
        }
    }

    return (
        <div ref={layout.wrap.ref} className='Layout' style={{maxWidth}}>
            <Drawer closeFunc={()=>dispatch(toggleDrawer())} isOpen={appState.drawer.on}>
                <DrawerContent/>
            </Drawer>
            <div className={ClassName('Layout__wrap',{'-menu':layout.mobile.on && layout.mobile.menu})}>
                {renderLeft()}
                <div className='Layout__body' ref={layout.body.ref}>
                    <CenterContainer changeRight={handlRightToggle} isRight={layout.right.open}/>
                </div>
                {renderRight()}
            </div>
        </div>
    )
};

export default MainContainer;