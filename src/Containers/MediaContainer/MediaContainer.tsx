import React, {useContext, useEffect, useState} from 'react';
import './MediaContainer.scss'
import Portal from "../../Components/Portal/Portal";
import SpinnerMax from "../../Components/Loader/SpinnerMax/SpinnerMax";
import {AppContext} from "../../Context/App/AppState";
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import InputX from "../../Components/UI/Input/InputX/InputX";
import IconX from "../../Components/UI/Icon/IconX";
import Backdrop from "../../Components/Backdrop/Backdrop";
import BIG from '../../assets/img/big.jpg';


interface IProps {
    // isOpen: boolean
}

const MediaContainer: React.FC<IProps> = (props) => {
    const {} = props;
    // const {appState, dispatch} = useContext(AppContext);
    return (
        <div className='MediaContainer'>
            {/*<Backdrop isOpen={true} classNames='show' onClick={null} timeout={0}/>*/}
            <div className='MediaContainer__top'>
                <div className='-left'>
                    Name
                </div>
                <div className='-right'>
                    <ButtonX type='icon-dark'><IconX name='icon_delete'/></ButtonX>
                    <ButtonX type='icon-dark'><IconX name='icon_forward'/></ButtonX>
                    <ButtonX type='icon-dark'><IconX name='icon_download'/></ButtonX>
                    <ButtonX type='icon-dark'><IconX name='icon_close'/></ButtonX>
                </div>
            </div>
            <div className='MediaContainer__body'>
                <div className='-wrap'>
                    <img src={BIG} alt=''/>
                    <div className='-nav -prev'><IconX name='icon_up'/></div>
                    <div className='-nav -next'><IconX name='icon_up'/></div>
                </div>
            </div>
            <div className='MediaContainer__footer'>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio dolor qui voluptate. Alias quasi repellat velit vero. Eius, eveniet vitae!
            </div>
        </div>
    );
};

export default MediaContainer;