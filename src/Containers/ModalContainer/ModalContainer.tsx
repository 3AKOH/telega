import React, {useContext, useEffect, useState} from 'react';
import ClassNames from 'classnames'
import TELEGRAM from '../../assets/svg-line/telegram_login.svg';
import BGLEFT from '../../assets/svg-line/bg-left.svg';
import BGRIGHT from '../../assets/svg-line/bg-right.svg';
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import CSSTransition from "react-transition-group/CSSTransition";
import {AppContext} from "../../Context/App/AppState";
import Themes from "../../Context/App/state/themes";
import Backdrop from "../../Components/Backdrop/Backdrop";
import KeysEmitter from "../../Emitter/KeysEmitter";
import {modalBack, modalClose, modalOpen} from "../../Context/App/state/modals";
import Modal, {GetModal} from "../../Modals/Modal";
import LoginSettings_Modal from "../../Modals/LoginSettings_Modal/LoginSettings_Modal";
import Modals from "../../Modals/Modals";
import TransitionGroup from "react-transition-group/TransitionGroup";

interface IProps {

}

interface IState {
    show: boolean
    data: boolean
    marginBottom: number
    speed: number
}

let State: IState = {
    show: true,
    data: true,
    marginBottom: 0,
    speed: 200,
};
const ModalContainer: React.FC<IProps> = ({}) => {
    const {appState, dispatch} = useContext(AppContext);
    useEffect(() => {
        // KeysEmitter.addListener("ESC", handleClose);
        // KeysEmitter.addListener("BACK", handleBack);
        KeysEmitter.addListener("SETTINGS", handleSettings);
        return () => {
            // KeysEmitter.removeListener("ESC", handleClose);
            // KeysEmitter.removeListener("BACK", handleBack);
            KeysEmitter.removeListener("SETTINGS", handleSettings);
        }
    }, []);
    const handleBack = (name?:string) => {
        dispatch(modalBack());
        return false;
    };
    const handleClose = (name?:string) => {
        dispatch(modalClose(name));
        return false;
    };
    const handleSettings = (backdrop: boolean) => {
        dispatch(modalOpen(LoginSettings_Modal));
    };
    return (
        <>
            <TransitionGroup>
                {
                    appState.modals.modalList.map((modals,index:number,list) => {
                        const isBg = index!==list.length-1 ? true : false;      //нужно ли блок затемнять?
                        let lastComponent = modals.list[modals.list.length-1];  //содержимое компоненты
                        return (
                            <Modals onBack={()=>handleBack(modals.name)} onClose={()=>handleClose(modals.name)} data={lastComponent} key={modals.name} anime='scale80' className={modals.name}  isBg={isBg}/>
                        )
                    })
                }
            </TransitionGroup>
        </>
    )
};

export default ModalContainer;