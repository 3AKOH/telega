import React, {useContext, useEffect, useState} from 'react';
import './PortalContainer.scss'
import Portal from "../../Components/Portal/Portal";
import SpinnerMax from "../../Components/Loader/SpinnerMax/SpinnerMax";
import {AppContext} from "../../Context/App/AppState";


interface IProps {
    isOpen: boolean
}

const PortalContainer: React.FC<IProps> = ({isOpen}) => {
    // const {appState, dispatch} = useContext(AppContext);
    return (
        <Portal isOpen={isOpen} speedRamki={1000} speedInner={500} initStatus={true}>
            {(show:boolean)=>(
                <div className='InitializeApp'>
                    <div className='InitializeApp__box'>
                        <SpinnerMax/>
                        <div className='InitializeApp__label'>
                            {show && 'Encryption Key Generation'}
                        </div>
                    </div>
                </div>
            )}
        </Portal>
    );
};

export default PortalContainer;