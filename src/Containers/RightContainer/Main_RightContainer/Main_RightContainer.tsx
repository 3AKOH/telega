import React, {useEffect, useState} from 'react';
import TopBar from "../../../Components/TopBar/TopBar";
import ButtonX from "../../../Components/UI/Button/ButtonX/ButtonX";
import IconX from "../../../Components/UI/Icon/IconX";
import GroupInfo from "../../../Components/InfoWrap/GroupInfo/GroupInfo";
import NavBar from "../../../Components/NavBar/NavBar";
import AvatarInfo from "../../../Components/Avatar/AvatarInfo/AvatarInfo";
import BoxScroll from "../../../Components/BoxScroll/BoxScroll/BoxScroll";
import InfoAvatar from "../../../Components/InfoWrap/InfoAvatar/InfoAvatar";
import Avatar from "../../../Components/Avatar/Avatar";
import InfoWrap from "../../../Components/InfoWrap/InfoWrap";
import InfoLink from "../../../Components/InfoWrap/InfoLink/InfoLink";
import RippleX from "../../../Components/UI/RippleX/RippleX";
import CheckboxX from "../../../Components/UI/Checkbox/CheckboxX/CheckboxX";
import InfoLinkX from "../../../Components/InfoWrap/InfoLinkX/InfoLinkX";
import MediaBlock from "../../../Components/MediaBlock/MediaBlock";
import InfoLinkXWrap from "../../../Components/InfoWrap/InfoLinkX/InfoLinkXWrap";
import CircleFlay from "../../../Components/Circle/CircleFlay/CircleFlay";
import CSSTransition from "react-transition-group/CSSTransition";
import MiniMenu from "../../../Components/MiniMenu/MiniMenu";
import MiniMenuItem from "../../../Components/MiniMenu/MiniMenuItem/MiniMenuItem";
import ButtonMenuWrap, {IButtonMenuWrap} from "../../../Components/UI/Button/ButtonMenu/ButtonMenu";
import MiniMenuItemX from "../../../Components/MiniMenu/MiniMenuItemX/MiniMenuItemX";
import ClassName from "classnames";
import {Route, Switch} from "react-router";
import Main_LeftContainer from "../../LeftContainer/Main_LeftContainer/Main_LeftContainer";
import Background_LeftContainer from "../../LeftContainer/Background_LeftContainer/Background_LeftContainer";
import Contacts_LeftContainer from "../../LeftContainer/Contacts_LeftContainer/Contacts_LeftContainer";
import NewChannel_LeftContainer from "../../LeftContainer/NewChannel_LeftContainer/NewChannel_LeftContainer";
import NewGroup_LeftContainer from "../../LeftContainer/NewGroup_LeftContainer/NewGroup_LeftContainer";

interface IProps {
}

const InitialState = {
    miniMenu: false,
    showCircle: false
};

const Main_RightContainer: React.FC<IProps> = ({children}) => {
    const [state, setState] = useState(InitialState);
    useEffect(() => {
        setTimeout(() => {
            setState({...state, showCircle: true});
        }, 2000)
    }, []);
    return (
        <>
            <CircleFlay>
                <CSSTransition in={state.showCircle} timeout={200} classNames='scale' mountOnEnter unmountOnExit>
                    <RippleX className='CircleFlay__button'>
                        <IconX name='icon_addmember_filled'/>
                    </RippleX>
                </CSSTransition>
            </CircleFlay>
            <NavBar>
                <div className='NavBar__left'>
                    <ButtonX type='icon-hover'><IconX name='icon_close'/></ButtonX>
                    <div className='NavBar__title'>Info</div>
                </div>
                <div className='NavBar__right'>
                    <ButtonX type='icon-hover'><IconX name='icon_edit'/></ButtonX>
                    <div className='NavBar__iconWrap'>
                        <ButtonMenuWrap menu={({isOpen,onToggle}:IButtonMenuWrap)=>(
                            <MiniMenu isOpen={isOpen} onClose={onToggle} type='botRight'>
                                <MiniMenuItemX>
                                    <div className='MiniMenu__icon'>
                                        <IconX name='icon_unmute'/>
                                    </div>
                                    <div className='MiniMenu__label'>Unmute</div>
                                </MiniMenuItemX>
                                <MiniMenuItemX status='red'>
                                    <div className='MiniMenu__icon'>
                                        <IconX name='icon_delete'/>
                                    </div>
                                    <div className='MiniMenu__label'>Delete and Leave</div>
                                </MiniMenuItemX>
                            </MiniMenu>
                        )}>{
                            ({isOpen,onToggle}:IButtonMenuWrap)=> <ButtonX type='icon-hover' className={ClassName({'-active':isOpen})} onClick={onToggle}><IconX name='icon_more'/></ButtonX>
                        }</ButtonMenuWrap>
                    </div>
                </div>
            </NavBar>
            <BoxScroll className='Modal__content' topLineOff={true} botLineOff={true}>
                <Switch>
                    <Route exact path="/p1" component={()=>(
                        <>
                            <AvatarInfo url={null} title={'Karen Stanford'} online={true} lastTime={'xz kogda'} desc={'2,500 members, 756 online'} type='user'/>
                            <InfoLinkXWrap>
                                <InfoLinkX hover={false} icon_name='icon_info' title={'About'}>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, ullam!\n
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, ullam!\n
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, ullam!\n
                                </InfoLinkX>
                                <InfoLinkX hover={false} icon_name='icon_username' title={'Link'}><a href='#'>t.me/party</a></InfoLinkX>
                                <InfoLinkX hover={false} icon_name='icon_phone' title={'Phone'}>+1 38594 38853</InfoLinkX>
                                <InfoLinkX component={RippleX} icon_name='icon_checkboxon' icon_active={true} title={'Disabled'}>Notifications</InfoLinkX>
                            </InfoLinkXWrap>
                            <MediaBlock/>
                        </>
                    )}/>
                    <Route exact component={()=><MediaBlock/>}/>
                </Switch>
            </BoxScroll>
        </>
    );
};

export default Main_RightContainer;