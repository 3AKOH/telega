import React, {useContext, useState} from 'react';
import TopBarSearch from "../../Components/TopBar/TopBarSearch/TopBarSearch";
import {AppContext} from "../../Context/App/AppState";
import {changeLeftType, toggleDrawer} from "../../Context/App/AppAC";
import BoxScroll from "../../Components/BoxScroll/BoxScroll/BoxScroll";
import Main_RightContainer from "./Main_RightContainer/Main_RightContainer";

interface IProps {
}
interface IState {
}

const RightContainer: React.FC<IProps> = (props) => {
    const [state, setState] = useState<IState>(false);
    const {appState, dispatch} = useContext(AppContext);
    return (
        <>
            <Main_RightContainer/>
        </>
    );
};

export default RightContainer;