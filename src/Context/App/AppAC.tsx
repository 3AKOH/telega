import {APP_INIT, DRAWER, LEFT_TYPE, RIGHT_TYPE, SET_THEME} from "./AppConst";
import {IAppState} from "./AppState";


export const appInitAC = (d:boolean) => ({type: APP_INIT, payload: d});


export const toggleDrawer = () => (dispatch:any,state:IAppState) => {
    dispatch({type: DRAWER, payload: !state.drawer.on})
};   //переключить Drawer
export const changeLeftType = (type:string='', search:string='') => (dispatch:any,state:IAppState) => {
    dispatch({type: LEFT_TYPE, payload:{type,search}})
};   //изменили тип левого меню
export const changeRightType = (type:string='') => (dispatch:any,state:IAppState) => {
    dispatch({type: RIGHT_TYPE, payload: type})
};   //изменили тип правого меню
export const setTheme = (name:string='default') => (dispatch:any,state:IAppState) => {
    dispatch({type: SET_THEME, payload: name})
};   //изменили тип правого меню