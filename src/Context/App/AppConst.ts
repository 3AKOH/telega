export const APP_INIT = "APP_INIT";         //проинициализировали приложение?
export const APP_MAIN = "APP_MAIN";       //произведен ли вход в систему?
export const DRAWER = "DRAWER";
export const LEFT_TYPE = "LEFT_TYPE";
export const RIGHT_TYPE = "RIGHT_TYPE";
export const SET_THEME = "SET_THEME";