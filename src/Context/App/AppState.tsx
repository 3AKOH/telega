import React, {Context, createContext, useContext, useEffect, useReducer, useState} from 'react';
// import {AppContext} from "./AppContext";
import Api from "../../Api/Api";
import SyncStore from "../../Util/SyncPromise/SyncStore";
import {APP_INIT, APP_MAIN, DRAWER, LEFT_TYPE, RIGHT_TYPE, SET_THEME} from "./AppConst";
import {appInitAC} from "./AppAC";
import Themes, {IThemes} from "./state/themes";
import Modals, {IModals, MODAL_REFRESH} from "./state/modals";
import useThunkReducer from "../../Util/useThunkReducer";
import AppStore from "../../Store/AppStore";

// HEADE //
export interface IAppContext {
    appState: IAppState
    dispatch: any
}
export let AppContext:Context<IAppContext>;

// BODY //
interface IAction {
    type: string
    payload?: any
}
export interface IAppState {
    themes: IThemes
    modals: IModals
    drawer: {
        on: boolean
    }
    app: {
        portal: boolean
        login: boolean
        main: boolean
        bgColor: string
    }
    left: {
        type: "" | "search" | "searchMessage"
        search: string
    }
    right: {
        type: "" | "smile"
    }
}
//ИНИЦИАЛИЗАЦИЯ STATE!!!
const State:IAppState = {
    themes: new Themes("default1"),
    modals: Modals,
    drawer: {
        on: false
    },
    app: {
        portal: false,
        login: false,
        main: true,
        bgColor: "#e6ebee"
    },
    left: {
        type: "",
        search: "",
    },
    right: {
        type: "",
    }
};

const reducer = (state=State, action:IAction) => {
    switch (action.type) {
        case APP_INIT:
            console.log('=========================');
            console.log('INIT',action.payload);
            return { ...state, appInit: action.payload };
        case MODAL_REFRESH:
            return { ...state, modals: action.payload, drawer:{...state.drawer, on: false} };
        case DRAWER:
            return { ...state, drawer: {...state.drawer, on: action.payload }, modals:{...state.modals, isExist: true} };
        case LEFT_TYPE:
            return { ...state, left: {...state.left, ...action.payload } };
        case RIGHT_TYPE:
            return { ...state, right: {...state.right, type: action.payload } };
        case APP_MAIN:
            return { ...state, initMain: action.payload };
        case SET_THEME:
            return { ...state, themes: {...state.themes, curTheme: action.payload} };
        default:
            console.error("ОТСУТСТВУЕТ REDUCER!!!!!!!! ", action);
            return state
    }
};
// const api = Api;
const AppState:React.FC = ({children}) => {
    const [appState, dispatch] = useThunkReducer(reducer,State);
    if(!AppContext) AppContext = createContext<IAppContext>({appState:appState,dispatch});
    return (
        <AppContext.Provider value={{appState, dispatch}}>
            {children}
        </AppContext.Provider>
    );
};

export default AppState;