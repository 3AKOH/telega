import React from "react";
import {IAppState} from "../AppState";
import LoginSettings_Modal from "../../../Modals/LoginSettings_Modal/LoginSettings_Modal";
import nanoid from 'nanoid'
import {dup} from "../../../Util/Common";

const SIZE_ID = 3;

export interface IModalComponent {
    id: string
    component: any
}
interface IModalList {
    name: string
    type: string
    list: IModalComponent[]
}

export interface IModals {
    isExist: boolean
    modalList: IModalList[]
}

const Modals: IModals = {
    isExist: false,
    modalList: [
        // {
        //     name: "level0",
        //     type: '+',
        //     list: [
        //         {
        //             id: "0",
        //             component: LoginSettings_Modal
        //         },
        //         {
        //             id: "1",
        //             component: LoginSettings_Modal
        //         },
        //         {
        //             id: "2",
        //             component: LoginSettings_Modal
        //         },
        //     ]
        // },
        // {
        //     name: "level1",
        //     type: '+',
        //     list: [
        //         {
        //             id: "4",
        //             component: LoginSettings_Modal
        //         },
        //         {
        //             id: "5",
        //             component: ()=>(<div>Lorem ipsum dolor <br/> sit amet, consectetur<br/> adipisicing elit.<br/> Excepturi, nulla.</div>)
        //         },
        //     ]
        // },
        // {
        //     name: "level2",
        //     type: '+',
        //     list: [
        //         {
        //             id: "4",
        //             component: LoginSettings_Modal
        //         },
        //     ]
        // }
    ]
}
export default Modals


export const MODAL_REFRESH = "MODAL_REFRESH";
//Открываем модальное окно
export const modalOpen = (component:any,name:string='level0') => (dispatch:any,state:IAppState) => {
    let newModals:IModals = dup(state.modals);   //глубокко копирование
    newModals.isExist = true;
    let findID = newModals.modalList.findIndex(elem=>elem.name===name);
    if(findID===-1){   //если есть такой блок
        newModals.modalList.push({
            name, type:"+", list:[]
        });
        findID = newModals.modalList.length-1;
    }
    newModals.modalList[findID].list.push({
        component,id:nanoid(SIZE_ID)
    });
    console.log(newModals);
    dispatch({type:MODAL_REFRESH,payload:newModals})
};
//Закрываем модальное окно
export const modalClose = (name?:string) => (dispatch:any,state:IAppState) => {
    console.log('xxx',state.modals,dup(state.modals));
    let newModals:IModals = dup(state.modals);      //новый объект модал
    console.log('do',dup(newModals.modalList));
    if(name){
        newModals.modalList = newModals.modalList.filter(elem=>{ return elem.name !== name})
    }else{
        newModals.modalList.splice(newModals.modalList.length-1,1)
    }
    console.log('do',dup(newModals.modalList));
    if(newModals.modalList.length){
        newModals.isExist = true
    }else{
        newModals.isExist = false
    }
    dispatch({type:MODAL_REFRESH,payload:newModals});
};
//Шаг назад в модалке
export const modalBack = () => (dispatch:any,state:IAppState) => {
    let newModals:IModals = {...state.modals,isExist: false};      //новый объект модал
    newModals.modalList = [];
    const list = state.modals.modalList;   //список БЛОКОВ МОДАЛОК
    let del = true; //нужно удалить последний элемент
    for(let i=list.length-1;i>=0;i--){
        //копирование DEEP
        let modals = {...list[i]};
        if(!modals.list.length){ modals.list = []}
        else {
            modals.list = modals.list.map(elem=>{
                return {...elem}
            })
        }
        //удаляем последний элемент, если нужно
        if(del && modals.list.length){
            modals.type = '-';
            modals.list.splice(modals.list.length-1,1);
            del = false;
        }
        //проверяем новый статус
        if(modals.list.length) {
            newModals.isExist = true;
            newModals.modalList.push(modals)
        }
    }
    newModals.modalList = newModals.modalList.reverse();
    dispatch({type:MODAL_REFRESH,payload:newModals})
};