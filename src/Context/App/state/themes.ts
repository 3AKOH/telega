export interface ITheme {
    [index: string]: any
}
interface IThemeButtons {
    main: any
    nav: any
    icon: any
}
interface IThemeWindow {
    [index: string]: any
}
type IThemeList = { [index: string]: ITheme }
const ListThemes:IThemeList = {
    "default": {
        button: {
            "main":{
                mainColor: "yellow",
                colorText: "#fff",
                colorButton: "red",
                colorButton2: "green",
                rapper: "blue"
            },
            "nav":{
                mainColor: "yellow",
                colorText: "#fff",
                colorButton: "red",
                colorButton2: "green",
                rapper: "blue"
            },
            "icon":{
                mainColor: "yellow",
                colorText: "#fff",
                colorButton: "red",
                colorButton2: "green",
                rapper: "blue"
            },
        },
        window: {
            test1: "string",
            test2: "string",
        },
    },
    "dark": {
        app: {
            body: "#282e33"
        },
        button: {
            "main":{
                bg: "#4888c0",
                bgHover: "#4f92c9",
                color: "#fff",
                colorHover: "#fff",
                rapper: "#a6c0dc",
                radius: 100
            },
            "nav":{
                bg: "#282e33",
                bgHover: "rgb(65, 72, 78)",
                color: "#91c7f6",
                colorHover: "#91c7f6",
                rapper: "rgb(183, 192, 199)",
                radius: 100
            },
            "icon":{
                bg: "transparent",
                bgHover: "rgb(65, 72, 78)",
                fill: "red",
                fillHover: "green",
                rapper: "rgb(183, 192, 199)",
                radius: 100,
                // className:"zakon"
            },
        },
        window: {
            test1: "string",
            test2: "string",
        },
    },
}
//////////==================================================

export interface IThemes {
    curTheme: string
    list: IThemeList
}


class Themes implements IThemes{
    curTheme: string;
    list: { [index: string]: ITheme };
    constructor(name=""){
        this.curTheme = name;
        this.list = ListThemes
    }
};
export const ThemesDefault = new Themes;
export default Themes;