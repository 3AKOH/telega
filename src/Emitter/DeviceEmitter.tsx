import React from 'react';
import {EventEmitter} from "events";


//инит блока!
const deviceRender = () => {
    let div = document.createElement('div');
    div.setAttribute('id','device');
    Object.assign(div.style,{position:"absolute",left:"-1000%",visibility:"hidden"});
    // div.style.position = 'absolute';
    // div.style.left = '-1000%';
    // div.style.visibility = 'hidden';
    ['vis-x','vis-xx','vis-xxt','vis-xxtt','vis-xxttd','vis-xxttdd'].forEach(elem => {
        let n = document.createElement('div');
        n.classList.add(elem);
        n.innerText = elem;
        div.append(n);
    });
    document.body.append(div);
    return div;
};
//инит блока!
const scrollWidthRender = () => {
    let div = document.createElement('div');
    div.setAttribute('id','scrollWidth');
    Object.assign(div.style,{position:"absolute",left:"-1000%",visibility:"hidden",overflowY:'scroll',width:'50px',height:'50px'});
    document.body.append(div);
    // const width = div.offsetWidth - div.clientWidth;
    return div
};
export interface IDeviceEmitter {name:string,type:number}
export interface IDeviceEmitterData {scroll:number,device:IDeviceEmitter}
class DeviceEmitter extends EventEmitter{
    on: (x:('SCROLL_WIDTH'|'DEVICE'|'RESIZE')|string,s:(...args: any[]) => void)=>this;
    id: {
        scroll:HTMLDivElement,
        device:HTMLDivElement,
    };
    data: IDeviceEmitterData;
    constructor(){
        super();
        this.on = super.on;
        this.id = {
            scroll: scrollWidthRender(),
            device: deviceRender(),
        };
        //runs
        this.data = {
            scroll: this.updateScrollWidth(),
            device: this.updateDevice(),
        };
        window.addEventListener("resize", this.resizeEvent);
    }
    resizeEvent = (e:EventModifierInit) =>{
        if(this.data.scroll !== this.updateScrollWidth()){
            const x = this.updateScrollWidth();
            this.data.scroll = x;
            this.emit('SCROLL_WIDTH',x);
        }
        //device
        const dev = this.updateDevice();
        if(this.data.device.type !== dev.type){
            this.data.device = dev;
            this.emit('DEVICE',dev);
        }
        //resize
        this.emit('RESIZE',this.data);
    };
    //вычисляем девайс
    updateScrollWidth = () => {
        return this.id.scroll.offsetWidth - this.id.scroll.clientWidth;
    };
    updateDevice = () => {
        let element = {name:'',type:-1};
        Array.from(this.id.device.children).find((el: any, index) => {
            if (getComputedStyle(el).display !== 'none') {
                element.name = (el.textContent.indexOf('d') !== -1) ? 'd' : (el.textContent.indexOf('t') !== -1) ? 't' : 'x';
                element.type = index+1;
                return true
            }
            return false
        });
        return element;
    };
    getData = () => this.data;
    getScrollWidth = () => this.data.scroll;
    getDevice = () => this.data.device;
}
export default new DeviceEmitter;


