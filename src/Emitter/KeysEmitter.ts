import {EventEmitter} from "events";

class KeysEmitter extends EventEmitter{
    constructor(){
        super();
        window.history.pushState(null, '', window.location.href);
        document.addEventListener("keydown", async e => {
            this.makeEvent(e);
        });
    }
    makeEvent = (e:any) => {
        // console.log(e);
        switch(e.keyCode){
            case 27:
                this.emit("ESC");
                break;
            case 38:
                if(e.altKey) this.emit("UP");
                break;
            case 37:
                if(e.altKey) this.emit("PREV");
                break;
            case 39:
                if(e.altKey) this.emit("NEXT");
                break;
            case 40:
                if(e.altKey) this.emit("DOWN");
                break;
            case 79:
                if(e.altKey) this.emit("SETTINGS");
                break;
            case 166:
                this.emit("BACK");
                e.preventDefault();
                return false;
                break;
            default:
                return true;
        }
        e.preventDefault();
        e.stopPropagation()
    };
}

export default new KeysEmitter;