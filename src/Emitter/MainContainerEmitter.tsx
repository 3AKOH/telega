import React from 'react';
import {EventEmitter} from "events";


export interface IMainContainerEmitter {name:string,type:number}
class MainContainerEmitter extends EventEmitter{
    on: (x:('UPDATE')|string,s:(...args: any[]) => void)=>this;
    width:number;
    constructor(){
        super();
        this.width = 0;
        this.on = super.on;
    }
    setWidth = (width:number) =>{
        if(!width || this.width !== width){
            this.width = width;
            this.emit('UPDATE',width);
        }
    }
}
export default new MainContainerEmitter;


