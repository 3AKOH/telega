import {EventEmitter} from "events";
import {dup} from "../Util/Common";

interface IMove {
    x: number
    y: number
}
enum TypeOn {
    CLICK="CLICK",
    TOUCH="TOUCH"
}

export interface IPointerEmitter {
    on:boolean
    type: 'click'|'touch'|''
    wind: {
        x0: number
        x1: number
        y0: number
        y1: number
    }       //координаты двигающейся точки
    move: IMove         //измененение точки относительно начальной
    speed: IMove        //скорость свайпа во время движения
    speedEnd: IMove     //скорость свайпа в конце клика!
    maxWindow: number
    pointInit: IMove,
    preMove:IMove
}
const dataDefault:IPointerEmitter = {
    on: false,
    type: '',         //тип
    wind: {           //сдвиг относительно окна
        x0: 0,
        x1: 0,
        y0: 0,
        y1: 0,
    },
    move: {             //на сколько сдвинули курсор
        x: 0,
        y: 0,
    },
    speed:{
        x: 0,
        y: 0,
    },
    speedEnd:{
        x: 0,
        y: 0,
    },
    maxWindow: 0,
    pointInit: {
        x: 0,
        y: 0,
    },
    preMove:{
        x: 0,
        y: 0,
    }
};



class PointerEmitter extends EventEmitter{
    on: (x:('DOWN'|'UP'|'MOVE')|string,s:(...args: any[]) => void)=>this;
    data:IPointerEmitter;
    constructor(){
        super();
        this.on = super.on;
        window.addEventListener('mousedown',this.down);
        window.addEventListener('mouseup',this.up);
        // window.addEventListener('touchstart',this.down);
        // window.addEventListener('touchend',this.up);
        this.data = dup(dataDefault);
    }
    down = (e:any) => {
        //определяем payload для главного окна!
        // this.data = dup(dataDefault);
        const style = getComputedStyle(document.body).width;
        this.data = {
            ...dup(dataDefault),
            maxWindow: parseInt(style || "",10),
            on: true,
            type: e.type==='mousedown'? "click" : "touch",
            preMove: {
                x: e.pageX,
                y: e.pageY,
            },
            pointInit: {
                x: e.pageX,
                y: e.pageY,
            },
            wind: {
                x0: e.pageX,
                x1: document.body.clientWidth-e.pageX,
                y0: e.pageY,
                y1: document.body.clientHeight-e.pageX,
            },
        };
        this.emit("DOWN",this.data);
        window.addEventListener('mousemove',this.update);
        // window.addEventListener('touchmove',this.update);
    };
    up = () => {
        //сбрасываем все на дефолтные настройки
        this.data.on = false;
        window.removeEventListener('mousemove',this.update);
        // window.removeEventListener('touchmove',this.update);
        this.emit("UP",this.data)
    };
    update = (e: any) => {
        // e.preventDefault();
        e.stopPropagation();
        //удаляем собетие если выход
        if (e.type==='click' && !e.which && !e.buttons){
            this.up();
            return
        }
        this.data = {
            ...this.data,
            wind: {
                x0: e.pageX,
                x1: document.body.clientWidth-e.pageX,
                y0: e.pageY,
                y1: document.body.clientHeight-e.pageX,
            },
            speed: {
                x: Math.abs(e.pageX-this.data.preMove.x),
                y: Math.abs(e.pageY-this.data.preMove.y)
            },
            move: {
                x: e.pageX-this.data.pointInit.x,
                y: e.pageY -this.data.pointInit.y,
            }
        };
        // console.log(e);
        //записываем новые данные - для скоркости
        this.data.preMove = {
            x: e.pageX,
            y: e.pageY,
        };
        this.emit("MOVE",this.data)
    };
}

export default new PointerEmitter;