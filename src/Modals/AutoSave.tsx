import React, {useEffect, useState} from "react";
import {FormSpy} from "react-final-form";
// @ts-ignore
import diff from 'object-diff'

export const AutoSave:React.FC<any> = (props)=>{
    const [local, setLocal] = useState({
        values: props.values
    });
    useEffect(()=>{
        const { values, send } = props;
        const difference = diff(local.values, values);
        if (Object.keys(difference).length) {
            setLocal({...local, values });
            send(difference)
        }
    });
    return null
};
const SKY:React.FC<any> = (props) => <FormSpy {...props} component={AutoSave} subscription={{values: true}} />;
export default SKY;