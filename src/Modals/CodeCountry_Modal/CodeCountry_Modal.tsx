import React, {createRef, useEffect, useState} from 'react';
import InfoWrap from "../../Components/InfoWrap/InfoWrap";
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import RadioWrapsDefault from "../../Components/UI/Layouts/RadioWraps/RadioWrapsDefault/RadioWrapsDefault";
import ModalHeader from "../Elements/ModalHeader/ModalHeader";
import ModalLink from "../ModalLink/ModalLink";
import LoginSettings_Modal2 from "../LoginSettings_Modal2/LoginSettings_Modal2";
import {FixedSizeList} from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";
import ModalFooter from "../Elements/ModalFooter/ModalFooter";
import RippleX from "../../Components/UI/RippleX/RippleX";
import '../Modal.scss';
import {ICodeContry} from "./Codes";
import InputXX from "../../Components/UI/Input/InputXX/InputXX";
import {Field} from "react-final-form";
import {dup} from "../../Util/Common";
import InputX from "../../Components/UI/Input/InputX/InputX";
import BoxScrollVirtual from "../../Components/BoxScroll/BoxScrollVirtual/BoxScrollVirtual";


interface IProps {
    onClose: any
    onChange?: any
    data: ICodeContry[]
}
interface IState {
    search: string,
    filterData: ICodeContry[]
}


const CodeCountry_Modal: React.FC<IProps> = (props) => {
    const [state, setState] = useState<IState>({
        search: '',
        filterData: props.data
    });
    const onChangeSearch = (e:any) => {
        if(e.target.value){
            setState({
                search: e.target.value,
                filterData: props.data.filter(elem=>{
                    return elem.s.slice(0,e.target.value.length).toLowerCase() === e.target.value.toLowerCase()
                })
            })
        }else{
            setState({
                search:'',
                filterData: props.data
            })
        }
    };
    const {onClose,data,onChange} = props;
    const Country = ({index, style}:any) => {
        const onSelect = (d:any) => {
            onChange(d);
            onClose();
        };
        return (
            <RippleX style={{...style}} className='CodeCountry_Modal__item' onClick={()=>onSelect(state.filterData[index])}>
                <b>{state.filterData[index].s}</b><span>+{state.filterData[index].i}</span>
            </RippleX>
        )
    };
    return (
        <>
            <div className='Modal__head'>
                <ModalHeader label='Выберите страну'/>
            </div>
            <div className='Modal__body' style={{height: 700}}>
                <InputX name='xx' defaultValue='' placeholder='Укажите страну' onChange={onChangeSearch} className='CodeCountry_Modal__search' autocomplete='off' focus={true}/>
                <BoxScrollVirtual className='Modal__content' scrollWidth={17} itemSize={40} itemCount={state.filterData.length}>
                    {Country}
                </BoxScrollVirtual>
            </div>
            <div className='Modal__footer'>
                <ModalFooter>
                    <ButtonX onClick={onClose} type='nav'>Закрыть</ButtonX>
                </ModalFooter>
            </div>
        </>
    );
};

export default CodeCountry_Modal;

