import React from 'react';
import './ModalFooter.scss';
import ButtonX from "../../../Components/UI/Button/ButtonX/ButtonX";
import IconX from "../../../Components/UI/Icon/IconX";

interface IProps {}

const ModalFooter:React.FC<IProps> = ({children}) => {
    return (
        <div className='ModalFooter'>
            {children}
        </div>
    );
};

export default ModalFooter;