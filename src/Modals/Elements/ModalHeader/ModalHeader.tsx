import React, {useState} from 'react';
import './ModalHeader.scss';
import ButtonX from "../../../Components/UI/Button/ButtonX/ButtonX";
import IconX from "../../../Components/UI/Icon/IconX";
import ClassName from "classnames";
import MiniMenu from "../../../Components/MiniMenu/MiniMenu";
import MiniMenuItem from "../../../Components/MiniMenu/MiniMenuItem/MiniMenuItem";

interface IProps {
    label?:string
    onClose?: any
    onBack?: any
    menu?: any
}

const ModalHeader:React.FC<IProps> = ({label,onClose,onBack,menu}) => {
    const [minimenu, setMinimenu] = useState(false);
    const handleToggleMinimenu = () =>{
        setMinimenu(!minimenu)
    }
    const handleClick = () =>{
        setMinimenu(false)
    };
    return (
        <div className='ModalHeader'>
            {
                onBack && (
                    <div className='ModalHeader__b'>
                        <ButtonX onClick={onBack} type='icon'><IconX name='icon_back'/></ButtonX>
                    </div>
                )
            }
            <div className='ModalHeader__l'>
                {label}
            </div>
            {
                onClose && (
                    <div className='ModalHeader__r'>
                        {
                            menu && (
                                <div style={{position:"relative"}}>
                                    <ButtonX type='icon' className={ClassName({'-active':minimenu})} onClick={handleToggleMinimenu}><IconX name='icon_dop'/></ButtonX>
                                    <MiniMenu isOpen={minimenu} onClose={handleToggleMinimenu}>
                                        {menu()}
                                    </MiniMenu>
                                </div>
                            )
                        }
                        <ButtonX onClick={onClose} type='icon'><IconX name='icon_close'/></ButtonX>
                    </div>
                )
            }
        </div>
    );
};

export default ModalHeader;