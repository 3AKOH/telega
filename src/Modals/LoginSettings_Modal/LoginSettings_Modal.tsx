import React, {createRef, useEffect, useState} from 'react';
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import RadioWrapsDefault from "../../Components/UI/Layouts/RadioWraps/RadioWrapsDefault/RadioWrapsDefault";
import ModalHeader from "../Elements/ModalHeader/ModalHeader";
import ModalLink from "../ModalLink/ModalLink";
import LoginSettings_Modal2 from "../LoginSettings_Modal2/LoginSettings_Modal2";
import CodeCountry_Modal from "../CodeCountry_Modal/CodeCountry_Modal";
import BoxScroll from "../../Components/BoxScroll/BoxScroll/BoxScroll";
import LangStore from "../../Store/LangStore";
import {VAR_SETTINGS} from "../../vars";
import CheckboxX from "../../Components/UI/Checkbox/CheckboxX/CheckboxX";
import Avatar from "../../Components/Avatar/Avatar";
import InfoAvatar from "../../Components/InfoWrap/InfoAvatar/InfoAvatar";
import InfoWrap from "../../Components/InfoWrap/InfoWrap";
import InfoLinkItem from "../../Components/InfoWrap/InfoLinkList/InfoLinkItem/InfoLinkItem";
import InfoLink from "../../Components/InfoWrap/InfoLink/InfoLink";
import RippleX from "../../Components/UI/RippleX/RippleX";
import MiniMenuItem from "../../Components/MiniMenu/MiniMenuItem/MiniMenuItem";
import MiniMenu from "../../Components/MiniMenu/MiniMenu";

// import '../Modal.scss';


interface IProps {
    onClose?: any
    onBack?: any
}

const LoginSettings_Modal: React.FC<IProps> = ({onClose, onBack}) => {
    return (
        <>
            <div className='Modal__head'>
                <ModalHeader label={LangStore.getString('SETTINGS', null, VAR_SETTINGS)} onClose={onClose} menu={() => (
                    <>
                        <MiniMenuItem onClick={() => {}} label='Покинуть канал'/>
                        <MiniMenuItem onClick={() => {}} label='Пожаловаться'/>
                    </>
                )}/>
            </div>
            <div className='Modal__body'>
                <BoxScroll className='Modal__content'>
                    <InfoWrap top={true} bot={true}>
                        <ModalLink link={(d: any) => <LoginSettings_Modal2 {...d}/>} nameLevel='level1'>
                            <ButtonX type='link' component='label' block={true}>
                                <div className='blockLink'>
                                    <div className='blockLink__l'>
                                        Язык
                                    </div>
                                    <div className='blockLink__r'>
                                        Русский
                                    </div>
                                </div>
                            </ButtonX>
                        </ModalLink>
                        <ButtonX type='link' component='label' block={true}>
                            <div className='blockLink'>
                                <div className='blockLink__l'>
                                    <div>Обновлять автоматически</div>
                                    <small>Версия 1.8.5</small>
                                </div>
                                <div className='blockLink__r'>
                                    <CheckboxX type='checkbox' checked={true} onChange={() => {
                                    }}/>
                                </div>
                            </div>
                        </ButtonX>
                    </InfoWrap>
                    <InfoWrap bot={true}>
                        <RadioWrapsDefault label='Показать иконку в трее' checked={true}/>
                        <RadioWrapsDefault label='Показать иконку на панели задач' checked={true}/>
                        <RadioWrapsDefault label='Запустить телеграм при запуске системы Запустить телеграм при запуске системы'/>
                    </InfoWrap>
                    <InfoWrap>
                        <ButtonX type='link' component='label' block={true}>
                            <div className='blockLink'>
                                <div className='blockLink__l'>
                                    Вопросы о Telegram
                                </div>
                            </div>
                        </ButtonX>
                    </InfoWrap>
                </BoxScroll>
            </div>
            <div className='Modal__footer'>
                {/*<ModalHeader label='Кончик'/>*/}
            </div>
        </>
    );
};

export default LoginSettings_Modal;