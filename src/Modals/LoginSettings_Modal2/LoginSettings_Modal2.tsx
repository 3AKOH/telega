import React, {createRef, useEffect, useState} from 'react';
import InfoWrap from "../../Components/InfoWrap/InfoWrap";
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import RadioWrapsDefault from "../../Components/UI/Layouts/RadioWraps/RadioWrapsDefault/RadioWrapsDefault";
import ModalHeader from "../Elements/ModalHeader/ModalHeader";
import ModalLink from "../ModalLink/ModalLink";
import BoxScroll from "../../Components/BoxScroll/BoxScroll/BoxScroll";
// import '../Modal.scss';


interface IProps {
    onBack?: any
    onClose?: any
}
const LoginSettings_Modal2: React.FC<IProps> = ({onClose,onBack}) => {
    return (
        <>
            <div className='Modal__head'>
                <ModalHeader label='Языки' onClose={onClose}/>
            </div>
            <div className='Modal__body'>
                <BoxScroll className='Modal__content' >
                    <InfoWrap bot={true}>
                        <RadioWrapsDefault label='Показать иконку в трее' checked={true}/>
                        <RadioWrapsDefault label='Показать иконку на панели задач' checked={true}/>
                        <RadioWrapsDefault label='Запустить телеграм при запуске системы Запустить телеграм при запуске системы'/>
                        <RadioWrapsDefault label='Запускать в свернутом виде'/>
                        <RadioWrapsDefault label='Добавить меню "Отправить..."'/>
                    </InfoWrap>
                    <InfoWrap top={true} bot='Lorem ipsum dolor sit amet, consectetur adipisicing elit.' botPadding={true}>
                        <ModalLink link={LoginSettings_Modal2} nameLevel='level0'>
                            <ButtonX type='link' component='label' block={true}>
                                <div className='blockLink'>
                                    <div className='blockLink__l'>
                                        Язык
                                    </div>
                                    <div className='blockLink__r'>
                                        Русский
                                    </div>
                                </div>
                            </ButtonX>
                        </ModalLink>
                        <ModalLink link={()=>(<div>SEX<br/>SEX<br/>SEX<br/>SEX<br/>SEX<br/>SEX<br/>SEX<br/>SEX<br/>SEX<br/>SEX<br/></div>)} nameLevel='level1'>
                            <ButtonX type='link' component='label' block={true}>
                                <div className='blockLink'>
                                    <div className='blockLink__l'>
                                        Язык
                                    </div>
                                    <div className='blockLink__r'>
                                        Русский
                                    </div>
                                </div>
                            </ButtonX>
                        </ModalLink>
                    </InfoWrap>
                    <InfoWrap>
                        <ButtonX type='link' component='label' block={true}>
                            <div className='blockLink'>
                                <div className='blockLink__l'>
                                    Вопросы о Telegram
                                </div>
                            </div>
                        </ButtonX>
                    </InfoWrap>
                </BoxScroll>
            </div>
        </>
    );
};

export default LoginSettings_Modal2;