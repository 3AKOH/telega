import React, {useEffect} from 'react';
import ClassName from 'classnames';
import './Modal.scss'
import Backdrop from "../Components/Backdrop/Backdrop";
import KeysEmitter from "../Emitter/KeysEmitter";

interface IProps2 {
    bgModalColor?: boolean
    component: any
    className?: string
    onClose?: any
    isOpen?: boolean
    backDropColor?: boolean
}

const Modal:React.FC<IProps2> = ({component,className,bgModalColor=false,onClose=()=>{},isOpen=true,backDropColor=true}) => {
    return (
        <div className='ModalContainer'>
            <div className='ModalContainer__wrap'>
                <div className='ModalContainer__in'>
                    {/*<Backdrop onClick={onClose} isOpen={isOpen} timeout={300} classNames='show' style={{background:`${backDropColor ?'':'none'}`}}/>*/}
                    <div className='ModalContainer__top'/>
                    <div className='ModalContainer__body'>
                        <div className={ClassName('Modal',{'-bg':bgModalColor})}>
                            {component}
                        </div>
                    </div>
                    <div className='ModalContainer__bot'/>
                </div>
            </div>
        </div>
    );
};

interface IProps {
    onClose: any
}
export const GetModal = (Component:any) => (props:IProps) => {
    const {onClose} = props;
    return (
        <div className='ModalContainer'>
            <div className='ModalContainer__wrap'>
                <div className='ModalContainer__in'>
                    {/*<Backdrop onClick={onClose} isOpen={true} timeout={300} classNames='show' />*/}
                    <div className='ModalContainer__top'/>
                    <div className='ModalContainer__body'>
                        <div className={ClassName('Modal')}>
                            <Component {...props}/>
                        </div>
                    </div>
                    <div className='ModalContainer__bot'/>
                </div>
            </div>
        </div>
    )
}
export const GetModal2 = (Component:any) => (props:IProps) => {
    return (
        <Component {...props}/>
    )
}

export default Modal;