import React, {createElement, useContext} from 'react';
import {AppContext} from "../../Context/App/AppState";
import {modalOpen} from "../../Context/App/state/modals";

interface IProps {
    link: any
    component?: any
    nameLevel?: string
    done?: any
    [idx: string]: any
}

//открываем наш компонент как модальное окно
const ModalLink: React.FC<IProps> = (props) => {
    const {component, children, link, nameLevel = 'level0'} = props;
    const {dispatch} = useContext(AppContext);
    const onClick = () => {
        dispatch(modalOpen(link, nameLevel));
    };
    return (
        <>
            {
                React.createElement(
                    component || 'div',
                    {onClick: onClick, ...props},
                    children
                )
            }
        </>
    );
};

export default ModalLink;