import React, {useEffect, useState} from 'react';
import Backdrop from "../Components/Backdrop/Backdrop";
import ClassName from "classnames";
import {IModalComponent, IModals} from "../Context/App/state/modals";
import TransitionGroup from "react-transition-group/TransitionGroup";
import CSSTransition from "react-transition-group/CSSTransition";
import {GetModal, GetModal2} from "./Modal";
import BlockModify from "../Components/BlockModify/BlockModify";

interface IProps {
    onBack: any
    onClose: any
    data: IModalComponent
    anime?:string
    isBg?:boolean
    [inx: string]: any
}

const Modals: React.FC<IProps> = (props) => {
    const [state, setState] = useState(()=>{
        // console.log('RENDER');
        return false;
    });
    const {onClose,onBack, data,anime='show',className,isBg=false} = props;
    useEffect(() => {
        // console.log('RENDER2');
        setState(prev=>{
            // console.log('props', props,prev);
            return props.in
        })
    }, [props.in])
    let speedAnime = 200;
    return (
        <CSSTransition timeout={{enter:0,exit:speedAnime}} in={props.in} mountOnEnter unmountOnExit>
            <div className='ModalContainer'>
                <div className={ClassName('ModalContainer__wrap',className)}>
                    <Backdrop onClick={onClose} isOpen={state} timeout={speedAnime} classNames='show' bgColor={false}/>
                    <CSSTransition timeout={speedAnime} classNames={anime} in={state}>
                        <div className='ModalContainer__in'>
                            <div className='ModalContainer__top'/>
                            <div className='ModalContainer__body'>
                                <div className='ModalContainer__dopClose' onClick={onClose}/>
                                <div className={ClassName('Modal',className,{'-bg':isBg})}>
                                    {data.component({
                                        onClose: onClose,
                                        onBack: onBack,
                                    })}
                                </div>
                            </div>
                            <div className='ModalContainer__bot'/>
                        </div>
                    </CSSTransition>
                </div>
            </div>
        </CSSTransition>
    );
};

export default Modals;