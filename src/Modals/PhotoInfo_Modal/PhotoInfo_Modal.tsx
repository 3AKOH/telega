import React, {createRef, useEffect, useState} from 'react';
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import RadioWrapsDefault from "../../Components/UI/Layouts/RadioWraps/RadioWrapsDefault/RadioWrapsDefault";
import ModalHeader from "../Elements/ModalHeader/ModalHeader";
import ModalLink from "../ModalLink/ModalLink";
import LoginSettings_Modal2 from "../LoginSettings_Modal2/LoginSettings_Modal2";
import CodeCountry_Modal from "../CodeCountry_Modal/CodeCountry_Modal";
import BoxScroll from "../../Components/BoxScroll/BoxScroll/BoxScroll";
import LangStore from "../../Store/LangStore";
import {VAR_PHOTOS, VAR_SETTINGS} from "../../vars";
import CheckboxX from "../../Components/UI/Checkbox/CheckboxX/CheckboxX";
import Avatar from "../../Components/Avatar/Avatar";
import InfoAvatar from "../../Components/InfoWrap/InfoAvatar/InfoAvatar";
import InfoWrap from "../../Components/InfoWrap/InfoWrap";
import InfoLinkItem from "../../Components/InfoWrap/InfoLinkList/InfoLinkItem/InfoLinkItem";
import InfoLink from "../../Components/InfoWrap/InfoLink/InfoLink";
import RippleX from "../../Components/UI/RippleX/RippleX";
import MiniMenuItem from "../../Components/MiniMenu/MiniMenuItem/MiniMenuItem";
import MiniMenu from "../../Components/MiniMenu/MiniMenu";

// import '../Modal.scss';


interface IProps {
    onClose?: any
    onBack?: any
}

const PhotoInfo_Modal: React.FC<IProps> = ({onClose, onBack}) => {
    return (
        <>
            <div className='Modal__head'>
                <ModalHeader label={LangStore.getString('SETTINGS', null, VAR_PHOTOS)} onClose={onClose} onBack={onBack}/>
            </div>
            <div className='Modal__body'>
                <BoxScroll className='Modal__content'>
                    dsds
                </BoxScroll>
            </div>
            <div className='Modal__footer'>
                {/*<ModalHeader label='Кончик'/>*/}
            </div>
        </>
    );
};

export default PhotoInfo_Modal;