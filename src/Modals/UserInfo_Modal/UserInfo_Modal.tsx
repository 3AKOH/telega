import React, {createRef, useEffect, useState} from 'react';
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import RadioWrapsDefault from "../../Components/UI/Layouts/RadioWraps/RadioWrapsDefault/RadioWrapsDefault";
import ModalHeader from "../Elements/ModalHeader/ModalHeader";
import ModalLink from "../ModalLink/ModalLink";
import LoginSettings_Modal2 from "../LoginSettings_Modal2/LoginSettings_Modal2";
import CodeCountry_Modal from "../CodeCountry_Modal/CodeCountry_Modal";
import BoxScroll from "../../Components/BoxScroll/BoxScroll/BoxScroll";
import LangStore from "../../Store/LangStore";
import {VAR_SETTINGS} from "../../vars";
import CheckboxX from "../../Components/UI/Checkbox/CheckboxX/CheckboxX";
import Avatar from "../../Components/Avatar/Avatar";
import InfoAvatar from "../../Components/InfoWrap/InfoAvatar/InfoAvatar";
import InfoWrap from "../../Components/InfoWrap/InfoWrap";
import InfoLinkItem from "../../Components/InfoWrap/InfoLinkList/InfoLinkItem/InfoLinkItem";
import InfoLink from "../../Components/InfoWrap/InfoLink/InfoLink";
import RippleX from "../../Components/UI/RippleX/RippleX";
import MiniMenuItem from "../../Components/MiniMenu/MiniMenuItem/MiniMenuItem";
import MiniMenu from "../../Components/MiniMenu/MiniMenu";
import PhotoInfo_Modal from "../PhotoInfo_Modal/PhotoInfo_Modal";

// import '../Modal.scss';


interface IProps {
    onClose?: any
    onBack?: any
}

const UserInfo_Modal: React.FC<IProps> = ({onClose, onBack}) => {
    return (
        <>
            <div className='Modal__head'>
                <ModalHeader label={LangStore.getString('SETTINGS', null, VAR_SETTINGS)} onClose={onClose} menu={() => (
                    <>
                        <MiniMenuItem onClick={() => {}} label='Покинуть канал'/>
                        <MiniMenuItem onClick={() => {}} label='Пожаловаться'/>
                    </>
                )}/>
            </div>
            <div className='Modal__body'>
                <BoxScroll className='Modal__content'>
                    <InfoAvatar
                        avatar={() => <Avatar url={null} size='big' txt='T' bg='T'/>}
                        title='TON Developers [ru]'
                        desc='511 участников'
                    />
                    <InfoWrap top={true}>
                        <InfoLink icon_name='icon_info' hover={false} flexTop={true}>
                            <div className='InfoUserText'>
                                <div className='InfoUserText__title'>
                                    <a href='#ds'>t.me/TONgramDev</a>
                                    <div>Ссылка</div>
                                </div>
                                <div className='InfoUserText__body'>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci animi deleniti eaque et illum incidunt natus nostrum provident quod!
                                    <br/>
                                    <br/>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci animi deleniti eaque et illum incidunt natus nostrum provident quod!
                                    <br/>
                                    <br/>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci animi deleniti eaque et illum incidunt natus nostrum provident quod!
                                </div>
                                <div className='InfoUserText__end'>Информация</div>
                            </div>
                        </InfoLink>
                        <InfoLink component={RippleX} icon_name='icon_kolokol' end={() => <CheckboxX onChange={() => {
                        }} checked={true}/>}>Уведомления</InfoLink>
                        <div className='InfoButton'>
                            <ButtonX type='nav' block={true}>
                                Открыть канал
                            </ButtonX>
                        </div>
                    </InfoWrap>
                    <InfoWrap top={true}>
                        <ModalLink link={(d:any)=><PhotoInfo_Modal {...d}/>}>
                            <InfoLink component={RippleX} icon_name='icon_img' >106 фотографий</InfoLink>
                        </ModalLink>
                        <InfoLink component={RippleX} icon_name='icon_video'>4 видео</InfoLink>
                        <InfoLink component={RippleX} icon_name='icon_file'>18 файлов</InfoLink>
                        <InfoLink component={RippleX} icon_name='icon_link'>294 ссылки</InfoLink>
                    </InfoWrap>
                    <InfoWrap top={true}>
                        <InfoLink component={RippleX} icon_name='icon_menu2'>Поделиться контактом</InfoLink>
                        <InfoLink component={RippleX} icon_name='icon_empty'>Изменить контакт</InfoLink>
                        <InfoLink component={RippleX} icon_name='icon_empty'>Удалить контакт</InfoLink>
                        <InfoLink component={RippleX} icon_name='icon_empty'>Очистить историю</InfoLink>
                        <InfoLink component={RippleX} icon_name='icon_empty'>Удалить сообщение</InfoLink>
                        <InfoLink component={RippleX} icon_name='icon_empty'>
                            <div className='color_red'>Заблокировать</div>
                        </InfoLink>
                    </InfoWrap>
                </BoxScroll>
            </div>
            <div className='Modal__footer'>
                {/*<ModalHeader label='Кончик'/>*/}
            </div>
        </>
    );
};

export default UserInfo_Modal;