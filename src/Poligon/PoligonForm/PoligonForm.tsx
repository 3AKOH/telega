import React, {Component, createContext, useContext, useEffect, useState} from 'react';
import {Field, Form} from "react-final-form";
import ClassName from "classnames";
import CheckboxX from "../../Components/UI/Checkbox/CheckboxX/CheckboxX";

// import

interface IProps {
}


// const Form = (
//     <Form onSubmit={handleSubmit}>
//         <label>
//             <input type='text' defaultValue='Веталь'/>
//         </label>
//         <label>
//             <input type='text' defaultValue='Веталь'/>
//         </label>
//         <button>отправить</button>
//     </Form>
// )

const sleep = (ms:any) => new Promise(resolve => setTimeout(resolve, ms))

const onSubmit = async (values:any) => {
    await sleep(300)
    window.alert(JSON.stringify(values, null, 2))
}

const CMyContext = createContext({data:{name:"zakon",age:27,holost:true},setState:(d?:any)=>{}});

const FN:React.FC<IProps> = ({input:{value,onChange}}:any) =>{
    return (
        <div>xaxa</div>
    )
}
const SSS:React.FC<IProps> = (props:any) => {
    const {className,input:{type,checked,onChange,value}} = props;

    return (
        <label className={ClassName('CheckboxX',className,'-'+'checkbox',{"-checked":checked})} >
            <input
                type={type}
                onChange={(e:any)=>onChange(value)}
                checked={checked}
            />
        </label>
    );
};

const MyHeader:React.FC = () => {
    const {data} = useContext(CMyContext)
    return (
        <div>
            <div>{data.name}</div>
        </div>
    )
}


const MyForm:React.FC = () => {
    const cxt = useContext(CMyContext);
    const send = (data:any)=>{
        cxt.setState({...cxt.data,...data})
    };
    return (
        <CMyContext.Consumer>
            {(d:any)=>{
                return (
                    <Form onSubmit={()=>{}}
                        initialValues={{name:d.data.name, age:d.data.age}}
                        subscription={{}}
                    >
                        {
                            ()=>(
                                <div>
                                    {/*<AutoSave send={send}/>*/}
                                    <Field name='name' type='text' component='input'/>
                                    <Field name='age' type='text' component='input'/>
                                    {/*<Field name='holost' type='checkbox' component={CheckboxX}/> */}
                                </div>
                            )
                        }
                    </Form>
                )
            }}
        </CMyContext.Consumer>
    )
}
const APS:React.FC = () => {
    const [state, setState] = useState({name:"zakon",age:27,holost:true});
    return (
        <CMyContext.Provider value={{data:state,setState}}>
            <div>
                <MyHeader/>
                <MyForm/>
            </div>
        </CMyContext.Provider>
    )
}
const PoligonForm: React.FC<IProps> = ({children}) => {
    const handleSubmit = (d: any) => {
        console.log('send11111111111', d);
    }
    return (
        <>
            <div className='fx'>
                <Form onSubmit={handleSubmit} initialValues={{xx: 'larry',stooge: 'curly', zak:'good'}} >
                    {(prop) => {
                        console.log(prop);
                        return (
                            <form onSubmit={prop.handleSubmit}>
                                <Field name='zakon' type='sex' defaultValue=''>
                                    {(prop) => {
                                        // console.log(prop);
                                        return (
                                            <>
                                                <input {...prop.input}/>
                                                {prop.meta.touched && prop.meta.error && prop.meta.error}
                                            </>
                                        )
                                    }}
                                </Field>
                                {/*<Field component={CheckboxX} name='zak' type='checkbox'/>*/}
                                <div>
                                    <label>Best Stooge</label>
                                    <div>
                                        <label>
                                            <Field
                                                name='stooge'
                                                component={SSS}
                                                type='radio'
                                                value='larry'
                                            />{' '}
                                            Larry
                                        </label>
                                        <label>
                                            <Field
                                                name='stooge'
                                                component={SSS}
                                                type='radio'
                                                value='moe'
                                            />{' '}
                                            Moe
                                        </label>
                                        <label>
                                            <Field
                                                name='stooge'
                                                component={SSS}
                                                type='radio'
                                                value='curly'
                                            />{' '}
                                            Curly
                                        </label>
                                    </div>
                                </div>
                                <button>отправить</button>
                            </form>
                        )
                    }}
                </Form>
            </div>
            <hr/>

            <Form
                onSubmit={onSubmit}
                initialValues={{stooge: 'larry', employed: false}}
                render={({handleSubmit, form, submitting, pristine, values}) => (
                    <form onSubmit={handleSubmit}>
                        <div>
                            <label>First Name</label>
                            <Field
                                name='firstName'
                                component='input'
                                type='text'
                                placeholder='First Name'
                            />
                        </div>
                        <div>
                            <label>Last Name</label>
                            <Field
                                name='lastName'
                                component='input'
                                type='text'
                                placeholder='Last Name'
                            />
                        </div>
                        <div>
                            <label>Employed</label>
                            <Field name='employed' component='input' type='checkbox'/>
                        </div>
                        <div>
                            <label>Favorite Color</label>
                            <Field name='favoriteColor' component='select'>
                                <option/>
                                <option value='#ff0000'>❤️ Red</option>
                                <option value='#00ff00'>💚 Green</option>
                                <option value='#0000ff'>💙 Blue</option>
                            </Field>
                        </div>
                        <div>
                            <label>Toppings</label>
                            <Field name='toppings' component='select' multiple>
                                <option value='chicken'>🐓 Chicken</option>
                                <option value='ham'>🐷 Ham</option>
                                <option value='mushrooms'>🍄 Mushrooms</option>
                                <option value='cheese'>🧀 Cheese</option>
                                <option value='tuna'>🐟 Tuna</option>
                                <option value='pineapple'>🍍 Pineapple</option>
                            </Field>
                        </div>
                        <div>
                            <label>Sauces</label>
                            <div>
                                <label>
                                    <Field
                                        name='sauces'
                                        component='input'
                                        type='checkbox'
                                        value='ketchup'
                                    />{' '}
                                    Ketchup
                                </label>
                                <label>
                                    <Field
                                        name='sauces'
                                        component='input'
                                        type='checkbox'
                                        value='mustard'
                                    />{' '}
                                    Mustard
                                </label>
                                <label>
                                    <Field
                                        name='sauces'
                                        component='input'
                                        type='checkbox'
                                        value='mayonnaise'
                                    />{' '}
                                    Mayonnaise
                                </label>
                                <label>
                                    <Field
                                        name='sauces'
                                        component='input'
                                        type='checkbox'
                                        value='guacamole'
                                    />{' '}
                                    Guacamole 🥑
                                </label>
                            </div>
                        </div>
                        <div>
                            <label>Best Stooge</label>
                            <div>
                                <label>
                                    <Field
                                        name='stooge'
                                        component='input'
                                        type='radio'
                                        value='larry'
                                    />{' '}
                                    Larry
                                </label>
                                <label>
                                    <Field
                                        name='stooge'
                                        component='input'
                                        type='radio'
                                        value='moe'
                                    />{' '}
                                    Moe
                                </label>
                                <label>
                                    <Field
                                        name='stooge'
                                        component='input'
                                        type='radio'
                                        value='curly'
                                    />{' '}
                                    Curly
                                </label>
                            </div>
                        </div>
                        <div>
                            <label>Notes</label>
                            <Field name='notes' component='textarea' placeholder='Notes'/>
                        </div>
                        <div className='buttons'>
                            <button type='submit' disabled={submitting || pristine}>
                                Submit
                            </button>
                            <button
                                type='button'
                                onClick={form.reset}
                                disabled={submitting || pristine}
                            >
                                Reset
                            </button>
                        </div>
                        <pre>{JSON.stringify(values, null, 2)}</pre>
                    </form>
                )}
            />
        </>
    );
};

export default APS;