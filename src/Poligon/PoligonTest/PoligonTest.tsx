import React from 'react';
import './PoligonTest.scss'
import ModalContainer from "../../Containers/ModalContainer/ModalContainer";
import LoginSettings_Modal from "../../Modals/LoginSettings_Modal/LoginSettings_Modal";

interface IProps {
}


const PoligonTest: React.FC<IProps> = ({children}) => {
    return (
        <>
            <ModalContainer/>
            <div className='ModalContainer'>
                <div className='ModalContainer__wrap'>
                    <div className='ModalContainer__in'>
                        <div className='ModalContainer__top'/>
                        <div className='ModalContainer__body'>
                            <div className={'Modal'}>
                                <LoginSettings_Modal/>
                            </div>
                        </div>
                        <div className='ModalContainer__bot'/>
                    </div>
                </div>
            </div>
        </>
    );
};

export default PoligonTest;