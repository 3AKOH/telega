import React, {createElement, useEffect, useState} from 'react'
import './PoligonUI.scss'
import Themes from "../../Context/App/state/themes";
import ButtonX from "../../Components/UI/Button/ButtonX/ButtonX";
import {createTheme} from "../../Components/Util";
import InputX from "../../Components/UI/Input/InputX/InputX";
import IconX from "../../Components/UI/Icon/IconX";
import ToggleX from "../../Components/UI/Toggle/ToggleX/ToggleX";
import CheckboxX from "../../Components/UI/Checkbox/CheckboxX/CheckboxX";
import RadioX from "../../Components/UI/Radio/RadioX/RadioX";
import {Field, Form} from "react-final-form";
// @ts-ignore
import CustomScroll from 'react-custom-scroll';
import BoxScroll from "../../Components/BoxScroll/BoxScroll/BoxScroll";

interface IProps {
}


const list = ["", "default", "dark"];
const State = {
    theme: new Themes(list[0]),
    bodyBg: "#fff",
}
const PoligonUI: React.FC<IProps> = ({children}) => {
    const [theme, setTheme] = useState(State);
    useEffect(() => {
        const t = localStorage.theme;
        const tm = new Themes(t);
        const bodBg = createTheme(tm).get(['app','body']) || "#fff";
        if (theme) {
            setTheme({...theme, theme: tm,bodyBg: ''+bodBg});
        }
    }, [])
    const handleChange = (e: any) => {
        localStorage.theme = e.target.value;
        const tm = new Themes(e.target.value);
        const bodBg = createTheme(tm).get(['app', 'body']) || "#fff";
        setTheme({...theme, theme: tm, bodyBg: '' + bodBg})
    };
    const style = {background: theme.bodyBg};
    return (
        <div className='fx PoligonUI'>
            <h1>UI</h1>
            <select onChange={handleChange} className='PoligonUI__theme' value={localStorage.theme || list[0]}>
                {
                    list.map((elm, index: number) => {
                        if (!elm) elm = 'по умолчанию';
                        return <option key={index}>{elm}</option>
                    })
                }
            </select>
            <dl>
                <dt>Scroll</dt>
                <dd style={{height: 100}}>
                    <BoxScroll style={{height:100,background:'#fff'}}>
                        lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>
                        lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>
                        lorem<br/>lorem<br/>lrem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>
                        lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>lorem<br/>
                    </BoxScroll>
                </dd>
            </dl>
            <dl>
                <dt>Всплывайка</dt>
                <dd>
                </dd>
            </dl>
            <dl>
                <dt>Переключатели</dt>
                <dd>
                    {/*<div className='element' style={style}>*/}
                    {/*    <div className='element__title'>Toggle</div>*/}
                    {/*    <div className='element__body'>*/}
                    {/*        <ToggleX/>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    {/*<div className='element' style={style}>*/}
                    {/*    <div className='element__title'>Checkbox</div>*/}
                    {/*    <div className='element__body'>*/}
                    {/*        <CheckboxX/>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    {/*<div className='element' style={style}>*/}
                    {/*    <div className='element__title'>Radio</div>*/}
                    {/*    <div className='element__body'>*/}
                    {/*        <RadioX/>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </dd>
            </dl>
            <dl>
                <dt>Поля ввода</dt>
                <dd>
                    <div className='element' style={style}>
                        <div className='element__title'>Главная</div>
                        <div className='element__body'>
                            <InputX defaultValue='Привет! xa'/>
                            <InputX defaultValue='Привет!' error={true} center={true}/>
                        </div>
                    </div>
                </dd>
            </dl>
            <dl>
                <dt>Кнопки</dt>
                <dd>
                    <div className='element' style={style}>
                        <div className='element__title'>Главная</div>
                        <div className='element__body'>
                            <ButtonX theme={theme.theme}>Продолжить</ButtonX>
                        </div>
                    </div>
                    <div className='element' style={style}>
                        <div className='element__title'>Навигационная</div>
                        <div className='element__body'>
                            <ButtonX theme={theme.theme} type={"nav"}>Войти в систему</ButtonX>
                        </div>
                    </div>
                    <div className='element' style={style}>
                        <div className='element__title'>Иконочная</div>
                        <div className='element__body'>
                            <ButtonX theme={theme.theme} type={"icon"}><IconX name='icon_menu'/></ButtonX>
                            <ButtonX theme={theme.theme} type={"icon"}><IconX name='icon_back'/></ButtonX>
                            <ButtonX theme={theme.theme} type={"icon"} className='ButtonX-offHover'><IconX name='icon_video'/></ButtonX>
                        </div>
                    </div>
                    <div className='element' style={style}>
                        <div className='element__title'>Главная блочная</div>
                        <div className='element__body'>
                            <ButtonX theme={theme.theme} className='ButtonX-block'>Войти в систему</ButtonX>
                        </div>
                    </div>
                </dd>
            </dl>
        </div>
    );
};

export default PoligonUI;





