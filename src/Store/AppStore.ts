import {EventEmitter} from "events";
import packageJson from "../../package.json";
import {getBrowser, getOSName, getSimpleType} from "../Util/Common";
import Api from "../Api/Api";
import index from "react-virtualized-auto-sizer";
import {CONNECTION_CHANGE_TIME} from "../Constants";
import LangStore from "./LangStore";
import SyncPromise from "../Util/SyncPromise/SyncPromise";

const OK = true;

export enum IAppStoreStatus {
    "waitPhone",
    "waitCode",
    "waitRegistration",
    "ready",
    "closing",
    "closed"
};
export interface IAppStore {
    timer: number | null
    ConnectionStatus: "connection" | "ok" | "update"
    AppStatus: IAppStoreStatus
    Authorization: boolean
    Options: { [idx: string]: any }
    DarkTheme: boolean
}

const InitialAppStore: IAppStore = {
    timer: null,
    ConnectionStatus: "connection",
    AppStatus: IAppStoreStatus["waitPhone"],
    Authorization: false,
    Options: {},
    DarkTheme: false,
};

/* [Emitter events list]
READY - когда можно производить анализ store (ждет номер, ...)
UPDATE_OPTIONS - изменились опции app
UPDATE_OPTION [test_mode] - изменилась опция
CONNECTION - изменился состояние соеднинения
 */
class AppStore extends EventEmitter {
    d: IAppStore;
    on: (x: ('INIT_PARAMETERS' | 'UPDATE_CONNECT' | 'UPDATE_AUTH' | 'UPDATE_OPTIONS' | 'UPDATE_OPTION [name]') | string, s: (...args: any[]) => void) => this;

    constructor() {
        super();
        this.d = InitialAppStore;
        this.on = super.on;
        this.setMaxListeners(Infinity);
    }

    switching = (upd: any) => {
        switch (upd['@type']) {
            case "ok":
                upd._show = OK;
                break;
            case "updateSelectedBackground":
                this.d.DarkTheme = upd.for_dark_theme;
                upd._show = OK;
                break;
            case "updateOption":
                switch (upd.value['@type']) {
                    case "optionValueInteger":
                        this.d.Options[upd.name] = upd.value.value;
                        break;
                    case "optionValueString":
                        this.d.Options[upd.name] = upd.value.value;
                        break;
                    case "optionValueBoolean":
                        this.d.Options[upd.name] = upd.value.value;
                        break;
                    default:
                        console.log('############### НЕ ОБРАБОТАЛ под SWITCH!', upd);
                }
                this.emit(`UPDATE_OPTION [${upd.name}]`, upd.value.value);
                this.emit('UPDATE_OPTIONS', this.d.Options);
                upd._show = OK;
                break;
            case "updateConnectionState":
                if (this.d.ConnectionStatus === 'ok') {
                    setTimeout(() => {
                        if (this.d.ConnectionStatus !== 'ok') this.emit('UPDATE_CONNECT', this.d.ConnectionStatus);
                    }, CONNECTION_CHANGE_TIME)
                } else {
                    this.emit('UPDATE_CONNECT', this.d.ConnectionStatus);
                }
                switch (upd.state['@type']) {
                    case "connectionStateConnecting":
                        this.d.ConnectionStatus = "connection";
                        break;
                    case "connectionStateReady":
                        this.d.ConnectionStatus = "ok";
                        break;
                    case "connectionStateUpdating":
                        this.d.ConnectionStatus = "update";
                        break;
                    default:
                        console.log('############### НЕ ОБРАБОТАЛ под SWITCH!', upd);
                }
                upd._show = OK;
                break;
            case "updateAuthorizationState":
                switch (upd.authorization_state['@type']) {
                    case "authorizationStateWaitTdlibParameters":
                        const _this = this;
                        this.d.timer = setTimeout(()=>{
                            this.emit('INIT_PARAMETERS', false);
                        },10000);
                        SyncPromise([
                            this._sendOption('localization_target', 'android'),
                            this._sendOption('language_pack_id', 'ru'),
                            LangStore._LoadingLangTargetList(false),        //возможные языки
                            LangStore._LoadingLangPackString('ru')          //подгрузить язык 
                        ], () => {
                            if(_this.d.timer) clearTimeout(_this.d.timer);
                            this.emit('INIT_PARAMETERS', true);
                        });
                        this.send_TdlibParametrs();
                        break;
                    case "authorizationStateWaitEncryptionKey":
                        this.send_TdlibEncryptionKey();
                        break;
                    case "authorizationStateWaitPhoneNumber":
                        this.d.AppStatus = IAppStoreStatus["waitPhone"];
                        this.emit('UPDATE_AUTH', this.d.AppStatus);
                        break;
                    case "authorizationStateWaitCode":
                        this.d.AppStatus = IAppStoreStatus["waitCode"];
                        this.emit('UPDATE_AUTH', this.d.AppStatus);
                        break;
                    case "authorizationStateReady":
                        this.d.AppStatus = IAppStoreStatus["ready"];
                        this.emit('UPDATE_AUTH', this.d.AppStatus);
                        break;
                    case "authorizationStateWaitRegistration":
                        this.d.AppStatus = IAppStoreStatus["waitRegistration"];
                        break;
                    case "authorizationStateClosing":
                        this.d.AppStatus = IAppStoreStatus["closing"];
                        break;
                    case "authorizationStateClosed":
                        this.d.AppStatus = IAppStoreStatus["closed"];
                        break;
                    default:
                        console.log('############### НЕ ОБРАБОТАЛ под SWITCH!', upd);
                }
                upd._show = OK;
                break;
            default:
                break;
        }
    };

    //приват
    private send_TdlibParametrs = () => {
        const {version} = packageJson;
        return Api.send({
            "@type": "setTdlibParameters",
            parameters: {
                "@type": "tdlibParameters",
                use_test_dc: false,
                api_id: parseInt(process.env.REACT_APP_TELEGRAM_API_ID || '', 10),
                api_hash: process.env.REACT_APP_TELEGRAM_API_HASH,
                system_language_code: navigator.language || "en",
                device_model: getBrowser(),
                system_version: getOSName(),
                application_version: version,
                use_secret_chats: false,
                use_message_database: true,
                use_file_database: false,
                database_directory: "/db",
                files_directory: "/"
            }
        });
    };
    private send_TdlibEncryptionKey = () => {
        return Api.send({
            "@type": "checkDatabaseEncryptionKey",
        });
    };

    //паблик
    _sendOption = (key: string, value: any) => {
        return Api.send({
            "@type": "setOption",
            name: key,
            value: {
                "@type": getSimpleType(value),
                value: value
            }
        });
    };
    _online = (s: boolean) => {
        return this._sendOption('online', s)
    };
    _sendPhoneNumber = (phone: string) => {
        return Api.send({
            "@type": "setAuthenticationPhoneNumber",
            phone_number: phone
        });
    };
    _sendTelegramCode = (code: string) => {
        return Api.send({
            "@type": "checkAuthenticationCode",
            code: code
        });
    };


    getStore = () => this.d;
    getOptions = () => this.d.Options;
    getOption = (name: string) => this.d.Options[name];
    getAuth = () => this.d.AppStatus;
}

export default new AppStore