import {EventEmitter} from "events";
import packageJson from "../../package.json";
import {getBrowser, getOSName} from "../Util/Common";
import Api from "../Api/Api";
import index from "react-virtualized-auto-sizer";
import {CONNECTION_CHANGE_TIME} from "../Constants";

const OK = true;
export interface IBasicGroupStoreItem {
    id: number
    is_active: boolean
    member_count: number
    status: any
    upgraded_to_supergroup_id: number
    [inx:string]:any
}
export interface IBasicGroupStore {
    groups: {
        [inx:number]:IBasicGroupStoreItem
    }
}

class BasicGroupStore extends EventEmitter {
    d: IBasicGroupStore;

    constructor() {
        super();
        this.d = {
            groups: {}
        };
        this.on = super.on;
        this.setMaxListeners(Infinity);
    }

    switching = (upd: any) => {
        switch (upd['@type']) {
            case "updateBasicGroup":
                this.d.groups[upd.basic_group.id] = upd.basic_group;
                upd._show = OK; this.emit(upd['@type'], upd.basic_group.id);       //ok
                break;        //список языков
            default:
                break;
        }
    };
    get = (id:number) => this.d.groups[id];

}

export default new BasicGroupStore