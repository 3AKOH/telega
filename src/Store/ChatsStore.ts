import {EventEmitter} from "events";
import packageJson from "../../package.json";
import {getBrowser, getOSName} from "../Util/Common";
import Api from "../Api/Api";
import index from "react-virtualized-auto-sizer";
import {CHATS_LIMIT, CHATS_MESSAGE_LIMIT, CONNECTION_CHANGE_TIME} from "../Constants";
import MessageStore from "./MessageStore";
import {IChatPhoto} from "./interface/IPhoto";

const OK = true;

export interface IChatsStoreItem {
    title: string
    can_be_deleted_for_all_users: boolean
    can_be_deleted_only_for_self: boolean
    can_be_reported: boolean
    client_data: string
    default_disable_notification: boolean
    id: number
    is_marked_as_unread: boolean
    is_pinned: boolean
    is_sponsored: boolean
    last_read_inbox_message_id: number
    last_read_outbox_message_id: number
    notification_settings: any
    order: string
    permissions: any
    photo: IChatPhoto
    pinned_message_id: number
    reply_markup_message_id: number
    type: any
    unread_count: number
    unread_mention_count: number
    last_message: any
    typing: {
        on: boolean
        user_id: number
    }
    [inx:string]:any
}
export interface IChatsStore {
    open: number
    store: {
        [inx:number]:IChatsStoreItem
    }
    chats: [ number? ]
}


class ChatsStore extends EventEmitter {
    d: IChatsStore;

    constructor() {
        super();
        this.d = {
            open: 0,
            store: {},
            chats: []
        };
        this.on = super.on;
        this.setMaxListeners(Infinity);
    }

    switching = (upd: any) => {
        switch (upd['@type']) {
            case "chats":
                this.d.chats = upd.chat_ids;
                upd._show = OK; this.emit(upd['@type'], this.d.chats);       //ok
                break;        //список языков
            case "updateNewChat":
                this.d.store[upd.chat.id] = upd.chat;
                upd._show = OK; this.emit(upd['@type'], upd.chat.id);       //ok
                break;        //список языков
            case "updateChatNotificationSettings":
                this.d.store[upd.chat_id].notification_settings = upd.notification_settings;
                upd._show = OK; this.emit(upd['@type'], upd.chat_id);       //ok
                break;        //список языков
            case "updateChatIsPinned":
                this.d.store[upd.chat_id].is_pinned = upd.is_pinned;
                this.d.store[upd.chat_id].order = upd.order;
                upd._show = OK; this.emit(upd['@type'], upd.chat_id);       //ok
                break;        //список языков
            case "updateChatReadInbox":
                this.d.store[upd.chat_id].last_read_inbox_message_id = upd.last_read_inbox_message_id;
                upd._show = OK; this.emit(upd['@type'], upd.chat_id);       //ok
                break;        //список языков
            case "updateChatReadOutbox":
                this.d.store[upd.chat_id].last_read_outbox_message_id = upd.last_read_outbox_message_id;
                upd._show = OK; this.emit(upd['@type'], upd.chat_id);       //ok
                break;        //список языков
            case "updateChatOrder":
                this.d.store[upd.chat_id].order = upd.order;
                upd._show = OK; this.emit(upd['@type'], upd.chat_id);       //ok
                break;        //список языков
            case "updateChatPinnedMessage":
                this.d.store[upd.chat_id].pinned_message_id = upd.pinned_message_id;
                upd._show = OK; this.emit(upd['@type'], upd.chat_id);       //ok
                break;        //список языков
            case "updateChatReplyMarkup":
                this.d.store[upd.chat_id].reply_markup_message_id = upd.reply_markup_message_id;
                upd._show = OK; this.emit(upd['@type'], upd.chat_id);       //ok
                break;        //список языков
            case "updateUserChatAction":
                switch (upd.action['@type']) {
                    case 'chatActionTyping':
                        this.d.store[upd.chat_id].typing = {user_id:upd.user_id,on:true};
                        upd._show = OK; this.emit(upd['@type'], upd.chat_id);       //ok
                        break;
                    case 'chatActionCancel':
                        this.d.store[upd.chat_id].typing = {user_id:0,on:false};
                        upd._show = OK; this.emit(upd['@type'], upd.chat_id);       //ok
                        break;
                    default:
                        console.log('############### НЕ ОБРАБОТАЛ под SWITCH!', upd);
                }
                break;        //список языков
            default:
                break;
        }
    };

    _getChats = (offset_order:number = 0) => {
        return Api.send({
            "@type": "getChats",
            limit: CHATS_LIMIT,
            offset_chat_id: 0,
            offset_order: '9223372036854775807'
        });
    };      //список возможных языков
    _closeChat = (chatId:number) =>{
        return Api.send({
            "@type": "closeChat",
            chat_id: chatId,
        });
    };
    _getHistory = (chatId:number,from_message_id:number) =>{
        return Api.send({
            "@type": "getChatHistory",
            chat_id: chatId,
            from_message_id,
            limit: CHATS_MESSAGE_LIMIT,
            offset: 0
        });
    };
    _openChat = (chatId:number) =>{
        if(this.d.open){
            this._closeChat(chatId)
        }
        this.emit("OPEN_CHAT", chatId);       //ok
        //открываем
        return Api.send({
            "@type": "openChat",
            chat_id: chatId,
        });
    };
    getChats = ():[number?] => this.d.chats;
    get = (id:number) => this.d.store[id];
    set = (id:number, field:string, data:any) => this.d.store[id][field] = data;

}

export default new ChatsStore