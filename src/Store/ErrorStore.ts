import {EventEmitter} from "events";
import packageJson from "../../package.json";
import {getBrowser, getOSName, getSimpleType} from "../Util/Common";
import Api from "../Api/Api";
import index from "react-virtualized-auto-sizer";
import {CONNECTION_CHANGE_TIME} from "../Constants";
import LangStore from "./LangStore";
import SyncPromise from "../Util/SyncPromise/SyncPromise";

const OK = true;

export interface IErrorStore {
    listErr: {[idx:string]:string}
}

const InitialErrorStore: IErrorStore = {
    listErr: {
        PHONE_NUMBER_INVALID:'InvalidPhoneNumber',
        PHONE_CODE_INVALID:'InvalidCode',
    }
};

/* [Emitter events list]
ERROR - когда можно производить анализ store (ждет номер, ...)
 */
class ErrorStore extends EventEmitter {
    d: IErrorStore;
    on: (x: ('ERROR') | string, s: (...args: any[]) => void) => this;

    constructor() {
        super();
        this.d = InitialErrorStore;
        this.on = super.on;
        this.setMaxListeners(Infinity);
    }

    switching = (upd: any) => {
        switch (upd['@type']) {
            case "error":
                this.emit('ERROR', upd.message);
                upd._show = OK;
                break;
            default:
                break;
        }
    };
    _getNameErr = (err:string) =>{
        if(this.d.listErr[err]) return this.d.listErr[err];
        return 'KEY ISSET'
    }
}

export default new ErrorStore