import {EventEmitter} from "events";
import packageJson from "../../package.json";
import {getBrowser, getOSName} from "../Util/Common";
import Api from "../Api/Api";
import index from "react-virtualized-auto-sizer";
import {CHATS_LIMIT, CONNECTION_CHANGE_TIME} from "../Constants";
import {IFile, ILocalFile, IProfilePhoto, IRemoteFile} from "./interface/IPhoto";
import SyncPromise from "../Util/SyncPromise/SyncPromise";

const OK = true;
export interface IFilesStoreItem {
    expected_size: number
    id: number
    local: ILocalFile
    remote: IRemoteFile
    size: number
    [inx:string]:any
}
export interface IFilesStore {
    urls: {
        [inx:number]:string
    }
    files: {
        [inx:number]:IFilesStoreItem
    }
    download: {
        [inx:number]:IFilesStoreItem
    }
}


class FilesStore extends EventEmitter {
    d: IFilesStore;

    constructor() {
        super();
        this.d = {
            urls: {},
            files: {},
            download: {},
        };
        this.on = super.on;
        this.setMaxListeners(Infinity);
    }

    switching = (upd: any) => {
        switch (upd['@type']) {
            case "file":
                // this.d.files[upd.file.id] = upd.file;
                upd._show = OK; this.emit(upd['@type'], upd.id);       //ok
                break;
            case "filePart":
                // this.d.urls[upd.file.id] = upd.file;
                upd._show = OK; //this.emit(upd['@type'], upd.id);       //ok
                break;
            case "updateFile":
                const _this = this;
                if(upd.file.size && upd.file.expected_size == upd.file.size){
                    this.d.files[upd.file.id] = upd.file;
                    delete(this.d.download[upd.file.id]);
                    SyncPromise([this._read(upd.file.id)],(d:any)=>{
                        if(d[0].data) this.d.urls[upd.file.id] = URL.createObjectURL(d[0].data);
                        _this.emit('LOADED_FILE',upd.file.id);
                    });
                }else{
                    this.d.download[upd.file.id] = upd.file;
                }
                upd._show = OK; this.emit(upd['@type'], upd.file.id);       //ok
                break;        //список языков
            default:
                break;
        }
    };

    _download = (fileId:number,priority:number = 32) => {
        return Api.send({
            "@type": "downloadFile",
            file_id: fileId,
            priority
        });
    }
    _read = (fileId:number,offset:number=0,size:number=0) => {
        return Api.send({
            "@type": "readFile",
            file_id: fileId,
            offset: 0,
            size: 0
        });
    }

    get = (id:number) => this.d.files[id];
    getUrl = (fileId:number) => {
        if(this.d.urls[fileId]){
            return this.d.urls[fileId];
        }else{
            this._download(fileId);
            return null
        }
    }

}

export default new FilesStore