import {EventEmitter} from "events";
import packageJson from "../../package.json";
import {getBrowser, getOSName} from "../Util/Common";
import Api from "../Api/Api";
import index from "react-virtualized-auto-sizer";
import {CONNECTION_CHANGE_TIME} from "../Constants";

const OK = true;

export interface IAppStore {
    LangTargetList: [any?]
    LangPackStrings: { [inx:string]:any }
}

const InitialAppStore: IAppStore = {
    LangTargetList: [],
    LangPackStrings: {}
};

/* [Emitter events list]
TARGET_LIST - загрузился весь список возможных языков
LOADING_LANG - загрузился язык
 */
class LangStore extends EventEmitter {
    d: IAppStore;
    on: (x:('TARGET_LIST'|'LOADING_LANG')|string,s:(...args: any[]) => void)=>this;

    constructor() {
        super();
        this.d = InitialAppStore;
        this.on = super.on;
        this.setMaxListeners(Infinity);
    }

    switching = (upd: any) => {
        switch (upd['@type']) {
            case "localizationTargetInfo":
                this.d.LangTargetList = upd.language_packs;
                this.emit('TARGET_LIST',this.d);
                upd._show = OK;
                break;        //список языков
            case "languagePackStrings":
                let obj:any = {};
                upd.strings.forEach((elem:any)=>{
                    obj[elem.key] = elem.value;
                });
                this.d.LangPackStrings = obj;
                this.emit('LOADING_LANG',this.d);
                upd._show = OK;
                break;          //слова по ключам
            default:
                break;
        }
    };

    //паблик
    _LoadingLangTargetList = (local:boolean = false) => {
        return Api.send({
            "@type": "getLocalizationTargetInfo",
            only_local: local
        });
    };      //список возможных языков
    _LoadingLangPackString = (lang:string) => {
        return Api.send({
            "@type": "getLanguagePackStrings",
            keys: [],
            language_pack_id: lang
        });
    };                  //загрузить выбранный язык

    getString = (key:string, args?:{[ind:string]:any}|null,def?:string) =>{
        if(!this.d.LangPackStrings) return false;
        const elem = this.d.LangPackStrings[key];
        if (!elem) return def;
        switch (elem['@type']) {
            case 'languagePackStringValueOrdinary':
                let s = elem['value'];
                for(let key in args){
                    s = s.replaceAll(`%${key}$s`,args[key]);
                }
                s = s.replaceAll(` **`,'<b>');
                s = s.replaceAll(`** `,'</b>');
                return s;
            case 'languagePackStringValuePluralized':
                return elem['one_value'];
            default:
                console.log('############### НЕ ОБРАБОТАЛ под SWITCH!', elem);
        }
    }
}

export default new LangStore