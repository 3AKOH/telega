import {EventEmitter} from "events";
import packageJson from "../../package.json";
import {getBrowser, getOSName} from "../Util/Common";
import Api from "../Api/Api";
import index from "react-virtualized-auto-sizer";
import {CHATS_LIMIT, CONNECTION_CHANGE_TIME} from "../Constants";
import ChatsStore from "./ChatsStore";

const OK = true;

export interface IMessageStoreItem {
    author_signature: string
    can_be_deleted_for_all_users: boolean
    can_be_deleted_only_for_self: boolean
    can_be_edited: boolean
    can_be_forwarded: boolean
    chat_id: number
    contains_unread_mention: boolean
    content: any
    date: number
    edit_date: number
    id: number
    is_channel_post: boolean
    is_outgoing: boolean
    media_album_id: string
    reply_to_message_id: number
    sender_user_id: number
    ttl: number
    ttl_expires_in: number
    via_bot_user_id: number
    views: number
    [inx:string]:any
}
export interface IMessageStore {
    messages: {
        [inx:number]:IMessageStoreItem
    }
}


class MessageStore extends EventEmitter {
    d: IMessageStore;

    constructor() {
        super();
        this.d = {
            messages: {}
        };
        this.on = super.on;
        this.setMaxListeners(Infinity);
    }

    switching = (upd: any) => {
        switch (upd['@type']) {
            case "message":
                let chat_id:number = 0;
                upd.messages.forEach((elem:IMessageStoreItem)=>{
                    chat_id = elem.chat_id;
                    this.d.messages[elem.chat_id] = elem;      //save
                });
                upd._show = OK; this.emit(upd['@type'], chat_id);       //ok
                break;        //список языков
            case "updateNewMessage":
                this.d.messages[upd.message.id] = upd.message;      //save
                ChatsStore.set(upd.message.chat_id,'last_message',upd.message);     //добавили в чат
                upd._show = OK; this.emit(upd['@type'], upd.message.id);       //ok
                break;        //список языков
            // case "updateChatLastMessage":
            //     this.d.messages[upd.chat_id] = upd.last_message;      //save
            //     ChatsStore.set(upd.chat_id,'last_message',upd.last_message);        //добавили в группу
            //     ChatsStore.set(upd.chat_id,'order',upd.order);     //добавили в чат
            //     upd._show = OK; this.emit(upd['@type'], upd.last_message.id);       //ok
            //     break;        //список языков
            default:
                break;
        }
    };

    //просмотрели соообщения
    _viewMessages = (chatId:number,msgIds:[number]) =>{
        return Api.send({
            "@type": "viewMessages",
            chat_id: chatId,
            message_ids: msgIds,
        });
    };
    set = (id:number,data:any) => this.d.messages[id] = data;
    get = (id:number) => this.d.messages[id];

}

export default new MessageStore