import {EventEmitter} from "events";
import packageJson from "../../package.json";
import {getBrowser, getOSName} from "../Util/Common";
import Api from "../Api/Api";
import index from "react-virtualized-auto-sizer";
import {CONNECTION_CHANGE_TIME} from "../Constants";

const OK = true;

export interface INotificationsStore {
    settings: { [inx:string]:any }
    pending: {
        have_delayed_notifications: boolean
        have_unreceived_notifications: boolean
        [inx:string]:any
    }
}

const InitialNotificationsStore: INotificationsStore = {
    settings: {},
    pending: {
        have_delayed_notifications: false,
        have_unreceived_notifications: false,
    },
};

/* [Emitter events list]
TARGET_LIST - загрузился весь список возможных языков
LOADING_LANG - загрузился язык
 */
class NotificationsStore extends EventEmitter {
    d: INotificationsStore;
    on: (x:('TARGET_LIST'|'LOADING_LANG')|string,s:(...args: any[]) => void)=>this;

    constructor() {
        super();
        this.d = InitialNotificationsStore;
        this.on = super.on;
        this.setMaxListeners(Infinity);
    }

    switching = (upd: any) => {
        switch (upd['@type']) {
            case "updateScopeNotificationSettings":
                this.d.settings[upd.scope['@type']] = upd.settings;
                upd._show = OK; this.emit(upd['@type'], upd.settings);       //ok
                break;        //список языков
            case "updateHavePendingNotifications":
                this.d.pending = {
                    have_delayed_notifications: upd.have_delayed_notifications,
                    have_unreceived_notifications: upd.have_unreceived_notifications,
                };
                upd._show = OK; this.emit(upd['@type'], this.d.pending);       //ok
                break;        //список языков
            default:
                break;
        }
    };

    //паблик
    _LoadingLangTargetList = (local:boolean = false) => {
        return Api.send({
            "@type": "getLocalizationTargetInfo",
            only_local: local
        });
    };      //список возможных языков

}

export default new NotificationsStore