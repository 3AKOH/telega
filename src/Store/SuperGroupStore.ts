import {EventEmitter} from "events";
import packageJson from "../../package.json";
import {getBrowser, getOSName} from "../Util/Common";
import Api from "../Api/Api";
import index from "react-virtualized-auto-sizer";
import {CONNECTION_CHANGE_TIME} from "../Constants";

const OK = true;
export interface ISuperGroupStoreItem {
    date: number
    id: number
    is_channel: boolean
    is_scam: boolean
    is_verified: boolean
    member_count: number
    restriction_reason: string
    sign_messages: boolean
    status: any
    username: string
    fullInfo: {
        administrator_count: number
        banned_count: number
        can_get_members: boolean
        can_set_sticker_set: boolean
        can_set_username: boolean
        can_view_statistics: boolean
        description: string
        invite_link: string
        is_all_history_available: boolean
        member_count: number
        restricted_count: number
        sticker_set_id: string
        upgraded_from_basic_group_id: number
        upgraded_from_max_message_id: number
        [inx:string]:any
    }
    [inx:string]:any
}
export interface ISuperGroupStore {
    groups: {
        [inx:number]:ISuperGroupStoreItem
    }
}

class SuperGroupStore extends EventEmitter {
    d: ISuperGroupStore;

    constructor() {
        super();
        this.d = {
            groups: {}
        };
        this.on = super.on;
        this.setMaxListeners(Infinity);
    }

    switching = (upd: any) => {
        switch (upd['@type']) {
            case "updateSupergroup":
                this.d.groups[upd.supergroup.id] = upd.supergroup;
                upd._show = OK; this.emit(upd['@type'], upd.supergroup.id);       //ok
                break;        //список языков
            case "updateSupergroupFullInfo":
                this.d.groups[upd.supergroup_id].fullInfo = upd.supergroup_full_info;
                upd._show = OK; this.emit(upd['@type'], upd.supergroup_id);       //ok
                break;        //список языков
            default:
                break;
        }
    };
    get = (id:number) => this.d.groups[id];

}

export default new SuperGroupStore