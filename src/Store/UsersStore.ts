import {EventEmitter} from "events";
import packageJson from "../../package.json";
import {getBrowser, getOSName} from "../Util/Common";
import Api from "../Api/Api";
import index from "react-virtualized-auto-sizer";
import {CONNECTION_CHANGE_TIME} from "../Constants";
import {IProfilePhoto} from "./interface/IPhoto";

const OK = true;
export interface IUserStoreItem {
    first_name: string
    have_access: boolean
    id: number
    incoming_link: any
    is_scam: boolean
    is_support: boolean
    is_verified: boolean
    language_code: string
    last_name: string
    outgoing_link: any
    phone_number: string
    profile_photo: IProfilePhoto
    restriction_reason: string
    status: {
        [inx:string]:any
        expires: number
    }
    type: any
    username: string
    fullInfo:{
        bio: string
        can_be_called: boolean
        group_in_common_count: number
        has_private_calls: boolean
        is_blocked: boolean
        share_text: string
        [inx:string]:any
    }
    [inx:string]:any
}
export interface IUserStore {
    users: {
        [inx:number]:IUserStoreItem
    }
}


class UsersStore extends EventEmitter {
    d: IUserStore;

    constructor() {
        super();
        this.d = {
            users: {}
        };
        this.on = super.on;
        this.setMaxListeners(Infinity);
    }

    switching = (upd: any) => {
        switch (upd['@type']) {
            case "updateUser":
                this.d.users[upd.user.id] = {...upd.user};
                upd._show = OK; this.emit(upd['@type'], upd.user.id);       //ok
                break;        //список языков
            case "updateUserFullInfo":
                this.d.users[upd.user_id].fullInfo = {...upd.user_full_info};
                upd._show = OK; this.emit(upd['@type'], upd.user_id);       //ok
                break;        //список языков
            case "userFullInfo":
                this.d.users[upd.user_id].fullInfo = {...upd};
                upd._show = OK; this.emit(upd['@type'], upd.user_id);       //ok
                break;        //список языков
            case "updateUserStatus":
                this.d.users[upd.user_id].status = {...upd.status};
                upd._show = OK; this.emit(upd['@type'], upd.user_id);       //ok
                break;        //список языков
            default:
                break;
        }
    };
    _getFullInfo = (id:number) => {
        return Api.send({
            "@type": "getUserFullInfo",
            user_id:id
        });
    };
    _getUser = (id:number) => {
        return Api.send({
            "@type": "getUser",
            user_id:id
        });
    };
    get = (id:number) => this.d.users[id];

}

export default new UsersStore