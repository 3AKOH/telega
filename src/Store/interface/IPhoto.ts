export interface ILocalFile {
    can_be_deleted: boolean
    can_be_downloaded: boolean
    download_offset: number
    downloaded_prefix_size: number
    downloaded_size: number
    is_downloading_active: boolean
    is_downloading_completed: boolean
    path: string
    [inx:string]:any
}
export interface IRemoteFile {
    id: string
    is_uploading_active: boolean
    is_uploading_completed: boolean
    uploaded_size: number
    [inx:string]:any
}
export interface IFile {
    expected_size: number
    id: number
    local: ILocalFile
    remote: IRemoteFile
    size: number
    [inx:string]:any
}
export interface IChatPhoto {
    big: IFile
    small: IFile
    [inx:string]:any
}
export interface IProfilePhoto {
    id: string
    big: IFile
    small: IFile
    [inx:string]:any
}