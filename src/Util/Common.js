
//вернуть тип переменной для tdlib
export function getSimpleType(d){
    if(typeof d === 'string'){
        return 'optionValueString'
    }
    if(typeof d === 'boolean'){
        return 'optionValueBoolean'
    }
    if(typeof d === 'number'){
        return 'optionValueInteger'
    }
}
//замена всех совпадений
String.prototype.replaceAll = function(search, replace){
    return this.split(search).join(replace);
};
//генерируем цвет по словам
export function genColor(str){
    let num = 0;
    for(let i=0;i<str.length;i++){
        num += str[i].charCodeAt();
    }
    return '#'+(0x1000000+(num/100)*0xffffff).toString(16).substr(1,6);
}

export function getBrowser(){
    let browser_name = "";
    let isIE = /*@cc_on!@*/ false || !!document.documentMode;
    let isEdge = !isIE && !!window.StyleMedia;
    if(navigator.userAgent.indexOf("Chrome") !== -1 && !isEdge){
        browser_name = "Chrome";
    }else if(navigator.userAgent.indexOf("Safari") !== -1 && !isEdge){
        browser_name = "Safari";
    }else if(navigator.userAgent.indexOf("Firefox") !== -1){
        browser_name = "Firefox";
    }else if(navigator.userAgent.indexOf("MSIE") !== -1 || !!document.documentMode === true){
        //IF IE > 10
        browser_name = "IE";
    }else if(isEdge){
        browser_name = "Edge";
    }else{
        browser_name = "Unknown";
    }
    return browser_name;
}
export function getOSName(){
    let OSName = "Unknown";
    if(window.navigator.userAgent.indexOf("Windows NT 10.0") !== -1) OSName = "Windows 10";
    if(window.navigator.userAgent.indexOf("Windows NT 6.2") !== -1) OSName = "Windows 8";
    if(window.navigator.userAgent.indexOf("Windows NT 6.1") !== -1) OSName = "Windows 7";
    if(window.navigator.userAgent.indexOf("Windows NT 6.0") !== -1) OSName = "Windows Vista";
    if(window.navigator.userAgent.indexOf("Windows NT 5.1") !== -1) OSName = "Windows XP";
    if(window.navigator.userAgent.indexOf("Windows NT 5.0") !== -1) OSName = "Windows 2000";
    if(window.navigator.userAgent.indexOf("Mac") !== -1) OSName = "Mac/iOS";
    if(window.navigator.userAgent.indexOf("X11") !== -1) OSName = "UNIX";
    if(window.navigator.userAgent.indexOf("Linux") !== -1) OSName = "Linux";
    return OSName;
}
//DEEP копирование объекта
// @ts-ignore
export function dup(o,i,r){r=o;if(r&&typeof o=="object"){r=Array.isArray(o)?[]:{};for(i in o)if(o.hasOwnProperty(i))r[i]=dup(o[i])}return r}

export const isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

export function is_touch_device() {
    return !!('ontouchstart' in window);
}