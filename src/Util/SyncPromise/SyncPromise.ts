import SyncStore from "./SyncStore";
import {SYNC_PROMISE_DELAY} from "../../Constants";

const SyncPromise = async (arr:any[], fn:any) =>{
    Promise.all(arr).then((res:any)=>{
        let nums:number[] = [];
        const IDTimer = setTimeout(()=>{
            console.error('remove SyncPromise');
            SyncStore.off("ADD", ADD_ANSWER);
        },SYNC_PROMISE_DELAY);
        const ADD_ANSWER = () =>{
            if(SyncStore.has(nums)){
                clearTimeout(IDTimer)
                SyncStore.off("ADD", ADD_ANSWER);
                fn(SyncStore.get(nums))
            }
        };
        SyncStore.on("ADD", ADD_ANSWER);
        res.forEach((elem:any)=>{
            if(elem && elem['@extra']) nums.push(elem['@extra'].query_id);
        });
        return
    })
    return
};

export default SyncPromise