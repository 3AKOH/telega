import {EventEmitter} from 'events';
import {SYNC_PROMISE_STORE_ELEM_MAX, SYNC_PROMISE_STORE_ELEM_SAVE} from "../../Constants";

class SyncStore extends EventEmitter{
    store = new Map();      //мапа ключей
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor(){
        super()
    }
    has(arr:number[]){
        if(!arr) return;
        let key = true;
        arr.forEach(num=>{
            if(!this.store.has(num)){
                key = false;
            }
        });
        return key
    }
    get(nums?:number[]){
        if(!nums){
            return this.store
        }else{
            if(this.store.size >= SYNC_PROMISE_STORE_ELEM_MAX){
                let i = this.store.size-SYNC_PROMISE_STORE_ELEM_SAVE;
                this.store.forEach((e,idx)=>{
                    if(i>=0){
                        this.store.delete(idx)
                    }
                    i--
                })
            }
            let res:any = [];
            nums.forEach(n=>{
                res.push(this.store.get(n))
            });
            return res;
        }
    }
    add(ans:any){
        if(ans && ans['@extra']){
            this.store.set(ans['@extra'].query_id,ans);
            this.emit('ADD');
        }
    }
}

export default new SyncStore