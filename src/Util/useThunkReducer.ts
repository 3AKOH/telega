import { useReducer, useRef } from "react";

const useThunkReducer = (reducer:any, initialState:any) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const thunkDispatchRef = useRef();

    const thunkDispatch:any = (action:any) =>
        typeof action === "function"
            ? action(thunkDispatchRef.current, state)
            : dispatch(action);

    thunkDispatchRef.current = thunkDispatch;

    return [state, thunkDispatch];
};

export default useThunkReducer;