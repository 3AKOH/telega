import React, {createContext, useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import './assets/scss/app.scss';
import * as serviceWorker from './serviceWorker';
// @ts-ignore
import {StyleRoot} from "radium";
import App from "./App";
import AppState from "./Context/App/AppState";
import PoligonUI from "./Poligon/PoligonUI/PoligonUI";
import {BrowserRouter,Route,Switch} from "react-router-dom";
import MainContainer from "./Containers/MainContainer/MainContainer";
import PoligonForm from "./Poligon/PoligonForm/PoligonForm";
import PoligonTest from "./Poligon/PoligonTest/PoligonTest";
import KeysEmitter from "./Emitter/KeysEmitter";

let Key = KeysEmitter;

const app = (
    <BrowserRouter>
        <StyleRoot>
            <AppState>
                {/*<App/>*/}
                <Switch>
                    <Route path="/ui" component={PoligonUI}/>
                    {/*<Route exact path="/main" component={MainContainer}/>*/}
                    <Route path="/test" component={PoligonTest}/>
                    <Route path="/form" component={PoligonForm}/>
                    {/*<Route exact path="/scroll" component={Test2}/>*/}
                    <Route component={App}/>
                </Switch>
            </AppState>
        </StyleRoot>
    </BrowserRouter>
)

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
