const x = [{
    "@type":"languagePackString",
    "key":"ActionPinnedMusic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) аудиофайл"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteAndExit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить и покинуть группу?"}
}, {
    "@type":"languagePackString",
    "key":"Forward",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Переслать"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteGIF",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить GIF?"}
}, {
    "@type":"languagePackString",
    "key":"HistoryClearedUndo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"История очищена."}
}, {
    "@type":"languagePackString",
    "key":"SecurityTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Безопасность"}
}, {
    "@type":"languagePackString",
    "key":"MetersAway2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s м отсюда"}
}, {
    "@type":"languagePackString",
    "key":"UsernameAvailable",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя пользователя %1$s свободно."}
}, {
    "@type":"languagePackString",
    "key":"AccDescrCameraFlashOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вспышка выключена"}
}, {
    "@type":"languagePackString",
    "key":"SearchMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сообщения"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrChanSilentOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить без уведомления"}
}, {
    "@type":"languagePackString",
    "key":"EmpryUsersPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить"}
}, {
    "@type":"languagePackString",
    "key":"ShareSendTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Искать..."}
}, {
    "@type":"languagePackString",
    "key":"NotificationGroupAddSelf",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s снова в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadMaxVideoSize",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Максимальный размер видео"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureLogout",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите выйти?\n\nВы можете пользоваться Telegram на всех Ваших устройствах одновременно.\n\nВажно: при выходе из аккаунта все секретные чаты на этом устройстве пропадут."
    }
}, {
    "@type":"languagePackString",
    "key":"AudioUnknownTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Неизвестный трек"}
}, {
    "@type":"languagePackString",
    "key":"MegaAddedBy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 добавил(а) вас в эту группу"}
}, {
    "@type":"languagePackString",
    "key":"PaymentSuccessfullyPaidNoItem",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы успешно перевели %1$s для %2$s"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrPlayerSpeed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ускоренное воспроизведение"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_TR",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на турецком языке"}
}, {
    "@type":"languagePackString",
    "key":"DeleteReportSpam",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сообщить о спаме"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsServiceInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Перезапускать приложение, если оно закрыто. Обеспечивает надежность уведомлений."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelSignMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подписывать сообщения"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyForwardsEverybody",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылка на Ваш аккаунт"}
}, {
    "@type":"languagePackString",
    "key":"KickFromGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить из группы"}
}, {
    "@type":"languagePackString",
    "key":"WrongNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Неправильный номер?"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedGeoLive",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) трансляцию геопозиции в группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"AttachGifRestricted",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Администраторы группы запретили Вам отправлять GIF до %1$s"
    }
}, {
    "@type":"languagePackString",
    "key":"AccActionMessageOptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Параметры сообщения"}
}, {
    "@type":"languagePackString",
    "key":"ChangePhoneNumberOccupied",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Номер %1$s уже привязан к аккаунту в Telegram. Удалите тот аккаунт, прежде чем перейти на новый номер."
    }
}, {
    "@type":"languagePackString",
    "key":"ResetAccountCancelledAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Ваши недавние попытки сброса аккаунта были отменены его активным пользователем. Попробуйте снова через 7 дней."
    }
}, {
    "@type":"languagePackString",
    "key":"ColorSepia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сепия"}
}, {
    "@type":"languagePackString",
    "key":"ContactsPermissionAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**Telegram** требуется доступ к Вашим контактам, чтобы Вы могли оставаться на связи с друзьями с любого устройства. Актуальная информация о Ваших контактах будет храниться зашифрованной в облаке Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"AttachDestructingPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Самоуничтожающееся фото"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrClosePlayer",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закрыть аудиоплеер"}
}, {
    "@type":"languagePackString",
    "key":"ShowAllMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показать все медиа"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightBrightness",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Порог яркости"}
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedGif",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) GIF"}
}, {
    "@type":"languagePackString",
    "key":"EventLogChangedLinkedGroup",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"un1 сделал(а) un2 группой для обсуждения публикацией в этом канале."
    }
}, {
    "@type":"languagePackString",
    "key":"PeopleNearbyHeader",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Люди рядом"}
}, {
    "@type":"languagePackString",
    "key":"SessionsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Активные сеансы"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrCollapsePanel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Свернуть панель"}
}, {
    "@type":"languagePackString",
    "key":"EventLogFilterEditedMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отредактированные сообщения"}
}, {
    "@type":"languagePackString",
    "key":"PassportEmailVerifyInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Введите код подтверждения, который был только что отправлен на %1$s."
    }
}, {
    "@type":"languagePackString",
    "key":"PermissionNoCamera",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Telegram требуется доступ к камере, чтобы Вы могли снимать фото и видео."
    }
}, {
    "@type":"languagePackString",
    "key":"AddCaption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить подпись..."}
}, {
    "@type":"languagePackString",
    "key":"ChatHintsDeleteAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите скрыть **%1$s** из подсказок?"}
}, {
    "@type":"languagePackString",
    "key":"EventLogFilterLeavingMembers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выход участников"}
}, {
    "@type":"languagePackString",
    "key":"LastSeenFormatted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"был(а) %1$s"}
}, {
    "@type":"languagePackString",
    "key":"LocalAudioCache",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Голосовые/видеосообщения"}
}, {
    "@type":"languagePackString",
    "key":"WhoCanAddMe",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Кто может добавлять меня в группы?"}
}, {
    "@type":"languagePackString",
    "key":"AttachVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Видео"}
}, {
    "@type":"languagePackString",
    "key":"SelectColorTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбор цвета"}
}, {
    "@type":"languagePackString",
    "key":"ChannelUsernamePlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ссылка"}
}, {
    "@type":"languagePackString",
    "key":"Invite",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пригласить"}
}, {
    "@type":"languagePackString",
    "key":"SaveToDownloads",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сохранить в загрузки"}
}, {
    "@type":"languagePackString",
    "key":"AddToTheGroupTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить %1$s в чат %2$s?"}
}, {
    "@type":"languagePackString",
    "key":"PassportInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"С помощью Telegram Passport можно зарегистрироваться на сайтах и сервисах, где требуется идентификация личности.\n\nВаша информация, персональные данные и документы защищены оконечным шифрованием (end-to-end). Никто, включая Telegram, не сможет получить к ним доступ без Вашего согласия.\n\nПочитать об этом подробнее можно в нашем FAQ."
    }
}, {
    "@type":"languagePackString",
    "key":"NetworkUsageRoaming",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"РОУМИНГ"}
}, {
    "@type":"languagePackString",
    "key":"PeopleNearbyInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Предложите другу зайти в этот раздел, чтобы обменяться номерами телефона."
    }
}, {
    "@type":"languagePackString",
    "key":"LoginPasswordText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы включили двухэтапную аутентификацию. Теперь Ваш аккаунт защищён дополнительным паролем."
    }
}, {
    "@type":"languagePackString",
    "key":"SlowmodeSelectSendError",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Включён медленный режим. Вы не можете выбрать больше элементов."
    }
}, {
    "@type":"languagePackString",
    "key":"AccDescrEditing",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Редактирование"}
}, {
    "@type":"languagePackString",
    "key":"AddAnOption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить ответ..."}
}, {
    "@type":"languagePackString",
    "key":"ActionYouLeftUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы покинули группу"}
}, {
    "@type":"languagePackString",
    "key":"AutoplayVideoInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звук можно включить кнопками громкости."}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureUnblockContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите разблокировать этот контакт?"}
}, {
    "@type":"languagePackString",
    "key":"NoSharedFilesSecret",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Здесь будут отображаться файлы и документы из этого чата."
    }
}, {
    "@type":"languagePackString",
    "key":"AreYouSureSecretChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хотите начать секретный чат?"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrShutter",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Затвор"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrMuteMic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключить микрофон"}
}, {
    "@type":"languagePackString",
    "key":"ChannelDeleteInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы потеряете все сообщения из этого канала."}
}, {
    "@type":"languagePackString",
    "key":"AccDescrExpandPanel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Развернуть панель"}
}, {
    "@type":"languagePackString",
    "key":"Page4Message",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**Telegram** защищает Вашу переписку от злоумышленников."
    }
}, {
    "@type":"languagePackString",
    "key":"AutoplayMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Автовоспроизведение"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadGroups",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"группы"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrGoBack",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Назад"}
}, {
    "@type":"languagePackString",
    "key":"Proxy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прокси"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSure",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Уверены?"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightScheduled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"По расписанию"}
}, {
    "@type":"languagePackString",
    "key":"MobileUsage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использование мобильной сети"}
}, {
    "@type":"languagePackString",
    "key":"ChannelDelete",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить канал"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrIVTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заголовок"}
}, {
    "@type":"languagePackString",
    "key":"CustomThemes",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пользовательские темы"}
}, {
    "@type":"languagePackString",
    "key":"chatFullDate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"d MMMM yyyy"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentIdentity",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Личная информация"}
}, {
    "@type":"languagePackString",
    "key":"AccActionOpenFile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открыть файл"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundPreviewLine1",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Подсказка: иногда фон лучше выглядит с эффектом размытия."
    }
}, {
    "@type":"languagePackString",
    "key":"LedColor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Цвет светодиода"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrHideAccounts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Скрыть аккаунты"}
}, {
    "@type":"languagePackString",
    "key":"SearchFriends",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск контактов"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundIntensity",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Интенсивность"}
}, {
    "@type":"languagePackString",
    "key":"AddBotAsAdmin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Бота можно добавить только как администратора."}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentPhone",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Номер телефона"}
}, {
    "@type":"languagePackString",
    "key":"AllowReadCall",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Разрешите приложению принимать звонки, чтобы код для входа срабатывал автоматически."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportDeleteScanAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить этот скан?"}
}, {
    "@type":"languagePackString",
    "key":"ColorDark",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Тёмный"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrCameraFlashOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вспышка включена"}
}, {
    "@type":"languagePackString",
    "key":"DistanceUnitsKilometers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Километры"}
}, {
    "@type":"languagePackString",
    "key":"TosDeclineDeleteAccount",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Внимание: в этом случае ваш аккаунт в Telegram будет необратимо удален вместе со всей информацией, которую вы храните на серверах Telegram.\n\nВажно: вы можете отменить удаление и сначала экспортировать данные своего аккаунта, чтобы не потерять их. (Для этого зайдите в аккаунт через свежую версию Telegram для ПК и выберите Настройки > Экспорт данных из Telegram.)"
    }
}, {
    "@type":"languagePackString",
    "key":"EnterCurrentPasscode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите текущий код-пароль"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupLiveLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) трансляцию геопозиции в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"SaveTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СОХРАНИТЬ ТЕМУ"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrBotKeyboard",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Клавиатура бота"}
}, {
    "@type":"languagePackString",
    "key":"SETTINGS",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Канал"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrGIFs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"GIF"}
}, {
    "@type":"languagePackString",
    "key":"ChannelSettingsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки канала"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocuments",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s получил следующие документы: %2$s"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrRotate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Повернуть"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminTransferSetPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Установить пароль"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessagePoll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новый опрос"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrPasscodeLock",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заблокировать приложение"}
}, {
    "@type":"languagePackString",
    "key":"TurnPasswordOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключить пароль"}
}, {
    "@type":"languagePackString",
    "key":"ChooseTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбрать тему"}
}, {
    "@type":"languagePackString",
    "key":"ArchiveHintText3",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете закрепить неограниченное количество чатов наверху архива."
    }
}, {
    "@type":"languagePackString",
    "key":"AccDescrMusicInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%2$s  %1$s"}
}, {
    "@type":"languagePackString",
    "key":"EventLogInfoDetail",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Это список всех служебных действий, совершённых участниками и администраторами группы за последние 48 часов."
    }
}, {
    "@type":"languagePackString",
    "key":"AllowReadSms",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Разрешите приложению получать SMS, чтобы код для входа подставлялся автоматически."
    }
}, {
    "@type":"languagePackString",
    "key":"UseProxySponsor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Спонсор прокси"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrSentDate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправлено %s"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsPriority",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Приоритет"}
}, {
    "@type":"languagePackString",
    "key":"JoinToGroupErrorNotExist",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Похоже, такого чата не существует."}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureClearHistoryWithSecretUser",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите удалить историю секретного чата с **%1$s**?"
    }
}, {
    "@type":"languagePackString",
    "key":"MusicInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Для отправки музыки"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикеры"}
}, {
    "@type":"languagePackString",
    "key":"SlowmodeSendError",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Включён медленный режим. Вы не можете отправить больше одного сообщения за раз."
    }
}, {
    "@type":"languagePackString",
    "key":"MessageLifetimeVideo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если вы установите таймер, видео автоматически удалится после просмотра."
    }
}, {
    "@type":"languagePackString",
    "key":"AccDescrUserOptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Параметры пользователя"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrCameraFlashAuto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вспышка автоматическая"}
}, {
    "@type":"languagePackString",
    "key":"AccReleaseForArchive",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Архив — отпустите."}
}, {
    "@type":"languagePackString",
    "key":"Long",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Долгий"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteSingleMessageMega",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите удалить сообщение для всех участников чата?"
    }
}, {
    "@type":"languagePackString",
    "key":"EditAdminTransferAlertText2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Зашли в аккаунт на этом устройстве не меньше **24 часов** назад."
    }
}, {
    "@type":"languagePackString",
    "key":"Page5Title",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мощный"}
}, {
    "@type":"languagePackString",
    "key":"SearchBackgrounds",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск фонов"}
}, {
    "@type":"languagePackString",
    "key":"LastSeenContactsMinusPlus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мои контакты (-%1$d, +%2$d)"}
}, {
    "@type":"languagePackString",
    "key":"CancelPollAlertText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы уверены, что хотите удалить этот опрос?"}
}, {
    "@type":"languagePackString",
    "key":"PassportSelfRequest",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Для доступа к личной информации введите свой пароль."}
}, {
    "@type":"languagePackString",
    "key":"Retry",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Повторить"}
}, {
    "@type":"languagePackString",
    "key":"AppName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Telegram"}
}, {
    "@type":"languagePackString",
    "key":"PasswordEmailInvalid",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Некорректный адрес электронной почты. Пожалуйста, проверьте, что правильно указываете адрес, и попробуйте ещё раз."
    }
}, {
    "@type":"languagePackString",
    "key":"WhenRoaming",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В роуминге"}
}, {
    "@type":"languagePackString",
    "key":"ConvertGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сделать супергруппой"}
}, {
    "@type":"languagePackString",
    "key":"AccessError",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ошибка доступа"}
}, {
    "@type":"languagePackString",
    "key":"ContactsPermissionAlertContinue",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПРОДОЛЖИТЬ"}
}, {
    "@type":"languagePackString",
    "key":"GroupTypeHeader",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Тип группы"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsUntil",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Срок ограничений"}
}, {
    "@type":"languagePackString",
    "key":"VoipIncoming",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Входящий звонок"}
}, {
    "@type":"languagePackString",
    "key":"AccountSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки аккаунта"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrReceivedDate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Получено %s"}
}, {
    "@type":"languagePackString",
    "key":"ChangePhoneNewNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новый номер"}
}, {
    "@type":"languagePackString",
    "key":"ActionAddUserYou",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 добавил(а) вас"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddTranslationTemporaryRegistrationInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Загрузите сканы заверенного перевода для документа о временной прописке."
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedInvoice",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) счёт в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"UseProxySponsorInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Этот канал показан прокси-сервером. Чтобы убрать канал из списка чатов, отключите прокси в настройках Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"Agree",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Принять"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCheckoutPay",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ОПЛАТИТЬ %1$s"}
}, {
    "@type":"languagePackString",
    "key":"ActionChangedPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 изменил(а) фото группы"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyForwards",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пересылка сообщений"}
}, {
    "@type":"languagePackString",
    "key":"ChannelSignMessagesInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Добавлять имена администраторов к их сообщениям в канале."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportInfo2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"С помощью Telegram Passport можно зарегистрироваться на сайтах и сервисах, где требуется идентификация личности.\n\nВаша информация, персональные данные и документы защищены оконечным шифрованием (end-to-end). Никто, включая Telegram, не сможет получить к ним доступ без Вашего согласия.\n\nПочитать об этом подробнее можно в нашем *FAQ*."
    }
}, {
    "@type":"languagePackString",
    "key":"TerminateWebSessionQuestion",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключить %1$s?"}
}, {
    "@type":"languagePackString",
    "key":"CustomNotifications",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Особые уведомления"}
}, {
    "@type":"languagePackString",
    "key":"PaymentPasswordEnter",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите пароль"}
}, {
    "@type":"languagePackString",
    "key":"ClearCacheInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Освободит память устройства; файлы останутся в облаке."
    }
}, {
    "@type":"languagePackString",
    "key":"AccDescrChanSilentOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить уведомления"}
}, {
    "@type":"languagePackString",
    "key":"SuggestContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подсказка людей при поиске"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentDriverLicence",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Водительские права"}
}, {
    "@type":"languagePackString",
    "key":"CallReportSent",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Спасибо, что помогаете делать звонки Telegram лучше."}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadTypes",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загружать мультимедиа"}
}, {
    "@type":"languagePackString",
    "key":"VibrationDefault",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"По умолчанию"}
}, {
    "@type":"languagePackString",
    "key":"ContactsPermissionAlertNotNow",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"НЕ СЕЙЧАС"}
}, {
    "@type":"languagePackString",
    "key":"EventLogLeft",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 покинул(а) группу"}
}, {
    "@type":"languagePackString",
    "key":"GroupType",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Тип группы"}
}, {
    "@type":"languagePackString",
    "key":"PeopleNearbyAllowAccess",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Разрешить доступ"}
}, {
    "@type":"languagePackString",
    "key":"RestorePasswordNoEmailText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Так как Вы не указали адрес электронной почты для восстановления пароля, то оставшиеся варианты — вспомнить пароль или сбросить аккаунт."
    }
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentIdentityCard",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удостоверение личности"}
}, {
    "@type":"languagePackString",
    "key":"ArchiveHiddenInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Потяните вниз, чтобы увидеть архив."}
}, {
    "@type":"languagePackString",
    "key":"AutodownloadPrivateChats",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Личные чаты"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentInternalPassport",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Внутренний паспорт"}
}, {
    "@type":"languagePackString",
    "key":"NeverAllowPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда запрещать..."}
}, {
    "@type":"languagePackString",
    "key":"OtherLoginCode",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Ваш код для входа: **%1$s**. Укажите его в приложении Telegram, в котором хотите зайти в свой аккаунт.\n\nНикому не передавайте этот код."
    }
}, {
    "@type":"languagePackString",
    "key":"TelegramFAQ",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вопросы о Telegram"}
}, {
    "@type":"languagePackString",
    "key":"AlwaysShareWith",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда делиться с"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentPassport",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Паспорт"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentTemporaryRegistration",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Временная прописка"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentUtilityBill",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Коммунальный счёт"}
}, {
    "@type":"languagePackString",
    "key":"HoldToVideo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Удерживайте для записи видео. Нажмите для переключения на аудио."
    }
}, {
    "@type":"languagePackString",
    "key":"NewThemePreviewReply",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Доброе утро! 👋"}
}, {
    "@type":"languagePackString",
    "key":"ActionCreateGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 создал(а) группу"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessagePhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новое фото"}
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) видео"}
}, {
    "@type":"languagePackString",
    "key":"PermissionNoLocationPosition",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Telegram требуется доступ к геопозиции."}
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedFile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) файл"}
}, {
    "@type":"languagePackString",
    "key":"ActionKickUserYou",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 удалил(а) вас"}
}, {
    "@type":"languagePackString",
    "key":"AddToExistingContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить в имеющийся"}
}, {
    "@type":"languagePackString",
    "key":"TapHereGifs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите для доступа к сохранённым GIF"}
}, {
    "@type":"languagePackString",
    "key":"EnterYourPasscode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите код-пароль"}
}, {
    "@type":"languagePackString",
    "key":"Page6Title",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Облачный"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentRentalAgreement",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Договор об аренде"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrRepeatOne",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Повтор один раз"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentEmail",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Электронная почта"}
}, {
    "@type":"languagePackString",
    "key":"Emoji7",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Символы"}
}, {
    "@type":"languagePackString",
    "key":"AddExceptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить исключения"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настроить ночную тему"}
}, {
    "@type":"languagePackString",
    "key":"ActionMigrateFromGroupNotify",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s стала супергруппой."}
}, {
    "@type":"languagePackString",
    "key":"ChannelAlertText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Каналы — инструмент, который позволяет транслировать сообщения на большую аудиторию."
    }
}, {
    "@type":"languagePackString",
    "key":"ActionInviteYou",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы вступили в группу по ссылке-приглашению"}
}, {
    "@type":"languagePackString",
    "key":"LinkInUse",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя уже занято."}
}, {
    "@type":"languagePackString",
    "key":"MessageScheduledReminderNotification",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"📅 Напоминание"}
}, {
    "@type":"languagePackString",
    "key":"formatterDay24H",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"HH:mm"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundMotion",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Движение"}
}, {
    "@type":"languagePackString",
    "key":"SaveToGallerySettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сохранять в галерее"}
}, {
    "@type":"languagePackString",
    "key":"VoipRateCallAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пожалуйста, оцените качество звонка через Telegram"}
}, {
    "@type":"languagePackString",
    "key":"Bold",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Жирный"}
}, {
    "@type":"languagePackString",
    "key":"SharedAudioFiles",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Аудиофайлы"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrOpenMenu",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открыть меню навигации"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrPageDown",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перейти в конец"}
}, {
    "@type":"languagePackString",
    "key":"SortedByName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сортировка по имени"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRestricted",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"un1 изменил(а) ограничения для пользователя un2 (%1$s)"
    }
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedGame",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) %1$s"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsExceptionsSingleAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Учтите, что **%1$s** в списке исключений, на которые не повлияет это изменение."
    }
}, {
    "@type":"languagePackString",
    "key":"PrivacyForwardsInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В отправленных Вами сообщениях не будет ссылки на Ваш аккаунт, если переслать их в другой чат."
    }
}, {
    "@type":"languagePackString",
    "key":"ChangeLanguageLater",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы всегда можете изменить язык в настройках."}
}, {
    "@type":"languagePackString",
    "key":"CreateNewThemeHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Нажмите на иконку палитры, чтобы видеть и редактировать список элементов на экране."
    }
}, {
    "@type":"languagePackString",
    "key":"September",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сентябрь"}
}, {
    "@type":"languagePackString",
    "key":"UseProxyTelegramInfo2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Этот прокси-сервер может показывать канал спонсора в списке Ваших чатов. Это не раскрывает Ваш трафик."
    }
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsDisabled",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Это действие отключено для всех участников в настройках группы"
    }
}, {
    "@type":"languagePackString",
    "key":"AccDescrAspectRatio",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Соотношение сторон"}
}, {
    "@type":"languagePackString",
    "key":"DebugSendLogs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить лог"}
}, {
    "@type":"languagePackString",
    "key":"CropSquare",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Квадрат"}
}, {
    "@type":"languagePackString",
    "key":"AppNameBeta",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Telegram"}
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedNoText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) сообщение"}
}, {
    "@type":"languagePackString",
    "key":"GifCaption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подпись к GIF"}
}, {
    "@type":"languagePackString",
    "key":"SendLiveLocationFor8h",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"8 часов"}
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) фото"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessagePoll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) опрос"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsAndSounds",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Уведомления и звук"}
}, {
    "@type":"languagePackString",
    "key":"AddContactFullChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ДОБАВИТЬ %1$s В КОНТАКТЫ"}
}, {
    "@type":"languagePackString",
    "key":"CalculatingSize",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подсчёт…"}
}, {
    "@type":"languagePackString",
    "key":"Username",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя пользователя"}
}, {
    "@type":"languagePackString",
    "key":"AccurateTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"С точностью до %1$s"}
}, {
    "@type":"languagePackString",
    "key":"CreateLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить ссылку"}
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedSticker",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) стикер"}
}, {
    "@type":"languagePackString",
    "key":"CreateNewThemeAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Настройте нужные цвета, чтобы создать собственную тему оформления.\n\nВ этом же разделе можно вернуться к теме по умолчанию."
    }
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) «%1$s»"}
}, {
    "@type":"languagePackString",
    "key":"ThemeSetUrlHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Можно использовать символы a-z, 0-9 и _.\nМинимальная длина ссылки — 5 символов."
    }
}, {
    "@type":"languagePackString",
    "key":"ActionBotAllowed",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы разрешили этому боту писать Вам, когда авторизовались на %1$s."
    }
}, {
    "@type":"languagePackString",
    "key":"AreYouSureClearHistoryGroup",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Удалить из кэша все сообщения и материалы этой группы?"
    }
}, {
    "@type":"languagePackString",
    "key":"AccDescrSwitchCamera",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Переключить камеру"}
}, {
    "@type":"languagePackString",
    "key":"InviteUser",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"У этого пользователя пока нет Telegram, отправить приглашение?"
    }
}, {
    "@type":"languagePackString",
    "key":"AutomaticMediaDownload",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Автозагрузка медиа"}
}, {
    "@type":"languagePackString",
    "key":"ChatSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки чатов"}
}, {
    "@type":"languagePackString",
    "key":"GalleryInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Для отправки изображений без сжатия"}
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedGeoLive",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) трансляцию геопозиции"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrPasscodeUnlock",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Разблокировать приложение"}
}, {
    "@type":"languagePackString",
    "key":"PaymentReceipt",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чек"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrInstantCamera",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Переключиться в режим камеры"}
}, {
    "@type":"languagePackString",
    "key":"ActionYouChangedPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы изменили фото группы"}
}, {
    "@type":"languagePackString",
    "key":"JoinToGroupErrorFull",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, группа переполнена."}
}, {
    "@type":"languagePackString",
    "key":"VoipOutgoingCall",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Текущий звонок Telegram"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGame",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s пригласил(а) вас сыграть в %2$s"}
}, {
    "@type":"languagePackString",
    "key":"AddAnOptionInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Можно добавить %1$s."}
}, {
    "@type":"languagePackString",
    "key":"AddAnotherAccount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить аккаунт"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadOnWiFiData",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Через Wi-Fi"}
}, {
    "@type":"languagePackString",
    "key":"ActionTakeScreenshootYou",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы сделали скриншот!"}
}, {
    "@type":"languagePackString",
    "key":"LinkCopiedPrivate",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Ссылка скопирована.\nОна будет работать только для участников чата."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogRemoved",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 исключил(а) пользователя un2"}
}, {
    "@type":"languagePackString",
    "key":"Back",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Назад"}
}, {
    "@type":"languagePackString",
    "key":"AccountSwitch",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Переключиться"}
}, {
    "@type":"languagePackString",
    "key":"AddContactChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В КОНТАКТЫ"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupContact2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) контакт %3$s в группу «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"SessionsInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Управление сеансами на других устройствах."}
}, {
    "@type":"languagePackString",
    "key":"AddMember",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить участника"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrMentionDown",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перейти к следующему упоминанию"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_FA",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на фарси"}
}, {
    "@type":"languagePackString",
    "key":"ThemeSetUrl",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Задать ссылку"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrCancelEdit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отмена редактирования"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenuReloadContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перезагрузить контакты"}
}, {
    "@type":"languagePackString",
    "key":"PaymentTestInvoice",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ТЕСТОВЫЙ СЧЁТ"}
}, {
    "@type":"languagePackString",
    "key":"LinkInvalidShortMega",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имена групп должны содержать не меньше 5 символов."}
}, {
    "@type":"languagePackString",
    "key":"AddMembersAlertCountText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите добавить %1$s в **%2$s**?"}
}, {
    "@type":"languagePackString",
    "key":"ChatLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Геопозиция"}
}, {
    "@type":"languagePackString",
    "key":"UnpinMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открепить"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedAddUsers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавление пользователей"}
}, {
    "@type":"languagePackString",
    "key":"AlwaysShareWithTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда показывать"}
}, {
    "@type":"languagePackString",
    "key":"ChannelDeleteFromList",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить из этого списка"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialogMessage3",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикер 🤷‍♂️"}
}, {
    "@type":"languagePackString",
    "key":"CancelLinkExpired",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылка неверна или устарела."}
}, {
    "@type":"languagePackString",
    "key":"AccDescrMsgNotPlayed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не воспроизведено"}
}, {
    "@type":"languagePackString",
    "key":"ClearHistoryCache",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить из кэша"}
}, {
    "@type":"languagePackString",
    "key":"DebugClearLogs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить лог"}
}, {
    "@type":"languagePackString",
    "key":"MessageLifetimeRemoved",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отключил(а) удаление по таймеру"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddInternalPassport",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить внутренний паспорт"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrStopLiveLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Остановить трансляцию геопозиции"}
}, {
    "@type":"languagePackString",
    "key":"AddShortcut",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Создать ярлык"}
}, {
    "@type":"languagePackString",
    "key":"AddContactTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить контакт"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrMsgRead",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прочитано"}
}, {
    "@type":"languagePackString",
    "key":"AddStickersCount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ДОБАВИТЬ %1$s"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrTakeMorePics",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сделать ещё один снимок"}
}, {
    "@type":"languagePackString",
    "key":"EditMessageMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить медиа"}
}, {
    "@type":"languagePackString",
    "key":"GroupUserCantBot",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, в группе слишком много ботов."}
}, {
    "@type":"languagePackString",
    "key":"EditAdminDeleteMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалять чужие сообщения"}
}, {
    "@type":"languagePackString",
    "key":"ThemeNameInvalid",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Некорректное название темы."}
}, {
    "@type":"languagePackString",
    "key":"BackgroundBrightness",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Яркость"}
}, {
    "@type":"languagePackString",
    "key":"AddSubscriber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить подписчика"}
}, {
    "@type":"languagePackString",
    "key":"ChannelJoined",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы подписались на этот канал"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedGroupTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 переименовал(а) группу в «%1$s»"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyPhone",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Номер телефона"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadLow",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Низкий"}
}, {
    "@type":"languagePackString",
    "key":"Ping",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"пинг: %1$d мс"}
}, {
    "@type":"languagePackString",
    "key":"AddStickersInstalled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавлены новые стикеры"}
}, {
    "@type":"languagePackString",
    "key":"LinkPreview",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предпросмотр ссылки"}
}, {
    "@type":"languagePackString",
    "key":"AddStickersNotFound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикеры не найдены"}
}, {
    "@type":"languagePackString",
    "key":"AccActionPlay",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Воспроизвести"}
}, {
    "@type":"languagePackString",
    "key":"PinnedPoll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закреплённый опрос"}
}, {
    "@type":"languagePackString",
    "key":"SetUrlInvalidShort",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Минимальная длина ссылки — 5 символов."}
}, {
    "@type":"languagePackString",
    "key":"LowDiskSpaceMessage",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В устройстве почти не осталось места. Чтобы освободить немного места, разрешите Telegram хранить в кэше только недавние медиафайлы."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportDeleteEmailAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить почту?"}
}, {
    "@type":"languagePackString",
    "key":"ArticleByAuthor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"от %1$s"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionLinkGroupPrivateAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Назначить **%1$s** группой для обсуждения **%2$s**?\n\nВсе участники группы смогут видеть публикации канала."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelMembers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Участники"}
}, {
    "@type":"languagePackString",
    "key":"RevokeLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить ссылку"}
}, {
    "@type":"languagePackString",
    "key":"RevokeLinkAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите сбросить ссылку **%1$s**?\n\nГруппа \"**%2$s**\" станет частной."
    }
}, {
    "@type":"languagePackString",
    "key":"ChooseStickerSetPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"набор стикеров"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialog7",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ник К"}
}, {
    "@type":"languagePackString",
    "key":"July",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Июль"}
}, {
    "@type":"languagePackString",
    "key":"AddToTheGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить %1$s в группу?"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrPhotoViewer",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Просмотр фото"}
}, {
    "@type":"languagePackString",
    "key":"PreviewFeedback",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Оставить отзыв об этом превью"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureWebSessions",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы действительно хотите отключить все сайты, где авторизованы через Telegram?"
    }
}, {
    "@type":"languagePackString",
    "key":"AutoNightUpdateLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обновить геопозицию"}
}, {
    "@type":"languagePackString",
    "key":"Page1Title",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Telegram"}
}, {
    "@type":"languagePackString",
    "key":"AdminWillBeRemoved",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"%1$s перестанет быть администратором, если потеряет базовые права."
    }
}, {
    "@type":"languagePackString",
    "key":"ArchivedStickersInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"У Вас может быть установлено до 200 наборов стикеров. При установке новых наборов неиспользуемые стикеры архивируются."
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) контакт в группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"Alert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показывать уведомления"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureShareMyContactInfoBot",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Бот будет знать номер Вашего телефона. Это может использоваться для интеграции с другими сервисами."
    }
}, {
    "@type":"languagePackString",
    "key":"AllAccounts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всех аккаунтов"}
}, {
    "@type":"languagePackString",
    "key":"AllMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все медиа"}
}, {
    "@type":"languagePackString",
    "key":"ChannelBlockUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Исключить пользователя"}
}, {
    "@type":"languagePackString",
    "key":"Gray",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Серый"}
}, {
    "@type":"languagePackString",
    "key":"PassportSelfieInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите фотографию, где вы держите свой документ."}
}, {
    "@type":"languagePackString",
    "key":"ArchivedStickersAlertInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Некоторые из ваших старых наборов стикеров были архивированы. Вы можете снова активировать их в настройках стикеров."
    }
}, {
    "@type":"languagePackString",
    "key":"InvalidLastName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Некорректная фамилия"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureClearHistory",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите очистить историю?"}
}, {
    "@type":"languagePackString",
    "key":"CurrentSession",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Текущий сеанс"}
}, {
    "@type":"languagePackString",
    "key":"FingerprintNotRecognized",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отпечаток пальца не распознан. Попробуйте ещё раз."}
}, {
    "@type":"languagePackString",
    "key":"ReportUnrelatedGroupText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пожалуйста, сообщите нам, если группа никак не связана с местом, где Вы находитесь:\n\n**%1$s**"
    }
}, {
    "@type":"languagePackString",
    "key":"DeleteAccountIfAwayFor2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Автоудаление аккаунта"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_PT",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на португальском языке"}
}, {
    "@type":"languagePackString",
    "key":"ReportUnrelatedGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Жалоба на группу"}
}, {
    "@type":"languagePackString",
    "key":"AudioUnknownArtist",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Неизвестный исполнитель"}
}, {
    "@type":"languagePackString",
    "key":"Checking",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Проверка..."}
}, {
    "@type":"languagePackString",
    "key":"AccDescrMoreOptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Дополнительные параметры"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureClearDrafts",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы действительно хотите удалить все черновики сообщений, сохраненные в облаке?"
    }
}, {
    "@type":"languagePackString",
    "key":"AddToMasks",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить в маски"}
}, {
    "@type":"languagePackString",
    "key":"PassportRequestPasswordInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Для расшифровки данных введите свой пароль от Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogYouGroupJoined",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы вступили в группу"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightUpdateLocationInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы определить время захода и восхода солнца, необходимо разово получить данные о Вашем примерном местоположении. Эта информация будет храниться только на данном устройстве.\n\nЗакат: %1$s\nРассвет: %2$s"
    }
}, {
    "@type":"languagePackString",
    "key":"AlternativeOptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Другие возможности"}
}, {
    "@type":"languagePackString",
    "key":"AlwaysAllowPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда разрешать..."}
}, {
    "@type":"languagePackString",
    "key":"AddAnOptionInfoMax",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы указали максимальное количество вариантов ответа."}
}, {
    "@type":"languagePackString",
    "key":"ShowInChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показать в чате"}
}, {
    "@type":"languagePackString",
    "key":"KMetersAway",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"км отсюда"}
}, {
    "@type":"languagePackString",
    "key":"Appearance",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Оформление"}
}, {
    "@type":"languagePackString",
    "key":"VoipErrorUnknown",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не удалось позвонить через Telegram."}
}, {
    "@type":"languagePackString",
    "key":"AlwaysShowPopup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показывать всегда"}
}, {
    "@type":"languagePackString",
    "key":"Online",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"в сети"}
}, {
    "@type":"languagePackString",
    "key":"Default",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"По умолчанию"}
}, {
    "@type":"languagePackString",
    "key":"EventLogFilterPinnedMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закреплённые сообщения"}
}, {
    "@type":"languagePackString",
    "key":"AllPhotos",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все фотографии"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrMsgUnread",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не прочитано"}
}, {
    "@type":"languagePackString",
    "key":"Next",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Далее"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrRepeatOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Повтор отключён"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupMap",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) геопозицию в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"AnonymousPoll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Анонимный опрос"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRestrictedSendMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправлять сообщения"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedGameScore",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) счёт игры в группе «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"PasscodePIN",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПИН-код"}
}, {
    "@type":"languagePackString",
    "key":"PassportCountry",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Страна"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRemovedLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 удалил(а) геопозицию группы"}
}, {
    "@type":"languagePackString",
    "key":"ApplyThemeFile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Применить файл темы"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenu",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Меню отладки"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrPhotoEditor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фоторедактор"}
}, {
    "@type":"languagePackString",
    "key":"Archive",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В архив"}
}, {
    "@type":"languagePackString",
    "key":"PaymentPasswordReEnter",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Повторите пароль"}
}, {
    "@type":"languagePackString",
    "key":"Red",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Красный"}
}, {
    "@type":"languagePackString",
    "key":"SharedLinks",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылки"}
}, {
    "@type":"languagePackString",
    "key":"ForwardTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Переслать..."}
}, {
    "@type":"languagePackString",
    "key":"AccDescrSearchPrev",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предыдущий результат поиска"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyP2P",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звонки через Peer-to-Peer"}
}, {
    "@type":"languagePackString",
    "key":"EventLogFilterNewMembers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новые участники"}
}, {
    "@type":"languagePackString",
    "key":"JumpToDate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перейти к дате"}
}, {
    "@type":"languagePackString",
    "key":"ArticleDateByAuthor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s от %2$s"}
}, {
    "@type":"languagePackString",
    "key":"PassportMidname",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отчество"}
}, {
    "@type":"languagePackString",
    "key":"SettingsSearchFaq",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"FAQ"}
}, {
    "@type":"languagePackString",
    "key":"Language",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Язык"}
}, {
    "@type":"languagePackString",
    "key":"ArchiveHintText1",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чаты со включенными уведомлениями покидают архив, когда приходят новые сообщения."
    }
}, {
    "@type":"languagePackString",
    "key":"ArchivedChats",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Архив"}
}, {
    "@type":"languagePackString",
    "key":"DeleteThemeAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить тему?"}
}, {
    "@type":"languagePackString",
    "key":"EncryptedDescription4",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Запрет пересылки"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddPassportRegistrationInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите скан страницы паспорта с пропиской"}
}, {
    "@type":"languagePackString",
    "key":"PassportIdentityInternalPassport",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите скан внутреннего паспорта"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteThisChatWithSecretUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить секретный чат с **%1$s**?"}
}, {
    "@type":"languagePackString",
    "key":"SearchEmojiHint",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск эмодзи"}
}, {
    "@type":"languagePackString",
    "key":"formatterBannedUntilThisYear24H",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"dd MMM, HH:mm"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMegaJoined",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы вступили в эту группу"}
}, {
    "@type":"languagePackString",
    "key":"SendGameToGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поделиться игрой с группой %1$s?"}
}, {
    "@type":"languagePackString",
    "key":"UseProxySocks5",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прокси SOCKS5"}
}, {
    "@type":"languagePackString",
    "key":"ArchivedStickersAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Архив стикеров"}
}, {
    "@type":"languagePackString",
    "key":"AttachPhotoExpired",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Срок жизни фотографии истёк"}
}, {
    "@type":"languagePackString",
    "key":"ActionChannelChangedTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя канала изменено на un2"}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingEmailPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Электронная почта"}
}, {
    "@type":"languagePackString",
    "key":"ChannelPublic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Публичный канал"}
}, {
    "@type":"languagePackString",
    "key":"AllVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все видео"}
}, {
    "@type":"languagePackString",
    "key":"IncomingCalls",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Входящие звонки"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsApplyChangesText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы изменили права пользователя в **%1$s**. Применить изменения?"
    }
}, {
    "@type":"languagePackString",
    "key":"InstantView",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПОСМОТРЕТЬ"}
}, {
    "@type":"languagePackString",
    "key":"RemovedFromFavorites",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикер удалён из избранных"}
}, {
    "@type":"languagePackString",
    "key":"AutoLockInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Запрашивать код доступа, если какое-то время не было активности."
    }
}, {
    "@type":"languagePackString",
    "key":"ContactShare",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить контакт"}
}, {
    "@type":"languagePackString",
    "key":"ArchivedStickersEmpty",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет стикеров в архиве"}
}, {
    "@type":"languagePackString",
    "key":"ChatDeletedUndo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чат удалён."}
}, {
    "@type":"languagePackString",
    "key":"ArchivedMasksAlertInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Некоторые из старых наборов масок были архивированы. Вы можете снова активировать их в настройках масок."
    }
}, {
    "@type":"languagePackString",
    "key":"AreYouSureClearHistoryCacheFewChats",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Удалить из кэша все сообщения и материалы выбранных чатов?"
    }
}, {
    "@type":"languagePackString",
    "key":"LanguageSuggested",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Установить \"%1$s\" язык?"}
}, {
    "@type":"languagePackString",
    "key":"ALongTimeAgo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"был(а) давно"}
}, {
    "@type":"languagePackString",
    "key":"PleaseLoginPassport",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы использовать Telegram Passport, войдите в аккаунт Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"KeepMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хранить медиа"}
}, {
    "@type":"languagePackString",
    "key":"ReportChatDescription",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Описание"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundPreview",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предпросмотр фона"}
}, {
    "@type":"languagePackString",
    "key":"ReportChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пожаловаться"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightFrom",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Начало"}
}, {
    "@type":"languagePackString",
    "key":"OpenInBrowser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ОТКРЫТЬ В..."}
}, {
    "@type":"languagePackString",
    "key":"CallText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Telegram позвонит Вам через %1$d:%2$02d"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialogMessage1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Есть китайская мудрость об эт..."}
}, {
    "@type":"languagePackString",
    "key":"EditAdminTransferAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Проверка безопасности"}
}, {
    "@type":"languagePackString",
    "key":"ActionTakeScreenshoot",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 сделал(а) скриншот!"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteFewMessagesMega",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите удалить сообщения для всех участников чата?"
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedChangeGroupInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменение информации о группе"}
}, {
    "@type":"languagePackString",
    "key":"AllVideos",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все видео"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminGroupTransfer",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Передать права на группу"}
}, {
    "@type":"languagePackString",
    "key":"Accept",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Принять"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteFewChats",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить выбранные чаты?"}
}, {
    "@type":"languagePackString",
    "key":"EventLogToggledSlowmodeOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 установил(а) таймер медленного режима на %1$s"}
}, {
    "@type":"languagePackString",
    "key":"Emoji6",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предметы"}
}, {
    "@type":"languagePackString",
    "key":"FontSizePreviewLine2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В Токио утро 😎"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRestrictedSendStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправлять стикеры и GIF"}
}, {
    "@type":"languagePackString",
    "key":"PrivacySettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Конфиденциальность"}
}, {
    "@type":"languagePackString",
    "key":"YourEmail",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Адрес электронной почты"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedContactChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) контакт"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupGameScored",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s набрал(а) %4$s очков в «%3$s» в группе «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"PublicChannelsTooMuch",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"У этого пользователя максимальное число публичных групп и каналов. Пожалуйста, предложите ему сперва удалить одну из публичных ссылок на его группу или канал."
    }
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadFilesOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Файлы"}
}, {
    "@type":"languagePackString",
    "key":"CropImage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Кадрировать"}
}, {
    "@type":"languagePackString",
    "key":"DeleteContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить контакт"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadOnAllChats",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Включено для всех чатов"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteThisChatWithGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить чат **%1$s**?"}
}, {
    "@type":"languagePackString",
    "key":"PassportName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить это видео?"}
}, {
    "@type":"languagePackString",
    "key":"Hashtags",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ХЕШТЕГИ"}
}, {
    "@type":"languagePackString",
    "key":"ChatHistoryHiddenInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Новые участники не будут видеть более ранних сообщений."
    }
}, {
    "@type":"languagePackString",
    "key":"InstallGoogleMaps",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Установить Google Maps?"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureRegistration",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите отменить регистрацию?"}
}, {
    "@type":"languagePackString",
    "key":"ActionChannelRemovedPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фото канала удалено"}
}, {
    "@type":"languagePackString",
    "key":"AlwaysAllow",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда разрешать"}
}, {
    "@type":"languagePackString",
    "key":"UseProxyTelegram",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прокси MTProto"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminTransferChangeOwner",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сменить владельца"}
}, {
    "@type":"languagePackString",
    "key":"ArchiveHintHeader1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Это Ваш архив"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialogMessage4",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ну что, есть идеи?"}
}, {
    "@type":"languagePackString",
    "key":"ActionLeftUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 покинул(а) группу"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCardTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Платёжная карта"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureSessions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите завершить все другие сеансы?"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrForwarding",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пересылка от"}
}, {
    "@type":"languagePackString",
    "key":"ChangePasscodeInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"После установки кода-пароля в списке чатов появится значок замка для блокировки и разблокировки приложения.\n\nВажно: если Вы забудете код-пароль, приложение придётся переустановить, и все секретные чаты будут утрачены."
    }
}, {
    "@type":"languagePackString",
    "key":"ConvertGroupAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Это действие нельзя будет отменить. Супергруппу невозможно сделать обычной группой."
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedContactChannel2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) контакт %2$s"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureShareMyContactInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите поделиться номером телефона?"}
}, {
    "@type":"languagePackString",
    "key":"InternalStorage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Внутреннее хранилище"}
}, {
    "@type":"languagePackString",
    "key":"RepeatDisabled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Откл."}
}, {
    "@type":"languagePackString",
    "key":"AskAQuestion",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Задать вопрос"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrSearchNext",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Следующий результат поиска"}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingAddress",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Адрес доставки"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadOnRoamingData",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Через роуминг"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageSticker",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) стикер"}
}, {
    "@type":"languagePackString",
    "key":"PhoneHidden",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Неизвестен"}
}, {
    "@type":"languagePackString",
    "key":"ConnectingToProxyTapToDisable",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите для отключения прокси..."}
}, {
    "@type":"languagePackString",
    "key":"ArchivePinnedInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Смахните влево, чтобы скрыть архив."}
}, {
    "@type":"languagePackString",
    "key":"LinkInvalidLong",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя не может быть длиннее 32 символов."}
}, {
    "@type":"languagePackString",
    "key":"ActionKickUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 удалил(а) un2"}
}, {
    "@type":"languagePackString",
    "key":"LanguageCode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ru"}
}, {
    "@type":"languagePackString",
    "key":"AskAQuestionInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пожалуйста, обратите внимание, что в поддержке Telegram отвечают волонтеры. Мы постараемся помочь как можно быстрее, но ожидание может занять время.\n\nПожалуйста, ознакомьтесь с <![CDATA[<a href=\"https://telegram.org/faq#general\">частыми вопросами о Telegram</a>]]>: там есть ответ на большинство вопросов и важные советы по <![CDATA[<a href=\"https://telegram.org/faq#troubleshooting\">устранению неполадок</a>]]>."
    }
}, {
    "@type":"languagePackString",
    "key":"ChatHintsDeleteAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Скрыть подсказку"}
}, {
    "@type":"languagePackString",
    "key":"ChatHistoryVisible",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Видна"}
}, {
    "@type":"languagePackString",
    "key":"ShareTelegram",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пригласить в Telegram..."}
}, {
    "@type":"languagePackString",
    "key":"MessagesDataUsage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сообщения и прочие данные"}
}, {
    "@type":"languagePackString",
    "key":"AskButton",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Спросить волонтёров"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminTransferGroupToast",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"**%1$s** теперь владелец группы."}
}, {
    "@type":"languagePackString",
    "key":"AccountAlreadyLoggedIn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы уже зашли в этот аккаунт."}
}, {
    "@type":"languagePackString",
    "key":"Deactivate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить сейчас"}
}, {
    "@type":"languagePackString",
    "key":"PassportSelectExpiredDate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Укажите срок действия"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) «%2$s» в группе %3$s"}
}, {
    "@type":"languagePackString",
    "key":"DirectShare",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Direct Share"}
}, {
    "@type":"languagePackString",
    "key":"AttachGame",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Игра"}
}, {
    "@type":"languagePackString",
    "key":"PassportIdentityDriverLicence",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите скан своих водительских прав"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRestrictedPinMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закреплять сообщения"}
}, {
    "@type":"languagePackString",
    "key":"NotificationGroupAddSelfMega",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s теперь в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"AttachGif",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"GIF"}
}, {
    "@type":"languagePackString",
    "key":"LanguageCustom",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сторонний"}
}, {
    "@type":"languagePackString",
    "key":"SlowmodeSeconds",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$d сек"}
}, {
    "@type":"languagePackString",
    "key":"EncryptedChatStartedOutgoing",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Теперь Вы и %s в секретном чате. "}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_LT",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на литовском языке"}
}, {
    "@type":"languagePackString",
    "key":"Add",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить"}
}, {
    "@type":"languagePackString",
    "key":"PhoneNumberHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Здесь Вы можете сменить номер телефона. Ваш аккаунт и все данные (сообщения, медиа, контакты и т. д.) будут перенесены на новый номер."
    }
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewLine4",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Эпическая фраза!"}
}, {
    "@type":"languagePackString",
    "key":"ChatHints",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Люди"}
}, {
    "@type":"languagePackString",
    "key":"AttachGifRestrictedForever",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Администраторы группы запретили Вам отправлять GIF."}
}, {
    "@type":"languagePackString",
    "key":"PeopleNearbyEmpty",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск пользователей рядом с Вами..."}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewSongTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"True Survivor"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsNoSendStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"без стикеров и GIF"}
}, {
    "@type":"languagePackString",
    "key":"AddMembersAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить %1$s"}
}, {
    "@type":"languagePackString",
    "key":"AddMembersForwardMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показать новым участникам последние 100 сообщений"}
}, {
    "@type":"languagePackString",
    "key":"ChangePassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сменить пароль"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 отредактировал(а) сообщение:"}
}, {
    "@type":"languagePackString",
    "key":"AttachLiveLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Трансляция геопозиции"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedVideoChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) видео"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrEmojiButton",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Эмодзи, стикеры и GIF"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupAudio",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) голосовое сообщение в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"AttachLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Геопозиция"}
}, {
    "@type":"languagePackString",
    "key":"Passcode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Код-пароль"}
}, {
    "@type":"languagePackString",
    "key":"AttachMusic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Музыка"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadOnWiFiDataInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Включите, чтобы файлы мультимедиа автоматически загружались, когда Вы используете Wi-Fi."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportAddTranslationPassportRegistrationInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Загрузите сканы заверенного перевода для страницы паспорта с пропиской."
    }
}, {
    "@type":"languagePackString",
    "key":"YourEmailCode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Код из письма"}
}, {
    "@type":"languagePackString",
    "key":"ColorCyan",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Голубой"}
}, {
    "@type":"languagePackString",
    "key":"AttachRound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Видеосообщение"}
}, {
    "@type":"languagePackString",
    "key":"AttachStickersRestricted",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Администраторы группы запретили Вам отправлять стикеры до %1$s"
    }
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadPhotosOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фото"}
}, {
    "@type":"languagePackString",
    "key":"AttachStickersRestrictedForever",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Администраторы группы запретили Вам отправлять стикеры"
    }
}, {
    "@type":"languagePackString",
    "key":"ReportChatChild",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Детская порнография"}
}, {
    "@type":"languagePackString",
    "key":"GroupStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикеры группы"}
}, {
    "@type":"languagePackString",
    "key":"IsTypingGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s печатает..."}
}, {
    "@type":"languagePackString",
    "key":"CallViaTelegram",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звонок через Telegram"}
}, {
    "@type":"languagePackString",
    "key":"ResetAccount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сброс аккаунта"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsChangeInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменение профиля группы"}
}, {
    "@type":"languagePackString",
    "key":"ChannelSubscribers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подписчики"}
}, {
    "@type":"languagePackString",
    "key":"Emoji",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Эмодзи"}
}, {
    "@type":"languagePackString",
    "key":"AttachVideoExpired",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Срок жизни видеозаписи истёк"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCheckout",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Оплатить"}
}, {
    "@type":"languagePackString",
    "key":"AttachInlineRestricted",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Администраторы группы запретили Вам отправлять сообщения с помощью ботов до %1$s"
    }
}, {
    "@type":"languagePackString",
    "key":"August",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Август"}
}, {
    "@type":"languagePackString",
    "key":"ChannelLinkTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Постоянная ссылка"}
}, {
    "@type":"languagePackString",
    "key":"PaymentPhoneEmailToProvider",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В качестве платежной информации %1$s получит ваш номер телефона и электронную почту."
    }
}, {
    "@type":"languagePackString",
    "key":"YourEmailSkipWarningText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Нет, серьёзно.\n\nЕсли Вы забудете пароль, Вы потеряете доступ к своему аккаунту. Не будет никакого способа его восстановить."
    }
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadChannels",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"каналы"}
}, {
    "@type":"languagePackString",
    "key":"EventLogLeftChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 покинул(а) канал"}
}, {
    "@type":"languagePackString",
    "key":"AllowReadCallAndSms",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Разрешите приложению принимать звонки, чтобы мы могли подтвердить Ваш номер телефона автоматически."
    }
}, {
    "@type":"languagePackString",
    "key":"ArchivedStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Архив стикеров"}
}, {
    "@type":"languagePackString",
    "key":"IsSendingGame",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s играет в игру..."}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadDataUsage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Расход трафика"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadFilesTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Автозагрузка файлов и музыки"}
}, {
    "@type":"languagePackString",
    "key":"WaitMore",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подождать ещё"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadHigh",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Высокий"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundSearchColor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Цвет:"}
}, {
    "@type":"languagePackString",
    "key":"ApkRestricted",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Приложению не разрешена установка APK. Вы можете изменить это в настройках системы."
    }
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialog6",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вероника"}
}, {
    "@type":"languagePackString",
    "key":"DiscardVoiceMessageTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отмена голосового сообщения"}
}, {
    "@type":"languagePackString",
    "key":"PassportDeleteDocument",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить документ"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadMaxFileSize",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Максимальный размер файла"}
}, {
    "@type":"languagePackString",
    "key":"FromGalley",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Из галереи"}
}, {
    "@type":"languagePackString",
    "key":"BlockContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заблокировать"}
}, {
    "@type":"languagePackString",
    "key":"EditCantEditPermissions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы не можете редактировать это разрешение."}
}, {
    "@type":"languagePackString",
    "key":"EditAdminChangeGroupInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменение профиля группы"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadMedium",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Средний"}
}, {
    "@type":"languagePackString",
    "key":"FeaturedStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Популярные стикеры"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageScheduledName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"📅 Вы"}
}, {
    "@type":"languagePackString",
    "key":"SharedMusicTab",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"АУДИО"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выключено"}
}, {
    "@type":"languagePackString",
    "key":"ResetAutomaticMediaDownloadAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите сбросить настройки автозагрузки?"}
}, {
    "@type":"languagePackString",
    "key":"ShowNearbyPlaces",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Потяните, чтобы выбрать место"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageFew",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) вам %2$s"}
}, {
    "@type":"languagePackString",
    "key":"ApplyLocalizationFile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Применить локализацию"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddBill",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить коммунальный счёт"}
}, {
    "@type":"languagePackString",
    "key":"PassportNoDocumentsAdd",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить документ"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrVoiceMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Записать голосовое сообщение"}
}, {
    "@type":"languagePackString",
    "key":"AttachPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фотография"}
}, {
    "@type":"languagePackString",
    "key":"AddMembersAlertNamesText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите добавить %1$s в **%2$s**?"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageMap",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новая геопозиция"}
}, {
    "@type":"languagePackString",
    "key":"AreTyping",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"печатают…"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadOnMobileDataInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Включите, чтобы файлы мультимедиа автоматически загружались, когда Вы используете мобильную сеть."
    }
}, {
    "@type":"languagePackString",
    "key":"AwaitingEncryption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ждём, когда %s появится в сети..."}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedChannelPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 добавил(а) новое фото канала"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsCanDo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пользователю разрешено:"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadOnRoamingDataInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Включите, чтобы файлы мультимедиа автоматически загружались, когда Вы находитесь в роуминге."
    }
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"контакты"}
}, {
    "@type":"languagePackString",
    "key":"NoRecentPhotos",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет недавних фотографий"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrMyLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Моя геопозиция"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddInternalPassportInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Загрузите сканы заверенного перевода для своего внутреннего паспорта."
    }
}, {
    "@type":"languagePackString",
    "key":"RemindDayYearAt",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"'Напомнить' d MMM yyyy 'в' HH:mm"}
}, {
    "@type":"languagePackString",
    "key":"EditContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить контакт"}
}, {
    "@type":"languagePackString",
    "key":"ConnectingToProxyDisableAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Хотите отключить прокси-сервер %1$s? Вы всегда можете включить его в настройках > Данные и память."
    }
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadPhotos",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фото"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadPreloadMusic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предзагрузка следующего аудио"}
}, {
    "@type":"languagePackString",
    "key":"EmojiBigSize",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Большие одиночные эмодзи"}
}, {
    "@type":"languagePackString",
    "key":"Pink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Розовый"}
}, {
    "@type":"languagePackString",
    "key":"ActionYouScored",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы набрали %1$s"}
}, {
    "@type":"languagePackString",
    "key":"VoipPeerOutdated",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**%1$s** использует приложение, которое не поддерживает звонки. Пользователю нужно обновить приложение, чтобы вы могли созвониться."
    }
}, {
    "@type":"languagePackString",
    "key":"AccDescrShowAccounts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показать аккаунты"}
}, {
    "@type":"languagePackString",
    "key":"ChannelUserCantAdd",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы не можете приглашать этого пользователя в каналы."}
}, {
    "@type":"languagePackString",
    "key":"PassportResidentialAddress",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Адрес проживания"}
}, {
    "@type":"languagePackString",
    "key":"TextCopied",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Текст скопирован в буфер обмена"}
}, {
    "@type":"languagePackString",
    "key":"EditedMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"изменено"}
}, {
    "@type":"languagePackString",
    "key":"AlwaysShareWithPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда показывать пользователям..."}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadFiles",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Файлы"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsDuration",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Срок"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightPreferred",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбранная ночная тема"}
}, {
    "@type":"languagePackString",
    "key":"AccActionRecordVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Записать видео"}
}, {
    "@type":"languagePackString",
    "key":"SetAdminsAllInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Все участники могут добавлять новых участников, закреплять сообщения, изменять имя и фото группы."
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupInvoice",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) счёт на %3$s в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"Account",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Аккаунт"}
}, {
    "@type":"languagePackString",
    "key":"AddOneMemberForwardMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показать **%1$s** последние 100 сообщений"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadPreloadVideoInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Подгружать первые секунды (1-2 МБ) для видеозаписей больше %1$s, чтобы воспроизводить их без задержки."
    }
}, {
    "@type":"languagePackString",
    "key":"NewThemePreviewName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Боб Харрис"}
}, {
    "@type":"languagePackString",
    "key":"PassportStreet1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Улица"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_DZ",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на дзонг-кэ"}
}, {
    "@type":"languagePackString",
    "key":"WithinAWeek",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"был(а) на этой неделе"}
}, {
    "@type":"languagePackString",
    "key":"ChannelAdministrator",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Администратор"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightAutomatic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Автоматически"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadUpToOnAllChats",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"До %1$s для всех чатов"}
}, {
    "@type":"languagePackString",
    "key":"AreTypingGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s печатают..."}
}, {
    "@type":"languagePackString",
    "key":"BlockUserMultiTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заблокировать"}
}, {
    "@type":"languagePackString",
    "key":"AudioTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Общая музыка"}
}, {
    "@type":"languagePackString",
    "key":"AutoLock",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Автоблокировка"}
}, {
    "@type":"languagePackString",
    "key":"ChangePhoneNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить номер"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrProfilePicture",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фотография профиля"}
}, {
    "@type":"languagePackString",
    "key":"AddMutual",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пригласить подписаться на канал"}
}, {
    "@type":"languagePackString",
    "key":"NoBlockedUsers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет пользователей в чёрном списке"}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingStatePlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Область"}
}, {
    "@type":"languagePackString",
    "key":"BotShare",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поделиться"}
}, {
    "@type":"languagePackString",
    "key":"ChannelNotifyMembersInfoOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Участники получат уведомление без звука"}
}, {
    "@type":"languagePackString",
    "key":"Calling",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звоним вам..."}
}, {
    "@type":"languagePackString",
    "key":"AccActionDownload",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузка"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_ID",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на индонезийском языке"}
}, {
    "@type":"languagePackString",
    "key":"AutoLockInTime",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"через %1$s"}
}, {
    "@type":"languagePackString",
    "key":"PleaseStreamDownload",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пожалуйста, дождитесь полной загрузки видео."}
}, {
    "@type":"languagePackString",
    "key":"ArchiveHidden",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Архив скрыт."}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadPm",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"личные чаты"}
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedPoll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) опрос"}
}, {
    "@type":"languagePackString",
    "key":"NoContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Контактов пока нет"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_HU",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на венгерском языке"}
}, {
    "@type":"languagePackString",
    "key":"ChatSetLocationInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пользователи будут видеть группу в разделе «Люди рядом»."
    }
}, {
    "@type":"languagePackString",
    "key":"AccDescrGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Группа"}
}, {
    "@type":"languagePackString",
    "key":"ConnectingConnectProxy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подключить прокси"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightSchedule",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Время использования"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminChannelTransfer",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Передать права на канал"}
}, {
    "@type":"languagePackString",
    "key":"SortedByLastSeen",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сортировка по времени входа"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedChangeChannelInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменение информации о канале"}
}, {
    "@type":"languagePackString",
    "key":"GalleryPermissionText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите, чтобы разрешить доступ к галерее"}
}, {
    "@type":"languagePackString",
    "key":"ChannelDiscuss",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ОБСУДИТЬ"}
}, {
    "@type":"languagePackString",
    "key":"AddToContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить в контакты"}
}, {
    "@type":"languagePackString",
    "key":"ClearSearchAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить историю поиска"}
}, {
    "@type":"languagePackString",
    "key":"OtherWebSessions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подключенные сайты"}
}, {
    "@type":"languagePackString",
    "key":"DeleteChatBackgroundsAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить выбранные фоны? "}
}, {
    "@type":"languagePackString",
    "key":"AutodownloadChannels",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Каналы"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadOnFor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s"}
}, {
    "@type":"languagePackString",
    "key":"ExternalStorage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Внешнее хранилище"}
}, {
    "@type":"languagePackString",
    "key":"CallReportHint",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Что пошло не так?"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_DV",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на дивехи"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadOnMobileData",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Через моб. сеть"}
}, {
    "@type":"languagePackString",
    "key":"UnofficialApp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"неофициальное приложение"}
}, {
    "@type":"languagePackString",
    "key":"CropOriginal",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Оригинал"}
}, {
    "@type":"languagePackString",
    "key":"AutodownloadGroupChats",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Группы"}
}, {
    "@type":"languagePackString",
    "key":"April",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Апрель"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureClearHistorySavedMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите очистить историю чата **Избранное**?"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_TH",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на тайском языке"}
}, {
    "@type":"languagePackString",
    "key":"AutoplayGIF",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"GIF"}
}, {
    "@type":"languagePackString",
    "key":"PeopleNearby",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Люди рядом"}
}, {
    "@type":"languagePackString",
    "key":"LocationUpdated",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"обновлена"}
}, {
    "@type":"languagePackString",
    "key":"TypePrivate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Частный"}
}, {
    "@type":"languagePackString",
    "key":"DeleteLocalization",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить локализацию?"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrReplying",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ответ для"}
}, {
    "@type":"languagePackString",
    "key":"LanguageTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить язык?"}
}, {
    "@type":"languagePackString",
    "key":"EnableStreaming",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стриминг аудиофайлов и видео"}
}, {
    "@type":"languagePackString",
    "key":"TintHighlights",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СВЕТ"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundColor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Цвет"}
}, {
    "@type":"languagePackString",
    "key":"ArchiveHintHeader3",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закрепленные чаты"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyProfilePhotoInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете выбрать, кому разрешаете видеть фотографию в своём профиле."
    }
}, {
    "@type":"languagePackString",
    "key":"BackgroundColorPreviewLine1",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Проведите влево или вправо, чтобы просмотреть другие цвета."
    }
}, {
    "@type":"languagePackString",
    "key":"ChatArchivedInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Смахните влево, чтобы скрыть архив."}
}, {
    "@type":"languagePackString",
    "key":"ChannelUserLeftError",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, пользователь решил покинуть канал, поэтому Вы не можете пригласить его обратно."
    }
}, {
    "@type":"languagePackString",
    "key":"ConvertGroupInfo3",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"**Важно:** это нельзя будет отменить."}
}, {
    "@type":"languagePackString",
    "key":"ScamMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"SCAM"}
}, {
    "@type":"languagePackString",
    "key":"GroupUserCantAdd",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, Вы не можете добавлять этого пользователя в группы."
    }
}, {
    "@type":"languagePackString",
    "key":"BackgroundColorPreviewLine2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фисташковый — вкус, а не цвет."}
}, {
    "@type":"languagePackString",
    "key":"PassportMale",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мужской"}
}, {
    "@type":"languagePackString",
    "key":"PassportPhone",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Номер телефона"}
}, {
    "@type":"languagePackString",
    "key":"DeleteAccountTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удаление аккаунта при неактивности"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundHexColorCode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Шестнадцатеричный код"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundPattern",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Узор"}
}, {
    "@type":"languagePackString",
    "key":"PassportCity",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Город"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyCallsP2PTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Peer-to-Peer"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundPreviewLine2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ого! Спасибо."}
}, {
    "@type":"languagePackString",
    "key":"AccDescrBotCommands",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Команды бота"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEmptyTextSearch",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Не удалось найти недавние действия, содержащие '**%1$s**'."
    }
}, {
    "@type":"languagePackString",
    "key":"SetUrlInUse",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, ссылка уже занята."}
}, {
    "@type":"languagePackString",
    "key":"CreateTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СОЗДАТЬ ТЕМУ"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundRestricted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фоновая передача данных ограничена."}
}, {
    "@type":"languagePackString",
    "key":"UsernameChecking",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Проверка имени пользователя..."}
}, {
    "@type":"languagePackString",
    "key":"ActionCreateMega",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Группа создана"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEmptyChannel",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**Действий пока нет**\n\nАдминистраторы канала\nне выполняли никаких служебных действий\nза последние 48 часов."
    }
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteAndExitName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить и покинуть группу **%1$s**?"}
}, {
    "@type":"languagePackString",
    "key":"ActionUserScoredInGame",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 набрал(а) %1$s в игре un2"}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Информация о доставке"}
}, {
    "@type":"languagePackString",
    "key":"BadgeNumberShow",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показывать счётчик"}
}, {
    "@type":"languagePackString",
    "key":"DeleteAllFrom",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить всё от %1$s"}
}, {
    "@type":"languagePackString",
    "key":"ConvertGroupMenu",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сделать супергруппой"}
}, {
    "@type":"languagePackString",
    "key":"Page5Message",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**Telegram** не ограничивает размер\n Ваших медиафайлов и чатов."
    }
}, {
    "@type":"languagePackString",
    "key":"DeclineDeactivate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отклонить и удалить"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminTransferAlertText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы сможете передать права на группу **%1$s**, если:"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionUnlinkGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отвязать группу"}
}, {
    "@type":"languagePackString",
    "key":"MuteDisable",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключить"}
}, {
    "@type":"languagePackString",
    "key":"NotificationUnrecognizedDevice",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"%1$s, в Ваш аккаунт выполнен вход с нового устройства %2$s\n\nУстройство: %3$s\nМесто: %4$s\n\nЕсли это были не Вы, перейдите в Настройки - Конфиденциальность - Сеансы и завершите этот сеанс.\n\nЕсли Вы считаете, что кто-то вошёл в Ваш аккаунт без Вашего ведома, советуем включить двухэтапную аутентификацию в разделе Конфиденциальность.\n\nКоманда Telegram"
    }
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingCountry",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Страна"}
}, {
    "@type":"languagePackString",
    "key":"BanFromTheGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"УДАЛИТЬ ИЗ ГРУППЫ"}
}, {
    "@type":"languagePackString",
    "key":"AttachAudio",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Голосовое сообщение"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrOpenChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открыть чат"}
}, {
    "@type":"languagePackString",
    "key":"Black",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чёрный"}
}, {
    "@type":"languagePackString",
    "key":"AudioAutodownload",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Голосовые сообщения"}
}, {
    "@type":"languagePackString",
    "key":"Emoji5",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Места и путешествия"}
}, {
    "@type":"languagePackString",
    "key":"FloodWait",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Слишком много попыток, пожалуйста, попробуйте снова позже"
    }
}, {
    "@type":"languagePackString",
    "key":"LastSeenEverybody",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedGameChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) игру"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новое видео"}
}, {
    "@type":"languagePackString",
    "key":"BlockUserChatsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ЧАТЫ"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadVideos",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Видео"}
}, {
    "@type":"languagePackString",
    "key":"AllowReadCallLog",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Разрешите приложению доступ к истории звонков, чтобы код для входа срабатывал автоматически."
    }
}, {
    "@type":"languagePackString",
    "key":"ActionChannelChangedPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фото канала изменено"}
}, {
    "@type":"languagePackString",
    "key":"BlockUserContactsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"КОНТАКТЫ"}
}, {
    "@type":"languagePackString",
    "key":"BlockUserTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заблокировать %1$s"}
}, {
    "@type":"languagePackString",
    "key":"LeaveChannelMenu",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Покинуть канал"}
}, {
    "@type":"languagePackString",
    "key":"Unblock",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Разблокировать"}
}, {
    "@type":"languagePackString",
    "key":"EnterListName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите имя списка"}
}, {
    "@type":"languagePackString",
    "key":"PaymentWarningText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Ни Telegram, ни %1$s не будут иметь доступа к Вашей платёжной информации. Все реквизиты карт обрабатываются только платёжной системой, %2$s.\n\nОплата осуществляется напрямую разработчику %1$s. Telegram не может предоставить никаких гарантий. В случае любых проблем, пожалуйста, свяжитесь с разработчиком %1$s или своим банком."
    }
}, {
    "@type":"languagePackString",
    "key":"BlockedEmpty",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет"}
}, {
    "@type":"languagePackString",
    "key":"BlockedUsers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чёрный список"}
}, {
    "@type":"languagePackString",
    "key":"CallBack",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перезвонить"}
}, {
    "@type":"languagePackString",
    "key":"ActionRemovedPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 удалил(а) фото группы"}
}, {
    "@type":"languagePackString",
    "key":"ChatListExpanded",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Трехстрочный"}
}, {
    "@type":"languagePackString",
    "key":"BlockedUsersInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Заблокированные пользователи не смогут связаться с Вами и не будут видеть время Вашей последней активности."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportDeleteSelfieAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить это селфи?"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedChannelTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 переименовал(а) канал в «%1$s»"}
}, {
    "@type":"languagePackString",
    "key":"CancelSending",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отменить отправку"}
}, {
    "@type":"languagePackString",
    "key":"Saturation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Насыщенн."}
}, {
    "@type":"languagePackString",
    "key":"CallMessageOutgoing",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Исходящий звонок"}
}, {
    "@type":"languagePackString",
    "key":"BlurOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Откл."}
}, {
    "@type":"languagePackString",
    "key":"EventLogRemovedYouGroupPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы удалили фото группы"}
}, {
    "@type":"languagePackString",
    "key":"BlurRadial",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Радиальное"}
}, {
    "@type":"languagePackString",
    "key":"NoMediaSecret",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Здесь будут отображаться фото и видео из этого чата."}
}, {
    "@type":"languagePackString",
    "key":"CompatibilityChat",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"%1$s использует старую версию Telegram, поэтому секретные фотографии отображаются в режиме совместимости.\n\nКогда %2$s обновит Telegram, для просмотра фотографий с таймером на минуту и меньше нужно будет нажать на фото и удерживать. Вы также будете видеть уведомление, если собеседник делает скриншот."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelAddAdmin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить администратора"}
}, {
    "@type":"languagePackString",
    "key":"ChannelAddMembers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить участников"}
}, {
    "@type":"languagePackString",
    "key":"CreateNewThemeMenu",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Создать новую тему"}
}, {
    "@type":"languagePackString",
    "key":"Available",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Доступен"}
}, {
    "@type":"languagePackString",
    "key":"PassportDeleteSelfie",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить селфи?"}
}, {
    "@type":"languagePackString",
    "key":"BotCantJoinGroups",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Этот бот не может вступать в группы."}
}, {
    "@type":"languagePackString",
    "key":"BotHelp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Помощь"}
}, {
    "@type":"languagePackString",
    "key":"BotInfoTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Что умеет этот бот?"}
}, {
    "@type":"languagePackString",
    "key":"ChannelLeaveAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите покинуть канал?"}
}, {
    "@type":"languagePackString",
    "key":"NotMounted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет подключения к хранилищу"}
}, {
    "@type":"languagePackString",
    "key":"SetAdditionalPasswordInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете установить пароль, который будет запрашиваться при входе с нового устройства в дополнение к коду из SMS."
    }
}, {
    "@type":"languagePackString",
    "key":"AccDescrRepeatList",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Повтор плейлиста"}
}, {
    "@type":"languagePackString",
    "key":"SetUrlInvalidLong",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Максимальная длина ссылки — 64 символа."}
}, {
    "@type":"languagePackString",
    "key":"TypeLocationGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылка"}
}, {
    "@type":"languagePackString",
    "key":"ChannelPublicInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Публичные каналы можно найти через поиск, подписаться на них может любой пользователь."
    }
}, {
    "@type":"languagePackString",
    "key":"Brown",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Коричневый"}
}, {
    "@type":"languagePackString",
    "key":"ShareContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поделиться контактом"}
}, {
    "@type":"languagePackString",
    "key":"ActionCreateChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Канал создан"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyPhoneInfo2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете добавить пользователей или целые группы в список исключений из настроек выше."
    }
}, {
    "@type":"languagePackString",
    "key":"DiscardVoiceMessageDescription",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите прекратить запись и удалить голосовое сообщение?"
    }
}, {
    "@type":"languagePackString",
    "key":"AccActionCancelDownload",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отмена загрузки"}
}, {
    "@type":"languagePackString",
    "key":"ShareYouLocationUnable",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Приложению не удалось определить Вашу геопозицию"}
}, {
    "@type":"languagePackString",
    "key":"BotInvite",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить в группу"}
}, {
    "@type":"languagePackString",
    "key":"AbortPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прервать настройку двухэтапной аутентификации"}
}, {
    "@type":"languagePackString",
    "key":"BotPermissionGameAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Разрешить боту %1$s передавать Ваше имя и id в Telegram (не номер телефона) сайтам, которые Вы откроете с его помощью?"
    }
}, {
    "@type":"languagePackString",
    "key":"MegaPublic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Публичная группа"}
}, {
    "@type":"languagePackString",
    "key":"BotSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки"}
}, {
    "@type":"languagePackString",
    "key":"TapToChange",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите для изменения"}
}, {
    "@type":"languagePackString",
    "key":"BotStatusCantRead",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"не имеет доступа к сообщениям"}
}, {
    "@type":"languagePackString",
    "key":"ActionYouChangedTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы изменили имя группы на un2"}
}, {
    "@type":"languagePackString",
    "key":"CancelEmailQuestion",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите прервать настройку почты для восстановления пароля?"
    }
}, {
    "@type":"languagePackString",
    "key":"Italic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Курсив"}
}, {
    "@type":"languagePackString",
    "key":"BotStop",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Остановить бота"}
}, {
    "@type":"languagePackString",
    "key":"ChannelLinkInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Любой пользователь Telegram сможет подписаться на Ваш канал, перейдя по этой ссылке."
    }
}, {
    "@type":"languagePackString",
    "key":"BuiltInThemes",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стандартные темы"}
}, {
    "@type":"languagePackString",
    "key":"CacheClear",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedVoiceChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) голосовое сообщение"}
}, {
    "@type":"languagePackString",
    "key":"CallAgain",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Позвонить снова"}
}, {
    "@type":"languagePackString",
    "key":"CallMessageIncoming",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Входящий звонок"}
}, {
    "@type":"languagePackString",
    "key":"ColorBlue",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Синий"}
}, {
    "@type":"languagePackString",
    "key":"Purple",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фиолетовый"}
}, {
    "@type":"languagePackString",
    "key":"NoVotes",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет голосов"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureBlockContact2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите заблокировать **%1$s**?"}
}, {
    "@type":"languagePackString",
    "key":"PermissionNoLocation",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Telegram требуется доступ к геопозиции, чтобы вы могли отправлять её друзьям."
    }
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Райнхардт"}
}, {
    "@type":"languagePackString",
    "key":"CallMessageIncomingDeclined",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отклонённый звонок"}
}, {
    "@type":"languagePackString",
    "key":"NoSharedLinks",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Поделитесь ссылками в этом чате, и они будут доступны вам с любого устройства."
    }
}, {
    "@type":"languagePackString",
    "key":"CallMessageIncomingMissed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пропущенный звонок"}
}, {
    "@type":"languagePackString",
    "key":"AttachMediaRestrictedForever",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Администраторы группы запретили Вам отправлять в нее фото и видео."
    }
}, {
    "@type":"languagePackString",
    "key":"Caption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подпись"}
}, {
    "@type":"languagePackString",
    "key":"ClearCacheFewChatsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить кэш для %1$s"}
}, {
    "@type":"languagePackString",
    "key":"CallMessageOutgoingMissed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отменённый звонок"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsCustom",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Особые"}
}, {
    "@type":"languagePackString",
    "key":"DeleteMegaMenu",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить группу"}
}, {
    "@type":"languagePackString",
    "key":"ReportSpamAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хотите сообщить о спаме от пользователя?"}
}, {
    "@type":"languagePackString",
    "key":"ShowAllFiles",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все файлы"}
}, {
    "@type":"languagePackString",
    "key":"ChannelTooMuchJoin",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"У Вас максимальное число групп и каналов. Пожалуйста, сперва покиньте какую-нибудь группу или канал."
    }
}, {
    "@type":"languagePackString",
    "key":"EmailPasswordConfirmText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пожалуйста, выполните эти шаги, чтобы закончить настройку двухэтапной аутентификации:\n\n1. Проверьте почту (не забудьте о папке со спамом)\n%1$s\n\n2. Перейдите по ссылке для подтверждения."
    }
}, {
    "@type":"languagePackString",
    "key":"ConnectingToProxyDisable",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключить"}
}, {
    "@type":"languagePackString",
    "key":"CallMessageReportProblem",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Оцените звонок"}
}, {
    "@type":"languagePackString",
    "key":"ActionYouScoredInGame",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы набрали %1$s в игре un2"}
}, {
    "@type":"languagePackString",
    "key":"FootsAway",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s фт отсюда"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsPriorityMax",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Максимальный"}
}, {
    "@type":"languagePackString",
    "key":"SendLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить свою геопозицию"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageDocument",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новый файл"}
}, {
    "@type":"languagePackString",
    "key":"ColorYellow",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Желтый"}
}, {
    "@type":"languagePackString",
    "key":"TelegramPassportCreatePasswordInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы защитить свои данные оконечным шифрованием, пожалуйста, задайте пароль.\n\nЭтот пароль также будет запрашиваться при входе в Telegram с нового устройства."
    }
}, {
    "@type":"languagePackString",
    "key":"AwayTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отсюда"}
}, {
    "@type":"languagePackString",
    "key":"MasksInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете добавлять маски на отправляемые фото. Для этого перед отправкой откройте фоторедактор."
    }
}, {
    "@type":"languagePackString",
    "key":"CallMessageWithDuration",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s (%2$s)"}
}, {
    "@type":"languagePackString",
    "key":"AbortEmail",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прервать настройку почты"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить этот контакт?"}
}, {
    "@type":"languagePackString",
    "key":"P2PContactsPlus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мои контакты (+%1$d)"}
}, {
    "@type":"languagePackString",
    "key":"StickersShare",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поделиться"}
}, {
    "@type":"languagePackString",
    "key":"ChromeCustomTabsInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открывать внешние ссылки в приложении"}
}, {
    "@type":"languagePackString",
    "key":"CallNotAvailable",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, **%1$s** не принимает звонки."}
}, {
    "@type":"languagePackString",
    "key":"URL",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"URL"}
}, {
    "@type":"languagePackString",
    "key":"CallReportLogsExplain",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Это не раскроет содержание разговора и поможет решить проблему быстрее."
    }
}, {
    "@type":"languagePackString",
    "key":"CallsTotalTime",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всего времени"}
}, {
    "@type":"languagePackString",
    "key":"MetersAway",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"м отсюда"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadPhotosTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Автозагрузка фото"}
}, {
    "@type":"languagePackString",
    "key":"ContactJobTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Должность"}
}, {
    "@type":"languagePackString",
    "key":"LinkInvalidStartNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя канала не может начинаться с цифры."}
}, {
    "@type":"languagePackString",
    "key":"WhoCanAddMembersAllMembers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все участники"}
}, {
    "@type":"languagePackString",
    "key":"CancelPollAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить опрос?"}
}, {
    "@type":"languagePackString",
    "key":"EnabledPasswordText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Включена двухэтапная аутентификация.\nДля входа в свой аккаунт в Telegram Вам нужно будет вводить установленный здесь пароль."
    }
}, {
    "@type":"languagePackString",
    "key":"ReportUnrelatedGroupTextNoAddress",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пожалуйста, сообщите нам, если группа никак не связана с местом, где Вы находитесь."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_ET",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на эстонском языке"}
}, {
    "@type":"languagePackString",
    "key":"ChannelUserAddLimit",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Извините, вы можете добавить в канал только первых 200 участников. А вот по пригласительной ссылке на канал может подписаться неограниченное количество человек."
    }
}, {
    "@type":"languagePackString",
    "key":"LanguageUnofficial",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Неофициальные переводы"}
}, {
    "@type":"languagePackString",
    "key":"Change",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить"}
}, {
    "@type":"languagePackString",
    "key":"ChangeChatBackground",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить фон чатов"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyCallsP2PHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если выключить peer-to-peer, все звонки будут идти через серверы Telegram. Собеседник не сможет узнать ваш IP-адрес, но качество звука немного ухудшится."
    }
}, {
    "@type":"languagePackString",
    "key":"LocatedChannelsTooMuch",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"У этого пользователя максимальное число групп с геопозицией. Пожалуйста, предложите ему сперва удалить или передать кому-либо одну из его групп."
    }
}, {
    "@type":"languagePackString",
    "key":"ChangePermissions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить разрешения"}
}, {
    "@type":"languagePackString",
    "key":"ThemeCreateHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"По этой ссылке тему может установить любой пользователь. Когда Вы редактируете тему, она будет автоматически обновляться у всех, кто её использует.\n\nНовая тема будет создана на основе темы и фона, которые Вы используете"
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedYouGroupPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы установили новое фото группы"}
}, {
    "@type":"languagePackString",
    "key":"ChangePublicLimitReached",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, Вы используете слишком много публичных ссылок.\nВы можете сбросить одну из уже занятых ссылок на свои группы или каналы или не создавать новую публичную ссылку."
    }
}, {
    "@type":"languagePackString",
    "key":"ThemePreview",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предпросмотр темы"}
}, {
    "@type":"languagePackString",
    "key":"ArchivePinned",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Архив закреплен."}
}, {
    "@type":"languagePackString",
    "key":"ChooseStickerSet",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ВЫБЕРИТЕ СТИКЕРЫ"}
}, {
    "@type":"languagePackString",
    "key":"ChannelAddException",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить исключение"}
}, {
    "@type":"languagePackString",
    "key":"PassportNativeHeaderLang",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваше имя %1$s"}
}, {
    "@type":"languagePackString",
    "key":"ChannelAdminsInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете назначать помощников для управления каналом. Чтобы удалить администратора, нажмите и удерживайте."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelAddTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пригласить %1$s в канал?"}
}, {
    "@type":"languagePackString",
    "key":"LastSeenContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мои контакты"}
}, {
    "@type":"languagePackString",
    "key":"CallReportIncludeLogs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Техническая информация"}
}, {
    "@type":"languagePackString",
    "key":"ChannelAddToGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить в группу"}
}, {
    "@type":"languagePackString",
    "key":"DeleteChatUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить чат"}
}, {
    "@type":"languagePackString",
    "key":"CancelAccountReset",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отмена сброса аккаунта"}
}, {
    "@type":"languagePackString",
    "key":"EventLogAdded",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 добавил(а) un2"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMute",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"УБРАТЬ ЗВУК"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrStickerSet",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Набор стикеров"}
}, {
    "@type":"languagePackString",
    "key":"ConvertGroupInfo2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**В супергруппах:**\n\n• Новые участники видят всю историю сообщений\n• Удалённые сообщения исчезают у всех участников\n• Администраторы могут указать описание группы\n• Создатель может выбрать для группы публичную ссылку"
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelAdmin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Админ"}
}, {
    "@type":"languagePackString",
    "key":"ChatListDefault",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Двустрочный"}
}, {
    "@type":"languagePackString",
    "key":"ChannelBlacklist",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чёрный список"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyFloodControlError",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Действие выполняется слишком часто. Не удалось изменить настройки приватности, пожалуйста, подождите."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelBots",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Боты"}
}, {
    "@type":"languagePackString",
    "key":"DeleteProxy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить прокси?"}
}, {
    "@type":"languagePackString",
    "key":"ChannelBroadcast",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Публикация"}
}, {
    "@type":"languagePackString",
    "key":"ChannelCantOpenBanned",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, Вам запрещено состоять в публичных группах."
    }
}, {
    "@type":"languagePackString",
    "key":"Invisible",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"невидимый"}
}, {
    "@type":"languagePackString",
    "key":"ChannelCantOpenNa",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, чат больше не доступен."}
}, {
    "@type":"languagePackString",
    "key":"NoPhotos",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фотографий пока нет"}
}, {
    "@type":"languagePackString",
    "key":"ChannelCantOpenPrivate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, чат больше не доступен."}
}, {
    "@type":"languagePackString",
    "key":"Connecting",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Соединение..."}
}, {
    "@type":"languagePackString",
    "key":"SetTimer",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Включить удаление по таймеру"}
}, {
    "@type":"languagePackString",
    "key":"ChannelContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Контакты в этом канале"}
}, {
    "@type":"languagePackString",
    "key":"BlockUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заблокировать"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadCustom",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Другой"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_UZ",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на узбекском языке"}
}, {
    "@type":"languagePackString",
    "key":"ManageGroupMenu",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Управление группой"}
}, {
    "@type":"languagePackString",
    "key":"CurvesBlue",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СИНИЙ"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Включены"}
}, {
    "@type":"languagePackString",
    "key":"UsernameInvalidStartNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя пользователя не может начинаться с цифры."}
}, {
    "@type":"languagePackString",
    "key":"ChannelDeleteMenu",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить канал"}
}, {
    "@type":"languagePackString",
    "key":"Blue",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Синий"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedYouGroupTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы переименовали группу в «%1$s»"}
}, {
    "@type":"languagePackString",
    "key":"ChannelEdit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPromoted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"изменены права для %1$s"}
}, {
    "@type":"languagePackString",
    "key":"RegisterText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Укажите имя и фамилию"}
}, {
    "@type":"languagePackString",
    "key":"ChannelEditPermissions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить разрешения"}
}, {
    "@type":"languagePackString",
    "key":"ChannelJoin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПРИСОЕДИНИТЬСЯ"}
}, {
    "@type":"languagePackString",
    "key":"AttachSticker",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикер"}
}, {
    "@type":"languagePackString",
    "key":"Satellite",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Спутник"}
}, {
    "@type":"languagePackString",
    "key":"HistoryCleared",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"История очищена"}
}, {
    "@type":"languagePackString",
    "key":"ChannelDeletedUndo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Канал удалён."}
}, {
    "@type":"languagePackString",
    "key":"ArchivedMasksAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Архив масок"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadAudioInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Голосовые сообщения имеют небольшой размер и всегда загружаются автоматически."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelJoinTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хотите подписаться на канал «%1$s»?"}
}, {
    "@type":"languagePackString",
    "key":"StickersCopy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Копировать ссылку"}
}, {
    "@type":"languagePackString",
    "key":"AddToFavorites",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить в избранные"}
}, {
    "@type":"languagePackString",
    "key":"EnableAnimations",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Анимация"}
}, {
    "@type":"languagePackString",
    "key":"PaymentConnectionFailed",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Не удалось соединиться с платёжным сервером. Пожалуйста, проверьте подключение к интернету и попробуйте снова."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelLeaveAlertWithName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите покинуть **%1$s**?"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_IS",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на исландском языке"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMembersInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Этот список видят только администраторы канала."}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить %1$s?"}
}, {
    "@type":"languagePackString",
    "key":"formatterYearMax",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"dd.MM.yyyy"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenuResetContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить контакты"}
}, {
    "@type":"languagePackString",
    "key":"ActionChangedTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 изменил(а) имя группы на un2"}
}, {
    "@type":"languagePackString",
    "key":"ChatAdmin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"админ"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedGeoChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) карту"}
}, {
    "@type":"languagePackString",
    "key":"RateCallDistorted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звук был искажён"}
}, {
    "@type":"languagePackString",
    "key":"LastSeen",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"был(а)"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канал %1$s отправили контакт"}
}, {
    "@type":"languagePackString",
    "key":"UseLessDataOnMobile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В мобильной сети"}
}, {
    "@type":"languagePackString",
    "key":"GettingLinkInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузка предпросмотра для ссылки..."}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageFew",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s опубликовал(а) %2$s"}
}, {
    "@type":"languagePackString",
    "key":"ChannelPrivateInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"На частные каналы можно подписаться только по ссылке-приглашению."
    }
}, {
    "@type":"languagePackString",
    "key":"CreateNewThemeInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Создайте собственную тему, изменяя цвета в приложении. Здесь Вы всегда можете вернуться к теме, которая использовалась по умолчанию."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageGIF",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новый GIF"}
}, {
    "@type":"languagePackString",
    "key":"ChannelCantSendMessage",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, Вы не можете отправлять сообщения в этот канал."
    }
}, {
    "@type":"languagePackString",
    "key":"Bot",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"бот"}
}, {
    "@type":"languagePackString",
    "key":"NotificationGroupLeftMember",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s больше не состоит в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageLiveLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новая трансляция геопозиции"}
}, {
    "@type":"languagePackString",
    "key":"ResetAccountButton",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СБРОСИТЬ АККАУНТ"}
}, {
    "@type":"languagePackString",
    "key":"ChatYourSelfName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы"}
}, {
    "@type":"languagePackString",
    "key":"ApplyTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПРИМЕНИТЬ"}
}, {
    "@type":"languagePackString",
    "key":"BadgeNumberMutedChats",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чаты без уведомлений"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageMusic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новый аудиофайл"}
}, {
    "@type":"languagePackString",
    "key":"ChatsArchived",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чаты перенесены в архив."}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageNoText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новая публикация"}
}, {
    "@type":"languagePackString",
    "key":"ColorWhite",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Белый"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageRound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новое видеосообщение"}
}, {
    "@type":"languagePackString",
    "key":"SignUp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Зарегистрироваться"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureShareMyContactInfoUser",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Хотите поделиться своим номером телефона %1$s с **%2$s**?"
    }
}, {
    "@type":"languagePackString",
    "key":"TapToChangePhone",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите, чтобы изменить номер телефона"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageStickerEmoji",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канал %1$s отправили стикер %2$s"}
}, {
    "@type":"languagePackString",
    "key":"UnsupportedAttachment",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вложение не поддерживается"}
}, {
    "@type":"languagePackString",
    "key":"ChannelDeleteAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Важно! Если удалить канал, все участники и все сообщения в нём будут потеряны. Всё равно удалить?"
    }
}, {
    "@type":"languagePackString",
    "key":"PrivacyLastSeen",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Последняя активность"}
}, {
    "@type":"languagePackString",
    "key":"LoginAttempts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Незавершенные попытки входа"}
}, {
    "@type":"languagePackString",
    "key":"ChatYourSelfDescription1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пересылайте сюда нужные сообщения"}
}, {
    "@type":"languagePackString",
    "key":"ChannelModerator",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Модератор"}
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedGeo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) карту"}
}, {
    "@type":"languagePackString",
    "key":"ChannelRemoveUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить из канала"}
}, {
    "@type":"languagePackString",
    "key":"CustomShareSettingsHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете добавить пользователей или целые группы в список исключений из настроек выше."
    }
}, {
    "@type":"languagePackString",
    "key":"TextSizeHeader",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Размер текста сообщений"}
}, {
    "@type":"languagePackString",
    "key":"ChannelOtherMembers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Другие участники"}
}, {
    "@type":"languagePackString",
    "key":"OnlyWhenScreenOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Только когда экран включён"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionChannelGroupSetHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Подписчики смогут перейти в **%1$s**, нажав на кнопку «Обсудить»."
    }
}, {
    "@type":"languagePackString",
    "key":"IncorrectTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Некорректный файл темы"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenuDisableCamera",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выключить встроенную камеру"}
}, {
    "@type":"languagePackString",
    "key":"NoUsernameFound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет аккаунта Telegram с таким именем пользователя."}
}, {
    "@type":"languagePackString",
    "key":"ChannelTooMuch",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, Вы участвуете в максимальном числе групп и каналов. Пожалуйста, покиньте какие-то из них, прежде чем создавать новые."
    }
}, {
    "@type":"languagePackString",
    "key":"LinkCopied",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылка скопирована в буфер обмена."}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_DE",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на немецком языке"}
}, {
    "@type":"languagePackString",
    "key":"NoStickersFound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикеры не найдены"}
}, {
    "@type":"languagePackString",
    "key":"SetPasscode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Установить код-пароль"}
}, {
    "@type":"languagePackString",
    "key":"ChatGallery",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Галерея"}
}, {
    "@type":"languagePackString",
    "key":"EncryptedPlaceholderTitleOutgoing",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы пригласили %s в секретный чат."}
}, {
    "@type":"languagePackString",
    "key":"AddPeopleNearby",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Найти людей рядом"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadPreloadVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предзагрузка длинных видео"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsPriorityDefault",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"По умолчанию"}
}, {
    "@type":"languagePackString",
    "key":"ChannelPermissions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Разрешения"}
}, {
    "@type":"languagePackString",
    "key":"formatterYear",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"dd.MM.yy"}
}, {
    "@type":"languagePackString",
    "key":"ChannelPermissionsHeader",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Возможности участников:"}
}, {
    "@type":"languagePackString",
    "key":"ErrorSendRestrictedPollsAll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В этой группе запрещено публиковать опросы."}
}, {
    "@type":"languagePackString",
    "key":"ChannelPhotoEditNotification",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фото канала %1$s обновлено"}
}, {
    "@type":"languagePackString",
    "key":"EventLogOriginalCaption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Исходная подпись"}
}, {
    "@type":"languagePackString",
    "key":"ChannelPrivate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Частный канал"}
}, {
    "@type":"languagePackString",
    "key":"CantPlayVideo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Не удалось воспроизвести видео в приложении. Попробовать сторонний видеоплеер?"
    }
}, {
    "@type":"languagePackString",
    "key":"MakeAdmin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"НАЗНАЧИТЬ"}
}, {
    "@type":"languagePackString",
    "key":"AutodownloadContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Контакты"}
}, {
    "@type":"languagePackString",
    "key":"ChannelRemoveUserAdmin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить из администраторов"}
}, {
    "@type":"languagePackString",
    "key":"ChromeCustomTabs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Встроенный браузер"}
}, {
    "@type":"languagePackString",
    "key":"BytesSent",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправлено байт"}
}, {
    "@type":"languagePackString",
    "key":"ManageChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Управление каналом"}
}, {
    "@type":"languagePackString",
    "key":"LanguageSame",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы уже используете этот язык (**%1$s**). Вы можете сменить язык в любое время в Настройках."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelSearchException",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск исключений"}
}, {
    "@type":"languagePackString",
    "key":"PassportSurnameCountry",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фамилия (%1$s)"}
}, {
    "@type":"languagePackString",
    "key":"CancelLinkSuccess",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Процесс удаления Вашего аккаунта %1$s был отменён. Теперь можете закрыть это окно."
    }
}, {
    "@type":"languagePackString",
    "key":"AllowFillNumber",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Разрешите приложению принимать звонки, чтобы мы могли подтвердить Ваш номер телефона автоматически."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelInviteViaLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пригласить по ссылке"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddCard",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить удостоверение личности"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsCustom",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Другой"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadOnUpToFor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"До %1$s: %2$s"}
}, {
    "@type":"languagePackString",
    "key":"VoipOngoingAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Сейчас Вы разговариваете с **%1$s**. Хотите завершить этот разговор и начать новый с **%2$s**?"
    }
}, {
    "@type":"languagePackString",
    "key":"DiscussionLinkGroupPublicPrivateAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Назначить **%1$s** группой для обсуждения **%2$s**?\n\nВсе подписчики канала смогут видеть сообщения в группе."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки"}
}, {
    "@type":"languagePackString",
    "key":"NewTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новая цветовая тема"}
}, {
    "@type":"languagePackString",
    "key":"ReportChatPornography",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Порнография"}
}, {
    "@type":"languagePackString",
    "key":"ChannelSettingsChangedAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы изменили настройки канала. Применить изменения?"}
}, {
    "@type":"languagePackString",
    "key":"ColorGreen",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Зелёный"}
}, {
    "@type":"languagePackString",
    "key":"ChatsMute",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Убрать звук"}
}, {
    "@type":"languagePackString",
    "key":"AddStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить стикеры"}
}, {
    "@type":"languagePackString",
    "key":"ChannelSilentBroadcast",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Тихая публикация"}
}, {
    "@type":"languagePackString",
    "key":"InstantViewNightMode",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Тёмная тема будет автоматически включаться в ночное время"
    }
}, {
    "@type":"languagePackString",
    "key":"Gallery",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Галерея"}
}, {
    "@type":"languagePackString",
    "key":"ChannelType",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Тип канала"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrPhotoAdjust",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройка"}
}, {
    "@type":"languagePackString",
    "key":"AddContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить контакт"}
}, {
    "@type":"languagePackString",
    "key":"UseProxyPort",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Порт"}
}, {
    "@type":"languagePackString",
    "key":"ChannelUnmute",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ВКЛ. ЗВУК"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedSticker",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) стикер в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"DescriptionOptionalPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Описание (необязательно)"}
}, {
    "@type":"languagePackString",
    "key":"ChatBackground",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить фон чатов"}
}, {
    "@type":"languagePackString",
    "key":"ArchivedMasksInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"У Вас может быть установлено до 200 наборов масок. При установке новых наборов неиспользуемые маски архивируются."
    }
}, {
    "@type":"languagePackString",
    "key":"StopPollAlertText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если Вы остановите опрос, никто больше не сможет проголосовать в нём. Это действие нельзя будет отменить."
    }
}, {
    "@type":"languagePackString",
    "key":"login_with_telegram",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Войти через Telegram"}
}, {
    "@type":"languagePackString",
    "key":"ChatDocument",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Файл"}
}, {
    "@type":"languagePackString",
    "key":"EventLogChangedChannelLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 изменил(а) ссылку на канал:"}
}, {
    "@type":"languagePackString",
    "key":"ChatHintsDelete",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Убрать %1$s из подсказок?"}
}, {
    "@type":"languagePackString",
    "key":"ChatHistory",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"История чата для новых участников"}
}, {
    "@type":"languagePackString",
    "key":"MessageLifetime",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удаление по таймеру"}
}, {
    "@type":"languagePackString",
    "key":"P2PContactsMinus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мои контакты (-%1$d)"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrSecretChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Секретный чат"}
}, {
    "@type":"languagePackString",
    "key":"ChatHistoryHidden",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Скрыта"}
}, {
    "@type":"languagePackString",
    "key":"ChatHistoryHiddenInfo2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Новые участники будут видеть не больше 100 последних сообщений."
    }
}, {
    "@type":"languagePackString",
    "key":"ChatCamera",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Камера"}
}, {
    "@type":"languagePackString",
    "key":"DeleteMessagesTextGroup",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы удалить отправленные Вами %1$s из истории для других участников группы, выберите «Отозвать мои сообщения»."
    }
}, {
    "@type":"languagePackString",
    "key":"ChatSetLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Указать геопозицию"}
}, {
    "@type":"languagePackString",
    "key":"AddToTheGroupForwardCount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сколько последних сообщений показать:"}
}, {
    "@type":"languagePackString",
    "key":"MegaUsernameHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если у группы будет постоянная публичная ссылка, другие пользователи смогут найти её и присоединиться.\n\nМожно использовать латиницу (a-z), цифры (0-9) и подчёркивание. Минимальная длина — 5 символов."
    }
}, {
    "@type":"languagePackString",
    "key":"VoipOfflineTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не в сети"}
}, {
    "@type":"languagePackString",
    "key":"ChatVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Видео"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageScheduled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"📅 Вы: %1$s"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadPreloadMusicInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Начать загрузку следующего трека во время прослушивания аудиофайла."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogGroupJoined",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 вступил(а) в группу"}
}, {
    "@type":"languagePackString",
    "key":"UnreadMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Непрочитанные сообщения"}
}, {
    "@type":"languagePackString",
    "key":"ChatYourSelf",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"сохранённые сообщения"}
}, {
    "@type":"languagePackString",
    "key":"ChatYourSelfDescription2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Храните здесь фото и видео"}
}, {
    "@type":"languagePackString",
    "key":"MapPreviewProviderNobody",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не использовать"}
}, {
    "@type":"languagePackString",
    "key":"PasswordCode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Код"}
}, {
    "@type":"languagePackString",
    "key":"SharedPhotosAndVideos",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фотографии и видео"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrIVHeading",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заголовок"}
}, {
    "@type":"languagePackString",
    "key":"ClearOtherWebSessionsHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете использовать свой аккаунт для авторизации на сайтах, которые поддерживают вход через Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"SetReminder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Задать напоминание"}
}, {
    "@type":"languagePackString",
    "key":"ChatYourSelfDescription3",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заходите в этот чат с любых устройств"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenuDisableLogs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выключить логирование"}
}, {
    "@type":"languagePackString",
    "key":"Page3Message",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**Telegram** бесплатный навсегда. Без рекламы.\nБез абонентской платы."
    }
}, {
    "@type":"languagePackString",
    "key":"ActionYouAddUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы добавили un2"}
}, {
    "@type":"languagePackString",
    "key":"FirstName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя (обязательно)"}
}, {
    "@type":"languagePackString",
    "key":"AddAdminErrorBlacklisted",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, Вы не можете назначить этого пользователя администратором, поскольку он находится в чёрном списке, а Вы не можете убирать людей из чёрного списка."
    }
}, {
    "@type":"languagePackString",
    "key":"ChatSetThisLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбрать эту геопозицию"}
}, {
    "@type":"languagePackString",
    "key":"ChatYourSelfDescription4",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Найти нужное сообщение поможет поиск"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsPriorityUrgent",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Срочный"}
}, {
    "@type":"languagePackString",
    "key":"AllowCustomQuickReply",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Задать свой текст"}
}, {
    "@type":"languagePackString",
    "key":"ActionAddUserSelfMega",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 теперь в группе"}
}, {
    "@type":"languagePackString",
    "key":"LastSeenDate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"был(а)"}
}, {
    "@type":"languagePackString",
    "key":"ChatsUnmute",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вкл. уведомления"}
}, {
    "@type":"languagePackString",
    "key":"NotificationContactJoined",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s теперь в Telegram!"}
}, {
    "@type":"languagePackString",
    "key":"ChooseCountry",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите страну"}
}, {
    "@type":"languagePackString",
    "key":"ChooseFromGallery",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузить из галереи"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundBlurred",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Размытие"}
}, {
    "@type":"languagePackString",
    "key":"Cancel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отмена"}
}, {
    "@type":"languagePackString",
    "key":"PasswordRecovery",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Восстановление пароля"}
}, {
    "@type":"languagePackString",
    "key":"ChooseFromSearch",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск в сети"}
}, {
    "@type":"languagePackString",
    "key":"ChooseFromYourStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите из своих наборов"}
}, {
    "@type":"languagePackString",
    "key":"BadgeNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Счётчик сообщений"}
}, {
    "@type":"languagePackString",
    "key":"ChoosePhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбрать фото"}
}, {
    "@type":"languagePackString",
    "key":"_tg_cancel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отмена"}
}, {
    "@type":"languagePackString",
    "key":"ChooseStickerSetMy",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете создать собственные наборы стикеров с помощью бота @stickers."
    }
}, {
    "@type":"languagePackString",
    "key":"KeepMediaInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Фото, видео и другие файлы, которые Вы **не смотрели** в течение этого срока, будут удалены с устройства для экономии места на телефоне.\n\nВсе медиа останутся в облаке Telegram, при необходимости Вы сможете загрузить их снова."
    }
}, {
    "@type":"languagePackString",
    "key":"TodayAtFormatted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"в %1$s"}
}, {
    "@type":"languagePackString",
    "key":"LocalDatabaseInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Очистка локальной базы данных удалит кэш сообщений и сожмёт базу данных для экономии места на внутреннем накопителе. Некоторые данные нужны для работы Telegram, поэтому размер базы данных останется больше нуля.\n\nЭта операция может занять несколько минут."
    }
}, {
    "@type":"languagePackString",
    "key":"AutoNightDisabled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не использовать"}
}, {
    "@type":"languagePackString",
    "key":"ChooseStickerSetNotFound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Набор стикеров не найден"}
}, {
    "@type":"languagePackString",
    "key":"NoOtherWebSessionsInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете использовать свой аккаунт для авторизации на сайтах, которые поддерживают вход через Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogFilterGroupInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Информация о группе"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureClearHistoryWithUser",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите очистить историю переписки с **%1$s**?"
    }
}, {
    "@type":"languagePackString",
    "key":"ChooseStickerSetNotFoundInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Попробуйте ещё раз или выберите из списка ниже"}
}, {
    "@type":"languagePackString",
    "key":"Code",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Код"}
}, {
    "@type":"languagePackString",
    "key":"ChooseYourLanguage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите язык"}
}, {
    "@type":"languagePackString",
    "key":"ClearFewChatsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить %1$s"}
}, {
    "@type":"languagePackString",
    "key":"YourName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваше имя"}
}, {
    "@type":"languagePackString",
    "key":"ClearHistory",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить историю"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeletePhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить фото?"}
}, {
    "@type":"languagePackString",
    "key":"NewConversationShortcut",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новая беседа"}
}, {
    "@type":"languagePackString",
    "key":"NoHandleAppInstalled",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"У вас нет приложения для работы с файлами типа «%1$s», пожалуйста, установите его, чтобы продолжить"
    }
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_RO",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на румынском языке"}
}, {
    "@type":"languagePackString",
    "key":"ClearHistoryOptionAlso",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Также очистить для %1$s"}
}, {
    "@type":"languagePackString",
    "key":"ChannelTypeHeader",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Тип канала"}
}, {
    "@type":"languagePackString",
    "key":"Decline",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отклонить"}
}, {
    "@type":"languagePackString",
    "key":"CopyLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Копировать ссылку"}
}, {
    "@type":"languagePackString",
    "key":"ClearMediaCache",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить кэш"}
}, {
    "@type":"languagePackString",
    "key":"ClearOtherSessionsHelp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выйти на всех устройствах, кроме этого."}
}, {
    "@type":"languagePackString",
    "key":"ClearRecentEmoji",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить недавние эмодзи?"}
}, {
    "@type":"languagePackString",
    "key":"Contacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Контакты"}
}, {
    "@type":"languagePackString",
    "key":"ClearSearch",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить историю поиска?"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrCancelForward",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отменить пересылку"}
}, {
    "@type":"languagePackString",
    "key":"PassportDeleteDocumentAddress",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить данные об адресе"}
}, {
    "@type":"languagePackString",
    "key":"VoipPeerIncompatible",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**%1$s** использует приложение с несовместимой версией протокола. Пользователю нужно обновить приложение, чтобы вы могли созвониться."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageAudio",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новое голосовое сообщение"}
}, {
    "@type":"languagePackString",
    "key":"ClearSearchAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите очистить историю поиска?"}
}, {
    "@type":"languagePackString",
    "key":"Close",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закрыть"}
}, {
    "@type":"languagePackString",
    "key":"CreateEncryptedChatError",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Произошла ошибка."}
}, {
    "@type":"languagePackString",
    "key":"EventLogChannelJoined",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 теперь подписан(а) на канал"}
}, {
    "@type":"languagePackString",
    "key":"ColorPink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Розовый"}
}, {
    "@type":"languagePackString",
    "key":"DeleteChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить и выйти"}
}, {
    "@type":"languagePackString",
    "key":"ColorTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Цветовая тема"}
}, {
    "@type":"languagePackString",
    "key":"SendMessageRestrictedForever",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Администраторы группы запретили Вам писать в ней."}
}, {
    "@type":"languagePackString",
    "key":"AccDescrSearchByUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фильтр по участнику"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsServiceConnectionInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Поддерживать минимальное фоновое соединение с Telegram, чтобы получать уведомления. Обеспечивает надежность уведомлений."
    }
}, {
    "@type":"languagePackString",
    "key":"ColorViolet",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фиолетовый"}
}, {
    "@type":"languagePackString",
    "key":"ConfirmDeleteCallLog",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить запись из истории звонков?"}
}, {
    "@type":"languagePackString",
    "key":"AttachDocument",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Файл"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) видео"}
}, {
    "@type":"languagePackString",
    "key":"ActionYouRemovedPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы удалили фото группы"}
}, {
    "@type":"languagePackString",
    "key":"Connected",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подключен"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsPinMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закрепление сообщений"}
}, {
    "@type":"languagePackString",
    "key":"ConnectingToProxy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подключение к прокси..."}
}, {
    "@type":"languagePackString",
    "key":"ArchiveHintText2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чаты с выключенными уведомлениями остаются в архиве, когда приходят новые сообщения."
    }
}, {
    "@type":"languagePackString",
    "key":"GroupSettingsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки группы"}
}, {
    "@type":"languagePackString",
    "key":"LinkChecking",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Проверка имени…"}
}, {
    "@type":"languagePackString",
    "key":"ConnectingToProxyEnable",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Включить"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedDeleteMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удаление сообщений"}
}, {
    "@type":"languagePackString",
    "key":"ResetNotificationsAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите сбросить настройки уведомлений и использовать настройки по умолчанию?"
    }
}, {
    "@type":"languagePackString",
    "key":"ContactBirthday",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"День рождения"}
}, {
    "@type":"languagePackString",
    "key":"ActionAddUserSelfYou",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы вернулись в группу"}
}, {
    "@type":"languagePackString",
    "key":"DiscardVoiceMessageAction",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Да"}
}, {
    "@type":"languagePackString",
    "key":"EventLogToggledSignaturesOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 отключил(а) подписи к сообщениям"}
}, {
    "@type":"languagePackString",
    "key":"ContactJoined",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Контакт присоединился к Telegram"}
}, {
    "@type":"languagePackString",
    "key":"SendingAudio",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"отправляет аудио..."}
}, {
    "@type":"languagePackString",
    "key":"ContactNotRegistered",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"%1$s пока нет в Telegram, хотите пригласить присоединиться?"
    }
}, {
    "@type":"languagePackString",
    "key":"GroupAddMembers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить участников"}
}, {
    "@type":"languagePackString",
    "key":"NoScheduledMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Здесь пока нет отложенных сообщений..."}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessagesPeopleDisplayOrder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s %2$s"}
}, {
    "@type":"languagePackString",
    "key":"ContactSupport",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обратиться в поддержку"}
}, {
    "@type":"languagePackString",
    "key":"DataUsage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использование сети и кэша"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить группу для комментариев."}
}, {
    "@type":"languagePackString",
    "key":"ChooseYourLanguageOther",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Другой"}
}, {
    "@type":"languagePackString",
    "key":"CreateGroupError",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы не можете создать группу с этими пользователями из-за их настроек приватности."
    }
}, {
    "@type":"languagePackString",
    "key":"LanguageUnknownTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Недостаточно данных"}
}, {
    "@type":"languagePackString",
    "key":"ChannelInviteLinkTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылка-приглашение"}
}, {
    "@type":"languagePackString",
    "key":"PopupEnabled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Включены"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightAdaptive",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Адаптивная"}
}, {
    "@type":"languagePackString",
    "key":"ContactSupportInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если возникли неполадки, сообщите нам; выход из аккаунта обычно не помогает."
    }
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedRound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) видеосообщение"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrBackspace",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Backspace"}
}, {
    "@type":"languagePackString",
    "key":"Continue",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Продолжить"}
}, {
    "@type":"languagePackString",
    "key":"CantAddBotAsAdmin",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, добавить бота в канал можно только как администратора."
    }
}, {
    "@type":"languagePackString",
    "key":"BotUnblock",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПЕРЕЗАПУСТИТЬ"}
}, {
    "@type":"languagePackString",
    "key":"ChannelPrivateLinkHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"По этой ссылке можно подписаться на Ваш канал. Вы можете сбросить её в любой момент."
    }
}, {
    "@type":"languagePackString",
    "key":"Contrast",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Контраст"}
}, {
    "@type":"languagePackString",
    "key":"AllowReadCallAndLog",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Разрешите приложению принимать звонки и видеть историю звонков, чтобы код для входа срабатывал автоматически."
    }
}, {
    "@type":"languagePackString",
    "key":"VoipOngoingAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Другой вызов на линии"}
}, {
    "@type":"languagePackString",
    "key":"ConvertGroupAlertWarning",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Внимание"}
}, {
    "@type":"languagePackString",
    "key":"BotStatusRead",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"имеет доступ к сообщениям"}
}, {
    "@type":"languagePackString",
    "key":"EditThemeTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить тему"}
}, {
    "@type":"languagePackString",
    "key":"NoMasks",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Масок пока нет"}
}, {
    "@type":"languagePackString",
    "key":"ContinueOnThisLanguage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Продолжить на русском"}
}, {
    "@type":"languagePackString",
    "key":"ConvertGroupInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**Максимальное количество участников.**\n\nСделайте группу супергруппой, чтобы увеличить число участников и получить другие возможности:\n\n• В супергруппах могут общаться до %1$s участников\n• Новые участники могут видеть всю историю сообщений\n• Удалённые сообщения исчезают у всех пользователей\n• Администраторы могут указать описание группы\n• Создатель может выбрать для группы публичную ссылку"
    }
}, {
    "@type":"languagePackString",
    "key":"Open",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открыть"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrPrevious",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Назад"}
}, {
    "@type":"languagePackString",
    "key":"CacheEmpty",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пусто"}
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadVideosTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Автозагрузка видео и GIF"}
}, {
    "@type":"languagePackString",
    "key":"LogOutTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выход"}
}, {
    "@type":"languagePackString",
    "key":"MessageScheduledBotAction",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Это действие будет доступно после публикации сообщения."
    }
}, {
    "@type":"languagePackString",
    "key":"Copy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Копировать"}
}, {
    "@type":"languagePackString",
    "key":"UpdateContactsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обновить контакты?"}
}, {
    "@type":"languagePackString",
    "key":"CountReceived",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Получено"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключены"}
}, {
    "@type":"languagePackString",
    "key":"PinnedMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закреплённые сообщения"}
}, {
    "@type":"languagePackString",
    "key":"ChannelRestrictedUsers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пользователи с ограничениями"}
}, {
    "@type":"languagePackString",
    "key":"ClearButton",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить"}
}, {
    "@type":"languagePackString",
    "key":"ClearCache",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить кэш"}
}, {
    "@type":"languagePackString",
    "key":"Create",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Создать"}
}, {
    "@type":"languagePackString",
    "key":"CloseEditor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ЗАКРЫТЬ РЕДАКТОР"}
}, {
    "@type":"languagePackString",
    "key":"ChatsNearbyHeader",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Группы рядом"}
}, {
    "@type":"languagePackString",
    "key":"EventLogChangedLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 изменил(а) геопозицию группы на \"%1$s\""}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureBlockContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите заблокировать этот контакт?"}
}, {
    "@type":"languagePackString",
    "key":"CropReset",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сброс"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureClearHistoryChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить из кэша текст и медиафайлы этого канала?"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedRoundChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) видеосообщение"}
}, {
    "@type":"languagePackString",
    "key":"PassportReverseSideInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите фотографию обратной стороны документа"}
}, {
    "@type":"languagePackString",
    "key":"chatDate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"d MMMM"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageAlbum",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) альбом"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrAttachButton",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прикрепить медиа"}
}, {
    "@type":"languagePackString",
    "key":"ColorThemes",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Цветовые темы"}
}, {
    "@type":"languagePackString",
    "key":"CurrentGroupStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикеры группы"}
}, {
    "@type":"languagePackString",
    "key":"ChannelAddedBy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 пригласил(а) Вас в этот канал"}
}, {
    "@type":"languagePackString",
    "key":"Search",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск"}
}, {
    "@type":"languagePackString",
    "key":"UseLessDataNever",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Никогда"}
}, {
    "@type":"languagePackString",
    "key":"CurvesAll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ВСЕ"}
}, {
    "@type":"languagePackString",
    "key":"PassportSelectNotExpire",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не истекает"}
}, {
    "@type":"languagePackString",
    "key":"NewChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Создать канал"}
}, {
    "@type":"languagePackString",
    "key":"CustomP2PInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Звонки через Peer-to-Peer будут использоваться или не использоваться для этих пользователей независимо от настроек выше."
    }
}, {
    "@type":"languagePackString",
    "key":"PeopleNearbyGpsInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете быстро связаться с теми, кто находится рядом с Вами и зашёл в этот раздел, а также найти локальные группы.\n\nДля этого необходимо разрешить приложению доступ к геопозиции."
    }
}, {
    "@type":"languagePackString",
    "key":"CameraPermissionText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите, чтобы разрешить доступ к камере"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenuCallSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки звонков"}
}, {
    "@type":"languagePackString",
    "key":"EnterChannelName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя канала"}
}, {
    "@type":"languagePackString",
    "key":"DataSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Данные и память"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenuClearMediaCache",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить кэш отправки файлов"}
}, {
    "@type":"languagePackString",
    "key":"PeopleNearbyAccessInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете быстро связаться с теми, кто находится рядом с Вами и зашёл в этот раздел, а также найти локальные группы.\n\nДля этого необходимо разрешить приложению доступ к геопозиции."
    }
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteThisChatWithUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить чат с **%1$s**?"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenuEnableCamera",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Включить встроенную камеру"}
}, {
    "@type":"languagePackString",
    "key":"SettingsNoResults",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ничего не найдено"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenuEnableLogs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Включить логирование"}
}, {
    "@type":"languagePackString",
    "key":"AutodownloadSizeLimit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"По размеру"}
}, {
    "@type":"languagePackString",
    "key":"DefaultRingtone",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"По умолчанию"}
}, {
    "@type":"languagePackString",
    "key":"ArchivedMasksEmpty",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет масок в архиве"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminCantEdit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы не можете изменять права этого администратора."}
}, {
    "@type":"languagePackString",
    "key":"WhoCanCallMe",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Кто может мне звонить?"}
}, {
    "@type":"languagePackString",
    "key":"DeleteAccountHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если Вы ни разу не заглянете в Telegram за это время, аккаунт будет удалён вместе со всеми сообщениями и контактами."
    }
}, {
    "@type":"languagePackString",
    "key":"formatterDay12H",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"h:mm a"}
}, {
    "@type":"languagePackString",
    "key":"InviteToGroupByLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пригласить в группу по ссылке"}
}, {
    "@type":"languagePackString",
    "key":"TintShadows",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ТЕНИ"}
}, {
    "@type":"languagePackString",
    "key":"DeleteAll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить все"}
}, {
    "@type":"languagePackString",
    "key":"ResetAccountStatus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы сможете сбросить свой аккаунт через:"}
}, {
    "@type":"languagePackString",
    "key":"DeleteAllMessagesAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Внимание! **Все сообщения** в чате будут удалены для Вас и **для Вашего собеседника**."
    }
}, {
    "@type":"languagePackString",
    "key":"AccSwipeForArchive",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Архив — проведите вниз."}
}, {
    "@type":"languagePackString",
    "key":"AccDescrChangeProfilePicture",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить фотографию в профиле"}
}, {
    "@type":"languagePackString",
    "key":"DeleteAndExit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить и покинуть группу"}
}, {
    "@type":"languagePackString",
    "key":"ShowAllThemes",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показать все темы"}
}, {
    "@type":"languagePackString",
    "key":"DeleteBanUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заблокировать пользователя"}
}, {
    "@type":"languagePackString",
    "key":"ChooseTakePhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сделать снимок"}
}, {
    "@type":"languagePackString",
    "key":"DeleteFewChatsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить %1$s"}
}, {
    "@type":"languagePackString",
    "key":"StorageUsage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использование памяти"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrVideoQuality",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Качество видео"}
}, {
    "@type":"languagePackString",
    "key":"AddAnotherAccountInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Подключите несколько аккаунтов Telegram и легко переключайтесь между ними."
    }
}, {
    "@type":"languagePackString",
    "key":"DeleteForAll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить у всех участников"}
}, {
    "@type":"languagePackString",
    "key":"ChangeRecoveryEmail",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сменить адрес электронной почты"}
}, {
    "@type":"languagePackString",
    "key":"DeleteForUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить для %1$s"}
}, {
    "@type":"languagePackString",
    "key":"DeleteFromFavorites",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить из избранных"}
}, {
    "@type":"languagePackString",
    "key":"ViewPackPreview",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Посмотреть набор"}
}, {
    "@type":"languagePackString",
    "key":"LocalDatabase",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Локальная база данных"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminRankInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Должность, которая будет показываться в подписи вместо «%1$s»."
    }
}, {
    "@type":"languagePackString",
    "key":"SetAdditionalPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Установить дополнительный пароль"}
}, {
    "@type":"languagePackString",
    "key":"DeleteGif",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить GIF?"}
}, {
    "@type":"languagePackString",
    "key":"GroupSettingsChangedAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы изменили настройки группы. Применить изменения?"}
}, {
    "@type":"languagePackString",
    "key":"BadgeNumberUnread",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Число сообщений"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_ES",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на испанском языке"}
}, {
    "@type":"languagePackString",
    "key":"HiddenName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалённый аккаунт"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedInvoiceChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) счёт"}
}, {
    "@type":"languagePackString",
    "key":"DeleteMega",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить группу"}
}, {
    "@type":"languagePackString",
    "key":"LocalPhotoCache",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фото"}
}, {
    "@type":"languagePackString",
    "key":"EncryptedDescription3",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удаление по таймеру"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessagePhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) фото"}
}, {
    "@type":"languagePackString",
    "key":"DeleteMessagesOptionAlso",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Также удалить для %1$s"}
}, {
    "@type":"languagePackString",
    "key":"VoipNotificationSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звонки"}
}, {
    "@type":"languagePackString",
    "key":"SearchImagesInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПОИСК В СЕТИ"}
}, {
    "@type":"languagePackString",
    "key":"DeleteMessagesTextGroupPart",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы удалить отправленные Вами %1$s из истории для других участников группы, выберите «Удалить у всех участников»."
    }
}, {
    "@type":"languagePackString",
    "key":"AttachDestructingVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Самоуничтожающееся видео"}
}, {
    "@type":"languagePackString",
    "key":"MuteNotifications",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключить уведомления"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddressHeader",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Адрес"}
}, {
    "@type":"languagePackString",
    "key":"DeleteMessagesTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить %1$s"}
}, {
    "@type":"languagePackString",
    "key":"NoBlockedChannel",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Заблокированные пользователи удалены из канала и могут вернуться обратно только по приглашению администратора. Пригласительные ссылки для них не работают."
    }
}, {
    "@type":"languagePackString",
    "key":"DeletePhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить фото"}
}, {
    "@type":"languagePackString",
    "key":"EncryptedDescription1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Оконечное шифрование"}
}, {
    "@type":"languagePackString",
    "key":"formatterStats12H",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"dd MMM yyyy, h:mm a"}
}, {
    "@type":"languagePackString",
    "key":"NewMessageTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новое сообщение"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyPolicy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Политика конфиденциальности"}
}, {
    "@type":"languagePackString",
    "key":"DeleteThisChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить чат"}
}, {
    "@type":"languagePackString",
    "key":"Question",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вопрос"}
}, {
    "@type":"languagePackString",
    "key":"DeleteThisGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить группу"}
}, {
    "@type":"languagePackString",
    "key":"AddedToFavorites",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикер добавлен в избранные"}
}, {
    "@type":"languagePackString",
    "key":"BannedPhoneNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Номер телефона заблокирован"}
}, {
    "@type":"languagePackString",
    "key":"UnpinMessageAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открепить сообщение"}
}, {
    "@type":"languagePackString",
    "key":"UploadItems",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузить %1$s"}
}, {
    "@type":"languagePackString",
    "key":"LiveLocationAlertGroup",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Выберите, как долго участники этого чата смогут видеть, где Вы находитесь."
    }
}, {
    "@type":"languagePackString",
    "key":"ChooseMapPreviewProvider",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите сервис для предпросмотра карты"}
}, {
    "@type":"languagePackString",
    "key":"DescriptionInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Можете указать дополнительное описание вашего канала."}
}, {
    "@type":"languagePackString",
    "key":"EventLogInfoTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Что такое недавние действия?"}
}, {
    "@type":"languagePackString",
    "key":"MessageLifetimeChangedOutgoing",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы включили удаление по таймеру через %1$s"}
}, {
    "@type":"languagePackString",
    "key":"ChannelPublicEmptyUsername",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Выберите публичную ссылку для канала, чтобы пользователи могли отправлять её друг другу, а канал можно было найти через поиск.\n\nЕсли Вы в этом не заинтересованы, предлагаем создать частный канал."
    }
}, {
    "@type":"languagePackString",
    "key":"DidNotGetTheCodeSms",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить код через SMS"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyProfilePhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фотография профиля"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsNoPinMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"без закрепления"}
}, {
    "@type":"languagePackString",
    "key":"AddToStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить в стикеры"}
}, {
    "@type":"languagePackString",
    "key":"DisappearingGif",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Секретный GIF"}
}, {
    "@type":"languagePackString",
    "key":"ChannelCreator",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Владелец"}
}, {
    "@type":"languagePackString",
    "key":"PhoneNumberSearch",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Номер телефона"}
}, {
    "@type":"languagePackString",
    "key":"DisappearingPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Секретное фото"}
}, {
    "@type":"languagePackString",
    "key":"GlobalSearch",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Глобальный поиск"}
}, {
    "@type":"languagePackString",
    "key":"DiscardChanges",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отменить изменения?"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminTransferAlertText1",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Не меньше **7 дней назад** включили двухэтапную аутентификацию."
    }
}, {
    "@type":"languagePackString",
    "key":"WiFiUsage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использование Wi-Fi"}
}, {
    "@type":"languagePackString",
    "key":"DiscardVideoMessageDescription",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите прекратить запись и удалить видеосообщение?"
    }
}, {
    "@type":"languagePackString",
    "key":"Discussion",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обсуждение"}
}, {
    "@type":"languagePackString",
    "key":"LocalMusicCache",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Музыка"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionChannelHelp2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Всё, что Вы публикуете в этом канале, будет автоматически пересылаться в группу."
    }
}, {
    "@type":"languagePackString",
    "key":"AttachBotsHelp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Список ботов ниже"}
}, {
    "@type":"languagePackString",
    "key":"NoRecent",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет недавних"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionCreateGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Создать группу"}
}, {
    "@type":"languagePackString",
    "key":"ChannelUsernameHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если у канала будет постоянная публичная ссылка, другие пользователи смогут найти его и подписаться.\n\nМожно использовать латиницу (a-z), цифры (0-9) и подчёркивание. Минимальная длина — 5 символов."
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s: %2$s"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionGroupHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Группа привязана к %1$s как площадка для обсуждения публикаций."
    }
}, {
    "@type":"languagePackString",
    "key":"Call",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Позвонить"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionLinkGroupAlertHistory",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новые участники группы будут видеть всю историю чата."}
}, {
    "@type":"languagePackString",
    "key":"ErrorSendRestrictedStickers",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Администраторы группы запретили Вам отправлять стикеры."
    }
}, {
    "@type":"languagePackString",
    "key":"DiscussionUnlink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отвязать"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsSend",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправка сообщений"}
}, {
    "@type":"languagePackString",
    "key":"MilesAway",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s мл отсюда"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) видео в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"AccActionPause",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пауза"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionUnlinkChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отвязать канал"}
}, {
    "@type":"languagePackString",
    "key":"DistanceUnits",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мера расстояния"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddPassport",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить паспорт"}
}, {
    "@type":"languagePackString",
    "key":"ArchivedMasks",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Архив масок"}
}, {
    "@type":"languagePackString",
    "key":"DocumentsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Общие файлы"}
}, {
    "@type":"languagePackString",
    "key":"Done",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Готово"}
}, {
    "@type":"languagePackString",
    "key":"ChannelUserCantBot",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, в канале слишком много ботов."}
}, {
    "@type":"languagePackString",
    "key":"EditAdmin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Права админов"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminAddUsersViaLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пригласительные ссылки"}
}, {
    "@type":"languagePackString",
    "key":"CallEmojiKeyTooltip",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если %1$s видит те же эмодзи, что и вы, звонок на 100%% защищён."
    }
}, {
    "@type":"languagePackString",
    "key":"EditAdminBanUsers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Блокировка пользователей"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminChangeChannelInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменять информацию о канале"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminGroupDeleteMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удаление сообщений"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundColorSinglePreviewLine1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чтобы применить фон, нажмите «Установить»"}
}, {
    "@type":"languagePackString",
    "key":"ChannelAlertCreate2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Создать канал"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminPinMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закрепление сообщений"}
}, {
    "@type":"languagePackString",
    "key":"ThemeGraphite",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Графитовая"}
}, {
    "@type":"languagePackString",
    "key":"AddToExceptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить в исключения"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminPostMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Публиковать сообщения"}
}, {
    "@type":"languagePackString",
    "key":"SetRecoveryEmail",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Указать адрес электронной почты"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyForwardsContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылка, если это разрешают настройки ниже"}
}, {
    "@type":"languagePackString",
    "key":"EncryptionKey",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ключ шифрования"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedRemovedTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Должность"}
}, {
    "@type":"languagePackString",
    "key":"SuggestStickersNone",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет"}
}, {
    "@type":"languagePackString",
    "key":"CodeExpired",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Время действия кода истекло. Попробуйте ещё раз."}
}, {
    "@type":"languagePackString",
    "key":"EditAdminRemoveAdmin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить из администраторов"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedPhotoChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) фото"}
}, {
    "@type":"languagePackString",
    "key":"QuestionHint",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Задайте вопрос"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminRights",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить права админа"}
}, {
    "@type":"languagePackString",
    "key":"NoPopup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не показывать"}
}, {
    "@type":"languagePackString",
    "key":"AddOneMemberAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить участника"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrShowKeyboard",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показать клавиатуру"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminTransferAlertText3",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Попробуйте позже."}
}, {
    "@type":"languagePackString",
    "key":"LocalGifCache",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"GIF"}
}, {
    "@type":"languagePackString",
    "key":"PaymentPasswordEmailTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Почта для восстановления"}
}, {
    "@type":"languagePackString",
    "key":"ProxyDetails",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прокси-сервер"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminTransferChannelToast",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"**%1$s** теперь владелец канала."}
}, {
    "@type":"languagePackString",
    "key":"FromFormatted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"От %1$s"}
}, {
    "@type":"languagePackString",
    "key":"VoipDeclineCall",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отклонить"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminTransferReadyAlertText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В этом случае **права** на **%1$s** будут полностью переданы **%2$s**."
    }
}, {
    "@type":"languagePackString",
    "key":"EnterNewFirstPasscode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите код-пароль"}
}, {
    "@type":"languagePackString",
    "key":"EditCantEditPermissionsPublic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В публичных группах нельзя разрешить это действие."}
}, {
    "@type":"languagePackString",
    "key":"ChannelAdministrators",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Администраторы"}
}, {
    "@type":"languagePackString",
    "key":"EditChannelAdminTransferAlertText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы сможете передать права на канал **%1$s**, если:"}
}, {
    "@type":"languagePackString",
    "key":"EditName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить имя"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddTranslationBankInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Загрузите сканы заверенного перевода для своей справки из банка."
    }
}, {
    "@type":"languagePackString",
    "key":"EmailCopied",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Email скопирован в буфер обмена"}
}, {
    "@type":"languagePackString",
    "key":"UnpinMessageAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хотите открепить это сообщение?"}
}, {
    "@type":"languagePackString",
    "key":"BotStart",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СТАРТ"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentAddress",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Адрес"}
}, {
    "@type":"languagePackString",
    "key":"EmailPasswordConfirmText3",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы подтвердить почту для восстановления пароля, проверьте %1$s (не забудьте о папке со спамом) и введите код, который получили в письме."
    }
}, {
    "@type":"languagePackString",
    "key":"Emoji1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Смайлы и люди"}
}, {
    "@type":"languagePackString",
    "key":"DescriptionPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Описание"}
}, {
    "@type":"languagePackString",
    "key":"Emoji3",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Еда и напитки"}
}, {
    "@type":"languagePackString",
    "key":"PassportNativeInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваше имя на языке страны проживания (%1$s)."}
}, {
    "@type":"languagePackString",
    "key":"AddMasksInstalled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавлены новые маски"}
}, {
    "@type":"languagePackString",
    "key":"telegram_passport",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Telegram Passport"}
}, {
    "@type":"languagePackString",
    "key":"GroupName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя группы"}
}, {
    "@type":"languagePackString",
    "key":"LocalVideoCache",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Видео"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGif",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) GIF"}
}, {
    "@type":"languagePackString",
    "key":"RestoreEmailSentInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пожалуйста, проверьте почту и введите 6-значный код, который мы на неё отправили."
    }
}, {
    "@type":"languagePackString",
    "key":"Emoji8",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Флаги"}
}, {
    "@type":"languagePackString",
    "key":"DeleteSingleMessagesTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить сообщение"}
}, {
    "@type":"languagePackString",
    "key":"CancelPasswordQuestion",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите прервать настройку двухэтапной аутентификации?"
    }
}, {
    "@type":"languagePackString",
    "key":"EmojiSuggestions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подсказки эмодзи"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_RU",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на русском языке"}
}, {
    "@type":"languagePackString",
    "key":"PassportPhoneUploadInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Обратите внимание: на указанный Вами номер телефона придет код подтверждения."
    }
}, {
    "@type":"languagePackString",
    "key":"UserRemovedBy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Исключил(а) %1$s"}
}, {
    "@type":"languagePackString",
    "key":"EmojiUseDefault",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использовать системные эмодзи"}
}, {
    "@type":"languagePackString",
    "key":"MegaLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Группа с геопозицией"}
}, {
    "@type":"languagePackString",
    "key":"Sharpen",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Резкость"}
}, {
    "@type":"languagePackString",
    "key":"EnableAllStreamingInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Потоковая загрузка недоступна для некоторых видеозаписей – например, отправленных с устройств Android с помощью Telegram версии 4.7 и ниже."
    }
}, {
    "@type":"languagePackString",
    "key":"EncryptedDescriptionTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Секретные чаты — это:"}
}, {
    "@type":"languagePackString",
    "key":"NoSharedAudio",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Делитесь музыкой в этом чате, и она будет доступна Вам с любого устройства."
    }
}, {
    "@type":"languagePackString",
    "key":"EncryptedPlaceholderTitleIncoming",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%s пригласил(а) вас в секретный чат."}
}, {
    "@type":"languagePackString",
    "key":"AccActionTakePicture",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сделать снимок"}
}, {
    "@type":"languagePackString",
    "key":"EncryptionKeyDescription",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Это изображение и текст были созданы на основе ключа шифрования для этого секретного чата с **%1$s**.\n\nЕсли они выглядят так же, как на устройстве **%2$s**, оконечное шифрование надежно.\n\nУзнать больше на telegram.org"
    }
}, {
    "@type":"languagePackString",
    "key":"PrivacyPhoneTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Кто видит мой номер телефона?"}
}, {
    "@type":"languagePackString",
    "key":"ChatYourSelfTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваше личное хранилище"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrGoToMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перейти к сообщению"}
}, {
    "@type":"languagePackString",
    "key":"WhoCanAddMeInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете выбрать, кому разрешаете приглашать Вас в группы и каналы."
    }
}, {
    "@type":"languagePackString",
    "key":"LargeEmoji",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Крупные эмодзи"}
}, {
    "@type":"languagePackString",
    "key":"EventLogChangedOwnership",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"права переданы %1$s"}
}, {
    "@type":"languagePackString",
    "key":"ChangePhoneHelp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"SMS с кодом подтверждения придет на Ваш новый номер."}
}, {
    "@type":"languagePackString",
    "key":"Draft",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Черновик"}
}, {
    "@type":"languagePackString",
    "key":"MessageScheduledVote",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Голосование будет доступно после публикации сообщения."
    }
}, {
    "@type":"languagePackString",
    "key":"Folder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Папка"}
}, {
    "@type":"languagePackString",
    "key":"EncryptionRejected",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Секретный чат отменён"}
}, {
    "@type":"languagePackString",
    "key":"AutoLockDisabled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключена"}
}, {
    "@type":"languagePackString",
    "key":"English",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Английский"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRemovedChannelPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 удалил(а) фото канала"}
}, {
    "@type":"languagePackString",
    "key":"VoipRingtoneInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете настроить рингтон, который используется, когда этот контакт звонит Вам в Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"EnterGroupNamePlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите имя группы"}
}, {
    "@type":"languagePackString",
    "key":"PassportDeleteScan",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить скан?"}
}, {
    "@type":"languagePackString",
    "key":"LinkInvalidShort",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Короткие имена должны содержать не меньше 5 символов."}
}, {
    "@type":"languagePackString",
    "key":"VoipWaiting",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ожидание"}
}, {
    "@type":"languagePackString",
    "key":"ErrorSendRestrictedMediaAll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В этой группе запрещено отправлять фото и видео."}
}, {
    "@type":"languagePackString",
    "key":"ChangePhoneNumberInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Перенесите на новый номер телефона свой аккаунт со всеми чатами и файлами."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogChangedLinkedChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 привязал(а) группу к un2"}
}, {
    "@type":"languagePackString",
    "key":"LinkInvalidStartNumberMega",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имена групп не могут начинаться с цифры."}
}, {
    "@type":"languagePackString",
    "key":"EventLogChannelRestricted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"заблокировал(а) %1$s"}
}, {
    "@type":"languagePackString",
    "key":"AttachMediaRestricted",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Администраторы группы запретили Вам отправлять фото и видео до %1$s"
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogChannelUnrestricted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"разблокировал(а) %1$s"}
}, {
    "@type":"languagePackString",
    "key":"EventLogDefaultPermissions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"изменены разрешения по умолчанию"}
}, {
    "@type":"languagePackString",
    "key":"InviteFriends",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пригласить друзей"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsDefaultOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"По умолчанию (выкл)"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedGroupDescription",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 изменил(а) описание группы:"}
}, {
    "@type":"languagePackString",
    "key":"InviteFriendsHelp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите людей, которых хотите пригласить в Telegram"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedGroupPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 добавил(а) новое фото группы"}
}, {
    "@type":"languagePackString",
    "key":"Calls",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звонки"}
}, {
    "@type":"languagePackString",
    "key":"Crop",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"КАДРИРОВАТЬ"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 отредактировал(а) медиа:"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedMediaCaption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 отредактировал(а) медиа и подпись:"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedYouChannelPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы установили новое фото канала"}
}, {
    "@type":"languagePackString",
    "key":"DistanceUnitsAutomatic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Автоматически"}
}, {
    "@type":"languagePackString",
    "key":"BackgroundColorSinglePreviewLine2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Наслаждайтесь видом"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEmptySearch",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**Действий не найдено**\n\nПо Вашему запросу не удалось найти недавних действий."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogFilterAll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все действия"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_HE",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на иврите"}
}, {
    "@type":"languagePackString",
    "key":"BotRestart",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перезапустить бота"}
}, {
    "@type":"languagePackString",
    "key":"PermissionDrawAboveOtherApps",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Telegram требуется доступ к работе поверх других приложений, чтобы показывать видео в режиме картинка в картинке."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogFilterChannelInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Описание канала"}
}, {
    "@type":"languagePackString",
    "key":"ProfilePopupNotificationInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Новые сообщения от этого контакта будут всплывать поверх экрана, когда Вы не в приложении."
    }
}, {
    "@type":"languagePackString",
    "key":"LastSeenContactsPlus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мои контакты (+%1$d)"}
}, {
    "@type":"languagePackString",
    "key":"ActionYouCreateGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы создали группу"}
}, {
    "@type":"languagePackString",
    "key":"SentCallCode",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Наш робот звонит на номер **%1$s**.\n\nОтвечать на звонок не требуется, Telegram автоматически получит код для входа."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogFilterChannelSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки канала"}
}, {
    "@type":"languagePackString",
    "key":"EventLogFilterNewAdmins",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Права админов"}
}, {
    "@type":"languagePackString",
    "key":"EventLogFilterNewRestrictions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новые исключения"}
}, {
    "@type":"languagePackString",
    "key":"EventLogInfoDetailChannel",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Это список всех служебных действий, совершённых администраторами канала за последние 48 часов."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogLeftGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 покинул(а) группу"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteFewMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить эти сообщения?"}
}, {
    "@type":"languagePackString",
    "key":"SuggestContactsAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В этом случае информация о том, каким пользователям Вы часто пишете и какими ботами обычно пользуетесь, будет удалена."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogPinnedMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) сообщение:"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPreviousChannelLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прежняя ссылка канала"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteThisChatSavedMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить чат **Избранное**?"}
}, {
    "@type":"languagePackString",
    "key":"EventLogOriginalMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Исходное сообщение"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPreviousGroupDescription",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прежнее описание"}
}, {
    "@type":"languagePackString",
    "key":"PasswordHintText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите подсказку для пароля"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedAddAdmins",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавление администраторов"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedBanUsers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Блокировки пользователей"}
}, {
    "@type":"languagePackString",
    "key":"InviteToTelegram",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПРИГЛАСИТЬ В TELEGRAM"}
}, {
    "@type":"languagePackString",
    "key":"RevokeAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите сбросить ссылку? После этого никто не сможет использовать ее, чтобы присоединиться."
    }
}, {
    "@type":"languagePackString",
    "key":"SyncContactsAdded",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Контакты с этого устройства были добавлены к Вашему аккаунту."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedInviteLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Приглашение по ссылке"}
}, {
    "@type":"languagePackString",
    "key":"ContactJob",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Работа"}
}, {
    "@type":"languagePackString",
    "key":"DeleteAndExitButton",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить и покинуть группу"}
}, {
    "@type":"languagePackString",
    "key":"LastSeenEverybodyMinus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все (-%1$d)"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedPinMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закрепление сообщений"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedPostMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Публикация сообщений"}
}, {
    "@type":"languagePackString",
    "key":"ChangePasscode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сменить код-пароль"}
}, {
    "@type":"languagePackString",
    "key":"SecretWebPage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предпросмотр ссылок"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRemovedChannelLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 удалил(а) ссылку на канал"}
}, {
    "@type":"languagePackString",
    "key":"ShareMyContactInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить свой телефон"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRemovedGroupLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 удалил(а) ссылку на группу"}
}, {
    "@type":"languagePackString",
    "key":"ActionUserScored",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 набрал(а) %1$s"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRemovedLinkedChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 отвязал(а) группу от un2"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRemovedLinkedGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 убрал(а) группу для обсуждения un2"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyPaymentsClearAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Удалить данные о доставке и запросить удаление сохраненных кредитных карт у платёжных провайдеров? Обратите внимание, что сам Telegram не хранит информацию о Ваших кредитных картах."
    }
}, {
    "@type":"languagePackString",
    "key":"Unvote",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отменить голос"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionGroupHelp2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Все публикации в этом канале автоматически пересылаются в группу."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogRemovedStickersSet",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 удалил(а) набор стикеров группы"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) видео в группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"ActionAddUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 добавил(а) un2"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedYouChannelTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы переименовали канал в «%1$s»"}
}, {
    "@type":"languagePackString",
    "key":"LanguageNameInEnglish",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Russian"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupGame",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s пригласил(а) «%2$s» сыграть в %3$s"}
}, {
    "@type":"languagePackString",
    "key":"Settings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRestrictedInviteUsers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавлять пользователей"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRestrictedUntil",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"изменены права для %1$s\n\nСрок: %2$s"}
}, {
    "@type":"languagePackString",
    "key":"EventLogSelectedEvents",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбранные действия"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s @ %2$s: %3$s"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyPhoneInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пользователи, которые уже знают Ваш номер и сохранили его в телефонную книгу, будут видеть его и в Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"UpdateContactsMessage",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Обнаружено много несинхронизированных контактов, хотите синхронизировать их? Нажмите «ОК», если сейчас вы используете своё устройство, SIM-карту и аккаунт Google."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogToggledInvitesHistoryOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 скрыл(а) историю группы от новых участников"}
}, {
    "@type":"languagePackString",
    "key":"EventLogToggledInvitesHistoryOn",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"un1 сделал(а) историю группы доступной новым участникам"
    }
}, {
    "@type":"languagePackString",
    "key":"LocalDocumentCache",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Файлы"}
}, {
    "@type":"languagePackString",
    "key":"EventLogToggledInvitesOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 разрешил(а) приглашения в группу"}
}, {
    "@type":"languagePackString",
    "key":"GroupUserCantAdmin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, у группы слишком много администраторов."}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureClearHistoryWithChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить историю переписки в **%1$s**?"}
}, {
    "@type":"languagePackString",
    "key":"EventLogToggledSignaturesOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 включил(а) подписи к сообщениям"}
}, {
    "@type":"languagePackString",
    "key":"EventLogToggledSlowmodeOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 отключил(а) медленный режим"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteSingleMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить это сообщение?"}
}, {
    "@type":"languagePackString",
    "key":"EventLogChangedGroupLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 изменил(а) ссылку на группу:"}
}, {
    "@type":"languagePackString",
    "key":"EventLogUnpinnedMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 открепил(а) сообщение"}
}, {
    "@type":"languagePackString",
    "key":"LastSeenNobody",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Никто"}
}, {
    "@type":"languagePackString",
    "key":"EventLogYouChannelJoined",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы присоединились к каналу"}
}, {
    "@type":"languagePackString",
    "key":"EventLogYouLeft",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы покинули группу"}
}, {
    "@type":"languagePackString",
    "key":"EventLogStopPoll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 остановил(а) опрос:"}
}, {
    "@type":"languagePackString",
    "key":"EventLogYouPromoted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы изменили привилегии пользователя un2 (%1$s)"}
}, {
    "@type":"languagePackString",
    "key":"Events",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"События"}
}, {
    "@type":"languagePackString",
    "key":"EmailPasswordConfirmText2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы закончить настройку двухэтапной аутентификации, проверьте почту (не забудьте о папке со спамом) и введите код, который получили в письме."
    }
}, {
    "@type":"languagePackString",
    "key":"Exposure",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Экспозиция"}
}, {
    "@type":"languagePackString",
    "key":"FavoriteStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Избранные"}
}, {
    "@type":"languagePackString",
    "key":"FeaturedStickersInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Эти стикеры сейчас популярны в Telegram. Вы можете добавлять свои стикеры с помощью бота @stickers."
    }
}, {
    "@type":"languagePackString",
    "key":"ImagesTab",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ИЗОБРАЖЕНИЯ"}
}, {
    "@type":"languagePackString",
    "key":"February",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Февраль"}
}, {
    "@type":"languagePackString",
    "key":"FilesDataUsage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Файлы"}
}, {
    "@type":"languagePackString",
    "key":"ChannelUserCantAdmin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, у канала слишком много администраторов."}
}, {
    "@type":"languagePackString",
    "key":"FinalResults",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Результаты"}
}, {
    "@type":"languagePackString",
    "key":"FingerprintInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подтвердите отпечаток пальца"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightThemeOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не используется"}
}, {
    "@type":"languagePackString",
    "key":"FloodWaitTime",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Слишком много попыток, попробуйте снова через %1$s"}
}, {
    "@type":"languagePackString",
    "key":"Map",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Карта"}
}, {
    "@type":"languagePackString",
    "key":"P2PContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мои контакты"}
}, {
    "@type":"languagePackString",
    "key":"FontSize",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Размер шрифта"}
}, {
    "@type":"languagePackString",
    "key":"NoSharedVoiceSecret",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Здесь будут отображаться голосовые сообщения из этого чата."
    }
}, {
    "@type":"languagePackString",
    "key":"AreYouSureSecretChatTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Секретный чат"}
}, {
    "@type":"languagePackString",
    "key":"SetPasscodeInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Включите код-пароль для разблокировки приложения на Вашем устройстве."
    }
}, {
    "@type":"languagePackString",
    "key":"FontSizePreviewLine1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Знаешь, который час?"}
}, {
    "@type":"languagePackString",
    "key":"FontSizePreviewName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Боб Харрис"}
}, {
    "@type":"languagePackString",
    "key":"MessagePreview",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показывать текст"}
}, {
    "@type":"languagePackString",
    "key":"ForwardedMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пересланное сообщение"}
}, {
    "@type":"languagePackString",
    "key":"NotificationGroupFew",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) %3$s в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGameScored",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s набрал(а) %3$s очков в игре «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"YourEmailSkip",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пропустить"}
}, {
    "@type":"languagePackString",
    "key":"FromYou",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialogMessage8",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Кто-то упомянул арахисовую пасту?"}
}, {
    "@type":"languagePackString",
    "key":"GifsTab",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"GIF"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionLinkGroupPublicAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Назначить %1$s группой для обсуждения %2$s?"}
}, {
    "@type":"languagePackString",
    "key":"MapPreviewProvider",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предпросмотр карты"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupNoText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) сообщение группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"CancelRegistration",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отмена регистрации"}
}, {
    "@type":"languagePackString",
    "key":"CreateNewTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Создать новую тему"}
}, {
    "@type":"languagePackString",
    "key":"GlobalAttachInlineRestricted",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В этой группе запрещено публиковать материалы с помощью ботов."
    }
}, {
    "@type":"languagePackString",
    "key":"EditAdminPromotedBy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Назначил(а): %1$s"}
}, {
    "@type":"languagePackString",
    "key":"EventLogAllAdmins",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все администраторы"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminEditMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Редактировать чужие сообщения"}
}, {
    "@type":"languagePackString",
    "key":"AccActionEnterSelectionMode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перейти в режим выбора"}
}, {
    "@type":"languagePackString",
    "key":"DeleteMessagesText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы удалить отправленные Вами %1$s из истории для **%2$s**, выберите «Отозвать мои сообщения»."
    }
}, {
    "@type":"languagePackString",
    "key":"Support",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поддержка"}
}, {
    "@type":"languagePackString",
    "key":"GlobalAttachMediaRestricted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В этой группе запрещено публиковать мультимедиа."}
}, {
    "@type":"languagePackString",
    "key":"InvalidFirstName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Некорректное имя"}
}, {
    "@type":"languagePackString",
    "key":"YourEmailSuccessText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Адрес для восстановления пароля двухэтапной аутентификации активирован."
    }
}, {
    "@type":"languagePackString",
    "key":"IsSendingPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправляет фото..."}
}, {
    "@type":"languagePackString",
    "key":"GlobalAttachStickersRestricted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В этой группе запрещено отправлять стикеры."}
}, {
    "@type":"languagePackString",
    "key":"CancelAccountResetInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Кто-то с доступом к Вашему номеру телефона **%1$s** запросил удаление Вашего аккаунта Telegram и сброс Вашего пароля двухэтапной аутентификации.\n\nЕсли это были не Вы, пожалуйста, введите код из SMS, только что отправленного на Ваш номер."
    }
}, {
    "@type":"languagePackString",
    "key":"GotAQuestion",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вопрос по Telegram?"}
}, {
    "@type":"languagePackString",
    "key":"MessageScheduledLimitReached",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы не можете добавить больше 100 отложенных сообщений."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelAddedByNotification",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s пригласил(а) вас в канал %2$s"}
}, {
    "@type":"languagePackString",
    "key":"PassportPhoneInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Укажите свой номер телефона"}
}, {
    "@type":"languagePackString",
    "key":"GroupContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Контакты в этой группе"}
}, {
    "@type":"languagePackString",
    "key":"ThemeUrl",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылка на тему"}
}, {
    "@type":"languagePackString",
    "key":"UseProxyForCallsInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использование прокси может ухудшить качество звонков."}
}, {
    "@type":"languagePackString",
    "key":"GroupDescription1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"До 200 000 участников"}
}, {
    "@type":"languagePackString",
    "key":"PassportEmailUploadInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Обратите внимание: на указанную Вами электронную почту придет код подтверждения."
    }
}, {
    "@type":"languagePackString",
    "key":"GroupDescription2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Общая история переписки"}
}, {
    "@type":"languagePackString",
    "key":"DistanceUnitsMiles",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мили"}
}, {
    "@type":"languagePackString",
    "key":"IsSendingVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправляет видео..."}
}, {
    "@type":"languagePackString",
    "key":"From",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"От:"}
}, {
    "@type":"languagePackString",
    "key":"PassportSelectBithdayDate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Укажите дату рождения"}
}, {
    "@type":"languagePackString",
    "key":"SmartNotificationsAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Частота звуковых уведомлений"}
}, {
    "@type":"languagePackString",
    "key":"RecentlyViewedHide",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СКРЫТЬ"}
}, {
    "@type":"languagePackString",
    "key":"GroupDescription4",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Разный уровень прав"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureClearHistoryFewChats",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите удалить историю переписки в выбранных чатах?"
    }
}, {
    "@type":"languagePackString",
    "key":"GroupEmptyTitle2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Возможности групп:"}
}, {
    "@type":"languagePackString",
    "key":"NotificationGroupForwardedFew",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s переслал(а) %3$s в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"BlockUserAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Запретить **%1$s** писать Вам сообщения и звонить через Telegram?"
    }
}, {
    "@type":"languagePackString",
    "key":"GroupNotifications",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Группы"}
}, {
    "@type":"languagePackString",
    "key":"GroupPhotosHelp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Объединить фотографии в альбом"}
}, {
    "@type":"languagePackString",
    "key":"GroupStickersInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете выбрать набор стикеров, который будет доступен всем участникам группы при общении в ней."
    }
}, {
    "@type":"languagePackString",
    "key":"GroupUserAddLimit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, группа переполнена."}
}, {
    "@type":"languagePackString",
    "key":"GroupsInCommon",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Общие группы"}
}, {
    "@type":"languagePackString",
    "key":"PassportPhoneUseSameEmailInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Используйте тот же адрес электронной почты, что и в Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"GroupsInCommonTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Общие группы"}
}, {
    "@type":"languagePackString",
    "key":"LeaveChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Покинуть канал"}
}, {
    "@type":"languagePackString",
    "key":"HideOnTop",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Скрыть"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenuImportContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Импорт контактов"}
}, {
    "@type":"languagePackString",
    "key":"Highlights",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Свет"}
}, {
    "@type":"languagePackString",
    "key":"SearchMembers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск участников"}
}, {
    "@type":"languagePackString",
    "key":"PassportPersonalDetails",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Личная информация"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRestrictedReadMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Читать сообщения"}
}, {
    "@type":"languagePackString",
    "key":"TelegramVersion",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Telegram для Android %1$s"}
}, {
    "@type":"languagePackString",
    "key":"HoldToAudio",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Удерживайте для записи аудио. Нажмите для переключения на видео."
    }
}, {
    "@type":"languagePackString",
    "key":"P2PEnabledWith",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использовать peer-to-peer для звонков"}
}, {
    "@type":"languagePackString",
    "key":"ImportContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Импорт контактов"}
}, {
    "@type":"languagePackString",
    "key":"InAppPreview",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показывать текст"}
}, {
    "@type":"languagePackString",
    "key":"InChatSound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звук в чате"}
}, {
    "@type":"languagePackString",
    "key":"ThemeDarkBlue",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Тёмно-синяя"}
}, {
    "@type":"languagePackString",
    "key":"IncorrectLocalization",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Некорректный файл локализации"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentBankStatement",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Справка из банка"}
}, {
    "@type":"languagePackString",
    "key":"PendingEmailText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Ваш адрес электронной почты для восстановления %1$s ещё не активирован и ожидает подтверждения."
    }
}, {
    "@type":"languagePackString",
    "key":"Info",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Информация"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenuReadAllDialogs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прочесть все чаты"}
}, {
    "@type":"languagePackString",
    "key":"InvalidCode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Неверный код"}
}, {
    "@type":"languagePackString",
    "key":"ChannelBlockedUsers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чёрный список"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialog8",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Adler Toberg"}
}, {
    "@type":"languagePackString",
    "key":"InviteText2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Привет, я использую Telegram для переписки. Присоединяйся! Скачать его можно здесь: %1$s"
    }
}, {
    "@type":"languagePackString",
    "key":"IsRecordingAudio",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s записывает голосовое сообщение..."}
}, {
    "@type":"languagePackString",
    "key":"IsSendingAudio",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправляет аудио..."}
}, {
    "@type":"languagePackString",
    "key":"ArchiveHintHeader2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чаты без уведомлений"}
}, {
    "@type":"languagePackString",
    "key":"IsSendingFile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправляет файл..."}
}, {
    "@type":"languagePackString",
    "key":"AccDescrVideoMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Записать видеосообщение"}
}, {
    "@type":"languagePackString",
    "key":"OpenUrlTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открыть ссылку"}
}, {
    "@type":"languagePackString",
    "key":"IsTyping",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"печатает…"}
}, {
    "@type":"languagePackString",
    "key":"JoinGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ВСТУПИТЬ В ГРУППУ"}
}, {
    "@type":"languagePackString",
    "key":"MegaPrivateLinkHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"По этой ссылке можно вступить в Вашу группу. Вы можете сбросить ссылку в любой момент."
    }
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadVideosOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Видео"}
}, {
    "@type":"languagePackString",
    "key":"KickFromSupergroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ограничить пользователя"}
}, {
    "@type":"languagePackString",
    "key":"LanguageAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Перевод (**%1$s**), который Вы собираетесь использовать, закончен на  %2$d%%.\n\nЭто изменит язык всего интерфейса. Предложить свою версию перевода можно через [платформу переводов].\n\nВы можете в любой момент изменить язык в настройках приложения."
    }
}, {
    "@type":"languagePackString",
    "key":"UseProxyTelegramError",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Подключенный вами прокси-сервер использует некорректные настройки и будет выключен. Пожалуйста, выберите другой."
    }
}, {
    "@type":"languagePackString",
    "key":"LanguageName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Русский"}
}, {
    "@type":"languagePackString",
    "key":"SelectChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите чат"}
}, {
    "@type":"languagePackString",
    "key":"ActionInviteUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 вступил(а) в группу по ссылке-приглашению"}
}, {
    "@type":"languagePackString",
    "key":"LanguageUnknownCustomAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, в этом неофициальном переводе (**%1$s**) нет текстов приложения Telegram для Android."
    }
}, {
    "@type":"languagePackString",
    "key":"PhoneMain",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Основной"}
}, {
    "@type":"languagePackString",
    "key":"LastName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фамилия (необязательно)"}
}, {
    "@type":"languagePackString",
    "key":"MegaDeleteAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Внимание! Удаление этой группы приведёт к исключению всех участников и потере всех сообщений. Вы точно хотите её удалить?"
    }
}, {
    "@type":"languagePackString",
    "key":"ActionAddUserSelf",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 снова в группе"}
}, {
    "@type":"languagePackString",
    "key":"LastSeenContactsMinus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мои контакты (-%1$d)"}
}, {
    "@type":"languagePackString",
    "key":"InAppVibrate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вибросигнал"}
}, {
    "@type":"languagePackString",
    "key":"ColorOrange",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Оранжевый"}
}, {
    "@type":"languagePackString",
    "key":"LastSeenTitle",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Кто видит время моего последнего входа и статус в сети?"
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogRestrictedSendPolls",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправлять опросы"}
}, {
    "@type":"languagePackString",
    "key":"SmsText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мы отправим Вам SMS через %1$d:%2$02d"}
}, {
    "@type":"languagePackString",
    "key":"Lately",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"был(а) недавно"}
}, {
    "@type":"languagePackString",
    "key":"CustomHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Важно: Вы не будете видеть появление в сети и время активности пользователей, от которых скрыли свои. Вместо времени будет видно примерное значение (недавно, на этой неделе, в этом месяце)."
    }
}, {
    "@type":"languagePackString",
    "key":"EncryptedDescription2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Никаких следов на серверах"}
}, {
    "@type":"languagePackString",
    "key":"Later",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПОЗЖЕ"}
}, {
    "@type":"languagePackString",
    "key":"ChannelAddToChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить в канал"}
}, {
    "@type":"languagePackString",
    "key":"LeaveMegaMenu",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Покинуть группу"}
}, {
    "@type":"languagePackString",
    "key":"LinkInvalid",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Некорректное имя."}
}, {
    "@type":"languagePackString",
    "key":"SystemRoot",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Корневой каталог"}
}, {
    "@type":"languagePackString",
    "key":"LinkedChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Привязанный канал"}
}, {
    "@type":"languagePackString",
    "key":"PaymentTransactionMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите перевести %1$s боту %2$s за %3$s?"}
}, {
    "@type":"languagePackString",
    "key":"CountSent",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправлено"}
}, {
    "@type":"languagePackString",
    "key":"EmojiSuggestionsUrl",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"https://translations.telegram.org/%1$s/emoji"}
}, {
    "@type":"languagePackString",
    "key":"PassportDeleteInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить"}
}, {
    "@type":"languagePackString",
    "key":"LiveLocationAlertPrivate",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Укажите, в течение какого времени %1$s будет видеть Ваше актуальное местоположение."
    }
}, {
    "@type":"languagePackString",
    "key":"LogOutInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"При выходе из аккаунта секретные чаты на этом устройстве пропадут."
    }
}, {
    "@type":"languagePackString",
    "key":"MarkAsUnread",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пометить как непрочитанное"}
}, {
    "@type":"languagePackString",
    "key":"AddRecipient",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить получателя"}
}, {
    "@type":"languagePackString",
    "key":"LiveLocations",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Трансляции геопозиции"}
}, {
    "@type":"languagePackString",
    "key":"Of",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$d из %2$d"}
}, {
    "@type":"languagePackString",
    "key":"LoadingFullImage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"загрузка в полном размере..."}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageLiveLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) трансляцию геопозиции"}
}, {
    "@type":"languagePackString",
    "key":"LocalCache",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Другие файлы"}
}, {
    "@type":"languagePackString",
    "key":"ChannelAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Что такое канал?"}
}, {
    "@type":"languagePackString",
    "key":"MembersCount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбрано %1$d из %2$d"}
}, {
    "@type":"languagePackString",
    "key":"AddContactByPhone",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить %1$s"}
}, {
    "@type":"languagePackString",
    "key":"LocationUpdatedFormatted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"обновлено %1$s"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsChannels",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Каналы"}
}, {
    "@type":"languagePackString",
    "key":"LogOut",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выход"}
}, {
    "@type":"languagePackString",
    "key":"TypePublic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Публичный"}
}, {
    "@type":"languagePackString",
    "key":"AutodownloadSizeLimitUpTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"до %1$s"}
}, {
    "@type":"languagePackString",
    "key":"ProfileJoinGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вступить в группу"}
}, {
    "@type":"languagePackString",
    "key":"LoginAttemptsInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"С этих устройств нет доступа к Вашему аккаунту. Код для входа был введен верно, но правильный пароль указан не был."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogPreviousLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прежняя ссылка"}
}, {
    "@type":"languagePackString",
    "key":"PassportInfoUrl",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"https://telegram.org/faq#passport"}
}, {
    "@type":"languagePackString",
    "key":"LowDiskSpaceNeverRemove",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Никогда не удалять"}
}, {
    "@type":"languagePackString",
    "key":"LowDiskSpaceTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Внимание"}
}, {
    "@type":"languagePackString",
    "key":"LowDiskSpaceTitle2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалять медиа старше"}
}, {
    "@type":"languagePackString",
    "key":"InAppNotifications",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В приложении"}
}, {
    "@type":"languagePackString",
    "key":"ManageChannelMenu",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Управление каналом"}
}, {
    "@type":"languagePackString",
    "key":"SlowmodeMinutes",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$d мин"}
}, {
    "@type":"languagePackString",
    "key":"MapPreviewProviderGoogle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Google"}
}, {
    "@type":"languagePackString",
    "key":"StartMessaging",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Начать общение"}
}, {
    "@type":"languagePackString",
    "key":"NearbyCreateGroupInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Все, кто находится рядом (соседи, коллеги, сокурсники, участники мероприятия), будут видеть группу в разделе «Люди рядом»."
    }
}, {
    "@type":"languagePackString",
    "key":"MapPreviewProviderTelegram",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Telegram"}
}, {
    "@type":"languagePackString",
    "key":"MarkAsRead",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пометить как прочитанное"}
}, {
    "@type":"languagePackString",
    "key":"MasksRemoved",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Маски удалены"}
}, {
    "@type":"languagePackString",
    "key":"PassportResidence",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Местожительство"}
}, {
    "@type":"languagePackString",
    "key":"SmartNotificationsInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$d / %2$s"}
}, {
    "@type":"languagePackString",
    "key":"May",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Май"}
}, {
    "@type":"languagePackString",
    "key":"BlurLinear",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Линейное"}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingZipPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Индекс"}
}, {
    "@type":"languagePackString",
    "key":"MegaDeleteInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы потеряете все сообщения из этой группы."}
}, {
    "@type":"languagePackString",
    "key":"SortLastName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фамилия"}
}, {
    "@type":"languagePackString",
    "key":"MegaLeaveAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите покинуть группу?"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionLinkGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПРИВЯЗАТЬ ГРУППУ"}
}, {
    "@type":"languagePackString",
    "key":"PasswordHint",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подсказка"}
}, {
    "@type":"languagePackString",
    "key":"MegaLeaveAlertWithName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите покинуть **%1$s**?"}
}, {
    "@type":"languagePackString",
    "key":"EventLogAllEvents",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все действия"}
}, {
    "@type":"languagePackString",
    "key":"PassportNameCheckAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пожалуйста, проверьте имя:\n\n%1$s %2$s %3$s"}
}, {
    "@type":"languagePackString",
    "key":"ShareYouLocationTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поделиться геопозицией?"}
}, {
    "@type":"languagePackString",
    "key":"MegaPrivate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Частная группа"}
}, {
    "@type":"languagePackString",
    "key":"MoreInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Больше информации"}
}, {
    "@type":"languagePackString",
    "key":"ActionYouKickUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы удалили un2"}
}, {
    "@type":"languagePackString",
    "key":"MegaPublicInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Публичные группы можно найти через поиск, история чата доступна всем, и присоединиться может любой пользователь."
    }
}, {
    "@type":"languagePackString",
    "key":"LinksTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Общие ссылки"}
}, {
    "@type":"languagePackString",
    "key":"GroupDeletedUndo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Группа удалена."}
}, {
    "@type":"languagePackString",
    "key":"MembersCountZero",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"до %1$s"}
}, {
    "@type":"languagePackString",
    "key":"SuggestStickersInstalled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикеры из своих наборов"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialog1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ева"}
}, {
    "@type":"languagePackString",
    "key":"RegisterText2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите своё имя и загрузите фотографию."}
}, {
    "@type":"languagePackString",
    "key":"Message",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сообщение"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddAddressUploadInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы подтвердить адрес, загрузите скан или фотографию выбранного документа (все страницы)."
    }
}, {
    "@type":"languagePackString",
    "key":"ExportTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Экспортировать"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsDeleteAllExceptionTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить все исключения"}
}, {
    "@type":"languagePackString",
    "key":"DisappearingVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Секретное видео"}
}, {
    "@type":"languagePackString",
    "key":"ErrorSendRestrictedStickersAll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В этой группе запрещено отправлять стикеры."}
}, {
    "@type":"languagePackString",
    "key":"MessageLifetimeChanged",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s включил(а) удаление по таймеру через %2$s"}
}, {
    "@type":"languagePackString",
    "key":"OptionHint",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ответ"}
}, {
    "@type":"languagePackString",
    "key":"MessageLifetimePhoto",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если вы поставите таймер, фото автоматически удалится после просмотра."
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedGeoLiveChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$@ закрепил(а) трансляцию геопозиции"}
}, {
    "@type":"languagePackString",
    "key":"Yesterday",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"вчера"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupGif",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) GIF в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightSystemDefault",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использовать настройки системы"}
}, {
    "@type":"languagePackString",
    "key":"MessageScheduleEditTime",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить время"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedPollChannel2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) опрос %2$s"}
}, {
    "@type":"languagePackString",
    "key":"MessageScheduleSend",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить сейчас"}
}, {
    "@type":"languagePackString",
    "key":"LedDisabled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Откл."}
}, {
    "@type":"languagePackString",
    "key":"VideoDoesNotSupportStreaming",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Это видео не рассчитано на потоковую загрузку. Возможно, для просмотра потребуется скачать его."
    }
}, {
    "@type":"languagePackString",
    "key":"MessageScheduleToday",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сегодня"}
}, {
    "@type":"languagePackString",
    "key":"UseLessDataOnRoaming",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Только в роуминге"}
}, {
    "@type":"languagePackString",
    "key":"MessageScheduledOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Запланировано на %1$s"}
}, {
    "@type":"languagePackString",
    "key":"PassportUploadNotImage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы можете загружать только изображения."}
}, {
    "@type":"languagePackString",
    "key":"MessagesSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сообщения"}
}, {
    "@type":"languagePackString",
    "key":"MobileHidden",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Номер скрыт"}
}, {
    "@type":"languagePackString",
    "key":"PassportNameCountry",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя (%1$s)"}
}, {
    "@type":"languagePackString",
    "key":"MobileHiddenExceptionInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Номер телефона будет виден, когда %1$s добавит Вас в контакты."
    }
}, {
    "@type":"languagePackString",
    "key":"ChannelAlertCreate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СОЗДАТЬ КАНАЛ"}
}, {
    "@type":"languagePackString",
    "key":"AutoplayVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Видео"}
}, {
    "@type":"languagePackString",
    "key":"MobileVisibleInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Когда Вы нажмёте **ГОТОВО**, %1$s начнёт видеть Ваш номер телефона."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_BG",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на болгарском языке"}
}, {
    "@type":"languagePackString",
    "key":"Mono",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Моно"}
}, {
    "@type":"languagePackString",
    "key":"ErrorSendRestrictedPolls",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Администраторы группы запретили Вам публиковать в ней опросы."
    }
}, {
    "@type":"languagePackString",
    "key":"UnhideFromTop",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закрепить"}
}, {
    "@type":"languagePackString",
    "key":"PassportDocument",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Данные документа"}
}, {
    "@type":"languagePackString",
    "key":"UsbActive",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Передача по USB активна"}
}, {
    "@type":"languagePackString",
    "key":"MuteFor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключить на %1$s"}
}, {
    "@type":"languagePackString",
    "key":"MegaAdminsInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Выберите помощников для управления группой. Чтобы удалить администратора, нажмите и удерживайте."
    }
}, {
    "@type":"languagePackString",
    "key":"VoipRinging",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вызов"}
}, {
    "@type":"languagePackString",
    "key":"NetworkUsageMobile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"МОБ. СЕТЬ"}
}, {
    "@type":"languagePackString",
    "key":"ActionBotDocumentPassportRegistration",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прописка"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedCaption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 отредактировал(а) подпись:"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessagePoll2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новый опрос %2$s"}
}, {
    "@type":"languagePackString",
    "key":"NetworkUsageSince",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использование сети с %1$s"}
}, {
    "@type":"languagePackString",
    "key":"NoMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Здесь пока ничего нет…"}
}, {
    "@type":"languagePackString",
    "key":"NetworkUsageWiFi",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"WI-FI"}
}, {
    "@type":"languagePackString",
    "key":"DeleteAccountIfAwayFor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Если Вас не было"}
}, {
    "@type":"languagePackString",
    "key":"NoOtherWebSessions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет активных авторизаций."}
}, {
    "@type":"languagePackString",
    "key":"MessageNotFound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сообщение не найдено"}
}, {
    "@type":"languagePackString",
    "key":"GroupsAndChannelsHelp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите, кто может приглашать Вас в группы и каналы."}
}, {
    "@type":"languagePackString",
    "key":"NeverAllow",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда запрещать"}
}, {
    "@type":"languagePackString",
    "key":"NeverShareWithTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда скрывать"}
}, {
    "@type":"languagePackString",
    "key":"EventLogDeletedMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 удалил(а) сообщение:"}
}, {
    "@type":"languagePackString",
    "key":"ScheduleMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить позже"}
}, {
    "@type":"languagePackString",
    "key":"NewBroadcastList",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новый список рассылки"}
}, {
    "@type":"languagePackString",
    "key":"SendingSms",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправка SMS..."}
}, {
    "@type":"languagePackString",
    "key":"NewContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новый контакт"}
}, {
    "@type":"languagePackString",
    "key":"NewGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Создать группу"}
}, {
    "@type":"languagePackString",
    "key":"ChatHistoryVisibleInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Новые участники будут видеть полную историю сообщений."
    }
}, {
    "@type":"languagePackString",
    "key":"EditMessageError",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, Вы не можете изменить сообщение."}
}, {
    "@type":"languagePackString",
    "key":"EventLogToggledInvitesOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 запретил(а) приглашения в группу"}
}, {
    "@type":"languagePackString",
    "key":"NewPoll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новый опрос"}
}, {
    "@type":"languagePackString",
    "key":"ReportChatSent",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Жалоба отправлена"}
}, {
    "@type":"languagePackString",
    "key":"NewSecretChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Создать секретный чат"}
}, {
    "@type":"languagePackString",
    "key":"ColorRed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Красный"}
}, {
    "@type":"languagePackString",
    "key":"NewThemePreviewLine1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Знаешь, который час?"}
}, {
    "@type":"languagePackString",
    "key":"NewThemePreviewLine2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В Токио утро 😎"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddressInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите подтверждение адреса"}
}, {
    "@type":"languagePackString",
    "key":"NewThemeTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новая тема"}
}, {
    "@type":"languagePackString",
    "key":"AreYouSureDeleteThisChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить этот чат?"}
}, {
    "@type":"languagePackString",
    "key":"NoAudio",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы видеть здесь файлы с музыкой, добавьте их в библиотеку на своем устройстве."
    }
}, {
    "@type":"languagePackString",
    "key":"NoBlocked",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заблокированных пользователей пока нет"}
}, {
    "@type":"languagePackString",
    "key":"NoBlockedChannel2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пользователи, которых исключили администраторы канала, не смогут присоединиться снова с помощью ссылки."
    }
}, {
    "@type":"languagePackString",
    "key":"NoOtherSessionsInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете войти в Telegram с другого смартфона, планшета или компьютера, используя тот же номер телефона. Все данные будут мгновенно синхронизированы."
    }
}, {
    "@type":"languagePackString",
    "key":"NoBlockedGroup",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Заблокированные пользователи удалены из группы и могут вернуться только по приглашению администратора. Пригласительные ссылки для них не работают."
    }
}, {
    "@type":"languagePackString",
    "key":"NearbyPlaces",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Или выберите место"}
}, {
    "@type":"languagePackString",
    "key":"NoCallLog",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы пока не пользовались звонками."}
}, {
    "@type":"languagePackString",
    "key":"SharedMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Общие медиа"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddPassportRegistration",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прописка"}
}, {
    "@type":"languagePackString",
    "key":"NoChatsContactsHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы написать сообщение, нажмите на значок карандаша в правом нижнем углу."
    }
}, {
    "@type":"languagePackString",
    "key":"SecretChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Секретные чаты"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedContact2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) контакт %3$s в группе «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedVoice",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) голосовое сообщение"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightBrightnessInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Переключать на выбранную ночную тему, если яркость внешнего освещения меньше %1$d%%."
    }
}, {
    "@type":"languagePackString",
    "key":"NoGIFsFound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"GIF не найдены"}
}, {
    "@type":"languagePackString",
    "key":"NoGroupsInCommon",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Общих групп пока нет"}
}, {
    "@type":"languagePackString",
    "key":"NoMailInstalled",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пожалуйста, отправьте нам письмо с описанием Вашей проблемы на sms@stel.com."
    }
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingCityPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Город"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminAddAdmins",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбор администраторов"}
}, {
    "@type":"languagePackString",
    "key":"PassportExpired",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Срок действия"}
}, {
    "@type":"languagePackString",
    "key":"PassportPostcode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Индекс"}
}, {
    "@type":"languagePackString",
    "key":"FingerprintHelp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сканер отпечатков пальцев"}
}, {
    "@type":"languagePackString",
    "key":"OtherSessions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Активные сеансы"}
}, {
    "@type":"languagePackString",
    "key":"EnterThemeName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите название темы"}
}, {
    "@type":"languagePackString",
    "key":"NoMedia",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Поделитесь фото и видео в этом чате, и они будут доступны вам с любого устройства."
    }
}, {
    "@type":"languagePackString",
    "key":"SetAdminsNotAllInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Только администраторы могут добавлять и удалять участников, закреплять сообщения, изменять имя и фото группы."
    }
}, {
    "@type":"languagePackString",
    "key":"SendGifPreview",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить GIF"}
}, {
    "@type":"languagePackString",
    "key":"RecoveryEmail",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Электронная почта"}
}, {
    "@type":"languagePackString",
    "key":"SendMessagesTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить сообщения %1$s?"}
}, {
    "@type":"languagePackString",
    "key":"WithinAMonth",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"был(а) в этом месяце"}
}, {
    "@type":"languagePackString",
    "key":"NoOtherSessions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет других активных сеансов"}
}, {
    "@type":"languagePackString",
    "key":"NoSharedAudioSecret",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Здесь будет отображаться музыка из этого чата."}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedGeo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) карту в группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"NoSound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Без звука"}
}, {
    "@type":"languagePackString",
    "key":"AutoNightTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Конец"}
}, {
    "@type":"languagePackString",
    "key":"NoStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикеров пока нет"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageAlbum",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новый альбом"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedFile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) файл в группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedGame",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) игру в группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"StickersAndMasks",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикеры и маски"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyForwardsTitle",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Кто может ссылаться на мой аккаунт при пересылке сообщений?"
    }
}, {
    "@type":"languagePackString",
    "key":"PaymentTransactionTotal",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Итого"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedGameScoreChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) очки в игре"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedGif",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) GIF в группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"NoBlockedGroup2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пользователи, которых исключили администраторы группы, не смогут присоединиться снова с помощью ссылки."
    }
}, {
    "@type":"languagePackString",
    "key":"EditMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Редактирование"}
}, {
    "@type":"languagePackString",
    "key":"ChannelOtherSubscribers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Другие подписчики"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedGifChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) GIF"}
}, {
    "@type":"languagePackString",
    "key":"NotificationInvitedToGroupByLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s вступил(а) в группу %2$s по ссылке-приглашению"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedMusic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) аудиофайл в группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"ActionPinnedContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 закрепил(а) контакт"}
}, {
    "@type":"languagePackString",
    "key":"EncryptedChatStartedIncoming",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы присоединились к секретному чату."}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedNoText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) сообщение в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedPoll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) опрос в группе «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"AttachContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Контакт"}
}, {
    "@type":"languagePackString",
    "key":"NoMediaAutoDownload",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не загружать"}
}, {
    "@type":"languagePackString",
    "key":"RemoveStickersCount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"УДАЛИТЬ %1$s"}
}, {
    "@type":"languagePackString",
    "key":"UnsupportedMedia",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Сообщение не поддерживается Вашей версией Telegram. Пожалуйста, обновите приложение, чтобы его увидеть: https://telegram.org/update"
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedPoll2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) опрос %3$s в группе «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRestrictedChangeInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменять профиль"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_HY",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на армянском языке"}
}, {
    "@type":"languagePackString",
    "key":"SendMessagesToGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить в %1$s?"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsMuted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Без уведомлений"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedPollChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) опрос"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrContactSorting",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить сортировку"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageRound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) видеосообщение"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedStickerEmoji",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) стикер %3$s в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedStickerEmojiChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) стикер %2$s"}
}, {
    "@type":"languagePackString",
    "key":"StopDownload",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Остановить загрузку"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedTextChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"NotificationEditedGroupName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s переименовал(а) группу %2$s"}
}, {
    "@type":"languagePackString",
    "key":"StopLiveLocationAlertToUser",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите прекратить транслировать геопозицию в чат с %1$s?"
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationGroupAddMember",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s пригласил(а) %3$s в группу %2$s"}
}, {
    "@type":"languagePackString",
    "key":"NotificationGroupAlbum",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) альбом в группу «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"GpsDisabledAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Похоже, у Вас отключен GPS, необходимо включить его. чтобы использовать геопозицию в приложении."
    }
}, {
    "@type":"languagePackString",
    "key":"OpenUrlAlert2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перейти по ссылке %1$s?"}
}, {
    "@type":"languagePackString",
    "key":"RestorePasswordResetAccount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СБРОС АККАУНТА"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) контакт"}
}, {
    "@type":"languagePackString",
    "key":"VoipOfflineOpenSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки"}
}, {
    "@type":"languagePackString",
    "key":"SearchImages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"НАЙТИ ИЗОБРАЖЕНИЯ"}
}, {
    "@type":"languagePackString",
    "key":"KickFromBroadcast",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить из списка рассылки"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddress",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Адрес"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageContact2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) контакт %2$s"}
}, {
    "@type":"languagePackString",
    "key":"ScheduledMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отложенная отправка"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageDocument",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) файл"}
}, {
    "@type":"languagePackString",
    "key":"ResetStatisticsAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить статистику"}
}, {
    "@type":"languagePackString",
    "key":"EventLogChangedStickersSet",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 изменил(а) набор стикеров группы"}
}, {
    "@type":"languagePackString",
    "key":"October",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Октябрь"}
}, {
    "@type":"languagePackString",
    "key":"ReportSpamTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сообщить о спаме"}
}, {
    "@type":"languagePackString",
    "key":"FromCamera",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"С камеры"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) контакт в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageMap",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) геопозицию"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) фото в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"ResetChatBackgroundsAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить фон чатов"}
}, {
    "@type":"languagePackString",
    "key":"PassportDeletePhoneAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить номер телефона?"}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя полностью"}
}, {
    "@type":"languagePackString",
    "key":"NotificationGroupKickYou",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s удалил(а) вас из «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"AccDescrCancelReply",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отмена ответа"}
}, {
    "@type":"languagePackString",
    "key":"RestorePasswordNoEmailTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сожалеем"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupPoll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) опрос в группу «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupRound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) видеосообщение в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_NE",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на непальском языке"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupStickerEmoji",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) стикер %3$s в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageMusic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) аудиофайл"}
}, {
    "@type":"languagePackString",
    "key":"Schedule",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Запланировать"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_BN",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на бенгальском языке"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageNoText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) сообщение"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessagePoll2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) опрос %2$s"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageSDVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) самоуничтожающееся видео"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageStickerEmoji",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) стикер %2$s"}
}, {
    "@type":"languagePackString",
    "key":"PassportNoDocuments",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы пока не загружали документы"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsAddAnException",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить исключение"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Должность: %1$s"}
}, {
    "@type":"languagePackString",
    "key":"VoipHangingUp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Завершение"}
}, {
    "@type":"languagePackString",
    "key":"YourPasswordSuccess",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Готово!"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsEnableCustom",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Включить особые уведомления"}
}, {
    "@type":"languagePackString",
    "key":"TypeLocationGroupEdit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите, чтобы добавить ссылку"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialogMessage7",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Это не те дроиды, что вы ищете."}
}, {
    "@type":"languagePackString",
    "key":"Emoji4",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Занятия"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPreviousGroupLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Прежняя ссылка группы"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsUntilForever",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Навсегда"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCheckoutProvider",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Платёжный провайдер"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsExceptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Исключения"}
}, {
    "@type":"languagePackString",
    "key":"PassportIdentityPassport",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите скан своего паспорта"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsGroups",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Группы"}
}, {
    "@type":"languagePackString",
    "key":"GlobalAttachGifRestricted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В этой группе запрещено отправлять GIF."}
}, {
    "@type":"languagePackString",
    "key":"ChatArchived",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чат перенесён в архив."}
}, {
    "@type":"languagePackString",
    "key":"RepeatNotificationsNever",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Никогда"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsLedColor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Цвет"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminWhatCanDo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Возможности администратора:"}
}, {
    "@type":"languagePackString",
    "key":"GroupsAndChannels",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Группы"}
}, {
    "@type":"languagePackString",
    "key":"ReverseOrder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обратный порядок"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsLedInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Светодиод — маленький мерцающий индикатор на некоторых устройствах, используемый для уведомления о новых сообщениях."
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupMusic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) аудиофайл в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"InviteToGroupError",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы не можете приглашать этого пользователя в группы из-за его настроек приватности."
    }
}, {
    "@type":"languagePackString",
    "key":"MapPreviewProviderYandex",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Yandex"}
}, {
    "@type":"languagePackString",
    "key":"StopAllLocationSharings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ОСТАНОВИТЬ ВСЕ"}
}, {
    "@type":"languagePackString",
    "key":"TerminateWebSessionStop",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заблокировать %1$s"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsOther",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Другое"}
}, {
    "@type":"languagePackString",
    "key":"Regular",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обычный"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsPriorityLow",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Низкий"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionUnlinkChannelAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите отвязать **%1$s** от этого канала?"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsPriorityMedium",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Средний"}
}, {
    "@type":"languagePackString",
    "key":"PassportFrontSideInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите фотографию лицевой стороны документа"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsPrioritySettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Как в настройках"}
}, {
    "@type":"languagePackString",
    "key":"ServiceNotifications",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Служебные уведомления"}
}, {
    "@type":"languagePackString",
    "key":"Hybrid",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Гибрид"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsPrivateChats",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Личные чаты"}
}, {
    "@type":"languagePackString",
    "key":"ResendCode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить ещё раз"}
}, {
    "@type":"languagePackString",
    "key":"NoSharedVoice",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Поделитесь голосовыми сообщениями в этом чате, и они будут доступны Вам с любого устройства."
    }
}, {
    "@type":"languagePackString",
    "key":"ReportSpamAlertChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хотите сообщить о спаме из этого канала?"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddIdentityCardInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Загрузите сканы заверенного перевода для своего удостоверения личности."
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationsService",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перезапуск при закрытии"}
}, {
    "@type":"languagePackString",
    "key":"PinMessageAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закрепить сообщение"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsUnmuted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Уведомления"}
}, {
    "@type":"languagePackString",
    "key":"formatterMonth",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"dd MMM"}
}, {
    "@type":"languagePackString",
    "key":"OnlyWhenScreenOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Только когда экран выключен"}
}, {
    "@type":"languagePackString",
    "key":"OpenChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПЕРЕЙТИ В КАНАЛ"}
}, {
    "@type":"languagePackString",
    "key":"EnterNewPasscode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите новый код-пароль"}
}, {
    "@type":"languagePackString",
    "key":"SharedPlace",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Место"}
}, {
    "@type":"languagePackString",
    "key":"OpenGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПЕРЕЙТИ В ГРУППУ"}
}, {
    "@type":"languagePackString",
    "key":"YouLeft",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы покинули группу"}
}, {
    "@type":"languagePackString",
    "key":"OpenInExternalApp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открыть в..."}
}, {
    "@type":"languagePackString",
    "key":"OpenMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПЕРЕЙТИ К СООБЩЕНИЮ"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCardSavePaymentInformationInfoLine2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы сделать это, пожалуйста, *настройте двухэтапную аутентификацию*."
    }
}, {
    "@type":"languagePackString",
    "key":"OpenTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПРОСМОТР ТЕМЫ"}
}, {
    "@type":"languagePackString",
    "key":"SentSmsCodeTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите код"}
}, {
    "@type":"languagePackString",
    "key":"TerminateAllWebSessions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключить все сайты"}
}, {
    "@type":"languagePackString",
    "key":"Emoji2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Животные и природа"}
}, {
    "@type":"languagePackString",
    "key":"RateCallDropped",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звонок оборвался"}
}, {
    "@type":"languagePackString",
    "key":"PasswordDoNotMatch",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пароли не совпадают"}
}, {
    "@type":"languagePackString",
    "key":"OpenUrlOption1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Авторизоваться на %1$s как **%2$s**"}
}, {
    "@type":"languagePackString",
    "key":"SinglePhotosHelp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"\nНе группировать фотографии"}
}, {
    "@type":"languagePackString",
    "key":"VoipOfflineAirplaneTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Режим полёта"}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingSaveInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете сохранить эти данные, чтобы в будущем использовать их для доставки."
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationsPriorityHigh",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Высокий"}
}, {
    "@type":"languagePackString",
    "key":"ShortMessageLifetimeForever",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Откл."}
}, {
    "@type":"languagePackString",
    "key":"OpenUrlOption2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Разрешить **%1$s** писать мне сообщения"}
}, {
    "@type":"languagePackString",
    "key":"UserUnblocked",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пользователь разблокирован"}
}, {
    "@type":"languagePackString",
    "key":"TermsOfService",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пользовательское соглашение"}
}, {
    "@type":"languagePackString",
    "key":"Orange",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Оранжевый"}
}, {
    "@type":"languagePackString",
    "key":"RemindTodayAt",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"'Отправить сегодня в' HH:mm"}
}, {
    "@type":"languagePackString",
    "key":"OutgoingCalls",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Исходящие звонки"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyPaymentsClearInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете удалить свою платёжную информацию и запросить удаление сохраненных кредитных карт у платёжных провайдеров. Обратите внимание, что сам Telegram не хранит информацию о Ваших кредитных картах."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportUploadAdditinalDocument",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузить дополнительные сканы"}
}, {
    "@type":"languagePackString",
    "key":"SelectImage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбрать изображение"}
}, {
    "@type":"languagePackString",
    "key":"P2PContactsMinusPlus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мои контакты (-%1$d, +%2$d)"}
}, {
    "@type":"languagePackString",
    "key":"P2PEverybody",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все"}
}, {
    "@type":"languagePackString",
    "key":"ResetChatBackgroundsAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите сбросить все фоны?"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageAudio",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) голосовое сообщение"}
}, {
    "@type":"languagePackString",
    "key":"PassportMidnameLatin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отчество (латиницей)"}
}, {
    "@type":"languagePackString",
    "key":"P2PEverybodyMinus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Все (-%1$d)"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCardSavePaymentInformationInfoLine1",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете сохранить эти данные, чтобы в будущем использовать их для оплаты."
    }
}, {
    "@type":"languagePackString",
    "key":"ViewExceptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показать исключения"}
}, {
    "@type":"languagePackString",
    "key":"Page1Message",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Самый **быстрый** мессенджер в мире.\n**Бесплатный** и **безопасный**."
    }
}, {
    "@type":"languagePackString",
    "key":"Page2Message",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**Telegram** доставляет сообщения быстрее\nлюбого другого приложения."
    }
}, {
    "@type":"languagePackString",
    "key":"Page2Title",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Быстрый"}
}, {
    "@type":"languagePackString",
    "key":"PaintOutlined",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Контур"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsForGroups",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Уведомления из групп"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsForChannels",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Уведомления из каналов"}
}, {
    "@type":"languagePackString",
    "key":"TosDecline",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, это означает, что Вы не сможете зарегистрироваться в Telegram.\n\nВ отличие от других сервисов, мы не используем Ваши данные для рекламы и других коммерческих целей. На серверах хранится только та информация, которая позволяет Вам полноценно пользоваться Telegram. Вы можете регулировать доступ к своим данным в настройках приложения, в разделе Конфиденциальность и безопасность.\n\nЕсли Вы не готовы предоставить минимальные нужные данные, увы, мы не сможем обеспечить работу Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"SendLiveLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Транслировать мою геопозицию"}
}, {
    "@type":"languagePackString",
    "key":"Unavailable",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Недоступен"}
}, {
    "@type":"languagePackString",
    "key":"PaintRegular",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обычный"}
}, {
    "@type":"languagePackString",
    "key":"December",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Декабрь"}
}, {
    "@type":"languagePackString",
    "key":"AddAccount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить аккаунт"}
}, {
    "@type":"languagePackString",
    "key":"ReEnterYourPasscode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите код-пароль ещё раз"}
}, {
    "@type":"languagePackString",
    "key":"PasscodeDoNotMatch",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Код-пароли не совпадают"}
}, {
    "@type":"languagePackString",
    "key":"DiscussChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"канал"}
}, {
    "@type":"languagePackString",
    "key":"GlobalSendMessageRestricted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В этой группе запрещено писать сообщения."}
}, {
    "@type":"languagePackString",
    "key":"PasscodePassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пароль"}
}, {
    "@type":"languagePackString",
    "key":"UpdateTelegram",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обновление Telegram"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddAgreement",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить договор об аренде"}
}, {
    "@type":"languagePackString",
    "key":"Fade",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Затемнение"}
}, {
    "@type":"languagePackString",
    "key":"FileUploadLimit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Размер файла не может превышать %1$s"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddBankInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите скан справки из банка"}
}, {
    "@type":"languagePackString",
    "key":"PinNotify",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Уведомление всем участникам"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddBillInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите скан коммунального платежа."}
}, {
    "@type":"languagePackString",
    "key":"RoamingUsage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использование сети в роуминге"}
}, {
    "@type":"languagePackString",
    "key":"ProxyConnections",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подключения"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialog5",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Старые пираты"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupSticker",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) стикер в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_PL",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на польском языке"}
}, {
    "@type":"languagePackString",
    "key":"SendWithoutSound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить без звука"}
}, {
    "@type":"languagePackString",
    "key":"PassportTranslation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перевод"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddDriverLicenceInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Загрузите сканы заверенного перевода для своих водительских прав."
    }
}, {
    "@type":"languagePackString",
    "key":"SyncContactsOn",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Контакты с этого устройства будут импортироваться для этого аккаунта."
    }
}, {
    "@type":"languagePackString",
    "key":"GroupMembers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Участники"}
}, {
    "@type":"languagePackString",
    "key":"SendingGame",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"играет в игру..."}
}, {
    "@type":"languagePackString",
    "key":"YourCode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Проверка телефона"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddPassportInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Загрузите сканы заверенного перевода для своего паспорта."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportAddTemporaryRegistration",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Временная прописка"}
}, {
    "@type":"languagePackString",
    "key":"DiscardVideoMessageTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отмена видеосообщения"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsTurnOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключить"}
}, {
    "@type":"languagePackString",
    "key":"SuggestContactsInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Показывать пользователей, которым Вы часто пишете, вверху в разделе поиска."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportAddTemporaryRegistrationInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузитe скан временной прописки."}
}, {
    "@type":"languagePackString",
    "key":"GroupEmptyTitle1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы создали **группу**."}
}, {
    "@type":"languagePackString",
    "key":"LastSeenNobodyPlus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Никто (+%1$d)"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddTranslationUploadInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пожалуйста, загрузите сканы заверенного перевода для выбранного документа."
    }
}, {
    "@type":"languagePackString",
    "key":"PaymentCheckoutShippingMethod",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Способ доставки"}
}, {
    "@type":"languagePackString",
    "key":"YourEmailInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пожалуйста, добавьте настоящий адрес электронной почты. Это единственный способ восстановить забытый пароль."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportAddressNoUploadInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Укажите свой адрес"}
}, {
    "@type":"languagePackString",
    "key":"AddAdminErrorNotAMember",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы не можете назначить этого пользователя администратором, потому что он не состоит в группе и Вы не можете его пригласить."
    }
}, {
    "@type":"languagePackString",
    "key":"DeleteMessagesOption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отозвать мои сообщения"}
}, {
    "@type":"languagePackString",
    "key":"EditCaption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить подпись"}
}, {
    "@type":"languagePackString",
    "key":"EncryptionProcessing",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обмен ключами шифрования..."}
}, {
    "@type":"languagePackString",
    "key":"PassportAuthorize",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Авторизоваться"}
}, {
    "@type":"languagePackString",
    "key":"StopLoading",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Остановить загрузку?"}
}, {
    "@type":"languagePackString",
    "key":"ThemeArcticBlue",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Холодная"}
}, {
    "@type":"languagePackString",
    "key":"Vignette",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Виньетка"}
}, {
    "@type":"languagePackString",
    "key":"UseProxyAddress",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сервер"}
}, {
    "@type":"languagePackString",
    "key":"PassportBirthdate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Дата рождения"}
}, {
    "@type":"languagePackString",
    "key":"PassportDeleteAddressAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить данные об адресе?"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRemovedWGroupPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 удалил(а) фото группы"}
}, {
    "@type":"languagePackString",
    "key":"PassportDeleteDocumentAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить этот документ?"}
}, {
    "@type":"languagePackString",
    "key":"SentCallOnly",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Мы звоним Вам по номеру **%1$s**, чтобы продиктовать код."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportDeletePersonalAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить личную информацию?"}
}, {
    "@type":"languagePackString",
    "key":"PhoneOther",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Другое"}
}, {
    "@type":"languagePackString",
    "key":"PassportDiscard",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отменить"}
}, {
    "@type":"languagePackString",
    "key":"PassportDocumentNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Номер документа"}
}, {
    "@type":"languagePackString",
    "key":"PassportSelectGender",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите пол"}
}, {
    "@type":"languagePackString",
    "key":"PassportDocuments",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сканы"}
}, {
    "@type":"languagePackString",
    "key":"PassportEmailCode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Код"}
}, {
    "@type":"languagePackString",
    "key":"formatterMonthName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"MMM"}
}, {
    "@type":"languagePackString",
    "key":"PassportEmailInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Укажите свой адрес электронной почты"}
}, {
    "@type":"languagePackString",
    "key":"PassportFemale",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Женский"}
}, {
    "@type":"languagePackString",
    "key":"TurnPasswordOffQuestion",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите отключить пароль?"}
}, {
    "@type":"languagePackString",
    "key":"ReportChatViolence",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Насилие"}
}, {
    "@type":"languagePackString",
    "key":"PassportFrontSide",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Лицевая сторона"}
}, {
    "@type":"languagePackString",
    "key":"PassportEmail",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Email"}
}, {
    "@type":"languagePackString",
    "key":"DistanceUnitsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Мера расстояния"}
}, {
    "@type":"languagePackString",
    "key":"PassportGender",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пол"}
}, {
    "@type":"languagePackString",
    "key":"SetAdmins",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Назначить администраторов"}
}, {
    "@type":"languagePackString",
    "key":"SendStickerPreview",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить стикер"}
}, {
    "@type":"languagePackString",
    "key":"PassportIdentityDocumentInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Загрузить скан паспорта или другого удостоверения личности"
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationsExceptionsAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Учтите, что **%1$s** в списке исключений, на которые не повлияет это изменение."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportAddTranslationAgreementInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Загрузите сканы заверенного перевода для своего договора об аренде."
    }
}, {
    "@type":"languagePackString",
    "key":"RaiseToSpeak",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Запись голоса при приближении"}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingMethod",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Способы доставки"}
}, {
    "@type":"languagePackString",
    "key":"PassportIdentityID",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите скан своего удостоверения личности"}
}, {
    "@type":"languagePackString",
    "key":"CurvesGreen",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ЗЕЛЁНЫЙ"}
}, {
    "@type":"languagePackString",
    "key":"BytesReceived",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Получено байт"}
}, {
    "@type":"languagePackString",
    "key":"PassportInfoTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Что такое Telegram Passport?"}
}, {
    "@type":"languagePackString",
    "key":"SetUrlChecking",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Проверка..."}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupPoll2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) опрос %3$s в группу «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"YouBlockedUser",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы не можете добавлять этого пользователя или бота в группы, так как Вы его заблокировали. Чтобы продолжить, нужно его разблокировать."
    }
}, {
    "@type":"languagePackString",
    "key":"PrivacyPaymentsClear",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить данные о платежах и доставке"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_AR",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на арабском языке"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_CS",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на чешском языке"}
}, {
    "@type":"languagePackString",
    "key":"EnableProxyAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хотите включить этот прокси?"}
}, {
    "@type":"languagePackString",
    "key":"DebugMenuResetDialogs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить кэш диалогов"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_JA",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на японском языке"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyBots",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Боты и сайты"}
}, {
    "@type":"languagePackString",
    "key":"AddMasks",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить маски"}
}, {
    "@type":"languagePackString",
    "key":"SendingVideoStatus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"отправляет видео..."}
}, {
    "@type":"languagePackString",
    "key":"SendMessageRestricted",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Администраторы группы запретили Вам отправлять сообщения до %1$s"
    }
}, {
    "@type":"languagePackString",
    "key":"ScreenCapture",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Разрешить снимки экрана"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_KA",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на грузинском языке"}
}, {
    "@type":"languagePackString",
    "key":"VoipFailed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не удалось соединиться"}
}, {
    "@type":"languagePackString",
    "key":"PaymentBillingAddress",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Платёжный адрес"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_KM",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на кхмерском языке"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_LV",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на латышском языке"}
}, {
    "@type":"languagePackString",
    "key":"WrongCountry",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Некорректный код страны"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_MK",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на македонском языке"}
}, {
    "@type":"languagePackString",
    "key":"VoipInCallBranding",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звонок Telegram"}
}, {
    "@type":"languagePackString",
    "key":"PinnedMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закреплённое сообщение"}
}, {
    "@type":"languagePackString",
    "key":"PassportUploadMaxReached",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Невозможно загрузить больше %1$s."}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_MS",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на малайском языке"}
}, {
    "@type":"languagePackString",
    "key":"EventLogFilterDeletedMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалённые сообщения"}
}, {
    "@type":"languagePackString",
    "key":"SmartNotificationsDetail",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s в течение %2$s"}
}, {
    "@type":"languagePackString",
    "key":"SetAsAdmin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сделать администратором"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_MY",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на бирманском языке"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageSDPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) самоуничтожающееся фото"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_NL",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на нидерландском языке"}
}, {
    "@type":"languagePackString",
    "key":"PinMessageAlertChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хотите закрепить это сообщение в чате?"}
}, {
    "@type":"languagePackString",
    "key":"EventLogYouRestricted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы изменили ограничения для пользователя un2 (%1$s)"}
}, {
    "@type":"languagePackString",
    "key":"AttachInlineRestrictedForever",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Администраторы группы запретили Вам отправлять сообщения с помощью ботов"
    }
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_SK",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на словацком языке"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_SL",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на словенском языке"}
}, {
    "@type":"languagePackString",
    "key":"P2PNobody",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не использовать"}
}, {
    "@type":"languagePackString",
    "key":"EmojiSuggestionsInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В **Telegram** можно искать эмодзи по множеству ключевых слов, но всегда есть что добавить. Вы можете предложить свои варианты слов для поиска здесь:"
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogOriginalCaptionEmpty",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пусто"}
}, {
    "@type":"languagePackString",
    "key":"TypePrivateGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Частная"}
}, {
    "@type":"languagePackString",
    "key":"formatDateSchedule",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"d MMM"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_TK",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на туркменском языке"}
}, {
    "@type":"languagePackString",
    "key":"EventLogRestrictedSendEmbed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предпросмотр для ссылок"}
}, {
    "@type":"languagePackString",
    "key":"June",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Июнь"}
}, {
    "@type":"languagePackString",
    "key":"PaymentTransactionReview",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваша транзакция"}
}, {
    "@type":"languagePackString",
    "key":"TosAgeText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Нажмите «Принять» для подтверждения, что Вам %1$s лет или более."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_FR",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на французском языке"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCardExpireDate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ММ/ГГ"}
}, {
    "@type":"languagePackString",
    "key":"NumberUnknown",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Неизвестен"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_UK",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на украинском языке"}
}, {
    "@type":"languagePackString",
    "key":"PassportMainPageInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузить фотографию главной страницы документа"}
}, {
    "@type":"languagePackString",
    "key":"PassportMidnameCountry",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отчество (%1$s)"}
}, {
    "@type":"languagePackString",
    "key":"UsernamePlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваше имя пользователя"}
}, {
    "@type":"languagePackString",
    "key":"ReturnToCall",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ВЕРНУТЬСЯ К РАЗГОВОРУ"}
}, {
    "@type":"languagePackString",
    "key":"PassportNameLatin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя (латиницей)"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageGroupDocument",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s отправил(а) файл в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"AddBannedErrorAdmin",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, Вы не можете заблокировать этого пользователя, поскольку он администрирует эту группу, а у Вас нет прав, чтобы его разжаловать."
    }
}, {
    "@type":"languagePackString",
    "key":"GroupDescription3",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Публичные ссылки вида t.me/title"}
}, {
    "@type":"languagePackString",
    "key":"CustomCallInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Этим пользователям будет разрешено или запрещено звонить Вам независимо от настроек выше."
    }
}, {
    "@type":"languagePackString",
    "key":"SlideToCancel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ВЛЕВО — ОТМЕНА"}
}, {
    "@type":"languagePackString",
    "key":"PassportNoDocumentsInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете добавить свой номер телефона, email, удостоверение личности или адрес проживания."
    }
}, {
    "@type":"languagePackString",
    "key":"ThemeInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбрать свою цветовую схему"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsDefaultOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"По умолчанию (вкл)"}
}, {
    "@type":"languagePackString",
    "key":"RepeatNotifications",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Повтор уведомлений"}
}, {
    "@type":"languagePackString",
    "key":"PassportNoExpireDate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет"}
}, {
    "@type":"languagePackString",
    "key":"ResetMyAccount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СБРОСИТЬ МОЙ АККАУНТ"}
}, {
    "@type":"languagePackString",
    "key":"PassportNoPolicy",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы отправите документы напрямую %1$s и разрешите их боту @%2$s писать вам сообщения."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportPersonal",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Личная информация"}
}, {
    "@type":"languagePackString",
    "key":"QuickReplyDefault4",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не могу ответить. Перезвони, пожалуйста."}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) фото в группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"UseProxySecret",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ключ"}
}, {
    "@type":"languagePackString",
    "key":"PassportPersonalDetailsInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Укажите данные о себе"}
}, {
    "@type":"languagePackString",
    "key":"ViewContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПРОСМОТР КОНТАКТА"}
}, {
    "@type":"languagePackString",
    "key":"SetColor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Задать цвет"}
}, {
    "@type":"languagePackString",
    "key":"PassportPhoneUseSame",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использовать %1$s"}
}, {
    "@type":"languagePackString",
    "key":"PassportProvidedInformation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предоставленная информация"}
}, {
    "@type":"languagePackString",
    "key":"MessageNotifications",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сообщения"}
}, {
    "@type":"languagePackString",
    "key":"StartEncryptedChat",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Начать секретный чат"}
}, {
    "@type":"languagePackString",
    "key":"Vibrate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вибросигнал"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddAgreementInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузите скан договора об аренде."}
}, {
    "@type":"languagePackString",
    "key":"PhoneWork",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Работа"}
}, {
    "@type":"languagePackString",
    "key":"PassportRequest",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Для регистрации **%1$s** запрашивает доступ к Вашей личной информации."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportReverseSide",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обратная сторона"}
}, {
    "@type":"languagePackString",
    "key":"ThemeHelpLink",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Когда Вы редактируете тему, она будет автоматически обновляться у всех, кто её использует.\n\nПо этой ссылке тему может установить любой пользователь:\n%1$s"
    }
}, {
    "@type":"languagePackString",
    "key":"VoipAudioRoutingSpeaker",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Динамик"}
}, {
    "@type":"languagePackString",
    "key":"LoginPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пароль"}
}, {
    "@type":"languagePackString",
    "key":"NotificationGroupKickMember",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s удалил(а) %3$s из группы %2$s"}
}, {
    "@type":"languagePackString",
    "key":"PassportSDK_Cancel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отмена"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewLine2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ты серьёзно?"}
}, {
    "@type":"languagePackString",
    "key":"PassportSDK_OpenGooglePlay",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открыть Google Play"}
}, {
    "@type":"languagePackString",
    "key":"LastSeenDateFormatted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"был(а) %1$s"}
}, {
    "@type":"languagePackString",
    "key":"UpdateAppAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, ваша версия приложения устарела и не может обработать этот запрос. Пожалуйста, обновите Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"Reply",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ответить"}
}, {
    "@type":"languagePackString",
    "key":"Page3Title",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Бесплатный"}
}, {
    "@type":"languagePackString",
    "key":"PassportSDK_TelegramPassport",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Telegram Passport"}
}, {
    "@type":"languagePackString",
    "key":"SendGameTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поделиться игрой с %1$s?"}
}, {
    "@type":"languagePackString",
    "key":"PassportScanPassport",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отсканируйте свой паспорт"}
}, {
    "@type":"languagePackString",
    "key":"PassportState",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Область"}
}, {
    "@type":"languagePackString",
    "key":"ResetAllNotifications",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить настройки уведомлений"}
}, {
    "@type":"languagePackString",
    "key":"PassportStreet2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Улица"}
}, {
    "@type":"languagePackString",
    "key":"PassportSurname",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фамилия"}
}, {
    "@type":"languagePackString",
    "key":"VoipExchangingKeys",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обмен ключами шифрования"}
}, {
    "@type":"languagePackString",
    "key":"PassportSurnameLatin",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фамилия (латиницей)"}
}, {
    "@type":"languagePackString",
    "key":"SharedLinksTab",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ССЫЛКИ"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsForChats",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Уведомления из чатов"}
}, {
    "@type":"languagePackString",
    "key":"StickersRemove",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить"}
}, {
    "@type":"languagePackString",
    "key":"ProfileJoinChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Присоединиться"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCardName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя и фамилия"}
}, {
    "@type":"languagePackString",
    "key":"DeleteAndStop",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить и остановить"}
}, {
    "@type":"languagePackString",
    "key":"AddProxy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить прокси"}
}, {
    "@type":"languagePackString",
    "key":"PaymentConfirmationMessage",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Ваша карта %1$s сохранена. Чтобы оплатить этой картой, пожалуйста, введите свой пароль двухэтапной аутентификации."
    }
}, {
    "@type":"languagePackString",
    "key":"PaymentCardSavePaymentInformation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сохранить реквизиты"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddLicence",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить водительские права"}
}, {
    "@type":"languagePackString",
    "key":"UndoAllCustom",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Сбросить особые настройки уведомлений для отдельных контактов, чатов и каналов."
    }
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingReceiver",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Получатель"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCheckoutEmail",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Контактный адрес"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminRank",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Должность"}
}, {
    "@type":"languagePackString",
    "key":"PassportCitizenship",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Гражданство"}
}, {
    "@type":"languagePackString",
    "key":"SetAdminsAll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Права администратора у всех"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCheckoutMethod",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Способ оплаты"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCheckoutPhoneNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Номер телефона"}
}, {
    "@type":"languagePackString",
    "key":"SavedMessagesInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сохранить на память"}
}, {
    "@type":"languagePackString",
    "key":"PaymentConfirmationNewCard",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите другую карту"}
}, {
    "@type":"languagePackString",
    "key":"PollOptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Варианты ответа"}
}, {
    "@type":"languagePackString",
    "key":"PassportUploadDocument",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузить сканы"}
}, {
    "@type":"languagePackString",
    "key":"PaymentFailed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, платёж отклонён."}
}, {
    "@type":"languagePackString",
    "key":"PaymentInvoice",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СЧЁТ"}
}, {
    "@type":"languagePackString",
    "key":"SendAsFiles",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Как файлы"}
}, {
    "@type":"languagePackString",
    "key":"EditAdminAddUsers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавление участников"}
}, {
    "@type":"languagePackString",
    "key":"ChatList",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Список чатов"}
}, {
    "@type":"languagePackString",
    "key":"TelegramFaqUrl",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"https://telegram.org/faq"}
}, {
    "@type":"languagePackString",
    "key":"PassportSelfie",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Селфи"}
}, {
    "@type":"languagePackString",
    "key":"formatterBannedUntil24H",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"dd MMM yyyy, HH:mm"}
}, {
    "@type":"languagePackString",
    "key":"PaymentPasswordInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы защитить свою платежную информацию, задайте пароль. Его нужно будет вводить для входа в Ваш аккаунт."
    }
}, {
    "@type":"languagePackString",
    "key":"SuggestStickersAll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Любые стикеры"}
}, {
    "@type":"languagePackString",
    "key":"PaymentPasswordTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пароль"}
}, {
    "@type":"languagePackString",
    "key":"PassportRequiredDocuments",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Необходимые документы"}
}, {
    "@type":"languagePackString",
    "key":"PaymentPasswordEmail",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваша электронная почта"}
}, {
    "@type":"languagePackString",
    "key":"TelegramPassportDelete",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить Telegram Passport"}
}, {
    "@type":"languagePackString",
    "key":"PaymentPhoneToProvider",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В качестве платёжной информации %1$s получит Ваш номер телефона."
    }
}, {
    "@type":"languagePackString",
    "key":"PaymentPrecheckoutFailed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, бот отменил платёж."}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingAddress1Placeholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Адрес 1 (улица)"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyClearShipping",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Информация о доставке"}
}, {
    "@type":"languagePackString",
    "key":"TimeToEditExpired",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Время на редактирование истекло."}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingSave",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сохранить адрес доставки"}
}, {
    "@type":"languagePackString",
    "key":"NobodyLikesSpam1",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, в данный момент Вы можете отправлять сообщения только взаимным контактам."
    }
}, {
    "@type":"languagePackString",
    "key":"PaymentSuccessfullyPaid",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы успешно перевели %1$s для %2$s за %3$s"}
}, {
    "@type":"languagePackString",
    "key":"PaymentNoShippingMethod",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, доставка по этому адресу невозможна."}
}, {
    "@type":"languagePackString",
    "key":"NeverShareWithPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда скрывать от пользователей..."}
}, {
    "@type":"languagePackString",
    "key":"NotificationsImportance",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Приоритет"}
}, {
    "@type":"languagePackString",
    "key":"Masks",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Маски"}
}, {
    "@type":"languagePackString",
    "key":"PermissionContacts",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Актуальная информация о Ваших контактах будет храниться зашифрованной в облаке Telegram, чтобы Вы могли связаться с друзьями с любого устройства."
    }
}, {
    "@type":"languagePackString",
    "key":"Strike",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Зачёркнутый"}
}, {
    "@type":"languagePackString",
    "key":"RecordingRound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"записывает видеосообщение..."}
}, {
    "@type":"languagePackString",
    "key":"PermissionNoAudio",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Telegram требуется доступ к микрофону, чтобы Вы могли отправлять голосовые сообщения."
    }
}, {
    "@type":"languagePackString",
    "key":"PermissionNoAudioVideo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Telegram необходим доступ к вашему микрофону, чтобы вы могли снимать видео."
    }
}, {
    "@type":"languagePackString",
    "key":"MessageLifetimeYouRemoved",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы отключили удаление по таймеру"}
}, {
    "@type":"languagePackString",
    "key":"PriorityInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Уведомления с максимальным приоритетом будут срабатывать, даже когда устройство в режиме «Не беспокоить»"
    }
}, {
    "@type":"languagePackString",
    "key":"PermissionXiaomiLockscreen",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Для корректной работы звонков, пожалуйста, разрешите приложению показываться на экране блокировки."
    }
}, {
    "@type":"languagePackString",
    "key":"SelectFromGallery",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбрать из галереи"}
}, {
    "@type":"languagePackString",
    "key":"SuggestStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подсказка по эмодзи"}
}, {
    "@type":"languagePackString",
    "key":"NotificationInvitedToGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s пригласил(а) вас в «%2$s»"}
}, {
    "@type":"languagePackString",
    "key":"UseProxySettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использовать прокси"}
}, {
    "@type":"languagePackString",
    "key":"Yellow",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Жёлтый"}
}, {
    "@type":"languagePackString",
    "key":"Phone",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Телефон"}
}, {
    "@type":"languagePackString",
    "key":"PaymentPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пароль и почта"}
}, {
    "@type":"languagePackString",
    "key":"PhoneHome",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Дом"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyForwardsInfo2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете добавить пользователей или целые группы в список исключений из настроек выше."
    }
}, {
    "@type":"languagePackString",
    "key":"SearchStickersHint",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск стикеров"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsDeleteAllExceptionAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить все исключения?"}
}, {
    "@type":"languagePackString",
    "key":"Page6Message",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В **Telegram** можно переписываться с любого количества устройств."
    }
}, {
    "@type":"languagePackString",
    "key":"RateCallSilentRemote",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Собеседник не слышал меня"}
}, {
    "@type":"languagePackString",
    "key":"TwoStepVerificationTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Двухэтапная аутентификация"}
}, {
    "@type":"languagePackString",
    "key":"PhoneNumberChange",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СМЕНИТЬ НОМЕР"}
}, {
    "@type":"languagePackString",
    "key":"PhoneNumberChange2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сменить номер"}
}, {
    "@type":"languagePackString",
    "key":"EnableProxyAlert2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы всегда можете изменить прокси-сервер в настройках > Данные и память."
    }
}, {
    "@type":"languagePackString",
    "key":"AutoNightLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Местное время рассвета и заката"}
}, {
    "@type":"languagePackString",
    "key":"PinMessageAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хотите закрепить это сообщение в группе?"}
}, {
    "@type":"languagePackString",
    "key":"RemindDayAt",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"'Отправить' d MMM 'в' HH:mm"}
}, {
    "@type":"languagePackString",
    "key":"PinMessageAlertChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хотите закрепить это сообщение в канале?"}
}, {
    "@type":"languagePackString",
    "key":"UserBlocked",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пользователь заблокирован"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCheckoutName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя"}
}, {
    "@type":"languagePackString",
    "key":"ViaBot",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"via"}
}, {
    "@type":"languagePackString",
    "key":"PinToTopLimitReached",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, закрепить можно не больше %1$s. Упорядочить список чатов можно также с помощью добавления в архив: в архиве можно закрепить неограниченное число чатов."
    }
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewLine3",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"А что не так с классикой? Обожаю Хассельхофа!"}
}, {
    "@type":"languagePackString",
    "key":"VoipQuickReplies",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ответить сообщением"}
}, {
    "@type":"languagePackString",
    "key":"PleaseDownload",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сначала загрузите медиафайл"}
}, {
    "@type":"languagePackString",
    "key":"PleaseEnterCurrentPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите пароль"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsSendPolls",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправка опросов"}
}, {
    "@type":"languagePackString",
    "key":"NobodyLikesSpam3",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, в данный момент Вы не можете отправлять сообщения в публичные группы."
    }
}, {
    "@type":"languagePackString",
    "key":"Edit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить"}
}, {
    "@type":"languagePackString",
    "key":"PleaseEnterCurrentPasswordTransfer",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите пароль, чтобы подтвердить передачу прав."}
}, {
    "@type":"languagePackString",
    "key":"PhoneNumberAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пользователи будут видеть Ваш новый номер, если он есть в их телефонной книге или если это разрешают Ваши настройки конфиденциальности: Настройки > Конфиденциальность > Номер телефона."
    }
}, {
    "@type":"languagePackString",
    "key":"TapForVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фото — нажмите, видео — удерживайте"}
}, {
    "@type":"languagePackString",
    "key":"SharedMediaTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Общие медиа"}
}, {
    "@type":"languagePackString",
    "key":"PleaseEnterPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите новый пароль"}
}, {
    "@type":"languagePackString",
    "key":"PleaseReEnterPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Повторите пароль"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsEmbedLinks",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Предпросмотр для ссылок"}
}, {
    "@type":"languagePackString",
    "key":"ResetChatBackgrounds",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить все фоны"}
}, {
    "@type":"languagePackString",
    "key":"Poll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Опрос"}
}, {
    "@type":"languagePackString",
    "key":"VoipOffline",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы не в сети. Чтобы позвонить, пожалуйста, подключитесь к интернету."
    }
}, {
    "@type":"languagePackString",
    "key":"WebSessionsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Авторизованные сайты"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyAdvanced",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Продвинутые настройки"}
}, {
    "@type":"languagePackString",
    "key":"PopupDisabled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отключены"}
}, {
    "@type":"languagePackString",
    "key":"ErrorOccurred",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Произошла ошибка."}
}, {
    "@type":"languagePackString",
    "key":"VoipAnsweringAsAccount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ответить как %s"}
}, {
    "@type":"languagePackString",
    "key":"SharedContent",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Общие материалы"}
}, {
    "@type":"languagePackString",
    "key":"PhoneNumberChangeTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сменить номер"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyAddAnException",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить людей или группы"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyClearPayment",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Платёжная информация"}
}, {
    "@type":"languagePackString",
    "key":"CreateNewContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Создать новый контакт"}
}, {
    "@type":"languagePackString",
    "key":"NoResult",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет результатов"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionUnlinkGroupAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите отвязать **%1$s** от этой группы?"}
}, {
    "@type":"languagePackString",
    "key":"TypeMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сообщение"}
}, {
    "@type":"languagePackString",
    "key":"RecordingAudio",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"записывает голосовое сообщение..."}
}, {
    "@type":"languagePackString",
    "key":"SelectContact",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите контакт"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyExceptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Исключения"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyPolicyAndTerms",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пользовательское соглашение и конфиденциальность"}
}, {
    "@type":"languagePackString",
    "key":"NearbyStartGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Создать группу"}
}, {
    "@type":"languagePackString",
    "key":"ScreenCaptureInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если выключено, Вы не сможете делать скриншоты приложения, но чаты не будут видны в окне переключения задач."
    }
}, {
    "@type":"languagePackString",
    "key":"PrivacyProfilePhotoInfo2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете добавить пользователей или целые группы в список исключений из настроек выше."
    }
}, {
    "@type":"languagePackString",
    "key":"RateCallSilentLocal",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Собеседника не слышно"}
}, {
    "@type":"languagePackString",
    "key":"PrivacySettingsChangedAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы изменили настройки приватности. Применить изменения?"
    }
}, {
    "@type":"languagePackString",
    "key":"SentSmsCode",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Мы отправили SMS с кодом проверки на Ваш телефон **%1$s**."
    }
}, {
    "@type":"languagePackString",
    "key":"formatDateScheduleDay",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"d MMM, EEE"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialogMessage5",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Йо-хо-хо"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Конфиденциальность"}
}, {
    "@type":"languagePackString",
    "key":"ShareLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Геопозиция"}
}, {
    "@type":"languagePackString",
    "key":"StartText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пожалуйста, укажите код страны и свой номер телефона."}
}, {
    "@type":"languagePackString",
    "key":"PrivacyBotsInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сайты, где Вы авторизовались через Telegram."}
}, {
    "@type":"languagePackString",
    "key":"ReportSpamUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ЗАБЛОКИРОВАТЬ"}
}, {
    "@type":"languagePackString",
    "key":"QuickReplyDefault1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Извини, сейчас не могу ответить."}
}, {
    "@type":"languagePackString",
    "key":"UnmuteNotifications",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Включить уведомления"}
}, {
    "@type":"languagePackString",
    "key":"QuickReplyDefault3",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перезвоню позже."}
}, {
    "@type":"languagePackString",
    "key":"EmptyExceptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет"}
}, {
    "@type":"languagePackString",
    "key":"InviteLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылка-приглашение"}
}, {
    "@type":"languagePackString",
    "key":"LanguageCustomAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Сторонний перевод (**%1$s**), который Вы собираетесь установить, закончен на  %2$d%%.\n\nЭто изменит язык всего интерфейса. Предложить свою версию перевода можно через [платформу переводов].\n\nВы можете в любой момент изменить язык в настройках приложения."
    }
}, {
    "@type":"languagePackString",
    "key":"RateCallInterruptions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Голос собеседника пропадал"}
}, {
    "@type":"languagePackString",
    "key":"GroupUserLeftError",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Поскольку пользователь был участником группы прежде, Вы не можете пригласить его обратно. Для этого требуется, чтобы вы были друг у друга в списке контактов.\n\nЕсли пользователя нет в чёрном списке группы, он может присоединиться с помощь ссылки-приглашения."
    }
}, {
    "@type":"languagePackString",
    "key":"UseDifferentThemeInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы также можете взять за основу для темы уже существующую пользовательскую тему."
    }
}, {
    "@type":"languagePackString",
    "key":"UseProxyUsername",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Логин"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_MN",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на монгольском языке"}
}, {
    "@type":"languagePackString",
    "key":"SendWithoutGrouping",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить без группировки"}
}, {
    "@type":"languagePackString",
    "key":"SlowmodeInfoOff",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Выберите, как часто каждый участник сможет писать в группу."
    }
}, {
    "@type":"languagePackString",
    "key":"PrivacyP2PHeader",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Peer-to-Peer"}
}, {
    "@type":"languagePackString",
    "key":"RateCallNoise",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Шум на заднем фоне"}
}, {
    "@type":"languagePackString",
    "key":"OpenUrlAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открыть URL %1$s?"}
}, {
    "@type":"languagePackString",
    "key":"Recent",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Недавние"}
}, {
    "@type":"languagePackString",
    "key":"RecentStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Недавние"}
}, {
    "@type":"languagePackString",
    "key":"UnpinFromTop",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открепить"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyP2P2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использовать peer-to-peer"}
}, {
    "@type":"languagePackString",
    "key":"DidNotGetTheCode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не получили код?"}
}, {
    "@type":"languagePackString",
    "key":"CallsDataUsage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звонки"}
}, {
    "@type":"languagePackString",
    "key":"PaymentCardCvv",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Код безопасности (CVV)"}
}, {
    "@type":"languagePackString",
    "key":"ViewDetails",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПОДРОБНЕЕ"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyPhoneTitle2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Кто может найти меня по номеру?"}
}, {
    "@type":"languagePackString",
    "key":"Slowmode",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Медленный режим"}
}, {
    "@type":"languagePackString",
    "key":"VoipAnswerCall",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ответить"}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingPhoneNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Номер телефона"}
}, {
    "@type":"languagePackString",
    "key":"SecretLinkPreviewAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы хотите включить предпросмотр ссылок в секретных чатах? Учтите, что предпросмотр создается на серверах Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"ReplyToGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ответить %1$s"}
}, {
    "@type":"languagePackString",
    "key":"Update",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ОБНОВИТЬ"}
}, {
    "@type":"languagePackString",
    "key":"ReportChatOther",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Другое"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageForwardFew",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s переслал(а) вам %2$s"}
}, {
    "@type":"languagePackString",
    "key":"SortFirstName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя"}
}, {
    "@type":"languagePackString",
    "key":"Unban",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Разблокировать"}
}, {
    "@type":"languagePackString",
    "key":"ReportSpam",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СООБЩИТЬ О СПАМЕ"}
}, {
    "@type":"languagePackString",
    "key":"UserBioEmpty",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не заполнено"}
}, {
    "@type":"languagePackString",
    "key":"VoipCallEnded",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звонок завершён"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsNoRead",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"без чтения"}
}, {
    "@type":"languagePackString",
    "key":"General",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Основные"}
}, {
    "@type":"languagePackString",
    "key":"ReportSpamAlertGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хотите сообщить о спаме от этой группы?"}
}, {
    "@type":"languagePackString",
    "key":"ReportSpamLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ГРУППА НЕ СВЯЗАНА С МЕСТОМ?"}
}, {
    "@type":"languagePackString",
    "key":"YesterdayAtFormatted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"вчера в %1$s"}
}, {
    "@type":"languagePackString",
    "key":"EventLogEditedChannelDescription",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"un1 изменил(а) описание канала:"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsTurnOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Включить"}
}, {
    "@type":"languagePackString",
    "key":"SoundDefault",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"По умолчанию"}
}, {
    "@type":"languagePackString",
    "key":"ProxySettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки прокси"}
}, {
    "@type":"languagePackString",
    "key":"StopLiveLocationAlertAll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите остановить трансляцию геопозиции?"}
}, {
    "@type":"languagePackString",
    "key":"WhoCanAddMembers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Кто может добавлять участников?"}
}, {
    "@type":"languagePackString",
    "key":"ResendCodeInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"На Ваш email отправлен код подтверждения."}
}, {
    "@type":"languagePackString",
    "key":"CurvesRed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"КРАСНЫЙ"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedFileChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) файл"}
}, {
    "@type":"languagePackString",
    "key":"ResetAccountInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Поскольку аккаунт **%1$s** используется и защищён паролем, в целях безопасности мы удалим его только через неделю.\n\nВы можете передумать и остановить отсчёт в любой момент."
    }
}, {
    "@type":"languagePackString",
    "key":"formatterMonthYear2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"MMM yyyy"}
}, {
    "@type":"languagePackString",
    "key":"formatterWeek",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"EEE"}
}, {
    "@type":"languagePackString",
    "key":"OK",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"OK"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyForwardsNobody",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Без ссылки на аккаунт"}
}, {
    "@type":"languagePackString",
    "key":"VoipBusy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Линия занята"}
}, {
    "@type":"languagePackString",
    "key":"EncryptionKeyLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"https://telegram.org/faq#secret-chats"}
}, {
    "@type":"languagePackString",
    "key":"LoopAnimatedStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Зациклить анимацию"}
}, {
    "@type":"languagePackString",
    "key":"ResetMyAccountText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если Вы сбросите аккаунт, все чаты, сообщения и файлы будут утеряны."
    }
}, {
    "@type":"languagePackString",
    "key":"OnlyIfSilent",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Только в беззвучном"}
}, {
    "@type":"languagePackString",
    "key":"ResetMyAccountWarningReset",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить аккаунт"}
}, {
    "@type":"languagePackString",
    "key":"ResetMyAccountWarning",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Внимание"}
}, {
    "@type":"languagePackString",
    "key":"InviteToChannelError",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы не можете приглашать этого пользователя в каналы из-за его настроек приватности."
    }
}, {
    "@type":"languagePackString",
    "key":"RevokeAlertNewLink",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Прежняя ссылка-приглашение больше не действует. Создана новая."
    }
}, {
    "@type":"languagePackString",
    "key":"RevokeButton",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить"}
}, {
    "@type":"languagePackString",
    "key":"SaveToGIFs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сохранить в свои GIF"}
}, {
    "@type":"languagePackString",
    "key":"SearchForPeople",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск людей..."}
}, {
    "@type":"languagePackString",
    "key":"SaveToGallery",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сохранить в галерею"}
}, {
    "@type":"languagePackString",
    "key":"Reset",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сброс"}
}, {
    "@type":"languagePackString",
    "key":"SaveToMusic",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сохранить в музыку"}
}, {
    "@type":"languagePackString",
    "key":"ScreenCaptureAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Ваши чаты не будут видны в окне переключения задач, но Вы не сможете делать скриншоты в Telegram.\n\nДля вступления настроек в силу может потребоваться перезапуск приложения."
    }
}, {
    "@type":"languagePackString",
    "key":"SendTodayAt",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"'Отправить сегодня в' HH:mm"}
}, {
    "@type":"languagePackString",
    "key":"SdCard",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Карта памяти"}
}, {
    "@type":"languagePackString",
    "key":"ThemeCreateHelp2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете выбрать другую ссылку для темы.\n\nМинимальная длина ссылки — 5 символов. Можно использовать символы a-z, 0-9 и подчеркивание."
    }
}, {
    "@type":"languagePackString",
    "key":"SearchGifs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"НАЙТИ GIF"}
}, {
    "@type":"languagePackString",
    "key":"SearchGifsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск GIF"}
}, {
    "@type":"languagePackString",
    "key":"SearchImagesTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск в сети"}
}, {
    "@type":"languagePackString",
    "key":"RecentlyViewed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Недавние"}
}, {
    "@type":"languagePackString",
    "key":"NoSharedLinksSecret",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Здесь будут отображаться общие ссылки из этого чата."}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_DA",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на датском языке"}
}, {
    "@type":"languagePackString",
    "key":"YouCreatedBroadcastList",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы создали список рассылки"}
}, {
    "@type":"languagePackString",
    "key":"SearchInSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск по настройкам и FAQ"}
}, {
    "@type":"languagePackString",
    "key":"VoipFeedbackCommentHint",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваш комментарий (необязательно)"}
}, {
    "@type":"languagePackString",
    "key":"P2PNobodyPlus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не использовать (+%1$d)"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_HR",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на хорватском языке"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedNoTextChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) сообщение"}
}, {
    "@type":"languagePackString",
    "key":"SecretChatContextBotAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Учтите, что боты создаются сторонними разработчиками. Всё, что Вы введёте после имени бота, будет отправлено его разработчику."
    }
}, {
    "@type":"languagePackString",
    "key":"PhoneCopied",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Телефон скопирован в буфер обмена"}
}, {
    "@type":"languagePackString",
    "key":"RestoreEmailTrouble",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет доступа к электронной почте %1$s?"}
}, {
    "@type":"languagePackString",
    "key":"formatDateAtTime",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s в %2$s"}
}, {
    "@type":"languagePackString",
    "key":"SavedMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Избранное"}
}, {
    "@type":"languagePackString",
    "key":"SecretChatName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Telegram"}
}, {
    "@type":"languagePackString",
    "key":"StopLiveLocationAlertToGroup",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите прекратить транслировать свою геопозицию в чат %1$s?"
    }
}, {
    "@type":"languagePackString",
    "key":"SecretWebPageInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Предпросмотр для ссылок создается на серверах Telegram. Мы не храним данные об отправленных Вами ссылках."
    }
}, {
    "@type":"languagePackString",
    "key":"FreeOfTotal",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Свободно %1$s из %2$s"}
}, {
    "@type":"languagePackString",
    "key":"SelectFile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите файл"}
}, {
    "@type":"languagePackString",
    "key":"UsernameInvalidShort",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Имя пользователя должно содержать не меньше 5 символов."
    }
}, {
    "@type":"languagePackString",
    "key":"OpenBackground",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПРОСМОТР ФОНА"}
}, {
    "@type":"languagePackString",
    "key":"TosUpdateDecline",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, в этом случае придется отказаться от использования Telegram. \n\nВ отличие от других сервисов, мы не используем Ваши данные для рекламы и иных коммерческих целей. На серверах хранится только та информация, которая позволяет Вам полноценно пользоваться Telegram. Вы можете регулировать доступ к своим данным в настройках приложения, в разделе Конфиденциальность и безопасность.\n\nЕсли Вы не готовы предоставить минимальные нужные данные , увы, мы не сможем обеспечить работу Telegram. Вы можете удалить аккаунт — или сперва осмотреться и удалить аккаунт позже, если останутся сомнения."
    }
}, {
    "@type":"languagePackString",
    "key":"AutoDownloadMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загружать автоматически"}
}, {
    "@type":"languagePackString",
    "key":"NetworkUsage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использование трафика"}
}, {
    "@type":"languagePackString",
    "key":"SendAsFile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Как файл"}
}, {
    "@type":"languagePackString",
    "key":"TypeScheduleMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отложенное сообщение"}
}, {
    "@type":"languagePackString",
    "key":"UseProxyTelegramInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки прокси MTProto."}
}, {
    "@type":"languagePackString",
    "key":"TerminateAllSessions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Завершить все другие сеансы"}
}, {
    "@type":"languagePackString",
    "key":"SendContactTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить контакт %1$s?"}
}, {
    "@type":"languagePackString",
    "key":"SendingPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"отправляет фото..."}
}, {
    "@type":"languagePackString",
    "key":"YourPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пароль"}
}, {
    "@type":"languagePackString",
    "key":"SendItems",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить %1$s"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsOffUntil",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Откл. до %1$s"}
}, {
    "@type":"languagePackString",
    "key":"PasswordReset",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пароль отключён"}
}, {
    "@type":"languagePackString",
    "key":"OpenFile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ОТКРЫТЬ ФАЙЛ"}
}, {
    "@type":"languagePackString",
    "key":"SettingsFaqSearchTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Часто задаваемые вопросы"}
}, {
    "@type":"languagePackString",
    "key":"YourEmailAlmostThereText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пожалуйста, проверьте почту (не забудьте о папке со спамом), чтобы закончить настройку двухэтапной аутентификации."
    }
}, {
    "@type":"languagePackString",
    "key":"UsernameInvalid",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Такое имя пользователя недоступно."}
}, {
    "@type":"languagePackString",
    "key":"SendLiveLocationInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обновляется в реальном времени"}
}, {
    "@type":"languagePackString",
    "key":"Stop",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Остановить"}
}, {
    "@type":"languagePackString",
    "key":"SendSticker",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ОТПРАВИТЬ СТИКЕР"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsNoSendPolls",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"без опросов"}
}, {
    "@type":"languagePackString",
    "key":"Green",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Зелёный"}
}, {
    "@type":"languagePackString",
    "key":"ThemeNotFound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Тема не найдена"}
}, {
    "@type":"languagePackString",
    "key":"SendWithoutCompression",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить без сжатия"}
}, {
    "@type":"languagePackString",
    "key":"November",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ноябрь"}
}, {
    "@type":"languagePackString",
    "key":"ResetAutomaticMediaDownloadAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить настройки"}
}, {
    "@type":"languagePackString",
    "key":"LinkNotFound",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, Вы не состоите в чате, где было опубликовано это сообщение."
    }
}, {
    "@type":"languagePackString",
    "key":"SendingVideo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправка видео..."}
}, {
    "@type":"languagePackString",
    "key":"VoipAudioRoutingBluetooth",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Bluetooth"}
}, {
    "@type":"languagePackString",
    "key":"SendByEnter",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправка по Enter"}
}, {
    "@type":"languagePackString",
    "key":"SentAppCode",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Мы отправили код в приложение **Telegram** на другом Вашем устройстве."
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedVoice",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) голосовое сообщение в группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"LinkAvailable",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s доступно."}
}, {
    "@type":"languagePackString",
    "key":"TodayAt",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"в"}
}, {
    "@type":"languagePackString",
    "key":"SlowmodeHours",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$d ч"}
}, {
    "@type":"languagePackString",
    "key":"ResetAutomaticMediaDownload",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить настройки автозагрузки"}
}, {
    "@type":"languagePackString",
    "key":"UserBioInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете добавить несколько строк о себе. Они будут видны всем, кто откроет Ваш профиль."
    }
}, {
    "@type":"languagePackString",
    "key":"SentAppCodeTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Проверьте сообщения в Telegram"}
}, {
    "@type":"languagePackString",
    "key":"ProfilePopupNotification",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всплывающие уведомления"}
}, {
    "@type":"languagePackString",
    "key":"KeepMediaForever",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда"}
}, {
    "@type":"languagePackString",
    "key":"SendMessageTo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить людей..."}
}, {
    "@type":"languagePackString",
    "key":"ResetChatBackgroundsInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Удалить все загруженные фоны и восстановить предустановленные фоны для всех тем."
    }
}, {
    "@type":"languagePackString",
    "key":"Set",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Установить"}
}, {
    "@type":"languagePackString",
    "key":"RemoveFromListText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите и удерживайте имя пользователя для удаления."}
}, {
    "@type":"languagePackString",
    "key":"PermissionOpenSettings",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"НАСТРОЙКИ"}
}, {
    "@type":"languagePackString",
    "key":"Loading",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузка..."}
}, {
    "@type":"languagePackString",
    "key":"SetAdminsTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Администраторы чата"}
}, {
    "@type":"languagePackString",
    "key":"SetBackground",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"УСТАНОВИТЬ ФОН"}
}, {
    "@type":"languagePackString",
    "key":"TypePublicGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Публичная"}
}, {
    "@type":"languagePackString",
    "key":"SupportStatus",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"поддержка"}
}, {
    "@type":"languagePackString",
    "key":"StopVerification",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы действительно хотите остановить подтверждение номера телефона?"
    }
}, {
    "@type":"languagePackString",
    "key":"RestoreEmailSent",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Мы отправили код восстановления на указанный вами адрес электронной почты:\n\n%1$s"
    }
}, {
    "@type":"languagePackString",
    "key":"VoipSettingsRingtone",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Рингтон"}
}, {
    "@type":"languagePackString",
    "key":"SetUrlInvalid",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Эта ссылка некорректна."}
}, {
    "@type":"languagePackString",
    "key":"SharedVoiceTab",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ГОЛОС"}
}, {
    "@type":"languagePackString",
    "key":"SetUrlPlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылка"}
}, {
    "@type":"languagePackString",
    "key":"SettingsHelp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Помощь"}
}, {
    "@type":"languagePackString",
    "key":"PhoneNumberFlood",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Извините, в последнее время Вы слишком часто удаляли и создавали свой аккаунт заново. Пожалуйста, подождите несколько дней перед повторной регистрацией."
    }
}, {
    "@type":"languagePackString",
    "key":"SettingsRecent",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"История поиска"}
}, {
    "@type":"languagePackString",
    "key":"PhoneMobile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Телефон"}
}, {
    "@type":"languagePackString",
    "key":"Shadows",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Тени"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_LO",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на лаосском языке"}
}, {
    "@type":"languagePackString",
    "key":"SendLiveLocationFor15m",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"15 минут"}
}, {
    "@type":"languagePackString",
    "key":"ShareComment",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Оставьте комментарий..."}
}, {
    "@type":"languagePackString",
    "key":"YourEmailSkipWarning",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Внимание"}
}, {
    "@type":"languagePackString",
    "key":"NotificationEditedGroupPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s изменил(а) фото группы на %2$s"}
}, {
    "@type":"languagePackString",
    "key":"StopPollAlertTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Остановить опрос?"}
}, {
    "@type":"languagePackString",
    "key":"ShareLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поделиться ссылкой"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsNoSendMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"без медиа"}
}, {
    "@type":"languagePackString",
    "key":"PeopleNearbyGps",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Включить"}
}, {
    "@type":"languagePackString",
    "key":"VoipUseLessData",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сократить трафик звонков"}
}, {
    "@type":"languagePackString",
    "key":"Delete",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить"}
}, {
    "@type":"languagePackString",
    "key":"PopupNotification",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всплывающие уведомления"}
}, {
    "@type":"languagePackString",
    "key":"WaitingForNetwork",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ожидание сети..."}
}, {
    "@type":"languagePackString",
    "key":"ShareMyPhone",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ПОКАЗАТЬ МОЙ НОМЕР ТЕЛЕФОНА"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsSendMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправка медиа"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsNoEmbedLinks",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"без предпросмотра ссылок"}
}, {
    "@type":"languagePackString",
    "key":"TelegramFaq",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вопросы о Telegram"}
}, {
    "@type":"languagePackString",
    "key":"SettingsNoRecent",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет недавних запросов"}
}, {
    "@type":"languagePackString",
    "key":"ShareYouLocationInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Это отправит боту Вашу геопозицию."}
}, {
    "@type":"languagePackString",
    "key":"ShareYouLocationInline",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Бот хочет получать Вашу геопозицию каждый раз, когда Вы отправляете ему запрос. Это может использоваться, чтобы показывать нужную выдачу в зависимости от того, где Вы."
    }
}, {
    "@type":"languagePackString",
    "key":"VoipInCallBrandingWithName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звонок %s через Telegram"}
}, {
    "@type":"languagePackString",
    "key":"UseProxyForCalls",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использовать прокси для звонков"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsBlock",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Заблокировать и удалить из группы"}
}, {
    "@type":"languagePackString",
    "key":"ShareYouLocationUnableManually",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбрать вручную"}
}, {
    "@type":"languagePackString",
    "key":"UsernameHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете выбрать публичное имя пользователя в **Telegram**. В этом случае другие люди смогут найти Вас по такому имени и связаться, не зная Вашего телефона.\n\nВы можете использовать символы **a–z**, **0–9** и подчёркивания. Минимальная длина – **5** символов."
    }
}, {
    "@type":"languagePackString",
    "key":"SelectContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбрать контакты"}
}, {
    "@type":"languagePackString",
    "key":"SharePhoneNumberWith",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показать %1$s мой номер телефона"}
}, {
    "@type":"languagePackString",
    "key":"SharedContentTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Общие материалы"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsDeleteAllException",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить все исключения"}
}, {
    "@type":"languagePackString",
    "key":"PinToTop",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закрепить"}
}, {
    "@type":"languagePackString",
    "key":"SharedMediaTab",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"МЕДИА"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewLine3Reply",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Лусио"}
}, {
    "@type":"languagePackString",
    "key":"SlowModeHint",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Включён медленный режим. Вы сможете написать через %1$s"
    }
}, {
    "@type":"languagePackString",
    "key":"ThemeBlue",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Моно"}
}, {
    "@type":"languagePackString",
    "key":"Enhance",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Улучшение"}
}, {
    "@type":"languagePackString",
    "key":"NotificationContactNewPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s обновил(а) фото профиля"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageContact2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале «%1$s» новый контакт %2$s"}
}, {
    "@type":"languagePackString",
    "key":"YesterdayAt",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"вчера в"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyPayments",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Платежи"}
}, {
    "@type":"languagePackString",
    "key":"RestoreEmailTroubleText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если Вы не можете восстановить доступ к Вашей электронной почте, то оставшиеся варианты — вспомнить пароль или сбросить аккаунт."
    }
}, {
    "@type":"languagePackString",
    "key":"SharingLiveLocationTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы транслируете геопозицию в %1$s"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialog2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Дух Соревнования"}
}, {
    "@type":"languagePackString",
    "key":"SharingYouAndOtherName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы и %1$s"}
}, {
    "@type":"languagePackString",
    "key":"Short",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Короткий"}
}, {
    "@type":"languagePackString",
    "key":"SmartNotificationsDisabled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Откл."}
}, {
    "@type":"languagePackString",
    "key":"SyncContactsDeleteInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В этом случае Ваши контакты будут удалены с серверов Telegram. Если включена опция «Синхронизировать контакты», контакты синхронизируются заново."
    }
}, {
    "@type":"languagePackString",
    "key":"ShowNotificationsForInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Отключите, если хотите получать только уведомления активного аккаунта."
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogRemovedYouChannelPhoto",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы удалили фото канала"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictions",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Разрешения"}
}, {
    "@type":"languagePackString",
    "key":"SlowmodeOff",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет"}
}, {
    "@type":"languagePackString",
    "key":"SetUrlAvailable",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылка %1$s доступна."}
}, {
    "@type":"languagePackString",
    "key":"IsRecordingRound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s записывает видеосообщение..."}
}, {
    "@type":"languagePackString",
    "key":"NoRecentGIFs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нет недавних GIF"}
}, {
    "@type":"languagePackString",
    "key":"LocationUpdatedJustNow",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"обновлена только что"}
}, {
    "@type":"languagePackString",
    "key":"ActionMigrateFromGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Эта группа стала супергруппой"}
}, {
    "@type":"languagePackString",
    "key":"SlowmodeInfoSelected",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Участники не смогут отправить больше одного сообщения за %1$s"
    }
}, {
    "@type":"languagePackString",
    "key":"PassportAddBank",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавить справку из банка"}
}, {
    "@type":"languagePackString",
    "key":"Underline",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подчёркнутый"}
}, {
    "@type":"languagePackString",
    "key":"SoftUserLimitAlert",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы сможете добавить в группу больше участников, если сделаете её супергруппой после создания."
    }
}, {
    "@type":"languagePackString",
    "key":"ManageGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Управление группой"}
}, {
    "@type":"languagePackString",
    "key":"Unarchive",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вернуть"}
}, {
    "@type":"languagePackString",
    "key":"HidAccount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Аккаунт скрыт пользователем"}
}, {
    "@type":"languagePackString",
    "key":"SelectAccount",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите аккаунт"}
}, {
    "@type":"languagePackString",
    "key":"SortBy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сортировать по"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyDeleteCloudDrafts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить черновики"}
}, {
    "@type":"languagePackString",
    "key":"Sound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звук"}
}, {
    "@type":"languagePackString",
    "key":"ResetStatistics",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сбросить статистику"}
}, {
    "@type":"languagePackString",
    "key":"PassportNativeHeader",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя"}
}, {
    "@type":"languagePackString",
    "key":"NoSharedFiles",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Поделитесь файлами и документами в этом чате, и они будут доступны вам с любого устройства."
    }
}, {
    "@type":"languagePackString",
    "key":"NotificationsServiceConnection",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Фоновое соединение"}
}, {
    "@type":"languagePackString",
    "key":"NearbyCreateGroupInfo2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Если группа не относится к выбранному месту, Вы рискуете потерять возможность создавать группы с геопозицией."
    }
}, {
    "@type":"languagePackString",
    "key":"Statistics",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Статистика"}
}, {
    "@type":"languagePackString",
    "key":"SendSelectedLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить выбранную геопозицию"}
}, {
    "@type":"languagePackString",
    "key":"VoipConnecting",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Соединение"}
}, {
    "@type":"languagePackString",
    "key":"PaymentPasswordEmailInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пожалуйста, добавьте настоящий email. Это единственный способ восстановить аккаунт в случае утраты пароля."
    }
}, {
    "@type":"languagePackString",
    "key":"PaintEdit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Изменить"}
}, {
    "@type":"languagePackString",
    "key":"UpgradeGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сделать супергруппой"}
}, {
    "@type":"languagePackString",
    "key":"YourPhone",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваш телефон"}
}, {
    "@type":"languagePackString",
    "key":"StopLiveLocation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Остановить трансляцию"}
}, {
    "@type":"languagePackString",
    "key":"ReplyToUser",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ответить %1$s"}
}, {
    "@type":"languagePackString",
    "key":"NobodyLikesSpam2",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы можете добавлять в группы только тех, кто добавил ваш номер в контакты."
    }
}, {
    "@type":"languagePackString",
    "key":"UsernameInvalidLong",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Имя пользователя не может быть длиннее 32 символов."}
}, {
    "@type":"languagePackString",
    "key":"SlowmodeSendErrorTooLong",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"К сожалению, текст слишком длинный для одного сообщения. \n\nВключён медленный режим. Вы не можете отправить больше одного сообщения за раз."
    }
}, {
    "@type":"languagePackString",
    "key":"StopPoll",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Остановить опрос"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedMusicChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) аудиофайл"}
}, {
    "@type":"languagePackString",
    "key":"ThemeDark",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Тёмная"}
}, {
    "@type":"languagePackString",
    "key":"ChannelMessageSticker",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"В канале %1$s новый стикер"}
}, {
    "@type":"languagePackString",
    "key":"ChannelNotifyMembersInfoOn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Участники получают уведомления о публикациях"}
}, {
    "@type":"languagePackString",
    "key":"SyncContactsDelete",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить импортированные контакты"}
}, {
    "@type":"languagePackString",
    "key":"SendDayYearAt",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"'Отправить' d MMM yyyy 'в' HH:mm"}
}, {
    "@type":"languagePackString",
    "key":"Undo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ОТМЕНА"}
}, {
    "@type":"languagePackString",
    "key":"SyncContactsInfoOn",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Контакты с этого устройства будут импортироваться в Ваш аккаунт."
    }
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsNoInviteUsers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"без приглашений"}
}, {
    "@type":"languagePackString",
    "key":"RevokeLinkAlertChannel",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы точно хотите сбросить ссылку **%1$s**?\n\nКанал \"**%2$s**\" станет частным."
    }
}, {
    "@type":"languagePackString",
    "key":"PhotoCaption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подпись к фото"}
}, {
    "@type":"languagePackString",
    "key":"Typing",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"печатает..."}
}, {
    "@type":"languagePackString",
    "key":"Reminders",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Напоминания"}
}, {
    "@type":"languagePackString",
    "key":"SyncContactsOff",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Контакты с этого устройства не будут импортироваться для этого аккаунта."
    }
}, {
    "@type":"languagePackString",
    "key":"KMetersAway2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s км отсюда"}
}, {
    "@type":"languagePackString",
    "key":"UnlockFingerprint",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Разблокировка отпечатком пальца"}
}, {
    "@type":"languagePackString",
    "key":"TapToDownload",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите для загрузки"}
}, {
    "@type":"languagePackString",
    "key":"NowInContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s теперь в списке Ваших контактов."}
}, {
    "@type":"languagePackString",
    "key":"TosAgeTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подтверждение возраста"}
}, {
    "@type":"languagePackString",
    "key":"UsernameInUse",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Такое имя пользователя занято."}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_VI",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на вьетнамском языке"}
}, {
    "@type":"languagePackString",
    "key":"Warmth",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Теплота"}
}, {
    "@type":"languagePackString",
    "key":"ShareFile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поделиться"}
}, {
    "@type":"languagePackString",
    "key":"TelegramPassport",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Telegram Passport"}
}, {
    "@type":"languagePackString",
    "key":"TelegramPassportCreatePassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ЗАДАТЬ ПАРОЛЬ"}
}, {
    "@type":"languagePackString",
    "key":"PasswordAsHintError",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подсказка не должна совпадать с паролем"}
}, {
    "@type":"languagePackString",
    "key":"ThemeNamePlaceholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Название"}
}, {
    "@type":"languagePackString",
    "key":"TelegramPassportDeleteAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите удалить свой Telegram Passport?"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsNewException",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Новое исключение"}
}, {
    "@type":"languagePackString",
    "key":"YouWereKicked",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вас исключили из группы"}
}, {
    "@type":"languagePackString",
    "key":"PassportPhoneUseOther",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Или введите другой номер телефона"}
}, {
    "@type":"languagePackString",
    "key":"SendContactToGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить контакт %1$s?"}
}, {
    "@type":"languagePackString",
    "key":"PassportMainPage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Главная страница"}
}, {
    "@type":"languagePackString",
    "key":"ResetMyAccountWarningText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Это действие необратимо.\n\nЕсли Вы сбросите свой аккаунт, все сообщения и чаты будут удалены."
    }
}, {
    "@type":"languagePackString",
    "key":"YourContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваши контакты в Telegram"}
}, {
    "@type":"languagePackString",
    "key":"ShowNotificationsFor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показывать уведомления"}
}, {
    "@type":"languagePackString",
    "key":"YourPasswordSuccessText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваш пароль для двухэтапной аутентификации включён."}
}, {
    "@type":"languagePackString",
    "key":"formatDateScheduleYear",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"d MMM yyyy"}
}, {
    "@type":"languagePackString",
    "key":"YourEmailCodeInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пожалуйста, введите код, который мы отправили на  %1$s, чтобы подтвердить свой адрес электронной почты."
    }
}, {
    "@type":"languagePackString",
    "key":"TotalDataUsage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всего"}
}, {
    "@type":"languagePackString",
    "key":"SyncContacts",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Синхронизировать контакты"}
}, {
    "@type":"languagePackString",
    "key":"Theme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Тема"}
}, {
    "@type":"languagePackString",
    "key":"SearchByColor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск по цвету"}
}, {
    "@type":"languagePackString",
    "key":"formatterBannedUntilThisYear12H",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"dd MMM, h:mm a"}
}, {
    "@type":"languagePackString",
    "key":"TermsOfServiceLogin",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Регистрируясь,\nВы принимаете *Пользовательское соглашение*."
    }
}, {
    "@type":"languagePackString",
    "key":"UserBioDetail",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Напишите немного о себе"}
}, {
    "@type":"languagePackString",
    "key":"VideoCaption",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Подпись к видео"}
}, {
    "@type":"languagePackString",
    "key":"UnknownError",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Неизвестная ошибка"}
}, {
    "@type":"languagePackString",
    "key":"UseLessDataAlways",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда"}
}, {
    "@type":"languagePackString",
    "key":"SearchForPeopleAndGroups",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поиск пользователей и групп..."}
}, {
    "@type":"languagePackString",
    "key":"SharedFilesTab",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ФАЙЛЫ"}
}, {
    "@type":"languagePackString",
    "key":"LanguageUnsupportedError",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выбранный язык не найден."}
}, {
    "@type":"languagePackString",
    "key":"UsernameEmpty",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Не задано"}
}, {
    "@type":"languagePackString",
    "key":"Notifications",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Уведомления"}
}, {
    "@type":"languagePackString",
    "key":"YourEmailAlmostThere",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Почти готово!"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialogMessage2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Кстати, обновил исходный код."}
}, {
    "@type":"languagePackString",
    "key":"EventLogRestrictedSendMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправлять медиа"}
}, {
    "@type":"languagePackString",
    "key":"ForgotPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Забыли пароль?"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewLine1",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Райнхардт, надо тебе подобрать музычки посвежее 🎶"}
}, {
    "@type":"languagePackString",
    "key":"QuickReplyCustom",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ваше сообщение..."}
}, {
    "@type":"languagePackString",
    "key":"MegaPrivateInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В частные группы можно вступить только по приглашению или пригласительной ссылке."
    }
}, {
    "@type":"languagePackString",
    "key":"Start",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Начать"}
}, {
    "@type":"languagePackString",
    "key":"TooManyTries",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Слишком много попыток.\nПожалуйста, повторите через %1$s."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportUseLatinOnly",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пожалуйста, используйте только латиницу."}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsInviteUsers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добавление участников"}
}, {
    "@type":"languagePackString",
    "key":"UserBio",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"О себе"}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedStickerChannel",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) стикер"}
}, {
    "@type":"languagePackString",
    "key":"TimeToEdit",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s"}
}, {
    "@type":"languagePackString",
    "key":"TurnPasswordOffPassport",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Внимание! Все данные, сохранённые в Вашем Telegram Passport, будут потеряны!"
    }
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_EL",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на греческом языке"}
}, {
    "@type":"languagePackString",
    "key":"PassportCorrectErrors",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите, чтобы внести исправления."}
}, {
    "@type":"languagePackString",
    "key":"VoipQuickRepliesExplain",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Напишите здесь подходящий текст, чтобы быстро отправлять его в ответ на звонок через Telegram."
    }
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsNoChangeInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"без изменений профиля"}
}, {
    "@type":"languagePackString",
    "key":"SmartNotifications",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Умные уведомления"}
}, {
    "@type":"languagePackString",
    "key":"NotificationMessageInvoice",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s прислал(а) счёт на %2$s"}
}, {
    "@type":"languagePackString",
    "key":"PinMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Закрепить"}
}, {
    "@type":"languagePackString",
    "key":"SetUrlInvalidStartNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ссылка не может начинаться с цифры."}
}, {
    "@type":"languagePackString",
    "key":"TwoStepVerification",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Двухэтапная аутентификация"}
}, {
    "@type":"languagePackString",
    "key":"PaymentShippingAddress2Placeholder",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Адрес 2 (улица)"}
}, {
    "@type":"languagePackString",
    "key":"Page4Title",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Безопасный"}
}, {
    "@type":"languagePackString",
    "key":"StickersName",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикеры и маски"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsCustomize",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настроить"}
}, {
    "@type":"languagePackString",
    "key":"PassportSDK_DownloadTelegram",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы авторизоваться в **%s**, требуется Telegram. Хотите установить его сейчас?"
    }
}, {
    "@type":"languagePackString",
    "key":"ShareYouPhoneNumberTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Поделиться номером телефона?"}
}, {
    "@type":"languagePackString",
    "key":"DiscussionChannelHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Подписчики смогут перейти в группу, нажав на кнопку «Обсудить»."
    }
}, {
    "@type":"languagePackString",
    "key":"ErrorSendRestrictedMedia",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Администраторы группы запретили Вам отправлять медиа."}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsRead",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Просмотр сообщений"}
}, {
    "@type":"languagePackString",
    "key":"NearbyCreateGroup",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Локальная группа"}
}, {
    "@type":"languagePackString",
    "key":"UnbanText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Нажмите и удерживайте имя пользователя для разблокировки."
    }
}, {
    "@type":"languagePackString",
    "key":"ReportChatSpam",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Спам"}
}, {
    "@type":"languagePackString",
    "key":"PleaseEnterFirstPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Введите пароль"}
}, {
    "@type":"languagePackString",
    "key":"UnblockText",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Нажмите и удерживайте имя пользователя для разблокировки"
    }
}, {
    "@type":"languagePackString",
    "key":"PrivacyForwardsMessageLine",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Чем ворон похож на письменный стол?"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyProfilePhotoTitle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Кто видит мою фотографию?"}
}, {
    "@type":"languagePackString",
    "key":"PaintDuplicate",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Дублировать"}
}, {
    "@type":"languagePackString",
    "key":"LinkInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Любой пользователь Telegram сможет вступить в Вашу группу, перейдя по этой ссылке."
    }
}, {
    "@type":"languagePackString",
    "key":"NoChats",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Добро пожаловать"}
}, {
    "@type":"languagePackString",
    "key":"download_telegram_text",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы авторизоваться в **%s**, требуется Telegram. Хотите установить его сейчас?"
    }
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewSongPerformer",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"David Hasselhoff"}
}, {
    "@type":"languagePackString",
    "key":"Teal",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Цвет морской волны"}
}, {
    "@type":"languagePackString",
    "key":"UpdateApp",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обновить приложение"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialogMessage6",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Салют!"}
}, {
    "@type":"languagePackString",
    "key":"ResetStatisticsAlert",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Хотите сбросить статистику использования?"}
}, {
    "@type":"languagePackString",
    "key":"AutoplayGifs",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Автозапуск GIF"}
}, {
    "@type":"languagePackString",
    "key":"NoEmojiFound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Эмодзи не найдены"}
}, {
    "@type":"languagePackString",
    "key":"AttachLiveLocationIsSharing",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s доступна %2$s"}
}, {
    "@type":"languagePackString",
    "key":"ThemeNotSupported",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Эта цветовая тема пока не поддерживается на Вашем устройстве."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_IT",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на итальянском языке"}
}, {
    "@type":"languagePackString",
    "key":"UpdateNow",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"ОБНОВИТЬ СЕЙЧАС"}
}, {
    "@type":"languagePackString",
    "key":"PassportRequestedInformation",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Необходимая информация"}
}, {
    "@type":"languagePackString",
    "key":"Save",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сохранить"}
}, {
    "@type":"languagePackString",
    "key":"VoipOfflineAirplane",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы в режиме полёта. Чтобы позвонить, пожалуйста, отключите его или подключитесь к Wi-Fi."
    }
}, {
    "@type":"languagePackString",
    "key":"PaymentWarning",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Внимание"}
}, {
    "@type":"languagePackString",
    "key":"SendDayAt",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"'Отправить' d MMM 'в' HH:mm"}
}, {
    "@type":"languagePackString",
    "key":"WhenConnectedOnWiFi",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Через сети Wi-Fi"}
}, {
    "@type":"languagePackString",
    "key":"SendingFile",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"отправляет файл..."}
}, {
    "@type":"languagePackString",
    "key":"WhoCanCallMeInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы можете выбрать, кому разрешаете позвонить Вам."}
}, {
    "@type":"languagePackString",
    "key":"PaymentCardNumber",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Номер карты"}
}, {
    "@type":"languagePackString",
    "key":"PaymentEmailToProvider",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В качестве платёжной информации %1$s получит данные о Вашей электронной почте."
    }
}, {
    "@type":"languagePackString",
    "key":"NeverShareWith",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Всегда скрывать"}
}, {
    "@type":"languagePackString",
    "key":"PassportDiscardChanges",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Вы точно хотите отменить всю введённую информацию?"}
}, {
    "@type":"languagePackString",
    "key":"NoExceptions",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В этом разделе будут показаны все чаты с особыми настройками уведомлений.\n\nЧтобы задать свои настройки для чата, откройте профиль чата > Уведомления."
    }
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsNoSend",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"без отправки"}
}, {
    "@type":"languagePackString",
    "key":"PassportPolicy",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы принимаете *Политику конфиденциальности %1$s* и разрешаете их боту @%2$s писать вам сообщения."
    }
}, {
    "@type":"languagePackString",
    "key":"PaymentCardInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Платёжная информация"}
}, {
    "@type":"languagePackString",
    "key":"SendLiveLocationFor1h",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"1 час"}
}, {
    "@type":"languagePackString",
    "key":"VoipRequesting",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Запрос"}
}, {
    "@type":"languagePackString",
    "key":"PassportSDK_LogInWithTelegram",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Войти через Telegram"}
}, {
    "@type":"languagePackString",
    "key":"UseProxyInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки SOCKS5-прокси"}
}, {
    "@type":"languagePackString",
    "key":"WhoCanAddMembersAdmins",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Только администраторы"}
}, {
    "@type":"languagePackString",
    "key":"ResetNotificationsText",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Настройки уведомлений сброшены на стандартные"}
}, {
    "@type":"languagePackString",
    "key":"EventLogPromotedEditMessages",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Редактирование сообщений"}
}, {
    "@type":"languagePackString",
    "key":"NoPlayerInstalled",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"У вас нет видеоплеера, пожалуйста, установите его для продолжения"
    }
}, {
    "@type":"languagePackString",
    "key":"SearchFrom",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"от:"}
}, {
    "@type":"languagePackString",
    "key":"VibrationDisabled",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Откл."}
}, {
    "@type":"languagePackString",
    "key":"InvalidPhoneNumber",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Некорректный номер телефона. Пожалуйста, проверьте номер и попробуйте ещё раз."
    }
}, {
    "@type":"languagePackString",
    "key":"UserRestricted",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"К сожалению, Вам запрещено это действие."}
}, {
    "@type":"languagePackString",
    "key":"NotificationActionPinnedRound",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s закрепил(а) видеосообщение в группе %2$s"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialog4",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Квентин"}
}, {
    "@type":"languagePackString",
    "key":"TerminateWebSessionInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите на сайт, чтобы отключить его."}
}, {
    "@type":"languagePackString",
    "key":"TerminateSessionInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Нажмите на сеанс, чтобы завершить его."}
}, {
    "@type":"languagePackString",
    "key":"CustomShareInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Этим пользователям будет разрешено или запрещено добавлять Вас в группы и каналы независимо от настроек выше."
    }
}, {
    "@type":"languagePackString",
    "key":"PermissionStorage",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Telegram требуется доступ к хранилищу, чтобы Вы могли отправлять и сохранять фото, видео, музыку и другие медиа."
    }
}, {
    "@type":"languagePackString",
    "key":"January",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Январь"}
}, {
    "@type":"languagePackString",
    "key":"PaintDelete",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить"}
}, {
    "@type":"languagePackString",
    "key":"_tg_open_google_play",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Открыть Google Play"}
}, {
    "@type":"languagePackString",
    "key":"DirectShareInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Показывать недавние чаты в меню"}
}, {
    "@type":"languagePackString",
    "key":"PassportTwoDocuments",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"%1$s или %2$s"}
}, {
    "@type":"languagePackString",
    "key":"UseDifferentTheme",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Использовать другую тему"}
}, {
    "@type":"languagePackString",
    "key":"LocalDatabaseClear",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Очистить кэш текстовых сообщений?"}
}, {
    "@type":"languagePackString",
    "key":"Streaming",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стриминг"}
}, {
    "@type":"languagePackString",
    "key":"NoChatsHelp",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы написать сообщение, нажмите на значок карандаша в правом нижнем углу."
    }
}, {
    "@type":"languagePackString",
    "key":"UsernameHelpLink",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"По этой ссылке открывается диалог с Вами:\n%1$s"}
}, {
    "@type":"languagePackString",
    "key":"PassportPhoneUseSameInfo",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Используйте тот же номер телефона, что и в Telegram."}
}, {
    "@type":"languagePackString",
    "key":"formatterBannedUntil12H",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"dd MMM yyyy, h:mm a"}
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_AZ",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на азербайджанском языке"}
}, {
    "@type":"languagePackString",
    "key":"YourLocatedChannelsTooMuch",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Вы создали максимальное число групп с геопозицией. Пожалуйста, сперва удалите одну из уже созданных групп."
    }
}, {
    "@type":"languagePackString",
    "key":"Send",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправить"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsLed",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Светодиод"}
}, {
    "@type":"languagePackString",
    "key":"VideoMessagesAutodownload",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Видеосообщения"}
}, {
    "@type":"languagePackString",
    "key":"VoipEndCall",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Завершить звонок"}
}, {
    "@type":"languagePackString",
    "key":"PassportAddTranslationBillInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Загрузите сканы заверенного перевода для своего коммунального счёта."
    }
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsSendStickers",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправка стикеров и GIF"}
}, {
    "@type":"languagePackString",
    "key":"PrivacyPhoneInfo3",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Пользователи, которые сохранили Ваш номер в телефонную книгу, будут видеть его в Telegram, только если Вы добавили их в контакты."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportLanguage_KO",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"на корейском языке"}
}, {
    "@type":"languagePackString",
    "key":"SendingGif",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Отправка GIF..."}
}, {
    "@type":"languagePackString",
    "key":"PrivacyPolicyUrl",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"https://telegram.org/privacy"}
}, {
    "@type":"languagePackString",
    "key":"VoipAudioRoutingEarpiece",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Наушники"}
}, {
    "@type":"languagePackString",
    "key":"VoipNeedMicPermission",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Telegram требуется доступ к микрофону, чтобы вы могли пользоваться звонками"
    }
}, {
    "@type":"languagePackString",
    "key":"EventLogEmpty",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"**Действий пока нет**\n\nУчастники и администраторы группы\nне выполняли никаких служебных действий\nза последние 48 часов."
    }
}, {
    "@type":"languagePackString",
    "key":"PassportScanPassportInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Чтобы автоматически заполнить личные данные, сканируйте свой паспорт или удостоверение личности с областью для считывания компьютером."
    }
}, {
    "@type":"languagePackString",
    "key":"QuickReplyDefault2",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Перезвоню, как только смогу."}
}, {
    "@type":"languagePackString",
    "key":"Updating",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Обновление..."}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsApplyChanges",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Применить изменения?"}
}, {
    "@type":"languagePackString",
    "key":"SyncContactsInfoOff",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"Включите, чтобы добавлять контакты с этого устройства к Вашему аккаунту."
    }
}, {
    "@type":"languagePackString",
    "key":"Grain",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Зернистость"}
}, {
    "@type":"languagePackString",
    "key":"NotificationsForPrivateChats",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Уведомления из личных чатов"}
}, {
    "@type":"languagePackString",
    "key":"Shuffle",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Случайный порядок"}
}, {
    "@type":"languagePackString",
    "key":"InAppSounds",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Звук"}
}, {
    "@type":"languagePackString",
    "key":"ThemePreviewDialog3",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Майк"}
}, {
    "@type":"languagePackString",
    "key":"SelectColor",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Выберите цвет"}
}, {
    "@type":"languagePackString",
    "key":"March",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Март"}
}, {
    "@type":"languagePackString",
    "key":"WhenUsingMobileData",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Через мобильную сеть"}
}, {
    "@type":"languagePackString",
    "key":"EventLog",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Недавние действия"}
}, {
    "@type":"languagePackString",
    "key":"TextSize",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Размер текста сообщений"}
}, {
    "@type":"languagePackString",
    "key":"FontSizePreviewReply",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Доброе утро! 👋"}
}, {
    "@type":"languagePackString",
    "key":"ReportSpamAndLeave",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"СООБЩИТЬ О СПАМЕ И ВЫЙТИ"}
}, {
    "@type":"languagePackString",
    "key":"PassportIdentityDocument",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удостоверение личности"}
}, {
    "@type":"languagePackString",
    "key":"WillUnmuteIn",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Через %1$s"}
}, {
    "@type":"languagePackString",
    "key":"StickersHide",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Архивировать"}
}, {
    "@type":"languagePackString",
    "key":"YouHaveNewMessage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"У вас новое сообщение"}
}, {
    "@type":"languagePackString",
    "key":"PassportPersonalUploadInfo",
    "value":{
        "@type":"languagePackStringValueOrdinary",
        "value":"В документе должны быть Ваша фотография, имя и фамилия, дата рождения, номер документа, страна получения и срок действия."
    }
}, {
    "@type":"languagePackString",
    "key":"InstantViewReference",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Сноска"}
}, {
    "@type":"languagePackString",
    "key":"UseProxyPassword",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Пароль"}
}, {
    "@type":"languagePackString",
    "key":"White",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Белый"}
}, {
    "@type":"languagePackString",
    "key":"UserRestrictionsBy",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Ограничил(а): %1$s"}
}, {
    "@type":"languagePackString",
    "key":"PassportDeleteDocumentPersonal",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Удалить личную информацию"}
}, {
    "@type":"languagePackString",
    "key":"UploadImage",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Загрузить изображение"}
}, {
    "@type":"languagePackString",
    "key":"StickersRemoved",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Стикеры удалены"}
}, {
    "@type":"languagePackString",
    "key":"formatterStats24H",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"dd MMM yyyy, HH:mm"}
}, {
    "@type":"languagePackString",
    "key":"TerminateSessionQuestion",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Завершить этот сеанс?"}
}, {
    "@type":"languagePackString",
    "key":"RateCallEcho",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"Слышно свой голос"}
}, {
    "@type":"languagePackString",
    "key":"formatterMonthYear",
    "value":{"@type":"languagePackStringValueOrdinary", "value":"MMMM yyyy"}
}, {
    "@type":"languagePackString",
    "key":"Exception",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d исключение",
        "two_value":"",
        "few_value":"%1$d исключения",
        "many_value":"%1$d исключений",
        "other_value":"%1$d исключений"
    }
}, {
    "@type":"languagePackString",
    "key":"Files",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d файл",
        "two_value":"",
        "few_value":"%1$d файла",
        "many_value":"%1$d файлов",
        "other_value":"%1$d файлов"
    }
}, {
    "@type":"languagePackString",
    "key":"Minutes",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d мин.",
        "two_value":"",
        "few_value":"%1$d мин.",
        "many_value":"%1$d мин.",
        "other_value":"%1$d мин."
    }
}, {
    "@type":"languagePackString",
    "key":"Chats",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d чат",
        "two_value":"",
        "few_value":"%1$d чата",
        "many_value":"%1$d чатов",
        "other_value":"%1$d чатов"
    }
}, {
    "@type":"languagePackString",
    "key":"Stickers",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d стикер",
        "two_value":"",
        "few_value":"%1$d стикера",
        "many_value":"%1$d стикеров",
        "other_value":"%1$d стикеров"
    }
}, {
    "@type":"languagePackString",
    "key":"Users",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пользователь",
        "two_value":"",
        "few_value":"%1$d пользователя",
        "many_value":"%1$d пользователей",
        "other_value":"%1$d пользователей"
    }
}, {
    "@type":"languagePackString",
    "key":"ThemeInstallCount",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"Тема установлена у %1$d человека",
        "two_value":"",
        "few_value":"Тема установлена у %1$d человек",
        "many_value":"Тема установлена у %1$d человек",
        "other_value":"Тема установлена у %1$d человек"
    }
}, {
    "@type":"languagePackString",
    "key":"ForwardedVideo",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пересланное видео",
        "two_value":"",
        "few_value":"%1$d пересланных видео",
        "many_value":"%1$d пересланных видео",
        "other_value":"%1$d пересланных видео"
    }
}, {
    "@type":"languagePackString",
    "key":"ChatsSelectedClear",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d чат",
        "two_value":"",
        "few_value":"%1$d чата",
        "many_value":"%1$d чатов",
        "other_value":"%1$d чатов"
    }
}, {
    "@type":"languagePackString",
    "key":"BlockedUsersCount",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d заблокированный пользователь",
        "two_value":"",
        "few_value":"%1$d заблокированных пользователя",
        "many_value":"%1$d заблокированных пользователей",
        "other_value":"%1$d заблокированных пользователей"
    }
}, {
    "@type":"languagePackString",
    "key":"Points",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d",
        "two_value":"",
        "few_value":"%1$d",
        "many_value":"%1$d",
        "other_value":"%1$d"
    }
}, {
    "@type":"languagePackString",
    "key":"Seconds",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d сек.",
        "two_value":"",
        "few_value":"%1$d сек.",
        "many_value":"%1$d сек.",
        "other_value":"%1$d сек."
    }
}, {
    "@type":"languagePackString",
    "key":"PhotosSelected",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"выбрана %1$d фотография",
        "two_value":"",
        "few_value":"выбраны %1$d фотографии",
        "many_value":"выбрано %1$d фотографий",
        "other_value":"выбрано %1$d фотографий"
    }
}, {
    "@type":"languagePackString",
    "key":"Days",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d день",
        "two_value":"",
        "few_value":"%1$d дня",
        "many_value":"%1$d дней",
        "other_value":"%1$d дней"
    }
}, {
    "@type":"languagePackString",
    "key":"UpdatedMinutes",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"обновлена %1$d минуту назад",
        "two_value":"",
        "few_value":"обновлена %1$d минуты назад",
        "many_value":"обновлена %1$d минут назад",
        "other_value":"обновлена %1$d минут назад"
    }
}, {
    "@type":"languagePackString",
    "key":"MediaSelected",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"выбран %1$d медиафайл",
        "two_value":"",
        "few_value":"выбраны %1$d медиафайла",
        "many_value":"выбрано %1$d медиафайлов",
        "other_value":"выбрано %1$d медиафайлов"
    }
}, {
    "@type":"languagePackString",
    "key":"DaysBold",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"**%1$d** д.",
        "two_value":"",
        "few_value":"**%1$d** д.",
        "many_value":"**%1$d** д.",
        "other_value":"**%1$d** д."
    }
}, {
    "@type":"languagePackString",
    "key":"ForwardedFile",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пересылаемый файл",
        "two_value":"",
        "few_value":"%1$d пересылаемых файла",
        "many_value":"%1$d пересылаемых файлов",
        "other_value":"%1$d пересылаемых файлов"
    }
}, {
    "@type":"languagePackString",
    "key":"ChatsSelected",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d чат",
        "two_value":"",
        "few_value":"%1$d чата",
        "many_value":"%1$d чатов",
        "other_value":"%1$d чатов"
    }
}, {
    "@type":"languagePackString",
    "key":"Channels",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d канал",
        "two_value":"",
        "few_value":"%1$d канала",
        "many_value":"%1$d каналов",
        "other_value":"%1$d каналов"
    }
}, {
    "@type":"languagePackString",
    "key":"ChatsSelectedClearCache",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d чата",
        "two_value":"",
        "few_value":"%1$d чатов",
        "many_value":"%1$d чатов",
        "other_value":"%1$d чатов"
    }
}, {
    "@type":"languagePackString",
    "key":"MinutesBold",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"**%1$d** минута",
        "two_value":"",
        "few_value":"**%1$d** минуты",
        "many_value":"**%1$d** минут",
        "other_value":"**%1$d** минут"
    }
}, {
    "@type":"languagePackString",
    "key":"OnlineCount",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d в сети",
        "two_value":"",
        "few_value":"%1$d в сети",
        "many_value":"%1$d в сети",
        "other_value":"%1$d в сети"
    }
}, {
    "@type":"languagePackString",
    "key":"HoursBold",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"**%1$d** ч.",
        "two_value":"",
        "few_value":"**%1$d** ч.",
        "many_value":"**%1$d** ч.",
        "other_value":"**%1$d** ч."
    }
}, {
    "@type":"languagePackString",
    "key":"Subscribers",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d подписчик",
        "two_value":"",
        "few_value":"%1$d подписчика",
        "many_value":"%1$d подписчиков",
        "other_value":"%1$d подписчиков"
    }
}, {
    "@type":"languagePackString",
    "key":"VideosSelected",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"выбрано %1$d видео",
        "two_value":"",
        "few_value":"выбрано %1$d видео",
        "many_value":"выбрано %1$d видео",
        "other_value":"выбрано %1$d видео"
    }
}, {
    "@type":"languagePackString",
    "key":"ForwardedAudio",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пересланное голосовое сообщение",
        "two_value":"",
        "few_value":"%1$d пересланных голосовых сообщения",
        "many_value":"%1$d пересланных голосовых сообщений",
        "other_value":"%1$d пересланных голосовых сообщений"
    }
}, {
    "@type":"languagePackString",
    "key":"MasksCount",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d маска",
        "two_value":"",
        "few_value":"%1$d маски",
        "many_value":"%1$d масок",
        "other_value":"%1$d масок"
    }
}, {
    "@type":"languagePackString",
    "key":"Years",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d год",
        "two_value":"",
        "few_value":"%1$d года",
        "many_value":"%1$d лет",
        "other_value":"%1$d лет"
    }
}, {
    "@type":"languagePackString",
    "key":"ForwardedContact",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пересылаемый контакт",
        "two_value":"",
        "few_value":"%1$d пересылаемых контакта",
        "many_value":"%1$d пересылаемых контактов",
        "other_value":"%1$d пересылаемых контактов"
    }
}, {
    "@type":"languagePackString",
    "key":"ForwardedSticker",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пересылаемый стикер",
        "two_value":"",
        "few_value":"%1$d пересылаемых стикера",
        "many_value":"%1$d пересылаемых стикеров",
        "other_value":"%1$d пересылаемых стикеров"
    }
}, {
    "@type":"languagePackString",
    "key":"Groups",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d группа",
        "two_value":"",
        "few_value":"%1$d группы",
        "many_value":"%1$d групп",
        "other_value":"%1$d групп"
    }
}, {
    "@type":"languagePackString",
    "key":"Hours",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d час",
        "two_value":"",
        "few_value":"%1$d часа",
        "many_value":"%1$d часов",
        "other_value":"%1$d часов"
    }
}, {
    "@type":"languagePackString",
    "key":"TelegramContacts",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d контакт в Telegram",
        "two_value":"",
        "few_value":"%1$d контакта в Telegram",
        "many_value":"%1$d контактов в Telegram",
        "other_value":"%1$d контактов в Telegram"
    }
}, {
    "@type":"languagePackString",
    "key":"messages",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d сообщение",
        "two_value":"",
        "few_value":"%1$d сообщения",
        "many_value":"%1$d сообщений",
        "other_value":"%1$d сообщений"
    }
}, {
    "@type":"languagePackString",
    "key":"FromChats",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"из %1$d чата",
        "two_value":"",
        "few_value":"из %1$d чатов",
        "many_value":"из %1$d чатов",
        "other_value":"из %1$d чатов"
    }
}, {
    "@type":"languagePackString",
    "key":"ForwardedLocation",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пересланная геопозиция",
        "two_value":"",
        "few_value":"%1$d пересланных геопозиции",
        "many_value":"%1$d пересланных геопозиций",
        "other_value":"%1$d пересланных геопозиций"
    }
}, {
    "@type":"languagePackString",
    "key":"ChatsException",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d чат",
        "two_value":"",
        "few_value":"%1$d чата",
        "many_value":"%1$d чатов",
        "other_value":"%1$d чатов"
    }
}, {
    "@type":"languagePackString",
    "key":"ForwardedMessageCount",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пересланное сообщение",
        "two_value":"",
        "few_value":"%1$d пересланных сообщения",
        "many_value":"%1$d пересланных сообщений",
        "other_value":"%1$d пересланных сообщений"
    }
}, {
    "@type":"languagePackString",
    "key":"InviteTextNum",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"Привет, я использую Telegram для переписки – как и ещё %1$d наш контакт. Присоединяйся! Скачать можно здесь: %@",
        "two_value":"",
        "few_value":"Привет, я использую Telegram для переписки – как и ещё %1$d наших контакта. Присоединяйся! Скачать можно здесь: %@",
        "many_value":"Привет, я использую Telegram для переписки – как и ещё %1$d наших контактов. Присоединяйся! Скачать можно здесь: %@",
        "other_value":"Привет, я использую Telegram для переписки – как и ещё %1$d наших контактов. Присоединяйся! Скачать можно здесь: %@"
    }
}, {
    "@type":"languagePackString",
    "key":"Meters",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d метра",
        "two_value":"",
        "few_value":"%1$d метров",
        "many_value":"%1$d метров",
        "other_value":"%1$d метров"
    }
}, {
    "@type":"languagePackString",
    "key":"AndMoreTyping",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"и ещё %1$d печатают",
        "two_value":"",
        "few_value":"и ещё %1$d печатают",
        "many_value":"и ещё %1$d печатают",
        "other_value":"и ещё %1$d печатают"
    }
}, {
    "@type":"languagePackString",
    "key":"NewMessages",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d новое сообщение",
        "two_value":"",
        "few_value":"%1$d новых сообщения",
        "many_value":"%1$d новых сообщений",
        "other_value":"%1$d новых сообщений"
    }
}, {
    "@type":"languagePackString",
    "key":"AndMoreTypingGroup",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$s и ещё %2$d печатает",
        "two_value":"",
        "few_value":"%1$s и ещё %2$d печатают",
        "many_value":"%1$s и ещё %2$d печатают",
        "other_value":"%1$s и ещё %2$d печатают"
    }
}, {
    "@type":"languagePackString",
    "key":"SecondsBold",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"**%1$d** сек.",
        "two_value":"",
        "few_value":"**%1$d** сек.",
        "many_value":"**%1$d** сек.",
        "other_value":"**%1$d** сек."
    }
}, {
    "@type":"languagePackString",
    "key":"Weeks",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d нед.",
        "two_value":"",
        "few_value":"%1$d нед.",
        "many_value":"%1$d нед.",
        "other_value":"%1$d нед."
    }
}, {
    "@type":"languagePackString",
    "key":"Photos",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d фото",
        "two_value":"",
        "few_value":"%1$d фото",
        "many_value":"%1$d фото",
        "other_value":"%1$d фото"
    }
}, {
    "@type":"languagePackString",
    "key":"ForwardedMusic",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пересылаемый аудиофайл",
        "two_value":"",
        "few_value":"%1$d пересылаемых аудиофайла",
        "many_value":"%1$d пересылаемых аудиофайлов",
        "other_value":"%1$d пересылаемых аудиофайлов"
    }
}, {
    "@type":"languagePackString",
    "key":"Recipient",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d получатель",
        "two_value":"",
        "few_value":"%1$d получателя",
        "many_value":"%1$d получателей",
        "other_value":"%1$d получателей"
    }
}, {
    "@type":"languagePackString",
    "key":"ForwardedPoll",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пересылаемый опрос",
        "two_value":"",
        "few_value":"%1$d пересылаемых опроса",
        "many_value":"%1$d пересылаемых опросов",
        "other_value":"%1$d пересылаемых опросов"
    }
}, {
    "@type":"languagePackString",
    "key":"Media",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d медиафайл",
        "two_value":"",
        "few_value":"%1$d медиафайла",
        "many_value":"%1$d медиафайлов",
        "other_value":"%1$d медиафайлов"
    }
}, {
    "@type":"languagePackString",
    "key":"AndOther",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"и ещё %1$d",
        "two_value":"",
        "few_value":"и ещё %1$d",
        "many_value":"и ещё %1$d",
        "other_value":"и ещё %1$d"
    }
}, {
    "@type":"languagePackString",
    "key":"Videos",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d видео",
        "two_value":"",
        "few_value":"%1$d видео",
        "many_value":"%1$d видео",
        "other_value":"%1$d видео"
    }
}, {
    "@type":"languagePackString",
    "key":"Members",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d участник",
        "two_value":"",
        "few_value":"%1$d участника",
        "many_value":"%1$d участников",
        "other_value":"%1$d участников"
    }
}, {
    "@type":"languagePackString",
    "key":"RemovedUser",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пользователь в чёрном списке",
        "two_value":"",
        "few_value":"%1$d пользователя в чёрном списке",
        "many_value":"%1$d пользователей в чёрном списке",
        "other_value":"%1$d пользователей в чёрном списке"
    }
}, {
    "@type":"languagePackString",
    "key":"UsersPluralized",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%@ пользователь",
        "two_value":"",
        "few_value":"%@ пользователя",
        "many_value":"%@ пользователей",
        "other_value":"%@ пользователей"
    }
}, {
    "@type":"languagePackString",
    "key":"ForwardedPhoto",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пересланное фото",
        "two_value":"",
        "few_value":"%1$d пересланных фото",
        "many_value":"%1$d пересланных фото",
        "other_value":"%1$d пересланных фото"
    }
}, {
    "@type":"languagePackString",
    "key":"Months",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d месяц",
        "two_value":"",
        "few_value":"%1$d месяца",
        "many_value":"%1$d месяцев",
        "other_value":"%1$d месяцев"
    }
}, {
    "@type":"languagePackString",
    "key":"Option",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"ещё %1$d вариант ответа",
        "two_value":"",
        "few_value":"ещё %1$d варианта ответа",
        "many_value":"ещё %1$d вариантов ответа",
        "other_value":"ещё %1$d вариантов ответа"
    }
}, {
    "@type":"languagePackString",
    "key":"ForwardedRound",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d пересланное видеосообщение",
        "two_value":"",
        "few_value":"%1$d пересланных видеосообщения",
        "many_value":"%1$d пересланных видеосообщений",
        "other_value":"%1$d пересланных видеосообщений"
    }
}, {
    "@type":"languagePackString",
    "key":"Times",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d раз",
        "two_value":"",
        "few_value":"%1$d раза",
        "many_value":"%1$d раз",
        "other_value":"%1$d раз"
    }
}, {
    "@type":"languagePackString",
    "key":"Vote",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d голос",
        "two_value":"",
        "few_value":"%1$d голоса",
        "many_value":"%1$d голосов",
        "other_value":"%1$d голосов"
    }
}, {
    "@type":"languagePackString",
    "key":"items",
    "value":{
        "@type":"languagePackStringValuePluralized",
        "zero_value":"",
        "one_value":"%1$d элемент",
        "two_value":"",
        "few_value":"%1$d элемента",
        "many_value":"%1$d элементов",
        "other_value":"%1$d элементов"
    }
}];