import React from 'react';
import Avatar from "../../Components/Avatar/Avatar";
import AvatarX from "../../Components/Avatar/AvatarX";

interface IProps {

}

const Test6: React.FC<IProps> = ({}) => {
    return (
        <div className='Chat'>
            <div className='Chat__wrap'>
                <div className='Chat__item'>
                    <div className='-ava'>
                        <AvatarX url={null} txt='ЮЛ' type={33}/>
                    </div>
                    <div className='-msg'>
                        <div className='-wrap'>
                            <div className='-ten'/>
                            <div className='-ugol'/>
                            <div className='-txt'>
                                <div className='-nick'>Dmitry Alekseev</div>
                                <div className='-t'>Мама мия!</div>
                            </div>
                            <div className='-date'>20:06</div>
                        </div>
                    </div>
                </div>
                <div className='Chat__item'>
                    <div className='-ava'>
                        <AvatarX url={null} txt='Dmitry Alekseev' type={33}/>
                    </div>
                    <div className='-msg'>
                        <div className='-wrap'>
                            <div className='-ten'/>
                            <div className='-ugol'/>
                            <div className='-txt'>
                                <div className='-nick'>Dmitry Alekseev</div>
                                <div className='-t'>Привет как твои дела!?</div>
                            </div>
                            <div className='-date'>20:06</div>
                        </div>
                    </div>
                </div>
                <div className='Chat__item'>
                    <div className='-ava'>
                        <AvatarX url={null} txt='Dmitry Alekseev' type={33}/>
                    </div>
                    <div className='-msg'>
                        <div className='-wrap'>
                            <div className='-ten'/>
                            <div className='-ugol'/>
                            <div className='-txt'>
                                <div className='-nick'>
                                    <div>Dmitry Alekseev</div>
                                    <span>Ответить</span>
                                </div>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab blanditiis consequatur expedita fugiat nemo optio perferendis quas sapiente, sequi tempora. Harum, iste praesentium. Ad asperiores cum deleniti dolor dolore et inventore magnam magni quis. A adipisci animi, consequatur consequuntur deleniti dolor doloremque doloribus ea eaque explicabo incidunt inventore nulla quasi qui quibusdam saepe sed, similique?
                            </div>
                            <div className='-date -newline'>20:06</div>
                        </div>
                    </div>
                </div>
                <div className='Chat__item'>
                    <div className='-ava'>
                        <AvatarX url={null} txt='Dmitr2y Alekseev' type={33}/>
                    </div>
                    <div className='-msg'>
                        <div className='-wrap'>
                            <div className='-ten'/>
                            <div className='-ugol'/>
                            <div className='-txt'>
                                <div className='-nick'>Dmitry Alekseev</div>
                                <div className='-t'>Мама мия!</div>
                            </div>
                            <div className='-date'>20:06</div>
                        </div>
                    </div>
                </div>
                <div className='Chat__item'>
                    <div className='-ava'>
                        <AvatarX url={null} txt='Dmitry Alekseev' type={33}/>
                    </div>
                    <div className='-msg'>
                        <div className='-wrap'>
                            <div className='-ten'/>
                            <div className='-ugol'/>
                            <div className='-txt'>
                                <div className='-nick'>Dmitry Alekseev</div>
                                <div className='-t'>Привет как твои дела!?</div>
                            </div>
                            <div className='-date'>20:06</div>
                        </div>
                    </div>
                </div>
                <div className='Chat__item'>
                    <div className='-ava'>
                        <AvatarX url={null} txt='Dmitry Alekseev' type={33}/>
                    </div>
                    <div className='-msg'>
                        <div className='-wrap'>
                            <div className='-ten'/>
                            <div className='-ugol'/>
                            <div className='-txt'>
                                <div className='-nick'>
                                    <div>Dmitry Alekseev</div>
                                    <span>Ответить</span>
                                </div>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab blanditiis consequatur expedita fugiat nemo optio perferendis quas sapiente, sequi tempora. Harum, iste praesentium. Ad asperiores cum deleniti dolor dolore et inventore magnam magni quis. A adipisci animi, consequatur consequuntur deleniti dolor doloremque doloribus ea eaque explicabo incidunt inventore nulla quasi qui quibusdam saepe sed, similique?
                            </div>
                            <div className='-date -newline'>20:06</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Test6;