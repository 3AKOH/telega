import React from 'react';

export const VAR_NONE_SIGNAL = (<><b>Отсутствует соединение с Telegram сервером</b><div>Воспользуйтесь браузером Opera<br/>с включенной опцией Proxy</div></>);
export const VAR_CREATE_GROUP = 'Создать группу';
export const VAR_CREATE_CHANNEL = 'Создать канал';
export const VAR_SETTINGS = 'Настройки';
export const VAR_DARK_THEME = 'Ночной режим';
export const VAR_PHOTOS = 'Фотографии';